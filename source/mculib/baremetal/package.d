module mculib.baremetal;


version(FreeStanding){}else static assert(0,"not supported not freestanding");
version(LDC){}else static assert(0,"not supported not freestanding");



/// 检查特定MCU型号模板
template CheckTargetCPU(T...) 
if (imported!"std.meta".allSatisfy!(imported!"std.traits".isSomeString,typeof(T)))
{
    static if(T.length > 1)
        enum CheckTargetCPU = __traits(targetCPU) == T[0] || CheckTargetCPU!(T[1..$]);
    else
        enum CheckTargetCPU = __traits(targetCPU) == T[0];
}


