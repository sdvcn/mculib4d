module mculib.baremetal.volatile.misc;



package:

int StringToInt(char c)
{
    import std.ascii:digits;
    foreach (i,key; digits)
    {
        if(c==key) return i;
    }
    return -1;
}

int StringToInt(string str)
{
    import std.ascii:isDigit;
    int ret;
    foreach(c;str)
    {
        if(!isDigit(c)) return -1;
        ret = ret * 10 + StringToInt(c);
    }
    return ret;
}