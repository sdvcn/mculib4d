module mculib.baremetal.volatile;


import mculib.arm.bitband:SupportBitBand;


public
{
    static if(SupportBitBand)
        import mculib.baremetal.volatile.bitband;
    else
        import mculib.baremetal.volatile.atomic;

    import mculib.baremetal.volatile.split;
}






