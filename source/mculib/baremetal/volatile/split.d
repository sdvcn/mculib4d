module mculib.baremetal.volatile.split;

import std.traits:isScalarType,isUnsigned;
import core.atomic;

/**
    拆分操作,拆分成更小单元方便操作
*/

struct Split(T)
if(isScalarType!T && isUnsigned!T)
{
private:
    /// 子类型
    alias SubType = getSubType!T;

    /// 存在子类型
    enum hasSubType = (!is(SubType==T));

    version(LittleEndian)
    {
        /// 定义一半类型的高部分游标
        enum halfHighCursor = 1;
        /// 定义一半类型的低部分游标
        enum halfLowCursor = 0;
    }
    else
    {
        /// 定义一半类型的高部分游标
        enum halfHighCursor = 0;
        /// 定义一半类型的低部分游标
        enum halfLowCursor = 1;
    }
    
    

    union
    {
        shared T val;
        static if(hasSubType)
        {
            version(LittleEndian){
                struct
                {
                    Split!SubType low;
                    Split!SubType high;
                }
            }else{
                struct
                {
                    Split!SubType high;
                    Split!SubType low;
                }
            }
        }        
    }
public:
    /// 读取高一半
    static if(hasSubType)
        ref Split!SubType halfHigh() @property => this.high;
    /// 读取低一半
    static if(hasSubType)
        ref Split!SubType halfLow() @property => this.low;

    /// 当前类型写入
    void value(T v) => atomicStore(this.val, v);
    /// 当前类型读取
    T value() const => atomicLoad(this.val);

    T pCast() const => this.val;
}




private
template getSubType(T)
{
    static if(is(T==ulong))
        alias getSubType = uint;
    else static if(is(T==uint))
        alias getSubType = ushort;
    else static if(is(T==ushort))
        alias getSubType = ubyte;
    else 
        alias getSubType = T;
}

/// 模板,判定一个字符串是否可以转位数字


