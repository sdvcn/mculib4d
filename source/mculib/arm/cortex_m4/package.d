module mculib.arm.cortex_m4;

static if(__traits(targetCPU) !is "cortex-m4") 
{
    static assert(0,"not supported:"~__traits(targetCPU));
}

import mculib.arm.cortex_m;
import mculib.arm.cortex_m4.vector;




enum VectorFunc[] Cortex_Mx = Cortex_M ~ Cortex_M4;
@isr_vector align(size_t.sizeof)  extern(C) immutable VectorFunc[Cortex_Mx.length] VectorTable = cast(immutable VectorFunc[])Cortex_Mx;
