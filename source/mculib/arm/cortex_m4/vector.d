module mculib.arm.cortex_m4.vector;

import mculib.chip;

import mculib.baremetal.attributes;
import std.traits:EnumMembers,staticMap;



public import mculib.baremetal.attributes:isr_vector;

alias VectorFunc = extern(C) void *;

/// 外部中断向量
alias VectorExt = Peripherals.IRQnEmum;

/**
* 构建空置的向量连接函数
*/



private
template SetHandler(string name )
{
    pragma(mangle, name~"_Handler")
    @weak pragma(LDC_extern_weak) extern extern(C) __gshared
    mixin("void "~ name ~"_Handler();");

    mixin("alias SetHandler =  "~name~"_Handler;");
    
    //alias SetHandler = void function();

}

template MyTemplate( E )
{
    enum allEnum = EnumMembers!E;
    
}



/// todo: 应实现由总线内声明自动构建空置的的中断向量表
/// 中断向量表
extern(C)
enum VectorFunc[] Cortex_M4 =[

    VectorExt.WWDG:&SetHandler!("WWDG"),
    VectorExt.PVD:&SetHandler!("PVD"),
    VectorExt.TAMP_STAMP:&SetHandler!("TAMP_STAMP"),
    VectorExt.RTC_WKUP:&SetHandler!("RTC_WKUP"),
    VectorExt.FLASH:&SetHandler!("FLASH"),
    VectorExt.RCC:&SetHandler!("RCC"),
    VectorExt.EXTI0:&SetHandler!("EXTI0"),
    VectorExt.EXTI1:&SetHandler!("EXTI1"),
    VectorExt.EXTI2:&SetHandler!("EXTI2"),
    VectorExt.EXTI3:&SetHandler!("EXTI3"),
    VectorExt.EXTI4:&SetHandler!("EXTI4"),
    VectorExt.DMA1_Stream0:&SetHandler!("DMA1_Stream0"),
    VectorExt.DMA1_Stream1:&SetHandler!("DMA1_Stream1"),
    VectorExt.DMA1_Stream2:&SetHandler!("DMA1_Stream2"),
    VectorExt.DMA1_Stream3:&SetHandler!("DMA1_Stream3"),
    VectorExt.DMA1_Stream4:&SetHandler!("DMA1_Stream4"),
    VectorExt.DMA1_Stream5:&SetHandler!("DMA1_Stream5"),
    VectorExt.DMA1_Stream6:&SetHandler!("DMA1_Stream6"),
    VectorExt.ADC:&SetHandler!("ADC"),
    VectorExt.EXTI9_5:&SetHandler!("EXTI9_5"),
    VectorExt.TIM1_BRK_TIM9:&SetHandler!("TIM1_BRK_TIM9"),
    VectorExt.TIM1_UP_TIM10:&SetHandler!("TIM1_UP_TIM10"),
    VectorExt.TIM1_TRG_COM_TIM11:&SetHandler!("TIM1_TRG_COM_TIM11"),
    VectorExt.TIM1_CC:&SetHandler!("TIM1_CC"),
    VectorExt.TIM2:&SetHandler!("TIM2"),
    VectorExt.TIM3:&SetHandler!("TIM3"),
    VectorExt.TIM4:&SetHandler!("TIM4"),
    VectorExt.I2C1_EV:&SetHandler!("I2C1_EV"),
    VectorExt.I2C1_ER:&SetHandler!("I2C1_ER"),
    VectorExt.I2C2_EV:&SetHandler!("I2C2_EV"),
    VectorExt.I2C2_ER:&SetHandler!("I2C2_ER"),
    VectorExt.SPI1:&SetHandler!("SPI1"),
    VectorExt.SPI2:&SetHandler!("SPI2"),
    VectorExt.USART1:&SetHandler!("USART1"),
    VectorExt.USART2:&SetHandler!("USART2"),
    VectorExt.EXTI15_10:&SetHandler!("EXTI15_10"),
    VectorExt.RTC_Alarm:&SetHandler!("RTC_Alarm"),
    VectorExt.OTG_FS_WKUP:&SetHandler!("OTG_FS_WKUP"),
    VectorExt.DMA1_Stream7:&SetHandler!("DMA1_Stream7"),
    VectorExt.SDIO:&SetHandler!("SDIO"),
    VectorExt.TIM5:&SetHandler!("TIM5"),
    VectorExt.SPI3:&SetHandler!("SPI3"),
    VectorExt.DMA2_Stream0:&SetHandler!("DMA2_Stream0"),
    VectorExt.DMA2_Stream1:&SetHandler!("DMA2_Stream1"),
    VectorExt.DMA2_Stream2:&SetHandler!("DMA2_Stream2"),
    VectorExt.DMA2_Stream3:&SetHandler!("DMA2_Stream3"),
    VectorExt.DMA2_Stream4:&SetHandler!("DMA2_Stream4"),
    VectorExt.OTG_FS:&SetHandler!("OTG_FS"),
    VectorExt.DMA2_Stream5:&SetHandler!("DMA2_Stream5"),
    VectorExt.DMA2_Stream6:&SetHandler!("DMA2_Stream6"),
    VectorExt.DMA2_Stream7:&SetHandler!("DMA2_Stream7"),
    VectorExt.USART6:&SetHandler!("USART6"),
    VectorExt.I2C3_EV:&SetHandler!("I2C3_EV"),
    VectorExt.I2C3_ER:&SetHandler!("I2C3_ER"),
    VectorExt.FPU:&SetHandler!("FPU"),
    //VectorExt.SPI4:&SetHandler!("SPI4")
];