module mculib.arm.iwdg;

/**
* 独立看门狗
*/

import mculib.chip;
version(None):
import arm.misc;

/// 判断 是否声明了 EXTI
private enum bool hasBusPeriph = HasPeriph!("IWDG");

static if(hasBusPeriph):