module mculib.arm.target_cortex_m;

version(ARM){}else{
    static assert(0,"not supported");
}

import mculib.baremetal;
import mculib.arm.misc;
import mculib.arm;

import core.attribute;










/// 中断函数定义


//---------------------------------------------------------------------

@naked extern(C) void defaultExceptionHandler()
{
    import mculib.arm.builtins;
    
	while(true)
	{
		__wfi();
	}
}
//---------------------------------------------------------------------



