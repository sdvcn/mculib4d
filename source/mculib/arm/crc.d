module mculib.arm.crc;



import mculib.chip;




static if(__traits(targetCPU) is "cortex-m4") 
    public import mculib.arm.cortex_m4.crc;
else
    public import mculib.arm.common.crc:crc32Of;

public
{
    import mculib.arm.common.crc:crc8Of;
}



//static if(HasPeriph!("CRC")) version = Hardware_CRC;

final abstract class DCrc
{
    //alias 
}

