module mculib.arm.bus;

/**
* MMIO总线抽象类
*/

import mculib.arm.misc:combineStr;
//import chip:Peripherals;
//import std.traits:getUDAs;
import core.volatile;
import core.bitop:bsf,bsr;
import core.atomic;
///import arm:BitBand;

import std.traits :isSomeString,isPointer;
import std.meta : allSatisfy;

package{
    /// 回调函数定义
    alias IRQHandler = void function();
    /**
    * 枚举,构建初始化顺序 SYSCFG -> PWR -> Flash -> NVIC -> SCB -> RCC -> STK -> Other
    */
    enum CrtFlag : ubyte
    {
        ROOT = 0,
        /// SYSCFG 优先级
        SYSCFG =10,
        /// PWR 优先级
        PWR = 20,
        /// Flash
        FLASH = 40,
        /// NVIC
        NVIC = 60,
        /// SCB
        SCB = 80,
        /// RCC
        RCC = 100,
        /// STK
        STK = 120,
        /// Other
        Other = 200,
    }
    /// 优先级模板
    template CrtPriority(CrtFlag f,ubyte n)
    {
        enum size_t CrtPriority = ( f << 8 | n);
    }
}

public
{
    /**
    * 外设流接口
    */
    interface PeriStream : MemoryStream
    {

    }
    interface MemoryStream
    {
        /**
        * 读取数据
        * Params:
        *  dst = 目标地址
        *  n = 读取的字节数
        * Returns:
        * 返回读取的字节数
        */
        size_t reads(void* dst,size_t n);
        /**
        * 写入数据
        * Params:
        *  src = 源地址
        *  n = 写入的字节数
        */
        void writes(immutable(char)* src, size_t n);
        
        //interface write(immutable(char)*, int);
    }

}

auto abs(T)(T v)
if (imported!"std.traits".isSigned!T)
{
    return cast(Unsigned!T) (v < 0 ? -v : v);
}


