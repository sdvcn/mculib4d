/**
* 处理器定义单元专用
*/


module mculib.chip.util;

import std.traits:EnumMembers;
import std.typecons;

import core.atomic;
//import core.volatile;
//import std.algorithm:map;
import std.meta:staticMap;



import mculib.baremetal.common:makeBitField,bitFieldWidth,combineMasks;

import mculib.baremetal.volatile.register;

package:

public{
    import mculib.baremetal.common:makeBitField,combineOr;
}


enum UsageToken
{
    registers,
    buffer,
    reserved
}

struct AddressBlock
{
    uint offset;
    uint size;
    UsageToken usage;
}


struct Interrupt
{
    string name;
    string desc;
    uint irq;
}

struct RegInfo
{
    string name;
    uint offset;
}




/**
    映射寄存器模板
    Params:
        paddr = 映射地址
        T = `struct`
*/
template MMIO( size_t paddr,T)
if(is(T==struct))
{
    enum T* MMIO = cast(T*)paddr;
}

/**
    寄存器位读写权限
*/
alias RegisterAccess = Access;

alias BitFieldAccess = Access;

/**
    寄存器位域模板
*/
alias RegBitField = BitField;

/**
* 寄存器操作模板
*/
alias RegVolatile = VolatileRegister;

struct RegBitFieldA(T)
{
    
}





