module mculib.chip;

import std.traits : isAutodecodableString;
import std.meta : allSatisfy;

//public import chip;

/// 导入芯片定义
public{
    //version(STM32F401CC) import chip.stm32f401cc;
    import mculib.chip.st.stm32f401cc;
    
}
/**
* 判定是否拥有对应的外设
* Params:
*   T = 外设名称,支持字符串及字符串数组
*/
template HasPeriph(T...) if (allSatisfy!(isAutodecodableString, typeof(T)) )
{
    static if(T.length > 1){
        enum HasPeriph = __traits(hasMember, Peripherals, T[0]) || HasPeriph!(T[1..$]);
    }else{
        enum HasPeriph = __traits(hasMember, Peripherals, T[0]);
    }
}