module unwind;

enum Unwind_Reason_Code
{
    URC_NO_REASON = 0,
    URC_OK = 0, /* used by ARM EHABI */
    URC_FOREIGN_EXCEPTION_CAUGHT = 1,
    URC_FATAL_PHASE2_ERROR = 2,
    URC_FATAL_PHASE1_ERROR = 3,
    URC_NORMAL_STOP = 4,
    URC_END_OF_STACK = 5,
    URC_HANDLER_FOUND = 6,
    URC_INSTALL_CONTEXT = 7,
    URC_CONTINUE_UNWIND = 8,
    URC_FAILURE = 9 /* used by ARM EHABI */
}

extern(C)
{
    Unwind_Reason_Code __aeabi_unwind_cpp_pr0(size_t state, void* ucbp, void* context)
    {
        return unwindOneFrame(state,ucbp,context);
    }
    Unwind_Reason_Code __aeabi_unwind_cpp_pr1(size_t state, void* ucbp, void* context)
    {
        return unwindOneFrame(state,ucbp,context);
    }
    Unwind_Reason_Code __aeabi_unwind_cpp_pr2(size_t state, void* ucbp, void* context)
    {
        return unwindOneFrame(state,ucbp,context);
    }

    

}

version(FreeStanding){
    extern(C)
    Unwind_Reason_Code unwindOneFrame(size_t state, void* ucbp, void* context)
    {
        return Unwind_Reason_Code.URC_OK;
    }
}