/** @file
 *  @brief XWOS移植实现层：线程本地存储
 *  @author
 *  + 隐星魂 (Roy Sun) <xwos@xwos.tech>
 *  @copyright
 *  + Copyright © 2015 xwos.tech, All Rights Reserved.
 *  > Licensed under the Apache License, Version 2.0 (the "License");
 *  > you may not use this file except in compliance with the License.
 *  > You may obtain a copy of the License at
 *  >
 *  >         http://www.apache.org/licenses/LICENSE-2.0
 *  >
 *  > Unless required by applicable law or agreed to in writing, software
 *  > distributed under the License is distributed on an "AS IS" BASIS,
 *  > WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  > See the License for the specific language governing permissions and
 *  > limitations under the License.
 */
// 这部分是一个多行注释，描述了文件的用途、作者、版权信息和许可证声明。

#include <xwos/standard.h>
#include <xwos/ospl/skd.h>
// 包含XWOS操作系统的标准头文件和特定的操作系统端口层头文件。

extern xwu8_t tdata_lma_base[]; // 声明线程数据的加载地址基准数组。
extern xwu8_t tdata_vma_base[]; // 声明线程数据的虚拟内存地址基准数组。
extern xwu8_t tdata_vma_end[]; // 声明线程数据虚拟内存地址的结束数组。

extern xwu8_t tbss_vma_base[]; // 声明线程BSS段的虚拟内存地址基准数组。
extern xwu8_t tbss_vma_end[]; // 声明线程BSS段虚拟内存地址的结束数组。
extern xwu8_t tbss_vma_offset[]; // 声明线程BSS段的虚拟内存地址偏移数组。

extern xwu8_t tls_offset[]; // 声明TLS偏移数组。

__xwbsp_code // 这是一个特定于XWOS的属性，可能表示代码应该被放置在特定的内存段。
void soc_tls_relocate(void * tls) // 函数用于重新定位TLS（线程本地存储）数据到指定的内存地址。
{
    xwsz_t cnt; // 定义一个计数器，用于在循环中使用。
    xwu8_t * src; // 定义源指针，指向线程数据的加载地址基准。
    xwu8_t * dst; // 定义目标指针，指向线程数据的虚拟内存地址。

    src = tdata_lma_base; // 初始化源指针。
    dst = tls; // 初始化目标指针为传入的TLS地址。

    if (dst != src) { // 检查源和目标是否相同，如果不同则执行复制。
        cnt = (xwsz_t)tdata_vma_end - (xwsz_t)tdata_vma_base; // 计算要复制的数据的大小。
        for (i = 0; i < cnt; i++) { // 循环复制数据。
            *dst = *src;
            dst++;
            src++;
        }
    }

    dst = tls + (xwptr_t)tbss_vma_offset; // 计算并设置目标指针到BSS段的虚拟内存地址。
    cnt = (xwsz_t)tbss_vma_end - (xwsz_t)tbss_vma_base; // 计算BSS段的大小。
    for (i = 0; i < cnt; i++) { // 循环将BSS段的内存初始化为0。
        *dst = 0;
        dst++;
    }
}

__xwbsp_code
void xwospl_tls_init(struct xwospl_skdobj_stack * stk) // 函数用于初始化TLS（线程本地存储）。
{
    xwsz_t tls_size; // 定义TLS的大小。

    tls_size = (xwsz_t)tbss_vma_end - (xwsz_t)tdata_vma_base; // 计算TLS的大小。

    stk->guard_base = (xwstk_t *)((xwptr_t)stk->base + // 设置stk->guard_base指向TLS的开始地址。
                                  XWBOP_ALIGN(tls_size, (xwsz_t)tls_offset));

    soc_tls_relocate(stk->tls); // 调用soc_tls_relocate函数来重新定位TLS数据。
    stk->tls = (void *)((xwptr_t)stk->tls - (xwptr_t)tls_offset); // 设置stk->tls指向TLS的开始地址。
}

/**
 * @brief XWOS移植层：返回TLS段的基地址
 * @return TLS段的基地址
 * @details
 * ARM EABI中定义的函数，用于返回TLS段的基地址。
 * 当使用C11（含）以后标准中的 `_Thread_local` 变量时，会链接到此函数。
 *
 * 编译器会假定此函数内只会使用 `r0` 寄存器，因此，若此函数内需要使用 `volatile` 寄存器，
 * 需要保护 `r1`, `r2`, `r3`, `lr`。
 */
__xwbsp_code __xwcc_naked // 这是一个特定于XWOS的属性，可能表示函数应该被编译器处理为"naked"，即不生成函数的默认入口和出口代码。
void * __aeabi_read_tp(void)
{
    __asm__ volatile("      push    {r1, r2, r3, lr}"); // 保存寄存器r1, r2, r3和lr。
    __asm__ volatile("      bl      xwosplcb_skd_get_cthd_lc"); // 调用xwosplcb_skd_get_cthd_lc函数获取当前线程的上下文。
    __asm__ volatile("      ldr     r0, [r0, %[__tls]]" // 将TLS的基地址加载到寄存器r0。
                         :
                         :[__tls] "I" (xwcc_offsetof(struct xwospl_thd, stack.tls))
                         :);

    __asm__ volatile("      pop     {r1, r2, r3, pc}"); // 恢复寄存器r1, r2, r3和lr。
}