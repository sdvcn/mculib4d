module source.cmsis;
 import std.compiler;

// todo: 未完成所有cmsis的标准节点结构定义

struct accessType
{
    string access;
}

struct protectionStringType
{
    
}

struct peripheralType
{

}

struct peripheralsType
{
    peripheralType[] peripheral;
}

struct DeviceType
{
    string vendor;
    string vendorID;
    string name;
    string series;
    string version_;
    string description;
    string licenseText;
    string cpu;
    string headerSystemFilename;
    string headerDefinitionsPrefix;
    uint addressUnitBits;
    uint width;

    /// 寄存器默认值
    uint size;
    accessType access;
    protectionStringType protection;
    uint resetValue;
    uint resetMask;
    peripheralsType peripherals;
    //anyType vendorExtensions 
}

