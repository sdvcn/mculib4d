import std.stdio;

import heard;
import std.getopt;

import std.typecons;

void main()
{
	import heard2;
	
	makeHeader("t.svd");
}

void main2(string[] args)
{
	string outFile;
	string jsonFile;

	auto helpInformation = getopt(
		args,
		"json","input JSON file",  &jsonFile,
		"out", "output D file",   &outFile,
		);    // enum

	if(outFile is null || jsonFile is null)
	{
		
		helpInformation.helpWanted = true;
	}
	

	if (helpInformation.helpWanted)
	{
		defaultGetoptPrinter("Please specify input json file and output D file.",
			helpInformation.options);
		return;
	}


	makeHeader(jsonFile,outFile);

}
