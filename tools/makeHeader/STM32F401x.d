module STM32F401x;
/// STM32F401x
import mculib.chip.util;
final abstract class Peripherals {
	pragma(LDC_no_typeinfo):
	enum IRQnEmum {
		WWDG = 0,		///< Window Watchdog interrupt
		PVD = 1,		///< PVD through EXTI line detection interrupt
		TAMP_STAMP = 2,		///< Tamper and TimeStamp interrupts through the EXTI line
		RTC_WKUP = 3,		///< RTC Wakeup interrupt through the EXTI line
		FLASH = 4,		///< FLASH global interrupt
		RCC = 5,		///< RCC global interrupt
		EXTI0 = 6,		///< EXTI Line0 interrupt
		EXTI1 = 7,		///< EXTI Line1 interrupt
		EXTI2 = 8,		///< EXTI Line2 interrupt
		EXTI3 = 9,		///< EXTI Line3 interrupt
		EXTI4 = 10,		///< EXTI Line4 interrupt
		DMA1_Stream0 = 11,		///< DMA1 Stream0 global interrupt
		DMA1_Stream1 = 12,		///< DMA1 Stream1 global interrupt
		DMA1_Stream2 = 13,		///< DMA1 Stream2 global interrupt
		DMA1_Stream3 = 14,		///< DMA1 Stream3 global interrupt
		DMA1_Stream4 = 15,		///< DMA1 Stream4 global interrupt
		DMA1_Stream5 = 16,		///< DMA1 Stream5 global interrupt
		DMA1_Stream6 = 17,		///< DMA1 Stream6 global interrupt
		ADC = 18,		///< ADC1 global interrupt
		EXTI9_5 = 23,		///< EXTI Line[9:5] interrupts
		TIM1_BRK_TIM9 = 24,		///< TIM1 Break interrupt and TIM9 global interrupt
		TIM1_UP_TIM10 = 25,		///< TIM1 Update interrupt and TIM10 global interrupt
		TIM1_TRG_COM_TIM11 = 26,		///< TIM1 Trigger and Commutation interrupts and TIM11 global interrupt
		TIM1_CC = 27,		///< TIM1 Capture Compare interrupt
		TIM2 = 28,		///< TIM2 global interrupt
		TIM3 = 29,		///< TIM3 global interrupt
		TIM4 = 30,		///< TIM4 global interrupt
		I2C1_EV = 31,		///< I2C1 event interrupt
		I2C1_ER = 32,		///< I2C1 error interrupt
		I2C2_EV = 33,		///< I2C2 event interrupt
		I2C2_ER = 34,		///< I2C2 error interrupt
		SPI1 = 35,		///< SPI1 global interrupt
		SPI2 = 36,		///< SPI2 global interrupt
		USART1 = 37,		///< USART1 global interrupt
		USART2 = 38,		///< USART2 global interrupt
		EXTI15_10 = 40,		///< EXTI Line[15:10] interrupts
		RTC_Alarm = 41,		///< RTC Alarms (A and B) through EXTI line interrupt
		OTG_FS_WKUP = 42,		///< USB On-The-Go FS Wakeup through EXTI line interrupt
		DMA1_Stream7 = 47,		///< DMA1 Stream7 global interrupt
		SDIO = 49,		///< SDIO global interrupt
		TIM5 = 50,		///< TIM5 global interrupt
		SPI3 = 51,		///< SPI3 global interrupt
		DMA2_Stream0 = 56,		///< DMA2 Stream0 global interrupt
		DMA2_Stream1 = 57,		///< DMA2 Stream1 global interrupt
		DMA2_Stream2 = 58,		///< DMA2 Stream2 global interrupt
		DMA2_Stream3 = 59,		///< DMA2 Stream3 global interrupt
		DMA2_Stream4 = 60,		///< DMA2 Stream4 global interrupt
		OTG_FS = 67,		///< USB On The Go FS global interrupt
		DMA2_Stream5 = 68,		///< DMA2 Stream5 global interrupt
		DMA2_Stream6 = 69,		///< DMA2 Stream6 global interrupt
		DMA2_Stream7 = 70,		///< DMA2 Stream7 global interrupt
		USART6 = 71,		///< USART6 global interrupt
		I2C3_EV = 72,		///< I2C3 event interrupt
		I2C3_ER = 73,		///< I2C3 error interrupt
		FPU = 81,		///< FPU interrupt
	}; // Enum size:55
	@AddressBlock(0, 1024, UsageToken.registers) 
	@Interrupt("FPU", "FPU interrupt", 0x51) 
	enum DBG_Type* DBG = MMIO!(0xe0042000, DBG_Type);	//Debug support
	@AddressBlock(0, 1024, UsageToken.registers) 
	@Interrupt("DMA2_Stream0", "DMA2 Stream0 global interrupt", 0x38) @Interrupt("DMA2_Stream1", "DMA2 Stream1 global interrupt", 0x39) @Interrupt("DMA2_Stream2", "DMA2 Stream2 global interrupt", 0x3a) @Interrupt("DMA2_Stream3", "DMA2 Stream3 global interrupt", 0x3b) @Interrupt("DMA2_Stream4", "DMA2 Stream4 global interrupt", 0x3c) @Interrupt("DMA2_Stream5", "DMA2 Stream5 global interrupt", 0x44) @Interrupt("DMA2_Stream6", "DMA2 Stream6 global interrupt", 0x45) @Interrupt("DMA2_Stream7", "DMA2 Stream7 global interrupt", 0x46) 
	enum DMA2_Type* DMA2 = MMIO!(0x40026400, DMA2_Type);	//DMA controller
	@Interrupt("DMA1_Stream0", "DMA1 Stream0 global interrupt", 0xb) @Interrupt("DMA1_Stream1", "DMA1 Stream1 global interrupt", 0xc) @Interrupt("DMA1_Stream2", "DMA1 Stream2 global interrupt", 0xd) @Interrupt("DMA1_Stream3", "DMA1 Stream3 global interrupt", 0xe) @Interrupt("DMA1_Stream4", "DMA1 Stream4 global interrupt", 0xf) @Interrupt("DMA1_Stream5", "DMA1 Stream5 global interrupt", 0x10) @Interrupt("DMA1_Stream6", "DMA1 Stream6 global interrupt", 0x11) @Interrupt("DMA1_Stream7", "DMA1 Stream7 global interrupt", 0x2f) 
	enum DMA2_Type* DMA1 = MMIO!(0x40026000, DMA2_Type);	//
	@AddressBlock(0, 1024, UsageToken.registers) 
	@Interrupt("RCC", "RCC global interrupt", 0x5) 
	enum RCC_Type* RCC = MMIO!(0x40023800, RCC_Type);	//Reset and clock control
	@AddressBlock(0, 1024, UsageToken.registers) 
	enum GPIOH_Type* GPIOH = MMIO!(0x40021c00, GPIOH_Type);	//General-purpose I/Os
	enum GPIOH_Type* GPIOE = MMIO!(0x40021000, GPIOH_Type);	//
	enum GPIOH_Type* GPIOD = MMIO!(0x40020c00, GPIOH_Type);	//
	enum GPIOH_Type* GPIOC = MMIO!(0x40020800, GPIOH_Type);	//
	@AddressBlock(0, 1024, UsageToken.registers) 
	enum GPIOB_Type* GPIOB = MMIO!(0x40020400, GPIOB_Type);	//General-purpose I/Os
	@AddressBlock(0, 1024, UsageToken.registers) 
	enum GPIOA_Type* GPIOA = MMIO!(0x40020000, GPIOA_Type);	//General-purpose I/Os
	@AddressBlock(0, 1024, UsageToken.registers) 
	enum SYSCFG_Type* SYSCFG = MMIO!(0x40013800, SYSCFG_Type);	//System configuration controller
	@AddressBlock(0, 1024, UsageToken.registers) 
	@Interrupt("SPI1", "SPI1 global interrupt", 0x23) 
	enum SPI1_Type* SPI1 = MMIO!(0x40013000, SPI1_Type);	//Serial peripheral interface
	@Interrupt("SPI2", "SPI2 global interrupt", 0x24) 
	enum SPI1_Type* SPI2 = MMIO!(0x40003800, SPI1_Type);	//
	@Interrupt("SPI3", "SPI3 global interrupt", 0x33) 
	enum SPI1_Type* SPI3 = MMIO!(0x40003c00, SPI1_Type);	//
	enum SPI1_Type* I2S2ext = MMIO!(0x40003400, SPI1_Type);	//
	enum SPI1_Type* I2S3ext = MMIO!(0x40004000, SPI1_Type);	//
	@AddressBlock(0, 1024, UsageToken.registers) 
	@Interrupt("SDIO", "SDIO global interrupt", 0x31) 
	enum SDIO_Type* SDIO = MMIO!(0x40012c00, SDIO_Type);	//Secure digital input/output interface
	@AddressBlock(0, 1024, UsageToken.registers) 
	@Interrupt("ADC", "ADC1 global interrupt", 0x12) 
	enum ADC1_Type* ADC1 = MMIO!(0x40012000, ADC1_Type);	//Analog-to-digital converter
	@AddressBlock(0, 1024, UsageToken.registers) 
	@Interrupt("USART6", "USART6 global interrupt", 0x47) 
	enum USART6_Type* USART6 = MMIO!(0x40011400, USART6_Type);	//Universal synchronous asynchronous receiver transmitter
	@Interrupt("USART1", "USART1 global interrupt", 0x25) 
	enum USART6_Type* USART1 = MMIO!(0x40011000, USART6_Type);	//
	@Interrupt("USART2", "USART2 global interrupt", 0x26) 
	enum USART6_Type* USART2 = MMIO!(0x40004400, USART6_Type);	//
	@AddressBlock(0, 1024, UsageToken.registers) 
	@Interrupt("PVD", "PVD through EXTI line detection interrupt", 0x1) 
	enum PWR_Type* PWR = MMIO!(0x40007000, PWR_Type);	//Power control
	@AddressBlock(0, 1024, UsageToken.registers) 
	@Interrupt("I2C3_EV", "I2C3 event interrupt", 0x48) @Interrupt("I2C3_ER", "I2C3 error interrupt", 0x49) 
	enum I2C3_Type* I2C3 = MMIO!(0x40005c00, I2C3_Type);	//Inter-integrated circuit
	@Interrupt("I2C2_EV", "I2C2 event interrupt", 0x21) @Interrupt("I2C2_ER", "I2C2 error interrupt", 0x22) 
	enum I2C3_Type* I2C2 = MMIO!(0x40005800, I2C3_Type);	//
	@Interrupt("I2C1_EV", "I2C1 event interrupt", 0x1f) @Interrupt("I2C1_ER", "I2C1 error interrupt", 0x20) 
	enum I2C3_Type* I2C1 = MMIO!(0x40005400, I2C3_Type);	//
	@AddressBlock(0, 1024, UsageToken.registers) 
	enum IWDG_Type* IWDG = MMIO!(0x40003000, IWDG_Type);	//Independent watchdog
	@AddressBlock(0, 1024, UsageToken.registers) 
	@Interrupt("WWDG", "Window Watchdog interrupt", 0x0) 
	enum WWDG_Type* WWDG = MMIO!(0x40002c00, WWDG_Type);	//Window watchdog
	@AddressBlock(0, 1024, UsageToken.registers) 
	@Interrupt("RTC_WKUP", "RTC Wakeup interrupt through the EXTI line", 0x3) @Interrupt("RTC_Alarm", "RTC Alarms (A and B) through EXTI line interrupt", 0x29) 
	enum RTC_Type* RTC = MMIO!(0x40002800, RTC_Type);	//Real-time clock
	@AddressBlock(0, 1024, UsageToken.registers) 
	@Interrupt("TIM1_BRK_TIM9", "TIM1 Break interrupt and TIM9 global interrupt", 0x18) @Interrupt("TIM1_UP_TIM10", "TIM1 Update interrupt and TIM10 global interrupt", 0x19) @Interrupt("TIM1_TRG_COM_TIM11", "TIM1 Trigger and Commutation interrupts and TIM11 global interrupt", 0x1a) @Interrupt("TIM1_CC", "TIM1 Capture Compare interrupt", 0x1b) 
	enum TIM1_Type* TIM1 = MMIO!(0x40010000, TIM1_Type);	//Advanced-timers
	@AddressBlock(0, 1024, UsageToken.registers) 
	@Interrupt("TIM2", "TIM2 global interrupt", 0x1c) 
	enum TIM2_Type* TIM2 = MMIO!(0x40000000, TIM2_Type);	//General purpose timers
	@AddressBlock(0, 1024, UsageToken.registers) 
	@Interrupt("TIM3", "TIM3 global interrupt", 0x1d) 
	enum TIM3_Type* TIM3 = MMIO!(0x40000400, TIM3_Type);	//General purpose timers
	@Interrupt("TIM4", "TIM4 global interrupt", 0x1e) 
	enum TIM3_Type* TIM4 = MMIO!(0x40000800, TIM3_Type);	//
	@AddressBlock(0, 1024, UsageToken.registers) 
	@Interrupt("TIM5", "TIM5 global interrupt", 0x32) 
	enum TIM5_Type* TIM5 = MMIO!(0x40000c00, TIM5_Type);	//General-purpose-timers
	@AddressBlock(0, 1024, UsageToken.registers) 
	@Interrupt("TIM1_BRK_TIM9", "TIM1 Break interrupt and TIM9 global interrupt", 0x18) 
	enum TIM9_Type* TIM9 = MMIO!(0x40014000, TIM9_Type);	//General purpose timers
	@AddressBlock(0, 1024, UsageToken.registers) 
	@Interrupt("TIM1_UP_TIM10", "TIM1 Update interrupt and TIM10 global interrupt", 0x19) 
	enum TIM10_Type* TIM10 = MMIO!(0x40014400, TIM10_Type);	//General-purpose-timers
	@AddressBlock(0, 1024, UsageToken.registers) 
	@Interrupt("TIM1_TRG_COM_TIM11", "TIM1 Trigger and Commutation interrupts and TIM11 global interrupt", 0x1a) 
	enum TIM11_Type* TIM11 = MMIO!(0x40014800, TIM11_Type);	//General-purpose-timers
	@AddressBlock(0, 1024, UsageToken.registers) 
	enum CRC_Type* CRC = MMIO!(0x40023000, CRC_Type);	//Cryptographic processor
	@AddressBlock(0, 1024, UsageToken.registers) 
	@Interrupt("OTG_FS_WKUP", "USB On-The-Go FS Wakeup through EXTI line interrupt", 0x2a) @Interrupt("OTG_FS", "USB On The Go FS global interrupt", 0x43) 
	enum OTG_FS_GLOBAL_Type* OTG_FS_GLOBAL = MMIO!(0x50000000, OTG_FS_GLOBAL_Type);	//USB on the go full speed
	@AddressBlock(0, 1024, UsageToken.registers) 
	enum OTG_FS_HOST_Type* OTG_FS_HOST = MMIO!(0x50000400, OTG_FS_HOST_Type);	//USB on the go full speed
	@AddressBlock(0, 1024, UsageToken.registers) 
	enum OTG_FS_DEVICE_Type* OTG_FS_DEVICE = MMIO!(0x50000800, OTG_FS_DEVICE_Type);	//USB on the go full speed
	@AddressBlock(0, 1024, UsageToken.registers) 
	enum OTG_FS_PWRCLK_Type* OTG_FS_PWRCLK = MMIO!(0x50000e00, OTG_FS_PWRCLK_Type);	//USB on the go full speed
	@AddressBlock(0, 1024, UsageToken.registers) 
	@Interrupt("FLASH", "FLASH global interrupt", 0x4) 
	enum FLASH_Type* FLASH = MMIO!(0x40023c00, FLASH_Type);	//FLASH
	@AddressBlock(0, 1024, UsageToken.registers) 
	@Interrupt("TAMP_STAMP", "Tamper and TimeStamp interrupts through the EXTI line", 0x2) @Interrupt("EXTI0", "EXTI Line0 interrupt", 0x6) @Interrupt("EXTI1", "EXTI Line1 interrupt", 0x7) @Interrupt("EXTI2", "EXTI Line2 interrupt", 0x8) @Interrupt("EXTI3", "EXTI Line3 interrupt", 0x9) @Interrupt("EXTI4", "EXTI Line4 interrupt", 0xa) @Interrupt("EXTI9_5", "EXTI Line[9:5] interrupts", 0x17) @Interrupt("EXTI15_10", "EXTI Line[15:10] interrupts", 0x28) 
	enum EXTI_Type* EXTI = MMIO!(0x40013c00, EXTI_Type);	//External interrupt/event controller
	@AddressBlock(0, 4097, UsageToken.registers) 
	enum NVIC_Type* NVIC = MMIO!(0xe000e000, NVIC_Type);	//Nested Vectored Interrupt Controller
	@AddressBlock(0, 1024, UsageToken.registers) 
	enum ADC_Common_Type* ADC_Common = MMIO!(0x40012300, ADC_Common_Type);	//ADC common registers
	@AddressBlock(0, 13, UsageToken.registers) 
	@Interrupt("FPU", "Floating point unit interrupt", 0x51) 
	enum FPU_Type* FPU = MMIO!(0xe000ef34, FPU_Type);	//Floting point unit
	@AddressBlock(0, 21, UsageToken.registers) 
	enum MPU_Type* MPU = MMIO!(0xe000ed90, MPU_Type);	//Memory protection unit
	@AddressBlock(0, 17, UsageToken.registers) 
	enum STK_Type* STK = MMIO!(0xe000e010, STK_Type);	//SysTick timer
	@AddressBlock(0, 65, UsageToken.registers) 
	enum SCB_Type* SCB = MMIO!(0xe000ed00, SCB_Type);	//System control block
	// DBG Group 
	// Debug support
	struct DBG_Type {
		@RegInfo("DBGMCU_IDCODE",0x0) RegVolatile!(mask_DBGMCU_IDCODE,uint,0xe0042000,0x10006411,RegisterAccess.RO) DBGMCU_IDCODE;		 // offset:0x0 DBGMCU_IDCODE IDCODE
		@RegInfo("DBGMCU_CR",0x4) RegVolatile!(mask_DBGMCU_CR,uint,0xe0042004,0x0,RegisterAccess.RW) DBGMCU_CR;		 // offset:0x4 DBGMCU_CR Control Register
		@RegInfo("DBGMCU_APB1_FZ",0x8) RegVolatile!(mask_DBGMCU_APB1_FZ,uint,0xe0042008,0x0,RegisterAccess.RW) DBGMCU_APB1_FZ;		 // offset:0x8 DBGMCU_APB1_FZ Debug MCU APB1 Freeze registe
		@RegInfo("DBGMCU_APB2_FZ",0xc) RegVolatile!(mask_DBGMCU_APB2_FZ,uint,0xe004200c,0x0,RegisterAccess.RW) DBGMCU_APB2_FZ;		 // offset:0x12 DBGMCU_APB2_FZ Debug MCU APB2 Freeze registe
		enum mask_DBGMCU_IDCODE {
				DEV_ID = RegBitField!uint(0,12,RegisterAccess.RO),		// DEV_ID
				REV_ID = RegBitField!uint(16,16,RegisterAccess.RO),		// REV_ID
		}
		enum mask_DBGMCU_CR {
				DBG_SLEEP = RegBitField!uint(0,1,RegisterAccess.RW),		// DBG_SLEEP
				DBG_STOP = RegBitField!uint(1,1,RegisterAccess.RW),		// DBG_STOP
				DBG_STANDBY = RegBitField!uint(2,1,RegisterAccess.RW),		// DBG_STANDBY
				TRACE_IOEN = RegBitField!uint(5,1,RegisterAccess.RW),		// TRACE_IOEN
				TRACE_MODE = RegBitField!uint(6,2,RegisterAccess.RW),		// TRACE_MODE
		}
		enum mask_DBGMCU_APB1_FZ {
				DBG_TIM2_STOP = RegBitField!uint(0,1,RegisterAccess.RW),		// DBG_TIM2_STOP
				DBG_TIM3_STOP = RegBitField!uint(1,1,RegisterAccess.RW),		// DBG_TIM3 _STOP
				DBG_TIM4_STOP = RegBitField!uint(2,1,RegisterAccess.RW),		// DBG_TIM4_STOP
				DBG_TIM5_STOP = RegBitField!uint(3,1,RegisterAccess.RW),		// DBG_TIM5_STOP
				DBG_WWDG_STOP = RegBitField!uint(11,1,RegisterAccess.RW),		// DBG_WWDG_STOP
				DBG_IWDEG_STOP = RegBitField!uint(12,1,RegisterAccess.RW),		// DBG_IWDEG_STOP
				DBG_I2C1_SMBUS_TIMEOUT = RegBitField!uint(21,1,RegisterAccess.RW),		// DBG_J2C1_SMBUS_TIMEOUT
				DBG_I2C2_SMBUS_TIMEOUT = RegBitField!uint(22,1,RegisterAccess.RW),		// DBG_J2C2_SMBUS_TIMEOUT
				DBG_I2C3SMBUS_TIMEOUT = RegBitField!uint(23,1,RegisterAccess.RW),		// DBG_J2C3SMBUS_TIMEOUT
		}
		enum mask_DBGMCU_APB2_FZ {
				DBG_TIM1_STOP = RegBitField!uint(0,1,RegisterAccess.RW),		// TIM1 counter stopped when core is halted
				DBG_TIM9_STOP = RegBitField!uint(16,1,RegisterAccess.RW),		// TIM9 counter stopped when core is halted
				DBG_TIM10_STOP = RegBitField!uint(17,1,RegisterAccess.RW),		// TIM10 counter stopped when core is halted
				DBG_TIM11_STOP = RegBitField!uint(18,1,RegisterAccess.RW),		// TIM11 counter stopped when core is halted
		}
	}
	// GPIO Group 
	// General-purpose I/Os
	struct GPIOH_Type {
		@RegInfo("MODER",0x0) RegVolatile!(mask_MODER,uint,0x40021c00,0x0,RegisterAccess.RW) MODER;		 // offset:0x0 MODER GPIO port mode register
		@RegInfo("OTYPER",0x4) RegVolatile!(mask_OTYPER,uint,0x40021c04,0x0,RegisterAccess.RW) OTYPER;		 // offset:0x4 OTYPER GPIO port output type register
		@RegInfo("OSPEEDR",0x8) RegVolatile!(mask_OSPEEDR,uint,0x40021c08,0x0,RegisterAccess.RW) OSPEEDR;		 // offset:0x8 OSPEEDR GPIO port output speed register
		@RegInfo("PUPDR",0xc) RegVolatile!(mask_PUPDR,uint,0x40021c0c,0x0,RegisterAccess.RW) PUPDR;		 // offset:0x12 PUPDR GPIO port pull-up/pull-down register
		@RegInfo("IDR",0x10) RegVolatile!(mask_IDR,uint,0x40021c10,0x0,RegisterAccess.RO) IDR;		 // offset:0x16 IDR GPIO port input data register
		@RegInfo("ODR",0x14) RegVolatile!(mask_ODR,uint,0x40021c14,0x0,RegisterAccess.RW) ODR;		 // offset:0x20 ODR GPIO port output data register
		@RegInfo("BSRR",0x18) RegVolatile!(mask_BSRR,uint,0x40021c18,0x0,RegisterAccess.WO) BSRR;		 // offset:0x24 BSRR GPIO port bit set/reset register
		@RegInfo("LCKR",0x1c) RegVolatile!(mask_LCKR,uint,0x40021c1c,0x0,RegisterAccess.RW) LCKR;		 // offset:0x28 LCKR GPIO port configuration lock register
		@RegInfo("AFRL",0x20) RegVolatile!(mask_AFRL,uint,0x40021c20,0x0,RegisterAccess.RW) AFRL;		 // offset:0x32 AFRL GPIO alternate function low register
		@RegInfo("AFRH",0x24) RegVolatile!(mask_AFRH,uint,0x40021c24,0x0,RegisterAccess.RW) AFRH;		 // offset:0x36 AFRH GPIO alternate function high register
		enum mask_MODER {
				MODER15 = RegBitField!uint(30,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				MODER14 = RegBitField!uint(28,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				MODER13 = RegBitField!uint(26,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				MODER12 = RegBitField!uint(24,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				MODER11 = RegBitField!uint(22,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				MODER10 = RegBitField!uint(20,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				MODER9 = RegBitField!uint(18,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				MODER8 = RegBitField!uint(16,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				MODER7 = RegBitField!uint(14,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				MODER6 = RegBitField!uint(12,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				MODER5 = RegBitField!uint(10,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				MODER4 = RegBitField!uint(8,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				MODER3 = RegBitField!uint(6,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				MODER2 = RegBitField!uint(4,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				MODER1 = RegBitField!uint(2,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				MODER0 = RegBitField!uint(0,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
		}
		enum mask_OTYPER {
				OT15 = RegBitField!uint(15,1,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				OT14 = RegBitField!uint(14,1,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				OT13 = RegBitField!uint(13,1,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				OT12 = RegBitField!uint(12,1,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				OT11 = RegBitField!uint(11,1,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				OT10 = RegBitField!uint(10,1,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				OT9 = RegBitField!uint(9,1,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				OT8 = RegBitField!uint(8,1,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				OT7 = RegBitField!uint(7,1,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				OT6 = RegBitField!uint(6,1,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				OT5 = RegBitField!uint(5,1,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				OT4 = RegBitField!uint(4,1,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				OT3 = RegBitField!uint(3,1,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				OT2 = RegBitField!uint(2,1,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				OT1 = RegBitField!uint(1,1,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				OT0 = RegBitField!uint(0,1,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
		}
		enum mask_OSPEEDR {
				OSPEEDR15 = RegBitField!uint(30,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				OSPEEDR14 = RegBitField!uint(28,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				OSPEEDR13 = RegBitField!uint(26,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				OSPEEDR12 = RegBitField!uint(24,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				OSPEEDR11 = RegBitField!uint(22,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				OSPEEDR10 = RegBitField!uint(20,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				OSPEEDR9 = RegBitField!uint(18,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				OSPEEDR8 = RegBitField!uint(16,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				OSPEEDR7 = RegBitField!uint(14,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				OSPEEDR6 = RegBitField!uint(12,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				OSPEEDR5 = RegBitField!uint(10,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				OSPEEDR4 = RegBitField!uint(8,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				OSPEEDR3 = RegBitField!uint(6,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				OSPEEDR2 = RegBitField!uint(4,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				OSPEEDR1 = RegBitField!uint(2,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				OSPEEDR0 = RegBitField!uint(0,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
		}
		enum mask_PUPDR {
				PUPDR15 = RegBitField!uint(30,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				PUPDR14 = RegBitField!uint(28,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				PUPDR13 = RegBitField!uint(26,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				PUPDR12 = RegBitField!uint(24,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				PUPDR11 = RegBitField!uint(22,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				PUPDR10 = RegBitField!uint(20,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				PUPDR9 = RegBitField!uint(18,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				PUPDR8 = RegBitField!uint(16,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				PUPDR7 = RegBitField!uint(14,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				PUPDR6 = RegBitField!uint(12,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				PUPDR5 = RegBitField!uint(10,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				PUPDR4 = RegBitField!uint(8,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				PUPDR3 = RegBitField!uint(6,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				PUPDR2 = RegBitField!uint(4,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				PUPDR1 = RegBitField!uint(2,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				PUPDR0 = RegBitField!uint(0,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
		}
		enum mask_IDR {
				IDR15 = RegBitField!uint(15,1,RegisterAccess.RO),		// Port input data (y = 0..15)
				IDR14 = RegBitField!uint(14,1,RegisterAccess.RO),		// Port input data (y = 0..15)
				IDR13 = RegBitField!uint(13,1,RegisterAccess.RO),		// Port input data (y = 0..15)
				IDR12 = RegBitField!uint(12,1,RegisterAccess.RO),		// Port input data (y = 0..15)
				IDR11 = RegBitField!uint(11,1,RegisterAccess.RO),		// Port input data (y = 0..15)
				IDR10 = RegBitField!uint(10,1,RegisterAccess.RO),		// Port input data (y = 0..15)
				IDR9 = RegBitField!uint(9,1,RegisterAccess.RO),		// Port input data (y = 0..15)
				IDR8 = RegBitField!uint(8,1,RegisterAccess.RO),		// Port input data (y = 0..15)
				IDR7 = RegBitField!uint(7,1,RegisterAccess.RO),		// Port input data (y = 0..15)
				IDR6 = RegBitField!uint(6,1,RegisterAccess.RO),		// Port input data (y = 0..15)
				IDR5 = RegBitField!uint(5,1,RegisterAccess.RO),		// Port input data (y = 0..15)
				IDR4 = RegBitField!uint(4,1,RegisterAccess.RO),		// Port input data (y = 0..15)
				IDR3 = RegBitField!uint(3,1,RegisterAccess.RO),		// Port input data (y = 0..15)
				IDR2 = RegBitField!uint(2,1,RegisterAccess.RO),		// Port input data (y = 0..15)
				IDR1 = RegBitField!uint(1,1,RegisterAccess.RO),		// Port input data (y = 0..15)
				IDR0 = RegBitField!uint(0,1,RegisterAccess.RO),		// Port input data (y = 0..15)
		}
		enum mask_ODR {
				ODR15 = RegBitField!uint(15,1,RegisterAccess.RW),		// Port output data (y = 0..15)
				ODR14 = RegBitField!uint(14,1,RegisterAccess.RW),		// Port output data (y = 0..15)
				ODR13 = RegBitField!uint(13,1,RegisterAccess.RW),		// Port output data (y = 0..15)
				ODR12 = RegBitField!uint(12,1,RegisterAccess.RW),		// Port output data (y = 0..15)
				ODR11 = RegBitField!uint(11,1,RegisterAccess.RW),		// Port output data (y = 0..15)
				ODR10 = RegBitField!uint(10,1,RegisterAccess.RW),		// Port output data (y = 0..15)
				ODR9 = RegBitField!uint(9,1,RegisterAccess.RW),		// Port output data (y = 0..15)
				ODR8 = RegBitField!uint(8,1,RegisterAccess.RW),		// Port output data (y = 0..15)
				ODR7 = RegBitField!uint(7,1,RegisterAccess.RW),		// Port output data (y = 0..15)
				ODR6 = RegBitField!uint(6,1,RegisterAccess.RW),		// Port output data (y = 0..15)
				ODR5 = RegBitField!uint(5,1,RegisterAccess.RW),		// Port output data (y = 0..15)
				ODR4 = RegBitField!uint(4,1,RegisterAccess.RW),		// Port output data (y = 0..15)
				ODR3 = RegBitField!uint(3,1,RegisterAccess.RW),		// Port output data (y = 0..15)
				ODR2 = RegBitField!uint(2,1,RegisterAccess.RW),		// Port output data (y = 0..15)
				ODR1 = RegBitField!uint(1,1,RegisterAccess.RW),		// Port output data (y = 0..15)
				ODR0 = RegBitField!uint(0,1,RegisterAccess.RW),		// Port output data (y = 0..15)
		}
		enum mask_BSRR {
				BR15 = RegBitField!uint(31,1,RegisterAccess.WO),		// Port x reset bit y (y = 0..15)
				BR14 = RegBitField!uint(30,1,RegisterAccess.WO),		// Port x reset bit y (y = 0..15)
				BR13 = RegBitField!uint(29,1,RegisterAccess.WO),		// Port x reset bit y (y = 0..15)
				BR12 = RegBitField!uint(28,1,RegisterAccess.WO),		// Port x reset bit y (y = 0..15)
				BR11 = RegBitField!uint(27,1,RegisterAccess.WO),		// Port x reset bit y (y = 0..15)
				BR10 = RegBitField!uint(26,1,RegisterAccess.WO),		// Port x reset bit y (y = 0..15)
				BR9 = RegBitField!uint(25,1,RegisterAccess.WO),		// Port x reset bit y (y = 0..15)
				BR8 = RegBitField!uint(24,1,RegisterAccess.WO),		// Port x reset bit y (y = 0..15)
				BR7 = RegBitField!uint(23,1,RegisterAccess.WO),		// Port x reset bit y (y = 0..15)
				BR6 = RegBitField!uint(22,1,RegisterAccess.WO),		// Port x reset bit y (y = 0..15)
				BR5 = RegBitField!uint(21,1,RegisterAccess.WO),		// Port x reset bit y (y = 0..15)
				BR4 = RegBitField!uint(20,1,RegisterAccess.WO),		// Port x reset bit y (y = 0..15)
				BR3 = RegBitField!uint(19,1,RegisterAccess.WO),		// Port x reset bit y (y = 0..15)
				BR2 = RegBitField!uint(18,1,RegisterAccess.WO),		// Port x reset bit y (y = 0..15)
				BR1 = RegBitField!uint(17,1,RegisterAccess.WO),		// Port x reset bit y (y = 0..15)
				BR0 = RegBitField!uint(16,1,RegisterAccess.WO),		// Port x set bit y (y= 0..15)
				BS15 = RegBitField!uint(15,1,RegisterAccess.WO),		// Port x set bit y (y= 0..15)
				BS14 = RegBitField!uint(14,1,RegisterAccess.WO),		// Port x set bit y (y= 0..15)
				BS13 = RegBitField!uint(13,1,RegisterAccess.WO),		// Port x set bit y (y= 0..15)
				BS12 = RegBitField!uint(12,1,RegisterAccess.WO),		// Port x set bit y (y= 0..15)
				BS11 = RegBitField!uint(11,1,RegisterAccess.WO),		// Port x set bit y (y= 0..15)
				BS10 = RegBitField!uint(10,1,RegisterAccess.WO),		// Port x set bit y (y= 0..15)
				BS9 = RegBitField!uint(9,1,RegisterAccess.WO),		// Port x set bit y (y= 0..15)
				BS8 = RegBitField!uint(8,1,RegisterAccess.WO),		// Port x set bit y (y= 0..15)
				BS7 = RegBitField!uint(7,1,RegisterAccess.WO),		// Port x set bit y (y= 0..15)
				BS6 = RegBitField!uint(6,1,RegisterAccess.WO),		// Port x set bit y (y= 0..15)
				BS5 = RegBitField!uint(5,1,RegisterAccess.WO),		// Port x set bit y (y= 0..15)
				BS4 = RegBitField!uint(4,1,RegisterAccess.WO),		// Port x set bit y (y= 0..15)
				BS3 = RegBitField!uint(3,1,RegisterAccess.WO),		// Port x set bit y (y= 0..15)
				BS2 = RegBitField!uint(2,1,RegisterAccess.WO),		// Port x set bit y (y= 0..15)
				BS1 = RegBitField!uint(1,1,RegisterAccess.WO),		// Port x set bit y (y= 0..15)
				BS0 = RegBitField!uint(0,1,RegisterAccess.WO),		// Port x set bit y (y= 0..15)
		}
		enum mask_LCKR {
				LCKK = RegBitField!uint(16,1,RegisterAccess.RW),		// Port x lock bit y (y= 0..15)
				LCK15 = RegBitField!uint(15,1,RegisterAccess.RW),		// Port x lock bit y (y= 0..15)
				LCK14 = RegBitField!uint(14,1,RegisterAccess.RW),		// Port x lock bit y (y= 0..15)
				LCK13 = RegBitField!uint(13,1,RegisterAccess.RW),		// Port x lock bit y (y= 0..15)
				LCK12 = RegBitField!uint(12,1,RegisterAccess.RW),		// Port x lock bit y (y= 0..15)
				LCK11 = RegBitField!uint(11,1,RegisterAccess.RW),		// Port x lock bit y (y= 0..15)
				LCK10 = RegBitField!uint(10,1,RegisterAccess.RW),		// Port x lock bit y (y= 0..15)
				LCK9 = RegBitField!uint(9,1,RegisterAccess.RW),		// Port x lock bit y (y= 0..15)
				LCK8 = RegBitField!uint(8,1,RegisterAccess.RW),		// Port x lock bit y (y= 0..15)
				LCK7 = RegBitField!uint(7,1,RegisterAccess.RW),		// Port x lock bit y (y= 0..15)
				LCK6 = RegBitField!uint(6,1,RegisterAccess.RW),		// Port x lock bit y (y= 0..15)
				LCK5 = RegBitField!uint(5,1,RegisterAccess.RW),		// Port x lock bit y (y= 0..15)
				LCK4 = RegBitField!uint(4,1,RegisterAccess.RW),		// Port x lock bit y (y= 0..15)
				LCK3 = RegBitField!uint(3,1,RegisterAccess.RW),		// Port x lock bit y (y= 0..15)
				LCK2 = RegBitField!uint(2,1,RegisterAccess.RW),		// Port x lock bit y (y= 0..15)
				LCK1 = RegBitField!uint(1,1,RegisterAccess.RW),		// Port x lock bit y (y= 0..15)
				LCK0 = RegBitField!uint(0,1,RegisterAccess.RW),		// Port x lock bit y (y= 0..15)
		}
		enum mask_AFRL {
				AFRL7 = RegBitField!uint(28,4,RegisterAccess.RW),		// Alternate function selection for port x bit y (y = 0..7)
				AFRL6 = RegBitField!uint(24,4,RegisterAccess.RW),		// Alternate function selection for port x bit y (y = 0..7)
				AFRL5 = RegBitField!uint(20,4,RegisterAccess.RW),		// Alternate function selection for port x bit y (y = 0..7)
				AFRL4 = RegBitField!uint(16,4,RegisterAccess.RW),		// Alternate function selection for port x bit y (y = 0..7)
				AFRL3 = RegBitField!uint(12,4,RegisterAccess.RW),		// Alternate function selection for port x bit y (y = 0..7)
				AFRL2 = RegBitField!uint(8,4,RegisterAccess.RW),		// Alternate function selection for port x bit y (y = 0..7)
				AFRL1 = RegBitField!uint(4,4,RegisterAccess.RW),		// Alternate function selection for port x bit y (y = 0..7)
				AFRL0 = RegBitField!uint(0,4,RegisterAccess.RW),		// Alternate function selection for port x bit y (y = 0..7)
		}
		enum mask_AFRH {
				AFRH15 = RegBitField!uint(28,4,RegisterAccess.RW),		// Alternate function selection for port x bit y (y = 8..15)
				AFRH14 = RegBitField!uint(24,4,RegisterAccess.RW),		// Alternate function selection for port x bit y (y = 8..15)
				AFRH13 = RegBitField!uint(20,4,RegisterAccess.RW),		// Alternate function selection for port x bit y (y = 8..15)
				AFRH12 = RegBitField!uint(16,4,RegisterAccess.RW),		// Alternate function selection for port x bit y (y = 8..15)
				AFRH11 = RegBitField!uint(12,4,RegisterAccess.RW),		// Alternate function selection for port x bit y (y = 8..15)
				AFRH10 = RegBitField!uint(8,4,RegisterAccess.RW),		// Alternate function selection for port x bit y (y = 8..15)
				AFRH9 = RegBitField!uint(4,4,RegisterAccess.RW),		// Alternate function selection for port x bit y (y = 8..15)
				AFRH8 = RegBitField!uint(0,4,RegisterAccess.RW),		// Alternate function selection for port x bit y (y = 8..15)
		}
	}
	// General-purpose I/Os
	struct GPIOB_Type {
		@RegInfo("MODER",0x0) RegVolatile!(mask_MODER,uint,0x40020400,0x280,RegisterAccess.RW) MODER;		 // offset:0x0 MODER GPIO port mode register
		@RegInfo("OTYPER",0x4) RegVolatile!(mask_OTYPER,uint,0x40020404,0x0,RegisterAccess.RW) OTYPER;		 // offset:0x4 OTYPER GPIO port output type register
		@RegInfo("OSPEEDR",0x8) RegVolatile!(mask_OSPEEDR,uint,0x40020408,0xc0,RegisterAccess.RW) OSPEEDR;		 // offset:0x8 OSPEEDR GPIO port output speed register
		@RegInfo("PUPDR",0xc) RegVolatile!(mask_PUPDR,uint,0x4002040c,0x100,RegisterAccess.RW) PUPDR;		 // offset:0x12 PUPDR GPIO port pull-up/pull-down register
		@RegInfo("IDR",0x10) RegVolatile!(mask_IDR,uint,0x40020410,0x0,RegisterAccess.RO) IDR;		 // offset:0x16 IDR GPIO port input data register
		@RegInfo("ODR",0x14) RegVolatile!(mask_ODR,uint,0x40020414,0x0,RegisterAccess.RW) ODR;		 // offset:0x20 ODR GPIO port output data register
		@RegInfo("BSRR",0x18) RegVolatile!(mask_BSRR,uint,0x40020418,0x0,RegisterAccess.WO) BSRR;		 // offset:0x24 BSRR GPIO port bit set/reset register
		@RegInfo("LCKR",0x1c) RegVolatile!(mask_LCKR,uint,0x4002041c,0x0,RegisterAccess.RW) LCKR;		 // offset:0x28 LCKR GPIO port configuration lock register
		@RegInfo("AFRL",0x20) RegVolatile!(mask_AFRL,uint,0x40020420,0x0,RegisterAccess.RW) AFRL;		 // offset:0x32 AFRL GPIO alternate function low register
		@RegInfo("AFRH",0x24) RegVolatile!(mask_AFRH,uint,0x40020424,0x0,RegisterAccess.RW) AFRH;		 // offset:0x36 AFRH GPIO alternate function high register
		enum mask_MODER {
				MODER15 = RegBitField!uint(30,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				MODER14 = RegBitField!uint(28,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				MODER13 = RegBitField!uint(26,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				MODER12 = RegBitField!uint(24,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				MODER11 = RegBitField!uint(22,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				MODER10 = RegBitField!uint(20,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				MODER9 = RegBitField!uint(18,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				MODER8 = RegBitField!uint(16,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				MODER7 = RegBitField!uint(14,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				MODER6 = RegBitField!uint(12,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				MODER5 = RegBitField!uint(10,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				MODER4 = RegBitField!uint(8,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				MODER3 = RegBitField!uint(6,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				MODER2 = RegBitField!uint(4,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				MODER1 = RegBitField!uint(2,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				MODER0 = RegBitField!uint(0,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
		}
		enum mask_OTYPER {
				OT15 = RegBitField!uint(15,1,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				OT14 = RegBitField!uint(14,1,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				OT13 = RegBitField!uint(13,1,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				OT12 = RegBitField!uint(12,1,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				OT11 = RegBitField!uint(11,1,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				OT10 = RegBitField!uint(10,1,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				OT9 = RegBitField!uint(9,1,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				OT8 = RegBitField!uint(8,1,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				OT7 = RegBitField!uint(7,1,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				OT6 = RegBitField!uint(6,1,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				OT5 = RegBitField!uint(5,1,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				OT4 = RegBitField!uint(4,1,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				OT3 = RegBitField!uint(3,1,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				OT2 = RegBitField!uint(2,1,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				OT1 = RegBitField!uint(1,1,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				OT0 = RegBitField!uint(0,1,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
		}
		enum mask_OSPEEDR {
				OSPEEDR15 = RegBitField!uint(30,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				OSPEEDR14 = RegBitField!uint(28,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				OSPEEDR13 = RegBitField!uint(26,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				OSPEEDR12 = RegBitField!uint(24,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				OSPEEDR11 = RegBitField!uint(22,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				OSPEEDR10 = RegBitField!uint(20,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				OSPEEDR9 = RegBitField!uint(18,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				OSPEEDR8 = RegBitField!uint(16,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				OSPEEDR7 = RegBitField!uint(14,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				OSPEEDR6 = RegBitField!uint(12,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				OSPEEDR5 = RegBitField!uint(10,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				OSPEEDR4 = RegBitField!uint(8,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				OSPEEDR3 = RegBitField!uint(6,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				OSPEEDR2 = RegBitField!uint(4,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				OSPEEDR1 = RegBitField!uint(2,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				OSPEEDR0 = RegBitField!uint(0,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
		}
		enum mask_PUPDR {
				PUPDR15 = RegBitField!uint(30,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				PUPDR14 = RegBitField!uint(28,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				PUPDR13 = RegBitField!uint(26,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				PUPDR12 = RegBitField!uint(24,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				PUPDR11 = RegBitField!uint(22,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				PUPDR10 = RegBitField!uint(20,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				PUPDR9 = RegBitField!uint(18,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				PUPDR8 = RegBitField!uint(16,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				PUPDR7 = RegBitField!uint(14,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				PUPDR6 = RegBitField!uint(12,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				PUPDR5 = RegBitField!uint(10,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				PUPDR4 = RegBitField!uint(8,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				PUPDR3 = RegBitField!uint(6,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				PUPDR2 = RegBitField!uint(4,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				PUPDR1 = RegBitField!uint(2,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				PUPDR0 = RegBitField!uint(0,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
		}
		enum mask_IDR {
				IDR15 = RegBitField!uint(15,1,RegisterAccess.RO),		// Port input data (y = 0..15)
				IDR14 = RegBitField!uint(14,1,RegisterAccess.RO),		// Port input data (y = 0..15)
				IDR13 = RegBitField!uint(13,1,RegisterAccess.RO),		// Port input data (y = 0..15)
				IDR12 = RegBitField!uint(12,1,RegisterAccess.RO),		// Port input data (y = 0..15)
				IDR11 = RegBitField!uint(11,1,RegisterAccess.RO),		// Port input data (y = 0..15)
				IDR10 = RegBitField!uint(10,1,RegisterAccess.RO),		// Port input data (y = 0..15)
				IDR9 = RegBitField!uint(9,1,RegisterAccess.RO),		// Port input data (y = 0..15)
				IDR8 = RegBitField!uint(8,1,RegisterAccess.RO),		// Port input data (y = 0..15)
				IDR7 = RegBitField!uint(7,1,RegisterAccess.RO),		// Port input data (y = 0..15)
				IDR6 = RegBitField!uint(6,1,RegisterAccess.RO),		// Port input data (y = 0..15)
				IDR5 = RegBitField!uint(5,1,RegisterAccess.RO),		// Port input data (y = 0..15)
				IDR4 = RegBitField!uint(4,1,RegisterAccess.RO),		// Port input data (y = 0..15)
				IDR3 = RegBitField!uint(3,1,RegisterAccess.RO),		// Port input data (y = 0..15)
				IDR2 = RegBitField!uint(2,1,RegisterAccess.RO),		// Port input data (y = 0..15)
				IDR1 = RegBitField!uint(1,1,RegisterAccess.RO),		// Port input data (y = 0..15)
				IDR0 = RegBitField!uint(0,1,RegisterAccess.RO),		// Port input data (y = 0..15)
		}
		enum mask_ODR {
				ODR15 = RegBitField!uint(15,1,RegisterAccess.RW),		// Port output data (y = 0..15)
				ODR14 = RegBitField!uint(14,1,RegisterAccess.RW),		// Port output data (y = 0..15)
				ODR13 = RegBitField!uint(13,1,RegisterAccess.RW),		// Port output data (y = 0..15)
				ODR12 = RegBitField!uint(12,1,RegisterAccess.RW),		// Port output data (y = 0..15)
				ODR11 = RegBitField!uint(11,1,RegisterAccess.RW),		// Port output data (y = 0..15)
				ODR10 = RegBitField!uint(10,1,RegisterAccess.RW),		// Port output data (y = 0..15)
				ODR9 = RegBitField!uint(9,1,RegisterAccess.RW),		// Port output data (y = 0..15)
				ODR8 = RegBitField!uint(8,1,RegisterAccess.RW),		// Port output data (y = 0..15)
				ODR7 = RegBitField!uint(7,1,RegisterAccess.RW),		// Port output data (y = 0..15)
				ODR6 = RegBitField!uint(6,1,RegisterAccess.RW),		// Port output data (y = 0..15)
				ODR5 = RegBitField!uint(5,1,RegisterAccess.RW),		// Port output data (y = 0..15)
				ODR4 = RegBitField!uint(4,1,RegisterAccess.RW),		// Port output data (y = 0..15)
				ODR3 = RegBitField!uint(3,1,RegisterAccess.RW),		// Port output data (y = 0..15)
				ODR2 = RegBitField!uint(2,1,RegisterAccess.RW),		// Port output data (y = 0..15)
				ODR1 = RegBitField!uint(1,1,RegisterAccess.RW),		// Port output data (y = 0..15)
				ODR0 = RegBitField!uint(0,1,RegisterAccess.RW),		// Port output data (y = 0..15)
		}
		enum mask_BSRR {
				BR15 = RegBitField!uint(31,1,RegisterAccess.WO),		// Port x reset bit y (y = 0..15)
				BR14 = RegBitField!uint(30,1,RegisterAccess.WO),		// Port x reset bit y (y = 0..15)
				BR13 = RegBitField!uint(29,1,RegisterAccess.WO),		// Port x reset bit y (y = 0..15)
				BR12 = RegBitField!uint(28,1,RegisterAccess.WO),		// Port x reset bit y (y = 0..15)
				BR11 = RegBitField!uint(27,1,RegisterAccess.WO),		// Port x reset bit y (y = 0..15)
				BR10 = RegBitField!uint(26,1,RegisterAccess.WO),		// Port x reset bit y (y = 0..15)
				BR9 = RegBitField!uint(25,1,RegisterAccess.WO),		// Port x reset bit y (y = 0..15)
				BR8 = RegBitField!uint(24,1,RegisterAccess.WO),		// Port x reset bit y (y = 0..15)
				BR7 = RegBitField!uint(23,1,RegisterAccess.WO),		// Port x reset bit y (y = 0..15)
				BR6 = RegBitField!uint(22,1,RegisterAccess.WO),		// Port x reset bit y (y = 0..15)
				BR5 = RegBitField!uint(21,1,RegisterAccess.WO),		// Port x reset bit y (y = 0..15)
				BR4 = RegBitField!uint(20,1,RegisterAccess.WO),		// Port x reset bit y (y = 0..15)
				BR3 = RegBitField!uint(19,1,RegisterAccess.WO),		// Port x reset bit y (y = 0..15)
				BR2 = RegBitField!uint(18,1,RegisterAccess.WO),		// Port x reset bit y (y = 0..15)
				BR1 = RegBitField!uint(17,1,RegisterAccess.WO),		// Port x reset bit y (y = 0..15)
				BR0 = RegBitField!uint(16,1,RegisterAccess.WO),		// Port x set bit y (y= 0..15)
				BS15 = RegBitField!uint(15,1,RegisterAccess.WO),		// Port x set bit y (y= 0..15)
				BS14 = RegBitField!uint(14,1,RegisterAccess.WO),		// Port x set bit y (y= 0..15)
				BS13 = RegBitField!uint(13,1,RegisterAccess.WO),		// Port x set bit y (y= 0..15)
				BS12 = RegBitField!uint(12,1,RegisterAccess.WO),		// Port x set bit y (y= 0..15)
				BS11 = RegBitField!uint(11,1,RegisterAccess.WO),		// Port x set bit y (y= 0..15)
				BS10 = RegBitField!uint(10,1,RegisterAccess.WO),		// Port x set bit y (y= 0..15)
				BS9 = RegBitField!uint(9,1,RegisterAccess.WO),		// Port x set bit y (y= 0..15)
				BS8 = RegBitField!uint(8,1,RegisterAccess.WO),		// Port x set bit y (y= 0..15)
				BS7 = RegBitField!uint(7,1,RegisterAccess.WO),		// Port x set bit y (y= 0..15)
				BS6 = RegBitField!uint(6,1,RegisterAccess.WO),		// Port x set bit y (y= 0..15)
				BS5 = RegBitField!uint(5,1,RegisterAccess.WO),		// Port x set bit y (y= 0..15)
				BS4 = RegBitField!uint(4,1,RegisterAccess.WO),		// Port x set bit y (y= 0..15)
				BS3 = RegBitField!uint(3,1,RegisterAccess.WO),		// Port x set bit y (y= 0..15)
				BS2 = RegBitField!uint(2,1,RegisterAccess.WO),		// Port x set bit y (y= 0..15)
				BS1 = RegBitField!uint(1,1,RegisterAccess.WO),		// Port x set bit y (y= 0..15)
				BS0 = RegBitField!uint(0,1,RegisterAccess.WO),		// Port x set bit y (y= 0..15)
		}
		enum mask_LCKR {
				LCKK = RegBitField!uint(16,1,RegisterAccess.RW),		// Port x lock bit y (y= 0..15)
				LCK15 = RegBitField!uint(15,1,RegisterAccess.RW),		// Port x lock bit y (y= 0..15)
				LCK14 = RegBitField!uint(14,1,RegisterAccess.RW),		// Port x lock bit y (y= 0..15)
				LCK13 = RegBitField!uint(13,1,RegisterAccess.RW),		// Port x lock bit y (y= 0..15)
				LCK12 = RegBitField!uint(12,1,RegisterAccess.RW),		// Port x lock bit y (y= 0..15)
				LCK11 = RegBitField!uint(11,1,RegisterAccess.RW),		// Port x lock bit y (y= 0..15)
				LCK10 = RegBitField!uint(10,1,RegisterAccess.RW),		// Port x lock bit y (y= 0..15)
				LCK9 = RegBitField!uint(9,1,RegisterAccess.RW),		// Port x lock bit y (y= 0..15)
				LCK8 = RegBitField!uint(8,1,RegisterAccess.RW),		// Port x lock bit y (y= 0..15)
				LCK7 = RegBitField!uint(7,1,RegisterAccess.RW),		// Port x lock bit y (y= 0..15)
				LCK6 = RegBitField!uint(6,1,RegisterAccess.RW),		// Port x lock bit y (y= 0..15)
				LCK5 = RegBitField!uint(5,1,RegisterAccess.RW),		// Port x lock bit y (y= 0..15)
				LCK4 = RegBitField!uint(4,1,RegisterAccess.RW),		// Port x lock bit y (y= 0..15)
				LCK3 = RegBitField!uint(3,1,RegisterAccess.RW),		// Port x lock bit y (y= 0..15)
				LCK2 = RegBitField!uint(2,1,RegisterAccess.RW),		// Port x lock bit y (y= 0..15)
				LCK1 = RegBitField!uint(1,1,RegisterAccess.RW),		// Port x lock bit y (y= 0..15)
				LCK0 = RegBitField!uint(0,1,RegisterAccess.RW),		// Port x lock bit y (y= 0..15)
		}
		enum mask_AFRL {
				AFRL7 = RegBitField!uint(28,4,RegisterAccess.RW),		// Alternate function selection for port x bit y (y = 0..7)
				AFRL6 = RegBitField!uint(24,4,RegisterAccess.RW),		// Alternate function selection for port x bit y (y = 0..7)
				AFRL5 = RegBitField!uint(20,4,RegisterAccess.RW),		// Alternate function selection for port x bit y (y = 0..7)
				AFRL4 = RegBitField!uint(16,4,RegisterAccess.RW),		// Alternate function selection for port x bit y (y = 0..7)
				AFRL3 = RegBitField!uint(12,4,RegisterAccess.RW),		// Alternate function selection for port x bit y (y = 0..7)
				AFRL2 = RegBitField!uint(8,4,RegisterAccess.RW),		// Alternate function selection for port x bit y (y = 0..7)
				AFRL1 = RegBitField!uint(4,4,RegisterAccess.RW),		// Alternate function selection for port x bit y (y = 0..7)
				AFRL0 = RegBitField!uint(0,4,RegisterAccess.RW),		// Alternate function selection for port x bit y (y = 0..7)
		}
		enum mask_AFRH {
				AFRH15 = RegBitField!uint(28,4,RegisterAccess.RW),		// Alternate function selection for port x bit y (y = 8..15)
				AFRH14 = RegBitField!uint(24,4,RegisterAccess.RW),		// Alternate function selection for port x bit y (y = 8..15)
				AFRH13 = RegBitField!uint(20,4,RegisterAccess.RW),		// Alternate function selection for port x bit y (y = 8..15)
				AFRH12 = RegBitField!uint(16,4,RegisterAccess.RW),		// Alternate function selection for port x bit y (y = 8..15)
				AFRH11 = RegBitField!uint(12,4,RegisterAccess.RW),		// Alternate function selection for port x bit y (y = 8..15)
				AFRH10 = RegBitField!uint(8,4,RegisterAccess.RW),		// Alternate function selection for port x bit y (y = 8..15)
				AFRH9 = RegBitField!uint(4,4,RegisterAccess.RW),		// Alternate function selection for port x bit y (y = 8..15)
				AFRH8 = RegBitField!uint(0,4,RegisterAccess.RW),		// Alternate function selection for port x bit y (y = 8..15)
		}
	}
	// General-purpose I/Os
	struct GPIOA_Type {
		@RegInfo("MODER",0x0) RegVolatile!(mask_MODER,uint,0x40020000,0xa8000000,RegisterAccess.RW) MODER;		 // offset:0x0 MODER GPIO port mode register
		@RegInfo("OTYPER",0x4) RegVolatile!(mask_OTYPER,uint,0x40020004,0x0,RegisterAccess.RW) OTYPER;		 // offset:0x4 OTYPER GPIO port output type register
		@RegInfo("OSPEEDR",0x8) RegVolatile!(mask_OSPEEDR,uint,0x40020008,0x0,RegisterAccess.RW) OSPEEDR;		 // offset:0x8 OSPEEDR GPIO port output speed register
		@RegInfo("PUPDR",0xc) RegVolatile!(mask_PUPDR,uint,0x4002000c,0x64000000,RegisterAccess.RW) PUPDR;		 // offset:0x12 PUPDR GPIO port pull-up/pull-down register
		@RegInfo("IDR",0x10) RegVolatile!(mask_IDR,uint,0x40020010,0x0,RegisterAccess.RO) IDR;		 // offset:0x16 IDR GPIO port input data register
		@RegInfo("ODR",0x14) RegVolatile!(mask_ODR,uint,0x40020014,0x0,RegisterAccess.RW) ODR;		 // offset:0x20 ODR GPIO port output data register
		@RegInfo("BSRR",0x18) RegVolatile!(mask_BSRR,uint,0x40020018,0x0,RegisterAccess.WO) BSRR;		 // offset:0x24 BSRR GPIO port bit set/reset register
		@RegInfo("LCKR",0x1c) RegVolatile!(mask_LCKR,uint,0x4002001c,0x0,RegisterAccess.RW) LCKR;		 // offset:0x28 LCKR GPIO port configuration lock register
		@RegInfo("AFRL",0x20) RegVolatile!(mask_AFRL,uint,0x40020020,0x0,RegisterAccess.RW) AFRL;		 // offset:0x32 AFRL GPIO alternate function low register
		@RegInfo("AFRH",0x24) RegVolatile!(mask_AFRH,uint,0x40020024,0x0,RegisterAccess.RW) AFRH;		 // offset:0x36 AFRH GPIO alternate function high register
		enum mask_MODER {
				MODER15 = RegBitField!uint(30,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				MODER14 = RegBitField!uint(28,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				MODER13 = RegBitField!uint(26,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				MODER12 = RegBitField!uint(24,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				MODER11 = RegBitField!uint(22,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				MODER10 = RegBitField!uint(20,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				MODER9 = RegBitField!uint(18,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				MODER8 = RegBitField!uint(16,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				MODER7 = RegBitField!uint(14,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				MODER6 = RegBitField!uint(12,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				MODER5 = RegBitField!uint(10,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				MODER4 = RegBitField!uint(8,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				MODER3 = RegBitField!uint(6,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				MODER2 = RegBitField!uint(4,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				MODER1 = RegBitField!uint(2,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				MODER0 = RegBitField!uint(0,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
		}
		enum mask_OTYPER {
				OT15 = RegBitField!uint(15,1,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				OT14 = RegBitField!uint(14,1,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				OT13 = RegBitField!uint(13,1,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				OT12 = RegBitField!uint(12,1,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				OT11 = RegBitField!uint(11,1,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				OT10 = RegBitField!uint(10,1,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				OT9 = RegBitField!uint(9,1,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				OT8 = RegBitField!uint(8,1,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				OT7 = RegBitField!uint(7,1,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				OT6 = RegBitField!uint(6,1,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				OT5 = RegBitField!uint(5,1,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				OT4 = RegBitField!uint(4,1,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				OT3 = RegBitField!uint(3,1,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				OT2 = RegBitField!uint(2,1,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				OT1 = RegBitField!uint(1,1,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				OT0 = RegBitField!uint(0,1,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
		}
		enum mask_OSPEEDR {
				OSPEEDR15 = RegBitField!uint(30,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				OSPEEDR14 = RegBitField!uint(28,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				OSPEEDR13 = RegBitField!uint(26,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				OSPEEDR12 = RegBitField!uint(24,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				OSPEEDR11 = RegBitField!uint(22,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				OSPEEDR10 = RegBitField!uint(20,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				OSPEEDR9 = RegBitField!uint(18,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				OSPEEDR8 = RegBitField!uint(16,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				OSPEEDR7 = RegBitField!uint(14,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				OSPEEDR6 = RegBitField!uint(12,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				OSPEEDR5 = RegBitField!uint(10,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				OSPEEDR4 = RegBitField!uint(8,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				OSPEEDR3 = RegBitField!uint(6,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				OSPEEDR2 = RegBitField!uint(4,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				OSPEEDR1 = RegBitField!uint(2,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				OSPEEDR0 = RegBitField!uint(0,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
		}
		enum mask_PUPDR {
				PUPDR15 = RegBitField!uint(30,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				PUPDR14 = RegBitField!uint(28,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				PUPDR13 = RegBitField!uint(26,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				PUPDR12 = RegBitField!uint(24,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				PUPDR11 = RegBitField!uint(22,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				PUPDR10 = RegBitField!uint(20,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				PUPDR9 = RegBitField!uint(18,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				PUPDR8 = RegBitField!uint(16,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				PUPDR7 = RegBitField!uint(14,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				PUPDR6 = RegBitField!uint(12,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				PUPDR5 = RegBitField!uint(10,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				PUPDR4 = RegBitField!uint(8,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				PUPDR3 = RegBitField!uint(6,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				PUPDR2 = RegBitField!uint(4,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				PUPDR1 = RegBitField!uint(2,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
				PUPDR0 = RegBitField!uint(0,2,RegisterAccess.RW),		// Port x configuration bits (y = 0..15)
		}
		enum mask_IDR {
				IDR15 = RegBitField!uint(15,1,RegisterAccess.RO),		// Port input data (y = 0..15)
				IDR14 = RegBitField!uint(14,1,RegisterAccess.RO),		// Port input data (y = 0..15)
				IDR13 = RegBitField!uint(13,1,RegisterAccess.RO),		// Port input data (y = 0..15)
				IDR12 = RegBitField!uint(12,1,RegisterAccess.RO),		// Port input data (y = 0..15)
				IDR11 = RegBitField!uint(11,1,RegisterAccess.RO),		// Port input data (y = 0..15)
				IDR10 = RegBitField!uint(10,1,RegisterAccess.RO),		// Port input data (y = 0..15)
				IDR9 = RegBitField!uint(9,1,RegisterAccess.RO),		// Port input data (y = 0..15)
				IDR8 = RegBitField!uint(8,1,RegisterAccess.RO),		// Port input data (y = 0..15)
				IDR7 = RegBitField!uint(7,1,RegisterAccess.RO),		// Port input data (y = 0..15)
				IDR6 = RegBitField!uint(6,1,RegisterAccess.RO),		// Port input data (y = 0..15)
				IDR5 = RegBitField!uint(5,1,RegisterAccess.RO),		// Port input data (y = 0..15)
				IDR4 = RegBitField!uint(4,1,RegisterAccess.RO),		// Port input data (y = 0..15)
				IDR3 = RegBitField!uint(3,1,RegisterAccess.RO),		// Port input data (y = 0..15)
				IDR2 = RegBitField!uint(2,1,RegisterAccess.RO),		// Port input data (y = 0..15)
				IDR1 = RegBitField!uint(1,1,RegisterAccess.RO),		// Port input data (y = 0..15)
				IDR0 = RegBitField!uint(0,1,RegisterAccess.RO),		// Port input data (y = 0..15)
		}
		enum mask_ODR {
				ODR15 = RegBitField!uint(15,1,RegisterAccess.RW),		// Port output data (y = 0..15)
				ODR14 = RegBitField!uint(14,1,RegisterAccess.RW),		// Port output data (y = 0..15)
				ODR13 = RegBitField!uint(13,1,RegisterAccess.RW),		// Port output data (y = 0..15)
				ODR12 = RegBitField!uint(12,1,RegisterAccess.RW),		// Port output data (y = 0..15)
				ODR11 = RegBitField!uint(11,1,RegisterAccess.RW),		// Port output data (y = 0..15)
				ODR10 = RegBitField!uint(10,1,RegisterAccess.RW),		// Port output data (y = 0..15)
				ODR9 = RegBitField!uint(9,1,RegisterAccess.RW),		// Port output data (y = 0..15)
				ODR8 = RegBitField!uint(8,1,RegisterAccess.RW),		// Port output data (y = 0..15)
				ODR7 = RegBitField!uint(7,1,RegisterAccess.RW),		// Port output data (y = 0..15)
				ODR6 = RegBitField!uint(6,1,RegisterAccess.RW),		// Port output data (y = 0..15)
				ODR5 = RegBitField!uint(5,1,RegisterAccess.RW),		// Port output data (y = 0..15)
				ODR4 = RegBitField!uint(4,1,RegisterAccess.RW),		// Port output data (y = 0..15)
				ODR3 = RegBitField!uint(3,1,RegisterAccess.RW),		// Port output data (y = 0..15)
				ODR2 = RegBitField!uint(2,1,RegisterAccess.RW),		// Port output data (y = 0..15)
				ODR1 = RegBitField!uint(1,1,RegisterAccess.RW),		// Port output data (y = 0..15)
				ODR0 = RegBitField!uint(0,1,RegisterAccess.RW),		// Port output data (y = 0..15)
		}
		enum mask_BSRR {
				BR15 = RegBitField!uint(31,1,RegisterAccess.WO),		// Port x reset bit y (y = 0..15)
				BR14 = RegBitField!uint(30,1,RegisterAccess.WO),		// Port x reset bit y (y = 0..15)
				BR13 = RegBitField!uint(29,1,RegisterAccess.WO),		// Port x reset bit y (y = 0..15)
				BR12 = RegBitField!uint(28,1,RegisterAccess.WO),		// Port x reset bit y (y = 0..15)
				BR11 = RegBitField!uint(27,1,RegisterAccess.WO),		// Port x reset bit y (y = 0..15)
				BR10 = RegBitField!uint(26,1,RegisterAccess.WO),		// Port x reset bit y (y = 0..15)
				BR9 = RegBitField!uint(25,1,RegisterAccess.WO),		// Port x reset bit y (y = 0..15)
				BR8 = RegBitField!uint(24,1,RegisterAccess.WO),		// Port x reset bit y (y = 0..15)
				BR7 = RegBitField!uint(23,1,RegisterAccess.WO),		// Port x reset bit y (y = 0..15)
				BR6 = RegBitField!uint(22,1,RegisterAccess.WO),		// Port x reset bit y (y = 0..15)
				BR5 = RegBitField!uint(21,1,RegisterAccess.WO),		// Port x reset bit y (y = 0..15)
				BR4 = RegBitField!uint(20,1,RegisterAccess.WO),		// Port x reset bit y (y = 0..15)
				BR3 = RegBitField!uint(19,1,RegisterAccess.WO),		// Port x reset bit y (y = 0..15)
				BR2 = RegBitField!uint(18,1,RegisterAccess.WO),		// Port x reset bit y (y = 0..15)
				BR1 = RegBitField!uint(17,1,RegisterAccess.WO),		// Port x reset bit y (y = 0..15)
				BR0 = RegBitField!uint(16,1,RegisterAccess.WO),		// Port x set bit y (y= 0..15)
				BS15 = RegBitField!uint(15,1,RegisterAccess.WO),		// Port x set bit y (y= 0..15)
				BS14 = RegBitField!uint(14,1,RegisterAccess.WO),		// Port x set bit y (y= 0..15)
				BS13 = RegBitField!uint(13,1,RegisterAccess.WO),		// Port x set bit y (y= 0..15)
				BS12 = RegBitField!uint(12,1,RegisterAccess.WO),		// Port x set bit y (y= 0..15)
				BS11 = RegBitField!uint(11,1,RegisterAccess.WO),		// Port x set bit y (y= 0..15)
				BS10 = RegBitField!uint(10,1,RegisterAccess.WO),		// Port x set bit y (y= 0..15)
				BS9 = RegBitField!uint(9,1,RegisterAccess.WO),		// Port x set bit y (y= 0..15)
				BS8 = RegBitField!uint(8,1,RegisterAccess.WO),		// Port x set bit y (y= 0..15)
				BS7 = RegBitField!uint(7,1,RegisterAccess.WO),		// Port x set bit y (y= 0..15)
				BS6 = RegBitField!uint(6,1,RegisterAccess.WO),		// Port x set bit y (y= 0..15)
				BS5 = RegBitField!uint(5,1,RegisterAccess.WO),		// Port x set bit y (y= 0..15)
				BS4 = RegBitField!uint(4,1,RegisterAccess.WO),		// Port x set bit y (y= 0..15)
				BS3 = RegBitField!uint(3,1,RegisterAccess.WO),		// Port x set bit y (y= 0..15)
				BS2 = RegBitField!uint(2,1,RegisterAccess.WO),		// Port x set bit y (y= 0..15)
				BS1 = RegBitField!uint(1,1,RegisterAccess.WO),		// Port x set bit y (y= 0..15)
				BS0 = RegBitField!uint(0,1,RegisterAccess.WO),		// Port x set bit y (y= 0..15)
		}
		enum mask_LCKR {
				LCKK = RegBitField!uint(16,1,RegisterAccess.RW),		// Port x lock bit y (y= 0..15)
				LCK15 = RegBitField!uint(15,1,RegisterAccess.RW),		// Port x lock bit y (y= 0..15)
				LCK14 = RegBitField!uint(14,1,RegisterAccess.RW),		// Port x lock bit y (y= 0..15)
				LCK13 = RegBitField!uint(13,1,RegisterAccess.RW),		// Port x lock bit y (y= 0..15)
				LCK12 = RegBitField!uint(12,1,RegisterAccess.RW),		// Port x lock bit y (y= 0..15)
				LCK11 = RegBitField!uint(11,1,RegisterAccess.RW),		// Port x lock bit y (y= 0..15)
				LCK10 = RegBitField!uint(10,1,RegisterAccess.RW),		// Port x lock bit y (y= 0..15)
				LCK9 = RegBitField!uint(9,1,RegisterAccess.RW),		// Port x lock bit y (y= 0..15)
				LCK8 = RegBitField!uint(8,1,RegisterAccess.RW),		// Port x lock bit y (y= 0..15)
				LCK7 = RegBitField!uint(7,1,RegisterAccess.RW),		// Port x lock bit y (y= 0..15)
				LCK6 = RegBitField!uint(6,1,RegisterAccess.RW),		// Port x lock bit y (y= 0..15)
				LCK5 = RegBitField!uint(5,1,RegisterAccess.RW),		// Port x lock bit y (y= 0..15)
				LCK4 = RegBitField!uint(4,1,RegisterAccess.RW),		// Port x lock bit y (y= 0..15)
				LCK3 = RegBitField!uint(3,1,RegisterAccess.RW),		// Port x lock bit y (y= 0..15)
				LCK2 = RegBitField!uint(2,1,RegisterAccess.RW),		// Port x lock bit y (y= 0..15)
				LCK1 = RegBitField!uint(1,1,RegisterAccess.RW),		// Port x lock bit y (y= 0..15)
				LCK0 = RegBitField!uint(0,1,RegisterAccess.RW),		// Port x lock bit y (y= 0..15)
		}
		enum mask_AFRL {
				AFRL7 = RegBitField!uint(28,4,RegisterAccess.RW),		// Alternate function selection for port x bit y (y = 0..7)
				AFRL6 = RegBitField!uint(24,4,RegisterAccess.RW),		// Alternate function selection for port x bit y (y = 0..7)
				AFRL5 = RegBitField!uint(20,4,RegisterAccess.RW),		// Alternate function selection for port x bit y (y = 0..7)
				AFRL4 = RegBitField!uint(16,4,RegisterAccess.RW),		// Alternate function selection for port x bit y (y = 0..7)
				AFRL3 = RegBitField!uint(12,4,RegisterAccess.RW),		// Alternate function selection for port x bit y (y = 0..7)
				AFRL2 = RegBitField!uint(8,4,RegisterAccess.RW),		// Alternate function selection for port x bit y (y = 0..7)
				AFRL1 = RegBitField!uint(4,4,RegisterAccess.RW),		// Alternate function selection for port x bit y (y = 0..7)
				AFRL0 = RegBitField!uint(0,4,RegisterAccess.RW),		// Alternate function selection for port x bit y (y = 0..7)
		}
		enum mask_AFRH {
				AFRH15 = RegBitField!uint(28,4,RegisterAccess.RW),		// Alternate function selection for port x bit y (y = 8..15)
				AFRH14 = RegBitField!uint(24,4,RegisterAccess.RW),		// Alternate function selection for port x bit y (y = 8..15)
				AFRH13 = RegBitField!uint(20,4,RegisterAccess.RW),		// Alternate function selection for port x bit y (y = 8..15)
				AFRH12 = RegBitField!uint(16,4,RegisterAccess.RW),		// Alternate function selection for port x bit y (y = 8..15)
				AFRH11 = RegBitField!uint(12,4,RegisterAccess.RW),		// Alternate function selection for port x bit y (y = 8..15)
				AFRH10 = RegBitField!uint(8,4,RegisterAccess.RW),		// Alternate function selection for port x bit y (y = 8..15)
				AFRH9 = RegBitField!uint(4,4,RegisterAccess.RW),		// Alternate function selection for port x bit y (y = 8..15)
				AFRH8 = RegBitField!uint(0,4,RegisterAccess.RW),		// Alternate function selection for port x bit y (y = 8..15)
		}
	}
	// ADC Group 
	// Analog-to-digital converter
	struct ADC1_Type {
		@RegInfo("SR",0x0) RegVolatile!(mask_SR,uint,0x40012000,0x0,RegisterAccess.RW) SR;		 // offset:0x0 SR status register
		@RegInfo("CR1",0x4) RegVolatile!(mask_CR1,uint,0x40012004,0x0,RegisterAccess.RW) CR1;		 // offset:0x4 CR1 control register 1
		@RegInfo("CR2",0x8) RegVolatile!(mask_CR2,uint,0x40012008,0x0,RegisterAccess.RW) CR2;		 // offset:0x8 CR2 control register 2
		@RegInfo("SMPR1",0xc) RegVolatile!(mask_SMPR1,uint,0x4001200c,0x0,RegisterAccess.RW) SMPR1;		 // offset:0x12 SMPR1 sample time register 1
		@RegInfo("SMPR2",0x10) RegVolatile!(mask_SMPR2,uint,0x40012010,0x0,RegisterAccess.RW) SMPR2;		 // offset:0x16 SMPR2 sample time register 2
		@RegInfo("JOFR1",0x14) RegVolatile!(mask_JOFR1,uint,0x40012014,0x0,RegisterAccess.RW) JOFR1;		 // offset:0x20 JOFR1 injected channel data offset register x
		@RegInfo("JOFR2",0x18) RegVolatile!(mask_JOFR2,uint,0x40012018,0x0,RegisterAccess.RW) JOFR2;		 // offset:0x24 JOFR2 injected channel data offset register x
		@RegInfo("JOFR3",0x1c) RegVolatile!(mask_JOFR3,uint,0x4001201c,0x0,RegisterAccess.RW) JOFR3;		 // offset:0x28 JOFR3 injected channel data offset register x
		@RegInfo("JOFR4",0x20) RegVolatile!(mask_JOFR4,uint,0x40012020,0x0,RegisterAccess.RW) JOFR4;		 // offset:0x32 JOFR4 injected channel data offset register x
		@RegInfo("HTR",0x24) RegVolatile!(mask_HTR,uint,0x40012024,0xfff,RegisterAccess.RW) HTR;		 // offset:0x36 HTR watchdog higher threshold register
		@RegInfo("LTR",0x28) RegVolatile!(mask_LTR,uint,0x40012028,0x0,RegisterAccess.RW) LTR;		 // offset:0x40 LTR watchdog lower threshold register
		@RegInfo("SQR1",0x2c) RegVolatile!(mask_SQR1,uint,0x4001202c,0x0,RegisterAccess.RW) SQR1;		 // offset:0x44 SQR1 regular sequence register 1
		@RegInfo("SQR2",0x30) RegVolatile!(mask_SQR2,uint,0x40012030,0x0,RegisterAccess.RW) SQR2;		 // offset:0x48 SQR2 regular sequence register 2
		@RegInfo("SQR3",0x34) RegVolatile!(mask_SQR3,uint,0x40012034,0x0,RegisterAccess.RW) SQR3;		 // offset:0x52 SQR3 regular sequence register 3
		@RegInfo("JSQR",0x38) RegVolatile!(mask_JSQR,uint,0x40012038,0x0,RegisterAccess.RW) JSQR;		 // offset:0x56 JSQR injected sequence register
		@RegInfo("JDR1",0x3c) RegVolatile!(mask_JDR1,uint,0x4001203c,0x0,RegisterAccess.RO) JDR1;		 // offset:0x60 JDR1 injected data register x
		@RegInfo("JDR2",0x40) RegVolatile!(mask_JDR2,uint,0x40012040,0x0,RegisterAccess.RO) JDR2;		 // offset:0x64 JDR2 injected data register x
		@RegInfo("JDR3",0x44) RegVolatile!(mask_JDR3,uint,0x40012044,0x0,RegisterAccess.RO) JDR3;		 // offset:0x68 JDR3 injected data register x
		@RegInfo("JDR4",0x48) RegVolatile!(mask_JDR4,uint,0x40012048,0x0,RegisterAccess.RO) JDR4;		 // offset:0x72 JDR4 injected data register x
		@RegInfo("DR",0x4c) RegVolatile!(mask_DR,uint,0x4001204c,0x0,RegisterAccess.RO) DR;		 // offset:0x76 DR regular data register
		enum mask_SR {
				OVR = RegBitField!uint(5,1,RegisterAccess.RW),		// Overrun
				STRT = RegBitField!uint(4,1,RegisterAccess.RW),		// Regular channel start flag
				JSTRT = RegBitField!uint(3,1,RegisterAccess.RW),		// Injected channel start flag
				JEOC = RegBitField!uint(2,1,RegisterAccess.RW),		// Injected channel end of conversion
				EOC = RegBitField!uint(1,1,RegisterAccess.RW),		// Regular channel end of conversion
				AWD = RegBitField!uint(0,1,RegisterAccess.RW),		// Analog watchdog flag
		}
		enum mask_CR1 {
				OVRIE = RegBitField!uint(26,1,RegisterAccess.RW),		// Overrun interrupt enable
				RES = RegBitField!uint(24,2,RegisterAccess.RW),		// Resolution
				AWDEN = RegBitField!uint(23,1,RegisterAccess.RW),		// Analog watchdog enable on regular channels
				JAWDEN = RegBitField!uint(22,1,RegisterAccess.RW),		// Analog watchdog enable on injected channels
				DISCNUM = RegBitField!uint(13,3,RegisterAccess.RW),		// Discontinuous mode channel count
				JDISCEN = RegBitField!uint(12,1,RegisterAccess.RW),		// Discontinuous mode on injected channels
				DISCEN = RegBitField!uint(11,1,RegisterAccess.RW),		// Discontinuous mode on regular channels
				JAUTO = RegBitField!uint(10,1,RegisterAccess.RW),		// Automatic injected group conversion
				AWDSGL = RegBitField!uint(9,1,RegisterAccess.RW),		// Enable the watchdog on a single channel in scan mode
				SCAN = RegBitField!uint(8,1,RegisterAccess.RW),		// Scan mode
				JEOCIE = RegBitField!uint(7,1,RegisterAccess.RW),		// Interrupt enable for injected channels
				AWDIE = RegBitField!uint(6,1,RegisterAccess.RW),		// Analog watchdog interrupt enable
				EOCIE = RegBitField!uint(5,1,RegisterAccess.RW),		// Interrupt enable for EOC
				AWDCH = RegBitField!uint(0,5,RegisterAccess.RW),		// Analog watchdog channel select bits
		}
		enum mask_CR2 {
				SWSTART = RegBitField!uint(30,1,RegisterAccess.RW),		// Start conversion of regular channels
				EXTEN = RegBitField!uint(28,2,RegisterAccess.RW),		// External trigger enable for regular channels
				EXTSEL = RegBitField!uint(24,4,RegisterAccess.RW),		// External event select for regular group
				JSWSTART = RegBitField!uint(22,1,RegisterAccess.RW),		// Start conversion of injected channels
				JEXTEN = RegBitField!uint(20,2,RegisterAccess.RW),		// External trigger enable for injected channels
				JEXTSEL = RegBitField!uint(16,4,RegisterAccess.RW),		// External event select for injected group
				ALIGN = RegBitField!uint(11,1,RegisterAccess.RW),		// Data alignment
				EOCS = RegBitField!uint(10,1,RegisterAccess.RW),		// End of conversion selection
				DDS = RegBitField!uint(9,1,RegisterAccess.RW),		// DMA disable selection (for single ADC mode)
				DMA = RegBitField!uint(8,1,RegisterAccess.RW),		// Direct memory access mode (for single ADC mode)
				CONT = RegBitField!uint(1,1,RegisterAccess.RW),		// Continuous conversion
				ADON = RegBitField!uint(0,1,RegisterAccess.RW),		// A/D Converter ON / OFF
		}
		enum mask_SMPR1 {
				SMPx_x = RegBitField!uint(0,32,RegisterAccess.RW),		// Sample time bits
		}
		enum mask_SMPR2 {
				SMPx_x = RegBitField!uint(0,32,RegisterAccess.RW),		// Sample time bits
		}
		enum mask_JOFR1 {
				JOFFSET1 = RegBitField!uint(0,12,RegisterAccess.RW),		// Data offset for injected channel x
		}
		enum mask_JOFR2 {
				JOFFSET2 = RegBitField!uint(0,12,RegisterAccess.RW),		// Data offset for injected channel x
		}
		enum mask_JOFR3 {
				JOFFSET3 = RegBitField!uint(0,12,RegisterAccess.RW),		// Data offset for injected channel x
		}
		enum mask_JOFR4 {
				JOFFSET4 = RegBitField!uint(0,12,RegisterAccess.RW),		// Data offset for injected channel x
		}
		enum mask_HTR {
				HT = RegBitField!uint(0,12,RegisterAccess.RW),		// Analog watchdog higher threshold
		}
		enum mask_LTR {
				LT = RegBitField!uint(0,12,RegisterAccess.RW),		// Analog watchdog lower threshold
		}
		enum mask_SQR1 {
				L = RegBitField!uint(20,4,RegisterAccess.RW),		// Regular channel sequence length
				SQ16 = RegBitField!uint(15,5,RegisterAccess.RW),		// 16th conversion in regular sequence
				SQ15 = RegBitField!uint(10,5,RegisterAccess.RW),		// 15th conversion in regular sequence
				SQ14 = RegBitField!uint(5,5,RegisterAccess.RW),		// 14th conversion in regular sequence
				SQ13 = RegBitField!uint(0,5,RegisterAccess.RW),		// 13th conversion in regular sequence
		}
		enum mask_SQR2 {
				SQ12 = RegBitField!uint(25,5,RegisterAccess.RW),		// 12th conversion in regular sequence
				SQ11 = RegBitField!uint(20,5,RegisterAccess.RW),		// 11th conversion in regular sequence
				SQ10 = RegBitField!uint(15,5,RegisterAccess.RW),		// 10th conversion in regular sequence
				SQ9 = RegBitField!uint(10,5,RegisterAccess.RW),		// 9th conversion in regular sequence
				SQ8 = RegBitField!uint(5,5,RegisterAccess.RW),		// 8th conversion in regular sequence
				SQ7 = RegBitField!uint(0,5,RegisterAccess.RW),		// 7th conversion in regular sequence
		}
		enum mask_SQR3 {
				SQ6 = RegBitField!uint(25,5,RegisterAccess.RW),		// 6th conversion in regular sequence
				SQ5 = RegBitField!uint(20,5,RegisterAccess.RW),		// 5th conversion in regular sequence
				SQ4 = RegBitField!uint(15,5,RegisterAccess.RW),		// 4th conversion in regular sequence
				SQ3 = RegBitField!uint(10,5,RegisterAccess.RW),		// 3rd conversion in regular sequence
				SQ2 = RegBitField!uint(5,5,RegisterAccess.RW),		// 2nd conversion in regular sequence
				SQ1 = RegBitField!uint(0,5,RegisterAccess.RW),		// 1st conversion in regular sequence
		}
		enum mask_JSQR {
				JL = RegBitField!uint(20,2,RegisterAccess.RW),		// Injected sequence length
				JSQ4 = RegBitField!uint(15,5,RegisterAccess.RW),		// 4th conversion in injected sequence
				JSQ3 = RegBitField!uint(10,5,RegisterAccess.RW),		// 3rd conversion in injected sequence
				JSQ2 = RegBitField!uint(5,5,RegisterAccess.RW),		// 2nd conversion in injected sequence
				JSQ1 = RegBitField!uint(0,5,RegisterAccess.RW),		// 1st conversion in injected sequence
		}
		enum mask_JDR1 {
				JDATA = RegBitField!uint(0,16,RegisterAccess.RO),		// Injected data
		}
		enum mask_JDR2 {
				JDATA = RegBitField!uint(0,16,RegisterAccess.RO),		// Injected data
		}
		enum mask_JDR3 {
				JDATA = RegBitField!uint(0,16,RegisterAccess.RO),		// Injected data
		}
		enum mask_JDR4 {
				JDATA = RegBitField!uint(0,16,RegisterAccess.RO),		// Injected data
		}
		enum mask_DR {
				DATA = RegBitField!uint(0,16,RegisterAccess.RO),		// Regular data
		}
	}
	// ADC common registers
	struct ADC_Common_Type {
		@RegInfo("CSR",0x0) RegVolatile!(mask_CSR,uint,0x40012300,0x0,RegisterAccess.RO) CSR;		 // offset:0x0 CSR ADC Common status register
		@RegInfo("CCR",0x4) RegVolatile!(mask_CCR,uint,0x40012304,0x0,RegisterAccess.RW) CCR;		 // offset:0x4 CCR ADC common control register
		enum mask_CSR {
				OVR3 = RegBitField!uint(21,1,RegisterAccess.RO),		// Overrun flag of ADC3
				STRT3 = RegBitField!uint(20,1,RegisterAccess.RO),		// Regular channel Start flag of ADC 3
				JSTRT3 = RegBitField!uint(19,1,RegisterAccess.RO),		// Injected channel Start flag of ADC 3
				JEOC3 = RegBitField!uint(18,1,RegisterAccess.RO),		// Injected channel end of conversion of ADC 3
				EOC3 = RegBitField!uint(17,1,RegisterAccess.RO),		// End of conversion of ADC 3
				AWD3 = RegBitField!uint(16,1,RegisterAccess.RO),		// Analog watchdog flag of ADC 3
				OVR2 = RegBitField!uint(13,1,RegisterAccess.RO),		// Overrun flag of ADC 2
				STRT2 = RegBitField!uint(12,1,RegisterAccess.RO),		// Regular channel Start flag of ADC 2
				JSTRT2 = RegBitField!uint(11,1,RegisterAccess.RO),		// Injected channel Start flag of ADC 2
				JEOC2 = RegBitField!uint(10,1,RegisterAccess.RO),		// Injected channel end of conversion of ADC 2
				EOC2 = RegBitField!uint(9,1,RegisterAccess.RO),		// End of conversion of ADC 2
				AWD2 = RegBitField!uint(8,1,RegisterAccess.RO),		// Analog watchdog flag of ADC 2
				OVR1 = RegBitField!uint(5,1,RegisterAccess.RO),		// Overrun flag of ADC 1
				STRT1 = RegBitField!uint(4,1,RegisterAccess.RO),		// Regular channel Start flag of ADC 1
				JSTRT1 = RegBitField!uint(3,1,RegisterAccess.RO),		// Injected channel Start flag of ADC 1
				JEOC1 = RegBitField!uint(2,1,RegisterAccess.RO),		// Injected channel end of conversion of ADC 1
				EOC1 = RegBitField!uint(1,1,RegisterAccess.RO),		// End of conversion of ADC 1
				AWD1 = RegBitField!uint(0,1,RegisterAccess.RO),		// Analog watchdog flag of ADC 1
		}
		enum mask_CCR {
				TSVREFE = RegBitField!uint(23,1,RegisterAccess.RW),		// Temperature sensor and VREFINT enable
				VBATE = RegBitField!uint(22,1,RegisterAccess.RW),		// VBAT enable
				ADCPRE = RegBitField!uint(16,2,RegisterAccess.RW),		// ADC prescaler
				DMA = RegBitField!uint(14,2,RegisterAccess.RW),		// Direct memory access mode for multi ADC mode
				DDS = RegBitField!uint(13,1,RegisterAccess.RW),		// DMA disable selection for multi-ADC mode
				DELAY = RegBitField!uint(8,4,RegisterAccess.RW),		// Delay between 2 sampling phases
		}
	}
	// PWR Group 
	// Power control
	struct PWR_Type {
		@RegInfo("CR",0x0) RegVolatile!(mask_CR,uint,0x40007000,0x0,RegisterAccess.RW) CR;		 // offset:0x0 CR power control register
		@RegInfo("CSR",0x4) RegVolatile!(mask_CSR,uint,0x40007004,0x0,RegisterAccess.NO) CSR;		 // offset:0x4 CSR power control/status register
		enum mask_CR {
				VOS = RegBitField!uint(14,2,RegisterAccess.RW),		// Regulator voltage scaling output selection
				ADCDC1 = RegBitField!uint(13,1,RegisterAccess.RW),		// ADCDC1
				FPDS = RegBitField!uint(9,1,RegisterAccess.RW),		// Flash power down in Stop mode
				DBP = RegBitField!uint(8,1,RegisterAccess.RW),		// Disable backup domain write protection
				PLS = RegBitField!uint(5,3,RegisterAccess.RW),		// PVD level selection
				PVDE = RegBitField!uint(4,1,RegisterAccess.RW),		// Power voltage detector enable
				CSBF = RegBitField!uint(3,1,RegisterAccess.RW),		// Clear standby flag
				CWUF = RegBitField!uint(2,1,RegisterAccess.RW),		// Clear wakeup flag
				PDDS = RegBitField!uint(1,1,RegisterAccess.RW),		// Power down deepsleep
				LPDS = RegBitField!uint(0,1,RegisterAccess.RW),		// Low-power deep sleep
		}
		enum mask_CSR {
				WUF = RegBitField!uint(0,1,RegisterAccess.RO),		// Wakeup flag
				SBF = RegBitField!uint(1,1,RegisterAccess.RO),		// Standby flag
				PVDO = RegBitField!uint(2,1,RegisterAccess.RO),		// PVD output
				BRR = RegBitField!uint(3,1,RegisterAccess.RO),		// Backup regulator ready
				EWUP = RegBitField!uint(8,1,RegisterAccess.RW),		// Enable WKUP pin
				BRE = RegBitField!uint(9,1,RegisterAccess.RW),		// Backup regulator enable
				VOSRDY = RegBitField!uint(14,1,RegisterAccess.RW),		// Regulator voltage scaling output selection ready bit
		}
	}
	// MPU Group 
	// Memory protection unit
	struct MPU_Type {
		@RegInfo("MPU_TYPER",0x0) RegVolatile!(mask_MPU_TYPER,uint,0xe000ed90,0x800,RegisterAccess.RO) MPU_TYPER;		 // offset:0x0 MPU_TYPER MPU type register
		@RegInfo("MPU_CTRL",0x4) RegVolatile!(mask_MPU_CTRL,uint,0xe000ed94,0x0,RegisterAccess.RO) MPU_CTRL;		 // offset:0x4 MPU_CTRL MPU control register
		@RegInfo("MPU_RNR",0x8) RegVolatile!(mask_MPU_RNR,uint,0xe000ed98,0x0,RegisterAccess.RW) MPU_RNR;		 // offset:0x8 MPU_RNR MPU region number register
		@RegInfo("MPU_RBAR",0xc) RegVolatile!(mask_MPU_RBAR,uint,0xe000ed9c,0x0,RegisterAccess.RW) MPU_RBAR;		 // offset:0x12 MPU_RBAR MPU region base address register
		@RegInfo("MPU_RASR",0x10) RegVolatile!(mask_MPU_RASR,uint,0xe000eda0,0x0,RegisterAccess.RW) MPU_RASR;		 // offset:0x16 MPU_RASR MPU region attribute and size register
		enum mask_MPU_TYPER {
				SEPARATE = RegBitField!uint(0,1,RegisterAccess.RO),		// Separate flag
				DREGION = RegBitField!uint(8,8,RegisterAccess.RO),		// Number of MPU data regions
				IREGION = RegBitField!uint(16,8,RegisterAccess.RO),		// Number of MPU instruction regions
		}
		enum mask_MPU_CTRL {
				ENABLE = RegBitField!uint(0,1,RegisterAccess.RO),		// Enables the MPU
				HFNMIENA = RegBitField!uint(1,1,RegisterAccess.RO),		// Enables the operation of MPU during hard fault
				PRIVDEFENA = RegBitField!uint(2,1,RegisterAccess.RO),		// Enable priviliged software access to default memory map
		}
		enum mask_MPU_RNR {
				REGION = RegBitField!uint(0,8,RegisterAccess.RW),		// MPU region
		}
		enum mask_MPU_RBAR {
				REGION = RegBitField!uint(0,4,RegisterAccess.RW),		// MPU region field
				VALID = RegBitField!uint(4,1,RegisterAccess.RW),		// MPU region number valid
				ADDR = RegBitField!uint(5,27,RegisterAccess.RW),		// Region base address field
		}
		enum mask_MPU_RASR {
				ENABLE = RegBitField!uint(0,1,RegisterAccess.RW),		// Region enable bit.
				SIZE = RegBitField!uint(1,5,RegisterAccess.RW),		// Size of the MPU protection region
				SRD = RegBitField!uint(8,8,RegisterAccess.RW),		// Subregion disable bits
				B = RegBitField!uint(16,1,RegisterAccess.RW),		// memory attribute
				C = RegBitField!uint(17,1,RegisterAccess.RW),		// memory attribute
				S = RegBitField!uint(18,1,RegisterAccess.RW),		// Shareable memory attribute
				TEX = RegBitField!uint(19,3,RegisterAccess.RW),		// memory attribute
				AP = RegBitField!uint(24,3,RegisterAccess.RW),		// Access permission
				XN = RegBitField!uint(28,1,RegisterAccess.RW),		// Instruction access disable bit
		}
	}
	// USB_OTG_FS Group 
	// USB on the go full speed
	struct OTG_FS_GLOBAL_Type {
		@RegInfo("FS_GOTGCTL",0x0) RegVolatile!(mask_FS_GOTGCTL,uint,0x50000000,0x800,RegisterAccess.NO) FS_GOTGCTL;		 // offset:0x0 FS_GOTGCTL OTG_FS control and status register (OTG_FS_GOTGCTL)
		@RegInfo("FS_GOTGINT",0x4) RegVolatile!(mask_FS_GOTGINT,uint,0x50000004,0x0,RegisterAccess.RW) FS_GOTGINT;		 // offset:0x4 FS_GOTGINT OTG_FS interrupt register (OTG_FS_GOTGINT)
		@RegInfo("FS_GAHBCFG",0x8) RegVolatile!(mask_FS_GAHBCFG,uint,0x50000008,0x0,RegisterAccess.RW) FS_GAHBCFG;		 // offset:0x8 FS_GAHBCFG OTG_FS AHB configuration register (OTG_FS_GAHBCFG)
		@RegInfo("FS_GUSBCFG",0xc) RegVolatile!(mask_FS_GUSBCFG,uint,0x5000000c,0xa00,RegisterAccess.NO) FS_GUSBCFG;		 // offset:0x12 FS_GUSBCFG OTG_FS USB configuration register (OTG_FS_GUSBCFG)
		@RegInfo("FS_GRSTCTL",0x10) RegVolatile!(mask_FS_GRSTCTL,uint,0x50000010,0x20000000,RegisterAccess.NO) FS_GRSTCTL;		 // offset:0x16 FS_GRSTCTL OTG_FS reset register (OTG_FS_GRSTCTL)
		@RegInfo("FS_GINTSTS",0x14) RegVolatile!(mask_FS_GINTSTS,uint,0x50000014,0x4000020,RegisterAccess.NO) FS_GINTSTS;		 // offset:0x20 FS_GINTSTS OTG_FS core interrupt register (OTG_FS_GINTSTS)
		@RegInfo("FS_GINTMSK",0x18) RegVolatile!(mask_FS_GINTMSK,uint,0x50000018,0x0,RegisterAccess.NO) FS_GINTMSK;		 // offset:0x24 FS_GINTMSK OTG_FS interrupt mask register (OTG_FS_GINTMSK)
		@RegInfo("FS_GRXSTSR_Device",0x1c) RegVolatile!(mask_FS_GRXSTSR_Device,uint,0x5000001c,0x0,RegisterAccess.RO) FS_GRXSTSR_Device;		 // offset:0x28 FS_GRXSTSR_Device OTG_FS Receive status debug read(Device mode)
		@RegInfo("FS_GRXSTSR_Host",0x1c) RegVolatile!(mask_FS_GRXSTSR_Host,uint,0x5000001c,0x0,RegisterAccess.RO) FS_GRXSTSR_Host;		 // offset:0x28 FS_GRXSTSR_Host OTG_FS Receive status debug read(Host mode)
		private uint[0x1] reserved9;	// skip 4 bytes
		@RegInfo("FS_GRXFSIZ",0x24) RegVolatile!(mask_FS_GRXFSIZ,uint,0x50000024,0x200,RegisterAccess.RW) FS_GRXFSIZ;		 // offset:0x36 FS_GRXFSIZ OTG_FS Receive FIFO size register (OTG_FS_GRXFSIZ)
		@RegInfo("FS_GNPTXFSIZ_Device",0x28) RegVolatile!(mask_FS_GNPTXFSIZ_Device,uint,0x50000028,0x200,RegisterAccess.RW) FS_GNPTXFSIZ_Device;		 // offset:0x40 FS_GNPTXFSIZ_Device OTG_FS non-periodic transmit FIFO size register (Device mode)
		@RegInfo("FS_GNPTXFSIZ_Host",0x28) RegVolatile!(mask_FS_GNPTXFSIZ_Host,uint,0x50000028,0x200,RegisterAccess.RW) FS_GNPTXFSIZ_Host;		 // offset:0x40 FS_GNPTXFSIZ_Host OTG_FS non-periodic transmit FIFO size register (Host mode)
		@RegInfo("FS_GNPTXSTS",0x2c) RegVolatile!(mask_FS_GNPTXSTS,uint,0x5000002c,0x80200,RegisterAccess.RO) FS_GNPTXSTS;		 // offset:0x44 FS_GNPTXSTS OTG_FS non-periodic transmit FIFO/queue status register (OTG_FS_GNPTXSTS)
		private uint[0x2] reserved13;	// skip 8 bytes
		@RegInfo("FS_GCCFG",0x38) RegVolatile!(mask_FS_GCCFG,uint,0x50000038,0x0,RegisterAccess.RW) FS_GCCFG;		 // offset:0x56 FS_GCCFG OTG_FS general core configuration register (OTG_FS_GCCFG)
		@RegInfo("FS_CID",0x3c) RegVolatile!(mask_FS_CID,uint,0x5000003c,0x1000,RegisterAccess.RW) FS_CID;		 // offset:0x60 FS_CID core ID register
		private uint[0x30] reserved15;	// skip 192 bytes
		@RegInfo("FS_HPTXFSIZ",0x100) RegVolatile!(mask_FS_HPTXFSIZ,uint,0x50000100,0x2000600,RegisterAccess.RW) FS_HPTXFSIZ;		 // offset:0x256 FS_HPTXFSIZ OTG_FS Host periodic transmit FIFO size register (OTG_FS_HPTXFSIZ)
		@RegInfo("FS_DIEPTXF1",0x104) RegVolatile!(mask_FS_DIEPTXF1,uint,0x50000104,0x2000400,RegisterAccess.RW) FS_DIEPTXF1;		 // offset:0x260 FS_DIEPTXF1 OTG_FS device IN endpoint transmit FIFO size register (OTG_FS_DIEPTXF2)
		@RegInfo("FS_DIEPTXF2",0x108) RegVolatile!(mask_FS_DIEPTXF2,uint,0x50000108,0x2000400,RegisterAccess.RW) FS_DIEPTXF2;		 // offset:0x264 FS_DIEPTXF2 OTG_FS device IN endpoint transmit FIFO size register (OTG_FS_DIEPTXF3)
		@RegInfo("FS_DIEPTXF3",0x10c) RegVolatile!(mask_FS_DIEPTXF3,uint,0x5000010c,0x2000400,RegisterAccess.RW) FS_DIEPTXF3;		 // offset:0x268 FS_DIEPTXF3 OTG_FS device IN endpoint transmit FIFO size register (OTG_FS_DIEPTXF4)
		enum mask_FS_GOTGCTL {
				SRQSCS = RegBitField!uint(0,1,RegisterAccess.RO),		// Session request success
				SRQ = RegBitField!uint(1,1,RegisterAccess.RW),		// Session request
				HNGSCS = RegBitField!uint(8,1,RegisterAccess.RO),		// Host negotiation success
				HNPRQ = RegBitField!uint(9,1,RegisterAccess.RW),		// HNP request
				HSHNPEN = RegBitField!uint(10,1,RegisterAccess.RW),		// Host set HNP enable
				DHNPEN = RegBitField!uint(11,1,RegisterAccess.RW),		// Device HNP enabled
				CIDSTS = RegBitField!uint(16,1,RegisterAccess.RO),		// Connector ID status
				DBCT = RegBitField!uint(17,1,RegisterAccess.RO),		// Long/short debounce time
				ASVLD = RegBitField!uint(18,1,RegisterAccess.RO),		// A-session valid
				BSVLD = RegBitField!uint(19,1,RegisterAccess.RO),		// B-session valid
		}
		enum mask_FS_GOTGINT {
				SEDET = RegBitField!uint(2,1,RegisterAccess.RW),		// Session end detected
				SRSSCHG = RegBitField!uint(8,1,RegisterAccess.RW),		// Session request success status change
				HNSSCHG = RegBitField!uint(9,1,RegisterAccess.RW),		// Host negotiation success status change
				HNGDET = RegBitField!uint(17,1,RegisterAccess.RW),		// Host negotiation detected
				ADTOCHG = RegBitField!uint(18,1,RegisterAccess.RW),		// A-device timeout change
				DBCDNE = RegBitField!uint(19,1,RegisterAccess.RW),		// Debounce done
		}
		enum mask_FS_GAHBCFG {
				GINT = RegBitField!uint(0,1,RegisterAccess.RW),		// Global interrupt mask
				TXFELVL = RegBitField!uint(7,1,RegisterAccess.RW),		// TxFIFO empty level
				PTXFELVL = RegBitField!uint(8,1,RegisterAccess.RW),		// Periodic TxFIFO empty level
		}
		enum mask_FS_GUSBCFG {
				TOCAL = RegBitField!uint(0,3,RegisterAccess.RW),		// FS timeout calibration
				PHYSEL = RegBitField!uint(6,1,RegisterAccess.WO),		// Full Speed serial transceiver select
				SRPCAP = RegBitField!uint(8,1,RegisterAccess.RW),		// SRP-capable
				HNPCAP = RegBitField!uint(9,1,RegisterAccess.RW),		// HNP-capable
				TRDT = RegBitField!uint(10,4,RegisterAccess.RW),		// USB turnaround time
				FHMOD = RegBitField!uint(29,1,RegisterAccess.RW),		// Force host mode
				FDMOD = RegBitField!uint(30,1,RegisterAccess.RW),		// Force device mode
				CTXPKT = RegBitField!uint(31,1,RegisterAccess.RW),		// Corrupt Tx packet
		}
		enum mask_FS_GRSTCTL {
				CSRST = RegBitField!uint(0,1,RegisterAccess.RW),		// Core soft reset
				HSRST = RegBitField!uint(1,1,RegisterAccess.RW),		// HCLK soft reset
				FCRST = RegBitField!uint(2,1,RegisterAccess.RW),		// Host frame counter reset
				RXFFLSH = RegBitField!uint(4,1,RegisterAccess.RW),		// RxFIFO flush
				TXFFLSH = RegBitField!uint(5,1,RegisterAccess.RW),		// TxFIFO flush
				TXFNUM = RegBitField!uint(6,5,RegisterAccess.RW),		// TxFIFO number
				AHBIDL = RegBitField!uint(31,1,RegisterAccess.RO),		// AHB master idle
		}
		enum mask_FS_GINTSTS {
				CMOD = RegBitField!uint(0,1,RegisterAccess.RO),		// Current mode of operation
				MMIS = RegBitField!uint(1,1,RegisterAccess.RW),		// Mode mismatch interrupt
				OTGINT = RegBitField!uint(2,1,RegisterAccess.RO),		// OTG interrupt
				SOF = RegBitField!uint(3,1,RegisterAccess.RW),		// Start of frame
				RXFLVL = RegBitField!uint(4,1,RegisterAccess.RO),		// RxFIFO non-empty
				NPTXFE = RegBitField!uint(5,1,RegisterAccess.RO),		// Non-periodic TxFIFO empty
				GINAKEFF = RegBitField!uint(6,1,RegisterAccess.RO),		// Global IN non-periodic NAK effective
				GOUTNAKEFF = RegBitField!uint(7,1,RegisterAccess.RO),		// Global OUT NAK effective
				ESUSP = RegBitField!uint(10,1,RegisterAccess.RW),		// Early suspend
				USBSUSP = RegBitField!uint(11,1,RegisterAccess.RW),		// USB suspend
				USBRST = RegBitField!uint(12,1,RegisterAccess.RW),		// USB reset
				ENUMDNE = RegBitField!uint(13,1,RegisterAccess.RW),		// Enumeration done
				ISOODRP = RegBitField!uint(14,1,RegisterAccess.RW),		// Isochronous OUT packet dropped interrupt
				EOPF = RegBitField!uint(15,1,RegisterAccess.RW),		// End of periodic frame interrupt
				IEPINT = RegBitField!uint(18,1,RegisterAccess.RO),		// IN endpoint interrupt
				OEPINT = RegBitField!uint(19,1,RegisterAccess.RO),		// OUT endpoint interrupt
				IISOIXFR = RegBitField!uint(20,1,RegisterAccess.RW),		// Incomplete isochronous IN transfer
				IPXFR_INCOMPISOOUT = RegBitField!uint(21,1,RegisterAccess.RW),		// Incomplete periodic transfer(Host mode)/Incomplete isochronous OUT transfer(Device mode)
				HPRTINT = RegBitField!uint(24,1,RegisterAccess.RO),		// Host port interrupt
				HCINT = RegBitField!uint(25,1,RegisterAccess.RO),		// Host channels interrupt
				PTXFE = RegBitField!uint(26,1,RegisterAccess.RO),		// Periodic TxFIFO empty
				CIDSCHG = RegBitField!uint(28,1,RegisterAccess.RW),		// Connector ID status change
				DISCINT = RegBitField!uint(29,1,RegisterAccess.RW),		// Disconnect detected interrupt
				SRQINT = RegBitField!uint(30,1,RegisterAccess.RW),		// Session request/new session detected interrupt
				WKUPINT = RegBitField!uint(31,1,RegisterAccess.RW),		// Resume/remote wakeup detected interrupt
		}
		enum mask_FS_GINTMSK {
				MMISM = RegBitField!uint(1,1,RegisterAccess.RW),		// Mode mismatch interrupt mask
				OTGINT = RegBitField!uint(2,1,RegisterAccess.RW),		// OTG interrupt mask
				SOFM = RegBitField!uint(3,1,RegisterAccess.RW),		// Start of frame mask
				RXFLVLM = RegBitField!uint(4,1,RegisterAccess.RW),		// Receive FIFO non-empty mask
				NPTXFEM = RegBitField!uint(5,1,RegisterAccess.RW),		// Non-periodic TxFIFO empty mask
				GINAKEFFM = RegBitField!uint(6,1,RegisterAccess.RW),		// Global non-periodic IN NAK effective mask
				GONAKEFFM = RegBitField!uint(7,1,RegisterAccess.RW),		// Global OUT NAK effective mask
				ESUSPM = RegBitField!uint(10,1,RegisterAccess.RW),		// Early suspend mask
				USBSUSPM = RegBitField!uint(11,1,RegisterAccess.RW),		// USB suspend mask
				USBRST = RegBitField!uint(12,1,RegisterAccess.RW),		// USB reset mask
				ENUMDNEM = RegBitField!uint(13,1,RegisterAccess.RW),		// Enumeration done mask
				ISOODRPM = RegBitField!uint(14,1,RegisterAccess.RW),		// Isochronous OUT packet dropped interrupt mask
				EOPFM = RegBitField!uint(15,1,RegisterAccess.RW),		// End of periodic frame interrupt mask
				EPMISM = RegBitField!uint(17,1,RegisterAccess.RW),		// Endpoint mismatch interrupt mask
				IEPINT = RegBitField!uint(18,1,RegisterAccess.RW),		// IN endpoints interrupt mask
				OEPINT = RegBitField!uint(19,1,RegisterAccess.RW),		// OUT endpoints interrupt mask
				IISOIXFRM = RegBitField!uint(20,1,RegisterAccess.RW),		// Incomplete isochronous IN transfer mask
				IPXFRM_IISOOXFRM = RegBitField!uint(21,1,RegisterAccess.RW),		// Incomplete periodic transfer mask(Host mode)/Incomplete isochronous OUT transfer mask(Device mode)
				PRTIM = RegBitField!uint(24,1,RegisterAccess.RO),		// Host port interrupt mask
				HCIM = RegBitField!uint(25,1,RegisterAccess.RW),		// Host channels interrupt mask
				PTXFEM = RegBitField!uint(26,1,RegisterAccess.RW),		// Periodic TxFIFO empty mask
				CIDSCHGM = RegBitField!uint(28,1,RegisterAccess.RW),		// Connector ID status change mask
				DISCINT = RegBitField!uint(29,1,RegisterAccess.RW),		// Disconnect detected interrupt mask
				SRQIM = RegBitField!uint(30,1,RegisterAccess.RW),		// Session request/new session detected interrupt mask
				WUIM = RegBitField!uint(31,1,RegisterAccess.RW),		// Resume/remote wakeup detected interrupt mask
		}
		enum mask_FS_GRXSTSR_Device {
				EPNUM = RegBitField!uint(0,4,RegisterAccess.RO),		// Endpoint number
				BCNT = RegBitField!uint(4,11,RegisterAccess.RO),		// Byte count
				DPID = RegBitField!uint(15,2,RegisterAccess.RO),		// Data PID
				PKTSTS = RegBitField!uint(17,4,RegisterAccess.RO),		// Packet status
				FRMNUM = RegBitField!uint(21,4,RegisterAccess.RO),		// Frame number
		}
		enum mask_FS_GRXSTSR_Host {
				EPNUM = RegBitField!uint(0,4,RegisterAccess.RO),		// Endpoint number
				BCNT = RegBitField!uint(4,11,RegisterAccess.RO),		// Byte count
				DPID = RegBitField!uint(15,2,RegisterAccess.RO),		// Data PID
				PKTSTS = RegBitField!uint(17,4,RegisterAccess.RO),		// Packet status
				FRMNUM = RegBitField!uint(21,4,RegisterAccess.RO),		// Frame number
		}
		enum mask_FS_GRXFSIZ {
				RXFD = RegBitField!uint(0,16,RegisterAccess.RW),		// RxFIFO depth
		}
		enum mask_FS_GNPTXFSIZ_Device {
				TX0FSA = RegBitField!uint(0,16,RegisterAccess.RW),		// Endpoint 0 transmit RAM start address
				TX0FD = RegBitField!uint(16,16,RegisterAccess.RW),		// Endpoint 0 TxFIFO depth
		}
		enum mask_FS_GNPTXFSIZ_Host {
				NPTXFSA = RegBitField!uint(0,16,RegisterAccess.RW),		// Non-periodic transmit RAM start address
				NPTXFD = RegBitField!uint(16,16,RegisterAccess.RW),		// Non-periodic TxFIFO depth
		}
		enum mask_FS_GNPTXSTS {
				NPTXFSAV = RegBitField!uint(0,16,RegisterAccess.RO),		// Non-periodic TxFIFO space available
				NPTQXSAV = RegBitField!uint(16,8,RegisterAccess.RO),		// Non-periodic transmit request queue space available
				NPTXQTOP = RegBitField!uint(24,7,RegisterAccess.RO),		// Top of the non-periodic transmit request queue
		}
		enum mask_FS_GCCFG {
				PWRDWN = RegBitField!uint(16,1,RegisterAccess.RW),		// Power down
				VBUSASEN = RegBitField!uint(18,1,RegisterAccess.RW),		// Enable the VBUS sensing device
				VBUSBSEN = RegBitField!uint(19,1,RegisterAccess.RW),		// Enable the VBUS sensing device
				SOFOUTEN = RegBitField!uint(20,1,RegisterAccess.RW),		// SOF output enable
		}
		enum mask_FS_CID {
				PRODUCT_ID = RegBitField!uint(0,32,RegisterAccess.RW),		// Product ID field
		}
		enum mask_FS_HPTXFSIZ {
				PTXSA = RegBitField!uint(0,16,RegisterAccess.RW),		// Host periodic TxFIFO start address
				PTXFSIZ = RegBitField!uint(16,16,RegisterAccess.RW),		// Host periodic TxFIFO depth
		}
		enum mask_FS_DIEPTXF1 {
				INEPTXSA = RegBitField!uint(0,16,RegisterAccess.RW),		// IN endpoint FIFO2 transmit RAM start address
				INEPTXFD = RegBitField!uint(16,16,RegisterAccess.RW),		// IN endpoint TxFIFO depth
		}
		enum mask_FS_DIEPTXF2 {
				INEPTXSA = RegBitField!uint(0,16,RegisterAccess.RW),		// IN endpoint FIFO3 transmit RAM start address
				INEPTXFD = RegBitField!uint(16,16,RegisterAccess.RW),		// IN endpoint TxFIFO depth
		}
		enum mask_FS_DIEPTXF3 {
				INEPTXSA = RegBitField!uint(0,16,RegisterAccess.RW),		// IN endpoint FIFO4 transmit RAM start address
				INEPTXFD = RegBitField!uint(16,16,RegisterAccess.RW),		// IN endpoint TxFIFO depth
		}
	}
	// USB on the go full speed
	struct OTG_FS_HOST_Type {
		@RegInfo("FS_HCFG",0x0) RegVolatile!(mask_FS_HCFG,uint,0x50000400,0x0,RegisterAccess.NO) FS_HCFG;		 // offset:0x0 FS_HCFG OTG_FS host configuration register (OTG_FS_HCFG)
		@RegInfo("HFIR",0x4) RegVolatile!(mask_HFIR,uint,0x50000404,0xea60,RegisterAccess.RW) HFIR;		 // offset:0x4 HFIR OTG_FS Host frame interval register
		@RegInfo("FS_HFNUM",0x8) RegVolatile!(mask_FS_HFNUM,uint,0x50000408,0x3fff,RegisterAccess.RO) FS_HFNUM;		 // offset:0x8 FS_HFNUM OTG_FS host frame number/frame time remaining register (OTG_FS_HFNUM)
		private uint[0x1] reserved3;	// skip 4 bytes
		@RegInfo("FS_HPTXSTS",0x10) RegVolatile!(mask_FS_HPTXSTS,uint,0x50000410,0x80100,RegisterAccess.NO) FS_HPTXSTS;		 // offset:0x16 FS_HPTXSTS OTG_FS_Host periodic transmit FIFO/queue status register (OTG_FS_HPTXSTS)
		@RegInfo("HAINT",0x14) RegVolatile!(mask_HAINT,uint,0x50000414,0x0,RegisterAccess.RO) HAINT;		 // offset:0x20 HAINT OTG_FS Host all channels interrupt register
		@RegInfo("HAINTMSK",0x18) RegVolatile!(mask_HAINTMSK,uint,0x50000418,0x0,RegisterAccess.RW) HAINTMSK;		 // offset:0x24 HAINTMSK OTG_FS host all channels interrupt mask register
		private uint[0x9] reserved6;	// skip 36 bytes
		@RegInfo("FS_HPRT",0x40) RegVolatile!(mask_FS_HPRT,uint,0x50000440,0x0,RegisterAccess.NO) FS_HPRT;		 // offset:0x64 FS_HPRT OTG_FS host port control and status register (OTG_FS_HPRT)
		private uint[0x2f] reserved7;	// skip 188 bytes
		@RegInfo("FS_HCCHAR0",0x100) RegVolatile!(mask_FS_HCCHAR0,uint,0x50000500,0x0,RegisterAccess.RW) FS_HCCHAR0;		 // offset:0x256 FS_HCCHAR0 OTG_FS host channel-0 characteristics register (OTG_FS_HCCHAR0)
		private uint[0x1] reserved8;	// skip 4 bytes
		@RegInfo("FS_HCINT0",0x108) RegVolatile!(mask_FS_HCINT0,uint,0x50000508,0x0,RegisterAccess.RW) FS_HCINT0;		 // offset:0x264 FS_HCINT0 OTG_FS host channel-0 interrupt register (OTG_FS_HCINT0)
		@RegInfo("FS_HCINTMSK0",0x10c) RegVolatile!(mask_FS_HCINTMSK0,uint,0x5000050c,0x0,RegisterAccess.RW) FS_HCINTMSK0;		 // offset:0x268 FS_HCINTMSK0 OTG_FS host channel-0 mask register (OTG_FS_HCINTMSK0)
		@RegInfo("FS_HCTSIZ0",0x110) RegVolatile!(mask_FS_HCTSIZ0,uint,0x50000510,0x0,RegisterAccess.RW) FS_HCTSIZ0;		 // offset:0x272 FS_HCTSIZ0 OTG_FS host channel-0 transfer size register
		private uint[0x3] reserved11;	// skip 12 bytes
		@RegInfo("FS_HCCHAR1",0x120) RegVolatile!(mask_FS_HCCHAR1,uint,0x50000520,0x0,RegisterAccess.RW) FS_HCCHAR1;		 // offset:0x288 FS_HCCHAR1 OTG_FS host channel-1 characteristics register (OTG_FS_HCCHAR1)
		private uint[0x1] reserved12;	// skip 4 bytes
		@RegInfo("FS_HCINT1",0x128) RegVolatile!(mask_FS_HCINT1,uint,0x50000528,0x0,RegisterAccess.RW) FS_HCINT1;		 // offset:0x296 FS_HCINT1 OTG_FS host channel-1 interrupt register (OTG_FS_HCINT1)
		@RegInfo("FS_HCINTMSK1",0x12c) RegVolatile!(mask_FS_HCINTMSK1,uint,0x5000052c,0x0,RegisterAccess.RW) FS_HCINTMSK1;		 // offset:0x300 FS_HCINTMSK1 OTG_FS host channel-1 mask register (OTG_FS_HCINTMSK1)
		@RegInfo("FS_HCTSIZ1",0x130) RegVolatile!(mask_FS_HCTSIZ1,uint,0x50000530,0x0,RegisterAccess.RW) FS_HCTSIZ1;		 // offset:0x304 FS_HCTSIZ1 OTG_FS host channel-1 transfer size register
		private uint[0x3] reserved15;	// skip 12 bytes
		@RegInfo("FS_HCCHAR2",0x140) RegVolatile!(mask_FS_HCCHAR2,uint,0x50000540,0x0,RegisterAccess.RW) FS_HCCHAR2;		 // offset:0x320 FS_HCCHAR2 OTG_FS host channel-2 characteristics register (OTG_FS_HCCHAR2)
		private uint[0x1] reserved16;	// skip 4 bytes
		@RegInfo("FS_HCINT2",0x148) RegVolatile!(mask_FS_HCINT2,uint,0x50000548,0x0,RegisterAccess.RW) FS_HCINT2;		 // offset:0x328 FS_HCINT2 OTG_FS host channel-2 interrupt register (OTG_FS_HCINT2)
		@RegInfo("FS_HCINTMSK2",0x14c) RegVolatile!(mask_FS_HCINTMSK2,uint,0x5000054c,0x0,RegisterAccess.RW) FS_HCINTMSK2;		 // offset:0x332 FS_HCINTMSK2 OTG_FS host channel-2 mask register (OTG_FS_HCINTMSK2)
		@RegInfo("FS_HCTSIZ2",0x150) RegVolatile!(mask_FS_HCTSIZ2,uint,0x50000550,0x0,RegisterAccess.RW) FS_HCTSIZ2;		 // offset:0x336 FS_HCTSIZ2 OTG_FS host channel-2 transfer size register
		private uint[0x3] reserved19;	// skip 12 bytes
		@RegInfo("FS_HCCHAR3",0x160) RegVolatile!(mask_FS_HCCHAR3,uint,0x50000560,0x0,RegisterAccess.RW) FS_HCCHAR3;		 // offset:0x352 FS_HCCHAR3 OTG_FS host channel-3 characteristics register (OTG_FS_HCCHAR3)
		private uint[0x1] reserved20;	// skip 4 bytes
		@RegInfo("FS_HCINT3",0x168) RegVolatile!(mask_FS_HCINT3,uint,0x50000568,0x0,RegisterAccess.RW) FS_HCINT3;		 // offset:0x360 FS_HCINT3 OTG_FS host channel-3 interrupt register (OTG_FS_HCINT3)
		@RegInfo("FS_HCINTMSK3",0x16c) RegVolatile!(mask_FS_HCINTMSK3,uint,0x5000056c,0x0,RegisterAccess.RW) FS_HCINTMSK3;		 // offset:0x364 FS_HCINTMSK3 OTG_FS host channel-3 mask register (OTG_FS_HCINTMSK3)
		@RegInfo("FS_HCTSIZ3",0x170) RegVolatile!(mask_FS_HCTSIZ3,uint,0x50000570,0x0,RegisterAccess.RW) FS_HCTSIZ3;		 // offset:0x368 FS_HCTSIZ3 OTG_FS host channel-3 transfer size register
		private uint[0x3] reserved23;	// skip 12 bytes
		@RegInfo("FS_HCCHAR4",0x180) RegVolatile!(mask_FS_HCCHAR4,uint,0x50000580,0x0,RegisterAccess.RW) FS_HCCHAR4;		 // offset:0x384 FS_HCCHAR4 OTG_FS host channel-4 characteristics register (OTG_FS_HCCHAR4)
		private uint[0x1] reserved24;	// skip 4 bytes
		@RegInfo("FS_HCINT4",0x188) RegVolatile!(mask_FS_HCINT4,uint,0x50000588,0x0,RegisterAccess.RW) FS_HCINT4;		 // offset:0x392 FS_HCINT4 OTG_FS host channel-4 interrupt register (OTG_FS_HCINT4)
		@RegInfo("FS_HCINTMSK4",0x18c) RegVolatile!(mask_FS_HCINTMSK4,uint,0x5000058c,0x0,RegisterAccess.RW) FS_HCINTMSK4;		 // offset:0x396 FS_HCINTMSK4 OTG_FS host channel-4 mask register (OTG_FS_HCINTMSK4)
		@RegInfo("FS_HCTSIZ4",0x190) RegVolatile!(mask_FS_HCTSIZ4,uint,0x50000590,0x0,RegisterAccess.RW) FS_HCTSIZ4;		 // offset:0x400 FS_HCTSIZ4 OTG_FS host channel-x transfer size register
		private uint[0x3] reserved27;	// skip 12 bytes
		@RegInfo("FS_HCCHAR5",0x1a0) RegVolatile!(mask_FS_HCCHAR5,uint,0x500005a0,0x0,RegisterAccess.RW) FS_HCCHAR5;		 // offset:0x416 FS_HCCHAR5 OTG_FS host channel-5 characteristics register (OTG_FS_HCCHAR5)
		private uint[0x1] reserved28;	// skip 4 bytes
		@RegInfo("FS_HCINT5",0x1a8) RegVolatile!(mask_FS_HCINT5,uint,0x500005a8,0x0,RegisterAccess.RW) FS_HCINT5;		 // offset:0x424 FS_HCINT5 OTG_FS host channel-5 interrupt register (OTG_FS_HCINT5)
		@RegInfo("FS_HCINTMSK5",0x1ac) RegVolatile!(mask_FS_HCINTMSK5,uint,0x500005ac,0x0,RegisterAccess.RW) FS_HCINTMSK5;		 // offset:0x428 FS_HCINTMSK5 OTG_FS host channel-5 mask register (OTG_FS_HCINTMSK5)
		@RegInfo("FS_HCTSIZ5",0x1b0) RegVolatile!(mask_FS_HCTSIZ5,uint,0x500005b0,0x0,RegisterAccess.RW) FS_HCTSIZ5;		 // offset:0x432 FS_HCTSIZ5 OTG_FS host channel-5 transfer size register
		private uint[0x3] reserved31;	// skip 12 bytes
		@RegInfo("FS_HCCHAR6",0x1c0) RegVolatile!(mask_FS_HCCHAR6,uint,0x500005c0,0x0,RegisterAccess.RW) FS_HCCHAR6;		 // offset:0x448 FS_HCCHAR6 OTG_FS host channel-6 characteristics register (OTG_FS_HCCHAR6)
		private uint[0x1] reserved32;	// skip 4 bytes
		@RegInfo("FS_HCINT6",0x1c8) RegVolatile!(mask_FS_HCINT6,uint,0x500005c8,0x0,RegisterAccess.RW) FS_HCINT6;		 // offset:0x456 FS_HCINT6 OTG_FS host channel-6 interrupt register (OTG_FS_HCINT6)
		@RegInfo("FS_HCINTMSK6",0x1cc) RegVolatile!(mask_FS_HCINTMSK6,uint,0x500005cc,0x0,RegisterAccess.RW) FS_HCINTMSK6;		 // offset:0x460 FS_HCINTMSK6 OTG_FS host channel-6 mask register (OTG_FS_HCINTMSK6)
		@RegInfo("FS_HCTSIZ6",0x1d0) RegVolatile!(mask_FS_HCTSIZ6,uint,0x500005d0,0x0,RegisterAccess.RW) FS_HCTSIZ6;		 // offset:0x464 FS_HCTSIZ6 OTG_FS host channel-6 transfer size register
		private uint[0x3] reserved35;	// skip 12 bytes
		@RegInfo("FS_HCCHAR7",0x1e0) RegVolatile!(mask_FS_HCCHAR7,uint,0x500005e0,0x0,RegisterAccess.RW) FS_HCCHAR7;		 // offset:0x480 FS_HCCHAR7 OTG_FS host channel-7 characteristics register (OTG_FS_HCCHAR7)
		private uint[0x1] reserved36;	// skip 4 bytes
		@RegInfo("FS_HCINT7",0x1e8) RegVolatile!(mask_FS_HCINT7,uint,0x500005e8,0x0,RegisterAccess.RW) FS_HCINT7;		 // offset:0x488 FS_HCINT7 OTG_FS host channel-7 interrupt register (OTG_FS_HCINT7)
		@RegInfo("FS_HCINTMSK7",0x1ec) RegVolatile!(mask_FS_HCINTMSK7,uint,0x500005ec,0x0,RegisterAccess.RW) FS_HCINTMSK7;		 // offset:0x492 FS_HCINTMSK7 OTG_FS host channel-7 mask register (OTG_FS_HCINTMSK7)
		@RegInfo("FS_HCTSIZ7",0x1f0) RegVolatile!(mask_FS_HCTSIZ7,uint,0x500005f0,0x0,RegisterAccess.RW) FS_HCTSIZ7;		 // offset:0x496 FS_HCTSIZ7 OTG_FS host channel-7 transfer size register
		enum mask_FS_HCFG {
				FSLSPCS = RegBitField!uint(0,2,RegisterAccess.RW),		// FS/LS PHY clock select
				FSLSS = RegBitField!uint(2,1,RegisterAccess.RO),		// FS- and LS-only support
		}
		enum mask_HFIR {
				FRIVL = RegBitField!uint(0,16,RegisterAccess.RW),		// Frame interval
		}
		enum mask_FS_HFNUM {
				FRNUM = RegBitField!uint(0,16,RegisterAccess.RO),		// Frame number
				FTREM = RegBitField!uint(16,16,RegisterAccess.RO),		// Frame time remaining
		}
		enum mask_FS_HPTXSTS {
				PTXFSAVL = RegBitField!uint(0,16,RegisterAccess.RW),		// Periodic transmit data FIFO space available
				PTXQSAV = RegBitField!uint(16,8,RegisterAccess.RO),		// Periodic transmit request queue space available
				PTXQTOP = RegBitField!uint(24,8,RegisterAccess.RO),		// Top of the periodic transmit request queue
		}
		enum mask_HAINT {
				HAINT = RegBitField!uint(0,16,RegisterAccess.RO),		// Channel interrupts
		}
		enum mask_HAINTMSK {
				HAINTM = RegBitField!uint(0,16,RegisterAccess.RW),		// Channel interrupt mask
		}
		enum mask_FS_HPRT {
				PCSTS = RegBitField!uint(0,1,RegisterAccess.RO),		// Port connect status
				PCDET = RegBitField!uint(1,1,RegisterAccess.RW),		// Port connect detected
				PENA = RegBitField!uint(2,1,RegisterAccess.RW),		// Port enable
				PENCHNG = RegBitField!uint(3,1,RegisterAccess.RW),		// Port enable/disable change
				POCA = RegBitField!uint(4,1,RegisterAccess.RO),		// Port overcurrent active
				POCCHNG = RegBitField!uint(5,1,RegisterAccess.RW),		// Port overcurrent change
				PRES = RegBitField!uint(6,1,RegisterAccess.RW),		// Port resume
				PSUSP = RegBitField!uint(7,1,RegisterAccess.RW),		// Port suspend
				PRST = RegBitField!uint(8,1,RegisterAccess.RW),		// Port reset
				PLSTS = RegBitField!uint(10,2,RegisterAccess.RO),		// Port line status
				PPWR = RegBitField!uint(12,1,RegisterAccess.RW),		// Port power
				PTCTL = RegBitField!uint(13,4,RegisterAccess.RW),		// Port test control
				PSPD = RegBitField!uint(17,2,RegisterAccess.RO),		// Port speed
		}
		enum mask_FS_HCCHAR0 {
				MPSIZ = RegBitField!uint(0,11,RegisterAccess.RW),		// Maximum packet size
				EPNUM = RegBitField!uint(11,4,RegisterAccess.RW),		// Endpoint number
				EPDIR = RegBitField!uint(15,1,RegisterAccess.RW),		// Endpoint direction
				LSDEV = RegBitField!uint(17,1,RegisterAccess.RW),		// Low-speed device
				EPTYP = RegBitField!uint(18,2,RegisterAccess.RW),		// Endpoint type
				MCNT = RegBitField!uint(20,2,RegisterAccess.RW),		// Multicount
				DAD = RegBitField!uint(22,7,RegisterAccess.RW),		// Device address
				ODDFRM = RegBitField!uint(29,1,RegisterAccess.RW),		// Odd frame
				CHDIS = RegBitField!uint(30,1,RegisterAccess.RW),		// Channel disable
				CHENA = RegBitField!uint(31,1,RegisterAccess.RW),		// Channel enable
		}
		enum mask_FS_HCINT0 {
				XFRC = RegBitField!uint(0,1,RegisterAccess.RW),		// Transfer completed
				CHH = RegBitField!uint(1,1,RegisterAccess.RW),		// Channel halted
				STALL = RegBitField!uint(3,1,RegisterAccess.RW),		// STALL response received interrupt
				NAK = RegBitField!uint(4,1,RegisterAccess.RW),		// NAK response received interrupt
				ACK = RegBitField!uint(5,1,RegisterAccess.RW),		// ACK response received/transmitted interrupt
				TXERR = RegBitField!uint(7,1,RegisterAccess.RW),		// Transaction error
				BBERR = RegBitField!uint(8,1,RegisterAccess.RW),		// Babble error
				FRMOR = RegBitField!uint(9,1,RegisterAccess.RW),		// Frame overrun
				DTERR = RegBitField!uint(10,1,RegisterAccess.RW),		// Data toggle error
		}
		enum mask_FS_HCINTMSK0 {
				XFRCM = RegBitField!uint(0,1,RegisterAccess.RW),		// Transfer completed mask
				CHHM = RegBitField!uint(1,1,RegisterAccess.RW),		// Channel halted mask
				STALLM = RegBitField!uint(3,1,RegisterAccess.RW),		// STALL response received interrupt mask
				NAKM = RegBitField!uint(4,1,RegisterAccess.RW),		// NAK response received interrupt mask
				ACKM = RegBitField!uint(5,1,RegisterAccess.RW),		// ACK response received/transmitted interrupt mask
				NYET = RegBitField!uint(6,1,RegisterAccess.RW),		// response received interrupt mask
				TXERRM = RegBitField!uint(7,1,RegisterAccess.RW),		// Transaction error mask
				BBERRM = RegBitField!uint(8,1,RegisterAccess.RW),		// Babble error mask
				FRMORM = RegBitField!uint(9,1,RegisterAccess.RW),		// Frame overrun mask
				DTERRM = RegBitField!uint(10,1,RegisterAccess.RW),		// Data toggle error mask
		}
		enum mask_FS_HCTSIZ0 {
				XFRSIZ = RegBitField!uint(0,19,RegisterAccess.RW),		// Transfer size
				PKTCNT = RegBitField!uint(19,10,RegisterAccess.RW),		// Packet count
				DPID = RegBitField!uint(29,2,RegisterAccess.RW),		// Data PID
		}
		enum mask_FS_HCCHAR1 {
				MPSIZ = RegBitField!uint(0,11,RegisterAccess.RW),		// Maximum packet size
				EPNUM = RegBitField!uint(11,4,RegisterAccess.RW),		// Endpoint number
				EPDIR = RegBitField!uint(15,1,RegisterAccess.RW),		// Endpoint direction
				LSDEV = RegBitField!uint(17,1,RegisterAccess.RW),		// Low-speed device
				EPTYP = RegBitField!uint(18,2,RegisterAccess.RW),		// Endpoint type
				MCNT = RegBitField!uint(20,2,RegisterAccess.RW),		// Multicount
				DAD = RegBitField!uint(22,7,RegisterAccess.RW),		// Device address
				ODDFRM = RegBitField!uint(29,1,RegisterAccess.RW),		// Odd frame
				CHDIS = RegBitField!uint(30,1,RegisterAccess.RW),		// Channel disable
				CHENA = RegBitField!uint(31,1,RegisterAccess.RW),		// Channel enable
		}
		enum mask_FS_HCINT1 {
				XFRC = RegBitField!uint(0,1,RegisterAccess.RW),		// Transfer completed
				CHH = RegBitField!uint(1,1,RegisterAccess.RW),		// Channel halted
				STALL = RegBitField!uint(3,1,RegisterAccess.RW),		// STALL response received interrupt
				NAK = RegBitField!uint(4,1,RegisterAccess.RW),		// NAK response received interrupt
				ACK = RegBitField!uint(5,1,RegisterAccess.RW),		// ACK response received/transmitted interrupt
				TXERR = RegBitField!uint(7,1,RegisterAccess.RW),		// Transaction error
				BBERR = RegBitField!uint(8,1,RegisterAccess.RW),		// Babble error
				FRMOR = RegBitField!uint(9,1,RegisterAccess.RW),		// Frame overrun
				DTERR = RegBitField!uint(10,1,RegisterAccess.RW),		// Data toggle error
		}
		enum mask_FS_HCINTMSK1 {
				XFRCM = RegBitField!uint(0,1,RegisterAccess.RW),		// Transfer completed mask
				CHHM = RegBitField!uint(1,1,RegisterAccess.RW),		// Channel halted mask
				STALLM = RegBitField!uint(3,1,RegisterAccess.RW),		// STALL response received interrupt mask
				NAKM = RegBitField!uint(4,1,RegisterAccess.RW),		// NAK response received interrupt mask
				ACKM = RegBitField!uint(5,1,RegisterAccess.RW),		// ACK response received/transmitted interrupt mask
				NYET = RegBitField!uint(6,1,RegisterAccess.RW),		// response received interrupt mask
				TXERRM = RegBitField!uint(7,1,RegisterAccess.RW),		// Transaction error mask
				BBERRM = RegBitField!uint(8,1,RegisterAccess.RW),		// Babble error mask
				FRMORM = RegBitField!uint(9,1,RegisterAccess.RW),		// Frame overrun mask
				DTERRM = RegBitField!uint(10,1,RegisterAccess.RW),		// Data toggle error mask
		}
		enum mask_FS_HCTSIZ1 {
				XFRSIZ = RegBitField!uint(0,19,RegisterAccess.RW),		// Transfer size
				PKTCNT = RegBitField!uint(19,10,RegisterAccess.RW),		// Packet count
				DPID = RegBitField!uint(29,2,RegisterAccess.RW),		// Data PID
		}
		enum mask_FS_HCCHAR2 {
				MPSIZ = RegBitField!uint(0,11,RegisterAccess.RW),		// Maximum packet size
				EPNUM = RegBitField!uint(11,4,RegisterAccess.RW),		// Endpoint number
				EPDIR = RegBitField!uint(15,1,RegisterAccess.RW),		// Endpoint direction
				LSDEV = RegBitField!uint(17,1,RegisterAccess.RW),		// Low-speed device
				EPTYP = RegBitField!uint(18,2,RegisterAccess.RW),		// Endpoint type
				MCNT = RegBitField!uint(20,2,RegisterAccess.RW),		// Multicount
				DAD = RegBitField!uint(22,7,RegisterAccess.RW),		// Device address
				ODDFRM = RegBitField!uint(29,1,RegisterAccess.RW),		// Odd frame
				CHDIS = RegBitField!uint(30,1,RegisterAccess.RW),		// Channel disable
				CHENA = RegBitField!uint(31,1,RegisterAccess.RW),		// Channel enable
		}
		enum mask_FS_HCINT2 {
				XFRC = RegBitField!uint(0,1,RegisterAccess.RW),		// Transfer completed
				CHH = RegBitField!uint(1,1,RegisterAccess.RW),		// Channel halted
				STALL = RegBitField!uint(3,1,RegisterAccess.RW),		// STALL response received interrupt
				NAK = RegBitField!uint(4,1,RegisterAccess.RW),		// NAK response received interrupt
				ACK = RegBitField!uint(5,1,RegisterAccess.RW),		// ACK response received/transmitted interrupt
				TXERR = RegBitField!uint(7,1,RegisterAccess.RW),		// Transaction error
				BBERR = RegBitField!uint(8,1,RegisterAccess.RW),		// Babble error
				FRMOR = RegBitField!uint(9,1,RegisterAccess.RW),		// Frame overrun
				DTERR = RegBitField!uint(10,1,RegisterAccess.RW),		// Data toggle error
		}
		enum mask_FS_HCINTMSK2 {
				XFRCM = RegBitField!uint(0,1,RegisterAccess.RW),		// Transfer completed mask
				CHHM = RegBitField!uint(1,1,RegisterAccess.RW),		// Channel halted mask
				STALLM = RegBitField!uint(3,1,RegisterAccess.RW),		// STALL response received interrupt mask
				NAKM = RegBitField!uint(4,1,RegisterAccess.RW),		// NAK response received interrupt mask
				ACKM = RegBitField!uint(5,1,RegisterAccess.RW),		// ACK response received/transmitted interrupt mask
				NYET = RegBitField!uint(6,1,RegisterAccess.RW),		// response received interrupt mask
				TXERRM = RegBitField!uint(7,1,RegisterAccess.RW),		// Transaction error mask
				BBERRM = RegBitField!uint(8,1,RegisterAccess.RW),		// Babble error mask
				FRMORM = RegBitField!uint(9,1,RegisterAccess.RW),		// Frame overrun mask
				DTERRM = RegBitField!uint(10,1,RegisterAccess.RW),		// Data toggle error mask
		}
		enum mask_FS_HCTSIZ2 {
				XFRSIZ = RegBitField!uint(0,19,RegisterAccess.RW),		// Transfer size
				PKTCNT = RegBitField!uint(19,10,RegisterAccess.RW),		// Packet count
				DPID = RegBitField!uint(29,2,RegisterAccess.RW),		// Data PID
		}
		enum mask_FS_HCCHAR3 {
				MPSIZ = RegBitField!uint(0,11,RegisterAccess.RW),		// Maximum packet size
				EPNUM = RegBitField!uint(11,4,RegisterAccess.RW),		// Endpoint number
				EPDIR = RegBitField!uint(15,1,RegisterAccess.RW),		// Endpoint direction
				LSDEV = RegBitField!uint(17,1,RegisterAccess.RW),		// Low-speed device
				EPTYP = RegBitField!uint(18,2,RegisterAccess.RW),		// Endpoint type
				MCNT = RegBitField!uint(20,2,RegisterAccess.RW),		// Multicount
				DAD = RegBitField!uint(22,7,RegisterAccess.RW),		// Device address
				ODDFRM = RegBitField!uint(29,1,RegisterAccess.RW),		// Odd frame
				CHDIS = RegBitField!uint(30,1,RegisterAccess.RW),		// Channel disable
				CHENA = RegBitField!uint(31,1,RegisterAccess.RW),		// Channel enable
		}
		enum mask_FS_HCINT3 {
				XFRC = RegBitField!uint(0,1,RegisterAccess.RW),		// Transfer completed
				CHH = RegBitField!uint(1,1,RegisterAccess.RW),		// Channel halted
				STALL = RegBitField!uint(3,1,RegisterAccess.RW),		// STALL response received interrupt
				NAK = RegBitField!uint(4,1,RegisterAccess.RW),		// NAK response received interrupt
				ACK = RegBitField!uint(5,1,RegisterAccess.RW),		// ACK response received/transmitted interrupt
				TXERR = RegBitField!uint(7,1,RegisterAccess.RW),		// Transaction error
				BBERR = RegBitField!uint(8,1,RegisterAccess.RW),		// Babble error
				FRMOR = RegBitField!uint(9,1,RegisterAccess.RW),		// Frame overrun
				DTERR = RegBitField!uint(10,1,RegisterAccess.RW),		// Data toggle error
		}
		enum mask_FS_HCINTMSK3 {
				XFRCM = RegBitField!uint(0,1,RegisterAccess.RW),		// Transfer completed mask
				CHHM = RegBitField!uint(1,1,RegisterAccess.RW),		// Channel halted mask
				STALLM = RegBitField!uint(3,1,RegisterAccess.RW),		// STALL response received interrupt mask
				NAKM = RegBitField!uint(4,1,RegisterAccess.RW),		// NAK response received interrupt mask
				ACKM = RegBitField!uint(5,1,RegisterAccess.RW),		// ACK response received/transmitted interrupt mask
				NYET = RegBitField!uint(6,1,RegisterAccess.RW),		// response received interrupt mask
				TXERRM = RegBitField!uint(7,1,RegisterAccess.RW),		// Transaction error mask
				BBERRM = RegBitField!uint(8,1,RegisterAccess.RW),		// Babble error mask
				FRMORM = RegBitField!uint(9,1,RegisterAccess.RW),		// Frame overrun mask
				DTERRM = RegBitField!uint(10,1,RegisterAccess.RW),		// Data toggle error mask
		}
		enum mask_FS_HCTSIZ3 {
				XFRSIZ = RegBitField!uint(0,19,RegisterAccess.RW),		// Transfer size
				PKTCNT = RegBitField!uint(19,10,RegisterAccess.RW),		// Packet count
				DPID = RegBitField!uint(29,2,RegisterAccess.RW),		// Data PID
		}
		enum mask_FS_HCCHAR4 {
				MPSIZ = RegBitField!uint(0,11,RegisterAccess.RW),		// Maximum packet size
				EPNUM = RegBitField!uint(11,4,RegisterAccess.RW),		// Endpoint number
				EPDIR = RegBitField!uint(15,1,RegisterAccess.RW),		// Endpoint direction
				LSDEV = RegBitField!uint(17,1,RegisterAccess.RW),		// Low-speed device
				EPTYP = RegBitField!uint(18,2,RegisterAccess.RW),		// Endpoint type
				MCNT = RegBitField!uint(20,2,RegisterAccess.RW),		// Multicount
				DAD = RegBitField!uint(22,7,RegisterAccess.RW),		// Device address
				ODDFRM = RegBitField!uint(29,1,RegisterAccess.RW),		// Odd frame
				CHDIS = RegBitField!uint(30,1,RegisterAccess.RW),		// Channel disable
				CHENA = RegBitField!uint(31,1,RegisterAccess.RW),		// Channel enable
		}
		enum mask_FS_HCINT4 {
				XFRC = RegBitField!uint(0,1,RegisterAccess.RW),		// Transfer completed
				CHH = RegBitField!uint(1,1,RegisterAccess.RW),		// Channel halted
				STALL = RegBitField!uint(3,1,RegisterAccess.RW),		// STALL response received interrupt
				NAK = RegBitField!uint(4,1,RegisterAccess.RW),		// NAK response received interrupt
				ACK = RegBitField!uint(5,1,RegisterAccess.RW),		// ACK response received/transmitted interrupt
				TXERR = RegBitField!uint(7,1,RegisterAccess.RW),		// Transaction error
				BBERR = RegBitField!uint(8,1,RegisterAccess.RW),		// Babble error
				FRMOR = RegBitField!uint(9,1,RegisterAccess.RW),		// Frame overrun
				DTERR = RegBitField!uint(10,1,RegisterAccess.RW),		// Data toggle error
		}
		enum mask_FS_HCINTMSK4 {
				XFRCM = RegBitField!uint(0,1,RegisterAccess.RW),		// Transfer completed mask
				CHHM = RegBitField!uint(1,1,RegisterAccess.RW),		// Channel halted mask
				STALLM = RegBitField!uint(3,1,RegisterAccess.RW),		// STALL response received interrupt mask
				NAKM = RegBitField!uint(4,1,RegisterAccess.RW),		// NAK response received interrupt mask
				ACKM = RegBitField!uint(5,1,RegisterAccess.RW),		// ACK response received/transmitted interrupt mask
				NYET = RegBitField!uint(6,1,RegisterAccess.RW),		// response received interrupt mask
				TXERRM = RegBitField!uint(7,1,RegisterAccess.RW),		// Transaction error mask
				BBERRM = RegBitField!uint(8,1,RegisterAccess.RW),		// Babble error mask
				FRMORM = RegBitField!uint(9,1,RegisterAccess.RW),		// Frame overrun mask
				DTERRM = RegBitField!uint(10,1,RegisterAccess.RW),		// Data toggle error mask
		}
		enum mask_FS_HCTSIZ4 {
				XFRSIZ = RegBitField!uint(0,19,RegisterAccess.RW),		// Transfer size
				PKTCNT = RegBitField!uint(19,10,RegisterAccess.RW),		// Packet count
				DPID = RegBitField!uint(29,2,RegisterAccess.RW),		// Data PID
		}
		enum mask_FS_HCCHAR5 {
				MPSIZ = RegBitField!uint(0,11,RegisterAccess.RW),		// Maximum packet size
				EPNUM = RegBitField!uint(11,4,RegisterAccess.RW),		// Endpoint number
				EPDIR = RegBitField!uint(15,1,RegisterAccess.RW),		// Endpoint direction
				LSDEV = RegBitField!uint(17,1,RegisterAccess.RW),		// Low-speed device
				EPTYP = RegBitField!uint(18,2,RegisterAccess.RW),		// Endpoint type
				MCNT = RegBitField!uint(20,2,RegisterAccess.RW),		// Multicount
				DAD = RegBitField!uint(22,7,RegisterAccess.RW),		// Device address
				ODDFRM = RegBitField!uint(29,1,RegisterAccess.RW),		// Odd frame
				CHDIS = RegBitField!uint(30,1,RegisterAccess.RW),		// Channel disable
				CHENA = RegBitField!uint(31,1,RegisterAccess.RW),		// Channel enable
		}
		enum mask_FS_HCINT5 {
				XFRC = RegBitField!uint(0,1,RegisterAccess.RW),		// Transfer completed
				CHH = RegBitField!uint(1,1,RegisterAccess.RW),		// Channel halted
				STALL = RegBitField!uint(3,1,RegisterAccess.RW),		// STALL response received interrupt
				NAK = RegBitField!uint(4,1,RegisterAccess.RW),		// NAK response received interrupt
				ACK = RegBitField!uint(5,1,RegisterAccess.RW),		// ACK response received/transmitted interrupt
				TXERR = RegBitField!uint(7,1,RegisterAccess.RW),		// Transaction error
				BBERR = RegBitField!uint(8,1,RegisterAccess.RW),		// Babble error
				FRMOR = RegBitField!uint(9,1,RegisterAccess.RW),		// Frame overrun
				DTERR = RegBitField!uint(10,1,RegisterAccess.RW),		// Data toggle error
		}
		enum mask_FS_HCINTMSK5 {
				XFRCM = RegBitField!uint(0,1,RegisterAccess.RW),		// Transfer completed mask
				CHHM = RegBitField!uint(1,1,RegisterAccess.RW),		// Channel halted mask
				STALLM = RegBitField!uint(3,1,RegisterAccess.RW),		// STALL response received interrupt mask
				NAKM = RegBitField!uint(4,1,RegisterAccess.RW),		// NAK response received interrupt mask
				ACKM = RegBitField!uint(5,1,RegisterAccess.RW),		// ACK response received/transmitted interrupt mask
				NYET = RegBitField!uint(6,1,RegisterAccess.RW),		// response received interrupt mask
				TXERRM = RegBitField!uint(7,1,RegisterAccess.RW),		// Transaction error mask
				BBERRM = RegBitField!uint(8,1,RegisterAccess.RW),		// Babble error mask
				FRMORM = RegBitField!uint(9,1,RegisterAccess.RW),		// Frame overrun mask
				DTERRM = RegBitField!uint(10,1,RegisterAccess.RW),		// Data toggle error mask
		}
		enum mask_FS_HCTSIZ5 {
				XFRSIZ = RegBitField!uint(0,19,RegisterAccess.RW),		// Transfer size
				PKTCNT = RegBitField!uint(19,10,RegisterAccess.RW),		// Packet count
				DPID = RegBitField!uint(29,2,RegisterAccess.RW),		// Data PID
		}
		enum mask_FS_HCCHAR6 {
				MPSIZ = RegBitField!uint(0,11,RegisterAccess.RW),		// Maximum packet size
				EPNUM = RegBitField!uint(11,4,RegisterAccess.RW),		// Endpoint number
				EPDIR = RegBitField!uint(15,1,RegisterAccess.RW),		// Endpoint direction
				LSDEV = RegBitField!uint(17,1,RegisterAccess.RW),		// Low-speed device
				EPTYP = RegBitField!uint(18,2,RegisterAccess.RW),		// Endpoint type
				MCNT = RegBitField!uint(20,2,RegisterAccess.RW),		// Multicount
				DAD = RegBitField!uint(22,7,RegisterAccess.RW),		// Device address
				ODDFRM = RegBitField!uint(29,1,RegisterAccess.RW),		// Odd frame
				CHDIS = RegBitField!uint(30,1,RegisterAccess.RW),		// Channel disable
				CHENA = RegBitField!uint(31,1,RegisterAccess.RW),		// Channel enable
		}
		enum mask_FS_HCINT6 {
				XFRC = RegBitField!uint(0,1,RegisterAccess.RW),		// Transfer completed
				CHH = RegBitField!uint(1,1,RegisterAccess.RW),		// Channel halted
				STALL = RegBitField!uint(3,1,RegisterAccess.RW),		// STALL response received interrupt
				NAK = RegBitField!uint(4,1,RegisterAccess.RW),		// NAK response received interrupt
				ACK = RegBitField!uint(5,1,RegisterAccess.RW),		// ACK response received/transmitted interrupt
				TXERR = RegBitField!uint(7,1,RegisterAccess.RW),		// Transaction error
				BBERR = RegBitField!uint(8,1,RegisterAccess.RW),		// Babble error
				FRMOR = RegBitField!uint(9,1,RegisterAccess.RW),		// Frame overrun
				DTERR = RegBitField!uint(10,1,RegisterAccess.RW),		// Data toggle error
		}
		enum mask_FS_HCINTMSK6 {
				XFRCM = RegBitField!uint(0,1,RegisterAccess.RW),		// Transfer completed mask
				CHHM = RegBitField!uint(1,1,RegisterAccess.RW),		// Channel halted mask
				STALLM = RegBitField!uint(3,1,RegisterAccess.RW),		// STALL response received interrupt mask
				NAKM = RegBitField!uint(4,1,RegisterAccess.RW),		// NAK response received interrupt mask
				ACKM = RegBitField!uint(5,1,RegisterAccess.RW),		// ACK response received/transmitted interrupt mask
				NYET = RegBitField!uint(6,1,RegisterAccess.RW),		// response received interrupt mask
				TXERRM = RegBitField!uint(7,1,RegisterAccess.RW),		// Transaction error mask
				BBERRM = RegBitField!uint(8,1,RegisterAccess.RW),		// Babble error mask
				FRMORM = RegBitField!uint(9,1,RegisterAccess.RW),		// Frame overrun mask
				DTERRM = RegBitField!uint(10,1,RegisterAccess.RW),		// Data toggle error mask
		}
		enum mask_FS_HCTSIZ6 {
				XFRSIZ = RegBitField!uint(0,19,RegisterAccess.RW),		// Transfer size
				PKTCNT = RegBitField!uint(19,10,RegisterAccess.RW),		// Packet count
				DPID = RegBitField!uint(29,2,RegisterAccess.RW),		// Data PID
		}
		enum mask_FS_HCCHAR7 {
				MPSIZ = RegBitField!uint(0,11,RegisterAccess.RW),		// Maximum packet size
				EPNUM = RegBitField!uint(11,4,RegisterAccess.RW),		// Endpoint number
				EPDIR = RegBitField!uint(15,1,RegisterAccess.RW),		// Endpoint direction
				LSDEV = RegBitField!uint(17,1,RegisterAccess.RW),		// Low-speed device
				EPTYP = RegBitField!uint(18,2,RegisterAccess.RW),		// Endpoint type
				MCNT = RegBitField!uint(20,2,RegisterAccess.RW),		// Multicount
				DAD = RegBitField!uint(22,7,RegisterAccess.RW),		// Device address
				ODDFRM = RegBitField!uint(29,1,RegisterAccess.RW),		// Odd frame
				CHDIS = RegBitField!uint(30,1,RegisterAccess.RW),		// Channel disable
				CHENA = RegBitField!uint(31,1,RegisterAccess.RW),		// Channel enable
		}
		enum mask_FS_HCINT7 {
				XFRC = RegBitField!uint(0,1,RegisterAccess.RW),		// Transfer completed
				CHH = RegBitField!uint(1,1,RegisterAccess.RW),		// Channel halted
				STALL = RegBitField!uint(3,1,RegisterAccess.RW),		// STALL response received interrupt
				NAK = RegBitField!uint(4,1,RegisterAccess.RW),		// NAK response received interrupt
				ACK = RegBitField!uint(5,1,RegisterAccess.RW),		// ACK response received/transmitted interrupt
				TXERR = RegBitField!uint(7,1,RegisterAccess.RW),		// Transaction error
				BBERR = RegBitField!uint(8,1,RegisterAccess.RW),		// Babble error
				FRMOR = RegBitField!uint(9,1,RegisterAccess.RW),		// Frame overrun
				DTERR = RegBitField!uint(10,1,RegisterAccess.RW),		// Data toggle error
		}
		enum mask_FS_HCINTMSK7 {
				XFRCM = RegBitField!uint(0,1,RegisterAccess.RW),		// Transfer completed mask
				CHHM = RegBitField!uint(1,1,RegisterAccess.RW),		// Channel halted mask
				STALLM = RegBitField!uint(3,1,RegisterAccess.RW),		// STALL response received interrupt mask
				NAKM = RegBitField!uint(4,1,RegisterAccess.RW),		// NAK response received interrupt mask
				ACKM = RegBitField!uint(5,1,RegisterAccess.RW),		// ACK response received/transmitted interrupt mask
				NYET = RegBitField!uint(6,1,RegisterAccess.RW),		// response received interrupt mask
				TXERRM = RegBitField!uint(7,1,RegisterAccess.RW),		// Transaction error mask
				BBERRM = RegBitField!uint(8,1,RegisterAccess.RW),		// Babble error mask
				FRMORM = RegBitField!uint(9,1,RegisterAccess.RW),		// Frame overrun mask
				DTERRM = RegBitField!uint(10,1,RegisterAccess.RW),		// Data toggle error mask
		}
		enum mask_FS_HCTSIZ7 {
				XFRSIZ = RegBitField!uint(0,19,RegisterAccess.RW),		// Transfer size
				PKTCNT = RegBitField!uint(19,10,RegisterAccess.RW),		// Packet count
				DPID = RegBitField!uint(29,2,RegisterAccess.RW),		// Data PID
		}
	}
	// USB on the go full speed
	struct OTG_FS_DEVICE_Type {
		@RegInfo("FS_DCFG",0x0) RegVolatile!(mask_FS_DCFG,uint,0x50000800,0x2200000,RegisterAccess.RW) FS_DCFG;		 // offset:0x0 FS_DCFG OTG_FS device configuration register (OTG_FS_DCFG)
		@RegInfo("FS_DCTL",0x4) RegVolatile!(mask_FS_DCTL,uint,0x50000804,0x0,RegisterAccess.NO) FS_DCTL;		 // offset:0x4 FS_DCTL OTG_FS device control register (OTG_FS_DCTL)
		@RegInfo("FS_DSTS",0x8) RegVolatile!(mask_FS_DSTS,uint,0x50000808,0x10,RegisterAccess.RO) FS_DSTS;		 // offset:0x8 FS_DSTS OTG_FS device status register (OTG_FS_DSTS)
		private uint[0x1] reserved3;	// skip 4 bytes
		@RegInfo("FS_DIEPMSK",0x10) RegVolatile!(mask_FS_DIEPMSK,uint,0x50000810,0x0,RegisterAccess.RW) FS_DIEPMSK;		 // offset:0x16 FS_DIEPMSK OTG_FS device IN endpoint common interrupt mask register (OTG_FS_DIEPMSK)
		@RegInfo("FS_DOEPMSK",0x14) RegVolatile!(mask_FS_DOEPMSK,uint,0x50000814,0x0,RegisterAccess.RW) FS_DOEPMSK;		 // offset:0x20 FS_DOEPMSK OTG_FS device OUT endpoint common interrupt mask register (OTG_FS_DOEPMSK)
		@RegInfo("FS_DAINT",0x18) RegVolatile!(mask_FS_DAINT,uint,0x50000818,0x0,RegisterAccess.RO) FS_DAINT;		 // offset:0x24 FS_DAINT OTG_FS device all endpoints interrupt register (OTG_FS_DAINT)
		@RegInfo("FS_DAINTMSK",0x1c) RegVolatile!(mask_FS_DAINTMSK,uint,0x5000081c,0x0,RegisterAccess.RW) FS_DAINTMSK;		 // offset:0x28 FS_DAINTMSK OTG_FS all endpoints interrupt mask register (OTG_FS_DAINTMSK)
		private uint[0x2] reserved7;	// skip 8 bytes
		@RegInfo("DVBUSDIS",0x28) RegVolatile!(mask_DVBUSDIS,uint,0x50000828,0x17d7,RegisterAccess.RW) DVBUSDIS;		 // offset:0x40 DVBUSDIS OTG_FS device VBUS discharge time register
		@RegInfo("DVBUSPULSE",0x2c) RegVolatile!(mask_DVBUSPULSE,uint,0x5000082c,0x5b8,RegisterAccess.RW) DVBUSPULSE;		 // offset:0x44 DVBUSPULSE OTG_FS device VBUS pulsing time register
		private uint[0x1] reserved9;	// skip 4 bytes
		@RegInfo("DIEPEMPMSK",0x34) RegVolatile!(mask_DIEPEMPMSK,uint,0x50000834,0x0,RegisterAccess.RW) DIEPEMPMSK;		 // offset:0x52 DIEPEMPMSK OTG_FS device IN endpoint FIFO empty interrupt mask register
		private uint[0x32] reserved10;	// skip 200 bytes
		@RegInfo("FS_DIEPCTL0",0x100) RegVolatile!(mask_FS_DIEPCTL0,uint,0x50000900,0x0,RegisterAccess.NO) FS_DIEPCTL0;		 // offset:0x256 FS_DIEPCTL0 OTG_FS device control IN endpoint 0 control register (OTG_FS_DIEPCTL0)
		private uint[0x1] reserved11;	// skip 4 bytes
		@RegInfo("DIEPINT0",0x108) RegVolatile!(mask_DIEPINT0,uint,0x50000908,0x80,RegisterAccess.NO) DIEPINT0;		 // offset:0x264 DIEPINT0 device endpoint-x interrupt register
		private uint[0x1] reserved12;	// skip 4 bytes
		@RegInfo("DIEPTSIZ0",0x110) RegVolatile!(mask_DIEPTSIZ0,uint,0x50000910,0x0,RegisterAccess.RW) DIEPTSIZ0;		 // offset:0x272 DIEPTSIZ0 device endpoint-0 transfer size register
		private uint[0x1] reserved13;	// skip 4 bytes
		@RegInfo("DTXFSTS0",0x118) RegVolatile!(mask_DTXFSTS0,uint,0x50000918,0x0,RegisterAccess.RO) DTXFSTS0;		 // offset:0x280 DTXFSTS0 OTG_FS device IN endpoint transmit FIFO status register
		private uint[0x1] reserved14;	// skip 4 bytes
		@RegInfo("DIEPCTL1",0x120) RegVolatile!(mask_DIEPCTL1,uint,0x50000920,0x0,RegisterAccess.NO) DIEPCTL1;		 // offset:0x288 DIEPCTL1 OTG device endpoint-1 control register
		private uint[0x1] reserved15;	// skip 4 bytes
		@RegInfo("DIEPINT1",0x128) RegVolatile!(mask_DIEPINT1,uint,0x50000928,0x80,RegisterAccess.NO) DIEPINT1;		 // offset:0x296 DIEPINT1 device endpoint-1 interrupt register
		private uint[0x1] reserved16;	// skip 4 bytes
		@RegInfo("DIEPTSIZ1",0x130) RegVolatile!(mask_DIEPTSIZ1,uint,0x50000930,0x0,RegisterAccess.RW) DIEPTSIZ1;		 // offset:0x304 DIEPTSIZ1 device endpoint-1 transfer size register
		private uint[0x1] reserved17;	// skip 4 bytes
		@RegInfo("DTXFSTS1",0x138) RegVolatile!(mask_DTXFSTS1,uint,0x50000938,0x0,RegisterAccess.RO) DTXFSTS1;		 // offset:0x312 DTXFSTS1 OTG_FS device IN endpoint transmit FIFO status register
		private uint[0x1] reserved18;	// skip 4 bytes
		@RegInfo("DIEPCTL2",0x140) RegVolatile!(mask_DIEPCTL2,uint,0x50000940,0x0,RegisterAccess.NO) DIEPCTL2;		 // offset:0x320 DIEPCTL2 OTG device endpoint-2 control register
		private uint[0x1] reserved19;	// skip 4 bytes
		@RegInfo("DIEPINT2",0x148) RegVolatile!(mask_DIEPINT2,uint,0x50000948,0x80,RegisterAccess.NO) DIEPINT2;		 // offset:0x328 DIEPINT2 device endpoint-2 interrupt register
		private uint[0x1] reserved20;	// skip 4 bytes
		@RegInfo("DIEPTSIZ2",0x150) RegVolatile!(mask_DIEPTSIZ2,uint,0x50000950,0x0,RegisterAccess.RW) DIEPTSIZ2;		 // offset:0x336 DIEPTSIZ2 device endpoint-2 transfer size register
		private uint[0x1] reserved21;	// skip 4 bytes
		@RegInfo("DTXFSTS2",0x158) RegVolatile!(mask_DTXFSTS2,uint,0x50000958,0x0,RegisterAccess.RO) DTXFSTS2;		 // offset:0x344 DTXFSTS2 OTG_FS device IN endpoint transmit FIFO status register
		private uint[0x1] reserved22;	// skip 4 bytes
		@RegInfo("DIEPCTL3",0x160) RegVolatile!(mask_DIEPCTL3,uint,0x50000960,0x0,RegisterAccess.NO) DIEPCTL3;		 // offset:0x352 DIEPCTL3 OTG device endpoint-3 control register
		private uint[0x1] reserved23;	// skip 4 bytes
		@RegInfo("DIEPINT3",0x168) RegVolatile!(mask_DIEPINT3,uint,0x50000968,0x80,RegisterAccess.NO) DIEPINT3;		 // offset:0x360 DIEPINT3 device endpoint-3 interrupt register
		private uint[0x1] reserved24;	// skip 4 bytes
		@RegInfo("DIEPTSIZ3",0x170) RegVolatile!(mask_DIEPTSIZ3,uint,0x50000970,0x0,RegisterAccess.RW) DIEPTSIZ3;		 // offset:0x368 DIEPTSIZ3 device endpoint-3 transfer size register
		private uint[0x1] reserved25;	// skip 4 bytes
		@RegInfo("DTXFSTS3",0x178) RegVolatile!(mask_DTXFSTS3,uint,0x50000978,0x0,RegisterAccess.RO) DTXFSTS3;		 // offset:0x376 DTXFSTS3 OTG_FS device IN endpoint transmit FIFO status register
		private uint[0x61] reserved26;	// skip 388 bytes
		@RegInfo("DOEPCTL0",0x300) RegVolatile!(mask_DOEPCTL0,uint,0x50000b00,0x8000,RegisterAccess.NO) DOEPCTL0;		 // offset:0x768 DOEPCTL0 device endpoint-0 control register
		private uint[0x1] reserved27;	// skip 4 bytes
		@RegInfo("DOEPINT0",0x308) RegVolatile!(mask_DOEPINT0,uint,0x50000b08,0x80,RegisterAccess.RW) DOEPINT0;		 // offset:0x776 DOEPINT0 device endpoint-0 interrupt register
		private uint[0x1] reserved28;	// skip 4 bytes
		@RegInfo("DOEPTSIZ0",0x310) RegVolatile!(mask_DOEPTSIZ0,uint,0x50000b10,0x0,RegisterAccess.RW) DOEPTSIZ0;		 // offset:0x784 DOEPTSIZ0 device OUT endpoint-0 transfer size register
		private uint[0x3] reserved29;	// skip 12 bytes
		@RegInfo("DOEPCTL1",0x320) RegVolatile!(mask_DOEPCTL1,uint,0x50000b20,0x0,RegisterAccess.NO) DOEPCTL1;		 // offset:0x800 DOEPCTL1 device endpoint-1 control register
		private uint[0x1] reserved30;	// skip 4 bytes
		@RegInfo("DOEPINT1",0x328) RegVolatile!(mask_DOEPINT1,uint,0x50000b28,0x80,RegisterAccess.RW) DOEPINT1;		 // offset:0x808 DOEPINT1 device endpoint-1 interrupt register
		private uint[0x1] reserved31;	// skip 4 bytes
		@RegInfo("DOEPTSIZ1",0x330) RegVolatile!(mask_DOEPTSIZ1,uint,0x50000b30,0x0,RegisterAccess.RW) DOEPTSIZ1;		 // offset:0x816 DOEPTSIZ1 device OUT endpoint-1 transfer size register
		private uint[0x3] reserved32;	// skip 12 bytes
		@RegInfo("DOEPCTL2",0x340) RegVolatile!(mask_DOEPCTL2,uint,0x50000b40,0x0,RegisterAccess.NO) DOEPCTL2;		 // offset:0x832 DOEPCTL2 device endpoint-2 control register
		private uint[0x1] reserved33;	// skip 4 bytes
		@RegInfo("DOEPINT2",0x348) RegVolatile!(mask_DOEPINT2,uint,0x50000b48,0x80,RegisterAccess.RW) DOEPINT2;		 // offset:0x840 DOEPINT2 device endpoint-2 interrupt register
		private uint[0x1] reserved34;	// skip 4 bytes
		@RegInfo("DOEPTSIZ2",0x350) RegVolatile!(mask_DOEPTSIZ2,uint,0x50000b50,0x0,RegisterAccess.RW) DOEPTSIZ2;		 // offset:0x848 DOEPTSIZ2 device OUT endpoint-2 transfer size register
		private uint[0x3] reserved35;	// skip 12 bytes
		@RegInfo("DOEPCTL3",0x360) RegVolatile!(mask_DOEPCTL3,uint,0x50000b60,0x0,RegisterAccess.NO) DOEPCTL3;		 // offset:0x864 DOEPCTL3 device endpoint-3 control register
		private uint[0x1] reserved36;	// skip 4 bytes
		@RegInfo("DOEPINT3",0x368) RegVolatile!(mask_DOEPINT3,uint,0x50000b68,0x80,RegisterAccess.RW) DOEPINT3;		 // offset:0x872 DOEPINT3 device endpoint-3 interrupt register
		private uint[0x1] reserved37;	// skip 4 bytes
		@RegInfo("DOEPTSIZ3",0x370) RegVolatile!(mask_DOEPTSIZ3,uint,0x50000b70,0x0,RegisterAccess.RW) DOEPTSIZ3;		 // offset:0x880 DOEPTSIZ3 device OUT endpoint-3 transfer size register
		enum mask_FS_DCFG {
				DSPD = RegBitField!uint(0,2,RegisterAccess.RW),		// Device speed
				NZLSOHSK = RegBitField!uint(2,1,RegisterAccess.RW),		// Non-zero-length status OUT handshake
				DAD = RegBitField!uint(4,7,RegisterAccess.RW),		// Device address
				PFIVL = RegBitField!uint(11,2,RegisterAccess.RW),		// Periodic frame interval
		}
		enum mask_FS_DCTL {
				RWUSIG = RegBitField!uint(0,1,RegisterAccess.RW),		// Remote wakeup signaling
				SDIS = RegBitField!uint(1,1,RegisterAccess.RW),		// Soft disconnect
				GINSTS = RegBitField!uint(2,1,RegisterAccess.RO),		// Global IN NAK status
				GONSTS = RegBitField!uint(3,1,RegisterAccess.RO),		// Global OUT NAK status
				TCTL = RegBitField!uint(4,3,RegisterAccess.RW),		// Test control
				SGINAK = RegBitField!uint(7,1,RegisterAccess.RW),		// Set global IN NAK
				CGINAK = RegBitField!uint(8,1,RegisterAccess.RW),		// Clear global IN NAK
				SGONAK = RegBitField!uint(9,1,RegisterAccess.RW),		// Set global OUT NAK
				CGONAK = RegBitField!uint(10,1,RegisterAccess.RW),		// Clear global OUT NAK
				POPRGDNE = RegBitField!uint(11,1,RegisterAccess.RW),		// Power-on programming done
		}
		enum mask_FS_DSTS {
				SUSPSTS = RegBitField!uint(0,1,RegisterAccess.RO),		// Suspend status
				ENUMSPD = RegBitField!uint(1,2,RegisterAccess.RO),		// Enumerated speed
				EERR = RegBitField!uint(3,1,RegisterAccess.RO),		// Erratic error
				FNSOF = RegBitField!uint(8,14,RegisterAccess.RO),		// Frame number of the received SOF
		}
		enum mask_FS_DIEPMSK {
				XFRCM = RegBitField!uint(0,1,RegisterAccess.RW),		// Transfer completed interrupt mask
				EPDM = RegBitField!uint(1,1,RegisterAccess.RW),		// Endpoint disabled interrupt mask
				TOM = RegBitField!uint(3,1,RegisterAccess.RW),		// Timeout condition mask (Non-isochronous endpoints)
				ITTXFEMSK = RegBitField!uint(4,1,RegisterAccess.RW),		// IN token received when TxFIFO empty mask
				INEPNMM = RegBitField!uint(5,1,RegisterAccess.RW),		// IN token received with EP mismatch mask
				INEPNEM = RegBitField!uint(6,1,RegisterAccess.RW),		// IN endpoint NAK effective mask
		}
		enum mask_FS_DOEPMSK {
				XFRCM = RegBitField!uint(0,1,RegisterAccess.RW),		// Transfer completed interrupt mask
				EPDM = RegBitField!uint(1,1,RegisterAccess.RW),		// Endpoint disabled interrupt mask
				STUPM = RegBitField!uint(3,1,RegisterAccess.RW),		// SETUP phase done mask
				OTEPDM = RegBitField!uint(4,1,RegisterAccess.RW),		// OUT token received when endpoint disabled mask
		}
		enum mask_FS_DAINT {
				IEPINT = RegBitField!uint(0,16,RegisterAccess.RO),		// IN endpoint interrupt bits
				OEPINT = RegBitField!uint(16,16,RegisterAccess.RO),		// OUT endpoint interrupt bits
		}
		enum mask_FS_DAINTMSK {
				IEPM = RegBitField!uint(0,16,RegisterAccess.RW),		// IN EP interrupt mask bits
				OEPINT = RegBitField!uint(16,16,RegisterAccess.RW),		// OUT endpoint interrupt bits
		}
		enum mask_DVBUSDIS {
				VBUSDT = RegBitField!uint(0,16,RegisterAccess.RW),		// Device VBUS discharge time
		}
		enum mask_DVBUSPULSE {
				DVBUSP = RegBitField!uint(0,12,RegisterAccess.RW),		// Device VBUS pulsing time
		}
		enum mask_DIEPEMPMSK {
				INEPTXFEM = RegBitField!uint(0,16,RegisterAccess.RW),		// IN EP Tx FIFO empty interrupt mask bits
		}
		enum mask_FS_DIEPCTL0 {
				MPSIZ = RegBitField!uint(0,2,RegisterAccess.RW),		// Maximum packet size
				USBAEP = RegBitField!uint(15,1,RegisterAccess.RO),		// USB active endpoint
				NAKSTS = RegBitField!uint(17,1,RegisterAccess.RO),		// NAK status
				EPTYP = RegBitField!uint(18,2,RegisterAccess.RO),		// Endpoint type
				STALL = RegBitField!uint(21,1,RegisterAccess.RW),		// STALL handshake
				TXFNUM = RegBitField!uint(22,4,RegisterAccess.RW),		// TxFIFO number
				CNAK = RegBitField!uint(26,1,RegisterAccess.WO),		// Clear NAK
				SNAK = RegBitField!uint(27,1,RegisterAccess.WO),		// Set NAK
				EPDIS = RegBitField!uint(30,1,RegisterAccess.RO),		// Endpoint disable
				EPENA = RegBitField!uint(31,1,RegisterAccess.RO),		// Endpoint enable
		}
		enum mask_DIEPINT0 {
				TXFE = RegBitField!uint(7,1,RegisterAccess.RO),		// TXFE
				INEPNE = RegBitField!uint(6,1,RegisterAccess.RW),		// INEPNE
				ITTXFE = RegBitField!uint(4,1,RegisterAccess.RW),		// ITTXFE
				TOC = RegBitField!uint(3,1,RegisterAccess.RW),		// TOC
				EPDISD = RegBitField!uint(1,1,RegisterAccess.RW),		// EPDISD
				XFRC = RegBitField!uint(0,1,RegisterAccess.RW),		// XFRC
		}
		enum mask_DIEPTSIZ0 {
				PKTCNT = RegBitField!uint(19,2,RegisterAccess.RW),		// Packet count
				XFRSIZ = RegBitField!uint(0,7,RegisterAccess.RW),		// Transfer size
		}
		enum mask_DTXFSTS0 {
				INEPTFSAV = RegBitField!uint(0,16,RegisterAccess.RO),		// IN endpoint TxFIFO space available
		}
		enum mask_DIEPCTL1 {
				EPENA = RegBitField!uint(31,1,RegisterAccess.RW),		// EPENA
				EPDIS = RegBitField!uint(30,1,RegisterAccess.RW),		// EPDIS
				SODDFRM_SD1PID = RegBitField!uint(29,1,RegisterAccess.WO),		// SODDFRM/SD1PID
				SD0PID_SEVNFRM = RegBitField!uint(28,1,RegisterAccess.WO),		// SD0PID/SEVNFRM
				SNAK = RegBitField!uint(27,1,RegisterAccess.WO),		// SNAK
				CNAK = RegBitField!uint(26,1,RegisterAccess.WO),		// CNAK
				TXFNUM = RegBitField!uint(22,4,RegisterAccess.RW),		// TXFNUM
				Stall = RegBitField!uint(21,1,RegisterAccess.RW),		// Stall
				EPTYP = RegBitField!uint(18,2,RegisterAccess.RW),		// EPTYP
				NAKSTS = RegBitField!uint(17,1,RegisterAccess.RO),		// NAKSTS
				EONUM_DPID = RegBitField!uint(16,1,RegisterAccess.RO),		// EONUM/DPID
				USBAEP = RegBitField!uint(15,1,RegisterAccess.RW),		// USBAEP
				MPSIZ = RegBitField!uint(0,11,RegisterAccess.RW),		// MPSIZ
		}
		enum mask_DIEPINT1 {
				TXFE = RegBitField!uint(7,1,RegisterAccess.RO),		// TXFE
				INEPNE = RegBitField!uint(6,1,RegisterAccess.RW),		// INEPNE
				ITTXFE = RegBitField!uint(4,1,RegisterAccess.RW),		// ITTXFE
				TOC = RegBitField!uint(3,1,RegisterAccess.RW),		// TOC
				EPDISD = RegBitField!uint(1,1,RegisterAccess.RW),		// EPDISD
				XFRC = RegBitField!uint(0,1,RegisterAccess.RW),		// XFRC
		}
		enum mask_DIEPTSIZ1 {
				MCNT = RegBitField!uint(29,2,RegisterAccess.RW),		// Multi count
				PKTCNT = RegBitField!uint(19,10,RegisterAccess.RW),		// Packet count
				XFRSIZ = RegBitField!uint(0,19,RegisterAccess.RW),		// Transfer size
		}
		enum mask_DTXFSTS1 {
				INEPTFSAV = RegBitField!uint(0,16,RegisterAccess.RO),		// IN endpoint TxFIFO space available
		}
		enum mask_DIEPCTL2 {
				EPENA = RegBitField!uint(31,1,RegisterAccess.RW),		// EPENA
				EPDIS = RegBitField!uint(30,1,RegisterAccess.RW),		// EPDIS
				SODDFRM = RegBitField!uint(29,1,RegisterAccess.WO),		// SODDFRM
				SD0PID_SEVNFRM = RegBitField!uint(28,1,RegisterAccess.WO),		// SD0PID/SEVNFRM
				SNAK = RegBitField!uint(27,1,RegisterAccess.WO),		// SNAK
				CNAK = RegBitField!uint(26,1,RegisterAccess.WO),		// CNAK
				TXFNUM = RegBitField!uint(22,4,RegisterAccess.RW),		// TXFNUM
				Stall = RegBitField!uint(21,1,RegisterAccess.RW),		// Stall
				EPTYP = RegBitField!uint(18,2,RegisterAccess.RW),		// EPTYP
				NAKSTS = RegBitField!uint(17,1,RegisterAccess.RO),		// NAKSTS
				EONUM_DPID = RegBitField!uint(16,1,RegisterAccess.RO),		// EONUM/DPID
				USBAEP = RegBitField!uint(15,1,RegisterAccess.RW),		// USBAEP
				MPSIZ = RegBitField!uint(0,11,RegisterAccess.RW),		// MPSIZ
		}
		enum mask_DIEPINT2 {
				TXFE = RegBitField!uint(7,1,RegisterAccess.RO),		// TXFE
				INEPNE = RegBitField!uint(6,1,RegisterAccess.RW),		// INEPNE
				ITTXFE = RegBitField!uint(4,1,RegisterAccess.RW),		// ITTXFE
				TOC = RegBitField!uint(3,1,RegisterAccess.RW),		// TOC
				EPDISD = RegBitField!uint(1,1,RegisterAccess.RW),		// EPDISD
				XFRC = RegBitField!uint(0,1,RegisterAccess.RW),		// XFRC
		}
		enum mask_DIEPTSIZ2 {
				MCNT = RegBitField!uint(29,2,RegisterAccess.RW),		// Multi count
				PKTCNT = RegBitField!uint(19,10,RegisterAccess.RW),		// Packet count
				XFRSIZ = RegBitField!uint(0,19,RegisterAccess.RW),		// Transfer size
		}
		enum mask_DTXFSTS2 {
				INEPTFSAV = RegBitField!uint(0,16,RegisterAccess.RO),		// IN endpoint TxFIFO space available
		}
		enum mask_DIEPCTL3 {
				EPENA = RegBitField!uint(31,1,RegisterAccess.RW),		// EPENA
				EPDIS = RegBitField!uint(30,1,RegisterAccess.RW),		// EPDIS
				SODDFRM = RegBitField!uint(29,1,RegisterAccess.WO),		// SODDFRM
				SD0PID_SEVNFRM = RegBitField!uint(28,1,RegisterAccess.WO),		// SD0PID/SEVNFRM
				SNAK = RegBitField!uint(27,1,RegisterAccess.WO),		// SNAK
				CNAK = RegBitField!uint(26,1,RegisterAccess.WO),		// CNAK
				TXFNUM = RegBitField!uint(22,4,RegisterAccess.RW),		// TXFNUM
				Stall = RegBitField!uint(21,1,RegisterAccess.RW),		// Stall
				EPTYP = RegBitField!uint(18,2,RegisterAccess.RW),		// EPTYP
				NAKSTS = RegBitField!uint(17,1,RegisterAccess.RO),		// NAKSTS
				EONUM_DPID = RegBitField!uint(16,1,RegisterAccess.RO),		// EONUM/DPID
				USBAEP = RegBitField!uint(15,1,RegisterAccess.RW),		// USBAEP
				MPSIZ = RegBitField!uint(0,11,RegisterAccess.RW),		// MPSIZ
		}
		enum mask_DIEPINT3 {
				TXFE = RegBitField!uint(7,1,RegisterAccess.RO),		// TXFE
				INEPNE = RegBitField!uint(6,1,RegisterAccess.RW),		// INEPNE
				ITTXFE = RegBitField!uint(4,1,RegisterAccess.RW),		// ITTXFE
				TOC = RegBitField!uint(3,1,RegisterAccess.RW),		// TOC
				EPDISD = RegBitField!uint(1,1,RegisterAccess.RW),		// EPDISD
				XFRC = RegBitField!uint(0,1,RegisterAccess.RW),		// XFRC
		}
		enum mask_DIEPTSIZ3 {
				MCNT = RegBitField!uint(29,2,RegisterAccess.RW),		// Multi count
				PKTCNT = RegBitField!uint(19,10,RegisterAccess.RW),		// Packet count
				XFRSIZ = RegBitField!uint(0,19,RegisterAccess.RW),		// Transfer size
		}
		enum mask_DTXFSTS3 {
				INEPTFSAV = RegBitField!uint(0,16,RegisterAccess.RO),		// IN endpoint TxFIFO space available
		}
		enum mask_DOEPCTL0 {
				EPENA = RegBitField!uint(31,1,RegisterAccess.WO),		// EPENA
				EPDIS = RegBitField!uint(30,1,RegisterAccess.RO),		// EPDIS
				SNAK = RegBitField!uint(27,1,RegisterAccess.WO),		// SNAK
				CNAK = RegBitField!uint(26,1,RegisterAccess.WO),		// CNAK
				Stall = RegBitField!uint(21,1,RegisterAccess.RW),		// Stall
				SNPM = RegBitField!uint(20,1,RegisterAccess.RW),		// SNPM
				EPTYP = RegBitField!uint(18,2,RegisterAccess.RO),		// EPTYP
				NAKSTS = RegBitField!uint(17,1,RegisterAccess.RO),		// NAKSTS
				USBAEP = RegBitField!uint(15,1,RegisterAccess.RO),		// USBAEP
				MPSIZ = RegBitField!uint(0,2,RegisterAccess.RO),		// MPSIZ
		}
		enum mask_DOEPINT0 {
				B2BSTUP = RegBitField!uint(6,1,RegisterAccess.RW),		// B2BSTUP
				OTEPDIS = RegBitField!uint(4,1,RegisterAccess.RW),		// OTEPDIS
				STUP = RegBitField!uint(3,1,RegisterAccess.RW),		// STUP
				EPDISD = RegBitField!uint(1,1,RegisterAccess.RW),		// EPDISD
				XFRC = RegBitField!uint(0,1,RegisterAccess.RW),		// XFRC
		}
		enum mask_DOEPTSIZ0 {
				STUPCNT = RegBitField!uint(29,2,RegisterAccess.RW),		// SETUP packet count
				PKTCNT = RegBitField!uint(19,1,RegisterAccess.RW),		// Packet count
				XFRSIZ = RegBitField!uint(0,7,RegisterAccess.RW),		// Transfer size
		}
		enum mask_DOEPCTL1 {
				EPENA = RegBitField!uint(31,1,RegisterAccess.RW),		// EPENA
				EPDIS = RegBitField!uint(30,1,RegisterAccess.RW),		// EPDIS
				SODDFRM = RegBitField!uint(29,1,RegisterAccess.WO),		// SODDFRM
				SD0PID_SEVNFRM = RegBitField!uint(28,1,RegisterAccess.WO),		// SD0PID/SEVNFRM
				SNAK = RegBitField!uint(27,1,RegisterAccess.WO),		// SNAK
				CNAK = RegBitField!uint(26,1,RegisterAccess.WO),		// CNAK
				Stall = RegBitField!uint(21,1,RegisterAccess.RW),		// Stall
				SNPM = RegBitField!uint(20,1,RegisterAccess.RW),		// SNPM
				EPTYP = RegBitField!uint(18,2,RegisterAccess.RW),		// EPTYP
				NAKSTS = RegBitField!uint(17,1,RegisterAccess.RO),		// NAKSTS
				EONUM_DPID = RegBitField!uint(16,1,RegisterAccess.RO),		// EONUM/DPID
				USBAEP = RegBitField!uint(15,1,RegisterAccess.RW),		// USBAEP
				MPSIZ = RegBitField!uint(0,11,RegisterAccess.RW),		// MPSIZ
		}
		enum mask_DOEPINT1 {
				B2BSTUP = RegBitField!uint(6,1,RegisterAccess.RW),		// B2BSTUP
				OTEPDIS = RegBitField!uint(4,1,RegisterAccess.RW),		// OTEPDIS
				STUP = RegBitField!uint(3,1,RegisterAccess.RW),		// STUP
				EPDISD = RegBitField!uint(1,1,RegisterAccess.RW),		// EPDISD
				XFRC = RegBitField!uint(0,1,RegisterAccess.RW),		// XFRC
		}
		enum mask_DOEPTSIZ1 {
				RXDPID_STUPCNT = RegBitField!uint(29,2,RegisterAccess.RW),		// Received data PID/SETUP packet count
				PKTCNT = RegBitField!uint(19,10,RegisterAccess.RW),		// Packet count
				XFRSIZ = RegBitField!uint(0,19,RegisterAccess.RW),		// Transfer size
		}
		enum mask_DOEPCTL2 {
				EPENA = RegBitField!uint(31,1,RegisterAccess.RW),		// EPENA
				EPDIS = RegBitField!uint(30,1,RegisterAccess.RW),		// EPDIS
				SODDFRM = RegBitField!uint(29,1,RegisterAccess.WO),		// SODDFRM
				SD0PID_SEVNFRM = RegBitField!uint(28,1,RegisterAccess.WO),		// SD0PID/SEVNFRM
				SNAK = RegBitField!uint(27,1,RegisterAccess.WO),		// SNAK
				CNAK = RegBitField!uint(26,1,RegisterAccess.WO),		// CNAK
				Stall = RegBitField!uint(21,1,RegisterAccess.RW),		// Stall
				SNPM = RegBitField!uint(20,1,RegisterAccess.RW),		// SNPM
				EPTYP = RegBitField!uint(18,2,RegisterAccess.RW),		// EPTYP
				NAKSTS = RegBitField!uint(17,1,RegisterAccess.RO),		// NAKSTS
				EONUM_DPID = RegBitField!uint(16,1,RegisterAccess.RO),		// EONUM/DPID
				USBAEP = RegBitField!uint(15,1,RegisterAccess.RW),		// USBAEP
				MPSIZ = RegBitField!uint(0,11,RegisterAccess.RW),		// MPSIZ
		}
		enum mask_DOEPINT2 {
				B2BSTUP = RegBitField!uint(6,1,RegisterAccess.RW),		// B2BSTUP
				OTEPDIS = RegBitField!uint(4,1,RegisterAccess.RW),		// OTEPDIS
				STUP = RegBitField!uint(3,1,RegisterAccess.RW),		// STUP
				EPDISD = RegBitField!uint(1,1,RegisterAccess.RW),		// EPDISD
				XFRC = RegBitField!uint(0,1,RegisterAccess.RW),		// XFRC
		}
		enum mask_DOEPTSIZ2 {
				RXDPID_STUPCNT = RegBitField!uint(29,2,RegisterAccess.RW),		// Received data PID/SETUP packet count
				PKTCNT = RegBitField!uint(19,10,RegisterAccess.RW),		// Packet count
				XFRSIZ = RegBitField!uint(0,19,RegisterAccess.RW),		// Transfer size
		}
		enum mask_DOEPCTL3 {
				EPENA = RegBitField!uint(31,1,RegisterAccess.RW),		// EPENA
				EPDIS = RegBitField!uint(30,1,RegisterAccess.RW),		// EPDIS
				SODDFRM = RegBitField!uint(29,1,RegisterAccess.WO),		// SODDFRM
				SD0PID_SEVNFRM = RegBitField!uint(28,1,RegisterAccess.WO),		// SD0PID/SEVNFRM
				SNAK = RegBitField!uint(27,1,RegisterAccess.WO),		// SNAK
				CNAK = RegBitField!uint(26,1,RegisterAccess.WO),		// CNAK
				Stall = RegBitField!uint(21,1,RegisterAccess.RW),		// Stall
				SNPM = RegBitField!uint(20,1,RegisterAccess.RW),		// SNPM
				EPTYP = RegBitField!uint(18,2,RegisterAccess.RW),		// EPTYP
				NAKSTS = RegBitField!uint(17,1,RegisterAccess.RO),		// NAKSTS
				EONUM_DPID = RegBitField!uint(16,1,RegisterAccess.RO),		// EONUM/DPID
				USBAEP = RegBitField!uint(15,1,RegisterAccess.RW),		// USBAEP
				MPSIZ = RegBitField!uint(0,11,RegisterAccess.RW),		// MPSIZ
		}
		enum mask_DOEPINT3 {
				B2BSTUP = RegBitField!uint(6,1,RegisterAccess.RW),		// B2BSTUP
				OTEPDIS = RegBitField!uint(4,1,RegisterAccess.RW),		// OTEPDIS
				STUP = RegBitField!uint(3,1,RegisterAccess.RW),		// STUP
				EPDISD = RegBitField!uint(1,1,RegisterAccess.RW),		// EPDISD
				XFRC = RegBitField!uint(0,1,RegisterAccess.RW),		// XFRC
		}
		enum mask_DOEPTSIZ3 {
				RXDPID_STUPCNT = RegBitField!uint(29,2,RegisterAccess.RW),		// Received data PID/SETUP packet count
				PKTCNT = RegBitField!uint(19,10,RegisterAccess.RW),		// Packet count
				XFRSIZ = RegBitField!uint(0,19,RegisterAccess.RW),		// Transfer size
		}
	}
	// USB on the go full speed
	struct OTG_FS_PWRCLK_Type {
		@RegInfo("FS_PCGCCTL",0x0) RegVolatile!(mask_FS_PCGCCTL,uint,0x50000e00,0x0,RegisterAccess.RW) FS_PCGCCTL;		 // offset:0x0 FS_PCGCCTL OTG_FS power and clock gating control register
		enum mask_FS_PCGCCTL {
				STPPCLK = RegBitField!uint(0,1,RegisterAccess.RW),		// Stop PHY clock
				GATEHCLK = RegBitField!uint(1,1,RegisterAccess.RW),		// Gate HCLK
				PHYSUSP = RegBitField!uint(4,1,RegisterAccess.RW),		// PHY Suspended
		}
	}
	// SYSCFG Group 
	// System configuration controller
	struct SYSCFG_Type {
		@RegInfo("MEMRM",0x0) RegVolatile!(mask_MEMRM,uint,0x40013800,0x0,RegisterAccess.RW) MEMRM;		 // offset:0x0 MEMRM memory remap register
		@RegInfo("PMC",0x4) RegVolatile!(mask_PMC,uint,0x40013804,0x0,RegisterAccess.RW) PMC;		 // offset:0x4 PMC peripheral mode configuration register
		@RegInfo("EXTICR1",0x8) RegVolatile!(mask_EXTICR1,uint,0x40013808,0x0,RegisterAccess.RW) EXTICR1;		 // offset:0x8 EXTICR1 external interrupt configuration register 1
		@RegInfo("EXTICR2",0xc) RegVolatile!(mask_EXTICR2,uint,0x4001380c,0x0,RegisterAccess.RW) EXTICR2;		 // offset:0x12 EXTICR2 external interrupt configuration register 2
		@RegInfo("EXTICR3",0x10) RegVolatile!(mask_EXTICR3,uint,0x40013810,0x0,RegisterAccess.RW) EXTICR3;		 // offset:0x16 EXTICR3 external interrupt configuration register 3
		@RegInfo("EXTICR4",0x14) RegVolatile!(mask_EXTICR4,uint,0x40013814,0x0,RegisterAccess.RW) EXTICR4;		 // offset:0x20 EXTICR4 external interrupt configuration register 4
		private uint[0x2] reserved6;	// skip 8 bytes
		@RegInfo("CMPCR",0x20) RegVolatile!(mask_CMPCR,uint,0x40013820,0x0,RegisterAccess.RO) CMPCR;		 // offset:0x32 CMPCR Compensation cell control register
		enum mask_MEMRM {
				MEM_MODE = RegBitField!uint(0,2,RegisterAccess.RW),		// MEM_MODE
		}
		enum mask_PMC {
				ADC1DC2 = RegBitField!uint(16,1,RegisterAccess.RW),		// ADC1DC2
		}
		enum mask_EXTICR1 {
				EXTI3 = RegBitField!uint(12,4,RegisterAccess.RW),		// EXTI x configuration (x = 0 to 3)
				EXTI2 = RegBitField!uint(8,4,RegisterAccess.RW),		// EXTI x configuration (x = 0 to 3)
				EXTI1 = RegBitField!uint(4,4,RegisterAccess.RW),		// EXTI x configuration (x = 0 to 3)
				EXTI0 = RegBitField!uint(0,4,RegisterAccess.RW),		// EXTI x configuration (x = 0 to 3)
		}
		enum mask_EXTICR2 {
				EXTI7 = RegBitField!uint(12,4,RegisterAccess.RW),		// EXTI x configuration (x = 4 to 7)
				EXTI6 = RegBitField!uint(8,4,RegisterAccess.RW),		// EXTI x configuration (x = 4 to 7)
				EXTI5 = RegBitField!uint(4,4,RegisterAccess.RW),		// EXTI x configuration (x = 4 to 7)
				EXTI4 = RegBitField!uint(0,4,RegisterAccess.RW),		// EXTI x configuration (x = 4 to 7)
		}
		enum mask_EXTICR3 {
				EXTI11 = RegBitField!uint(12,4,RegisterAccess.RW),		// EXTI x configuration (x = 8 to 11)
				EXTI10 = RegBitField!uint(8,4,RegisterAccess.RW),		// EXTI10
				EXTI9 = RegBitField!uint(4,4,RegisterAccess.RW),		// EXTI x configuration (x = 8 to 11)
				EXTI8 = RegBitField!uint(0,4,RegisterAccess.RW),		// EXTI x configuration (x = 8 to 11)
		}
		enum mask_EXTICR4 {
				EXTI15 = RegBitField!uint(12,4,RegisterAccess.RW),		// EXTI x configuration (x = 12 to 15)
				EXTI14 = RegBitField!uint(8,4,RegisterAccess.RW),		// EXTI x configuration (x = 12 to 15)
				EXTI13 = RegBitField!uint(4,4,RegisterAccess.RW),		// EXTI x configuration (x = 12 to 15)
				EXTI12 = RegBitField!uint(0,4,RegisterAccess.RW),		// EXTI x configuration (x = 12 to 15)
		}
		enum mask_CMPCR {
				READY = RegBitField!uint(8,1,RegisterAccess.RO),		// READY
				CMP_PD = RegBitField!uint(0,1,RegisterAccess.RO),		// Compensation cell power-down
		}
	}
	// WWDG Group 
	// Window watchdog
	struct WWDG_Type {
		@RegInfo("CR",0x0) RegVolatile!(mask_CR,uint,0x40002c00,0x7f,RegisterAccess.RW) CR;		 // offset:0x0 CR Control register
		@RegInfo("CFR",0x4) RegVolatile!(mask_CFR,uint,0x40002c04,0x7f,RegisterAccess.RW) CFR;		 // offset:0x4 CFR Configuration register
		@RegInfo("SR",0x8) RegVolatile!(mask_SR,uint,0x40002c08,0x0,RegisterAccess.RW) SR;		 // offset:0x8 SR Status register
		enum mask_CR {
				WDGA = RegBitField!uint(7,1,RegisterAccess.RW),		// Activation bit
				T = RegBitField!uint(0,7,RegisterAccess.RW),		// 7-bit counter (MSB to LSB)
		}
		enum mask_CFR {
				EWI = RegBitField!uint(9,1,RegisterAccess.RW),		// Early wakeup interrupt
				WDGTB1 = RegBitField!uint(8,1,RegisterAccess.RW),		// Timer base
				WDGTB0 = RegBitField!uint(7,1,RegisterAccess.RW),		// Timer base
				W = RegBitField!uint(0,7,RegisterAccess.RW),		// 7-bit window value
		}
		enum mask_SR {
				EWIF = RegBitField!uint(0,1,RegisterAccess.RW),		// Early wakeup interrupt flag
		}
	}
	// EXTI Group 
	// External interrupt/event controller
	struct EXTI_Type {
		@RegInfo("IMR",0x0) RegVolatile!(mask_IMR,uint,0x40013c00,0x0,RegisterAccess.RW) IMR;		 // offset:0x0 IMR Interrupt mask register (EXTI_IMR)
		@RegInfo("EMR",0x4) RegVolatile!(mask_EMR,uint,0x40013c04,0x0,RegisterAccess.RW) EMR;		 // offset:0x4 EMR Event mask register (EXTI_EMR)
		@RegInfo("RTSR",0x8) RegVolatile!(mask_RTSR,uint,0x40013c08,0x0,RegisterAccess.RW) RTSR;		 // offset:0x8 RTSR Rising Trigger selection register (EXTI_RTSR)
		@RegInfo("FTSR",0xc) RegVolatile!(mask_FTSR,uint,0x40013c0c,0x0,RegisterAccess.RW) FTSR;		 // offset:0x12 FTSR Falling Trigger selection register (EXTI_FTSR)
		@RegInfo("SWIER",0x10) RegVolatile!(mask_SWIER,uint,0x40013c10,0x0,RegisterAccess.RW) SWIER;		 // offset:0x16 SWIER Software interrupt event register (EXTI_SWIER)
		@RegInfo("PR",0x14) RegVolatile!(mask_PR,uint,0x40013c14,0x0,RegisterAccess.RW) PR;		 // offset:0x20 PR Pending register (EXTI_PR)
		enum mask_IMR {
				MR0 = RegBitField!uint(0,1,RegisterAccess.RW),		// Interrupt Mask on line 0
				MR1 = RegBitField!uint(1,1,RegisterAccess.RW),		// Interrupt Mask on line 1
				MR2 = RegBitField!uint(2,1,RegisterAccess.RW),		// Interrupt Mask on line 2
				MR3 = RegBitField!uint(3,1,RegisterAccess.RW),		// Interrupt Mask on line 3
				MR4 = RegBitField!uint(4,1,RegisterAccess.RW),		// Interrupt Mask on line 4
				MR5 = RegBitField!uint(5,1,RegisterAccess.RW),		// Interrupt Mask on line 5
				MR6 = RegBitField!uint(6,1,RegisterAccess.RW),		// Interrupt Mask on line 6
				MR7 = RegBitField!uint(7,1,RegisterAccess.RW),		// Interrupt Mask on line 7
				MR8 = RegBitField!uint(8,1,RegisterAccess.RW),		// Interrupt Mask on line 8
				MR9 = RegBitField!uint(9,1,RegisterAccess.RW),		// Interrupt Mask on line 9
				MR10 = RegBitField!uint(10,1,RegisterAccess.RW),		// Interrupt Mask on line 10
				MR11 = RegBitField!uint(11,1,RegisterAccess.RW),		// Interrupt Mask on line 11
				MR12 = RegBitField!uint(12,1,RegisterAccess.RW),		// Interrupt Mask on line 12
				MR13 = RegBitField!uint(13,1,RegisterAccess.RW),		// Interrupt Mask on line 13
				MR14 = RegBitField!uint(14,1,RegisterAccess.RW),		// Interrupt Mask on line 14
				MR15 = RegBitField!uint(15,1,RegisterAccess.RW),		// Interrupt Mask on line 15
				MR16 = RegBitField!uint(16,1,RegisterAccess.RW),		// Interrupt Mask on line 16
				MR17 = RegBitField!uint(17,1,RegisterAccess.RW),		// Interrupt Mask on line 17
				MR18 = RegBitField!uint(18,1,RegisterAccess.RW),		// Interrupt Mask on line 18
				MR19 = RegBitField!uint(19,1,RegisterAccess.RW),		// Interrupt Mask on line 19
				MR20 = RegBitField!uint(20,1,RegisterAccess.RW),		// Interrupt Mask on line 20
				MR21 = RegBitField!uint(21,1,RegisterAccess.RW),		// Interrupt Mask on line 21
				MR22 = RegBitField!uint(22,1,RegisterAccess.RW),		// Interrupt Mask on line 22
		}
		enum mask_EMR {
				MR0 = RegBitField!uint(0,1,RegisterAccess.RW),		// Event Mask on line 0
				MR1 = RegBitField!uint(1,1,RegisterAccess.RW),		// Event Mask on line 1
				MR2 = RegBitField!uint(2,1,RegisterAccess.RW),		// Event Mask on line 2
				MR3 = RegBitField!uint(3,1,RegisterAccess.RW),		// Event Mask on line 3
				MR4 = RegBitField!uint(4,1,RegisterAccess.RW),		// Event Mask on line 4
				MR5 = RegBitField!uint(5,1,RegisterAccess.RW),		// Event Mask on line 5
				MR6 = RegBitField!uint(6,1,RegisterAccess.RW),		// Event Mask on line 6
				MR7 = RegBitField!uint(7,1,RegisterAccess.RW),		// Event Mask on line 7
				MR8 = RegBitField!uint(8,1,RegisterAccess.RW),		// Event Mask on line 8
				MR9 = RegBitField!uint(9,1,RegisterAccess.RW),		// Event Mask on line 9
				MR10 = RegBitField!uint(10,1,RegisterAccess.RW),		// Event Mask on line 10
				MR11 = RegBitField!uint(11,1,RegisterAccess.RW),		// Event Mask on line 11
				MR12 = RegBitField!uint(12,1,RegisterAccess.RW),		// Event Mask on line 12
				MR13 = RegBitField!uint(13,1,RegisterAccess.RW),		// Event Mask on line 13
				MR14 = RegBitField!uint(14,1,RegisterAccess.RW),		// Event Mask on line 14
				MR15 = RegBitField!uint(15,1,RegisterAccess.RW),		// Event Mask on line 15
				MR16 = RegBitField!uint(16,1,RegisterAccess.RW),		// Event Mask on line 16
				MR17 = RegBitField!uint(17,1,RegisterAccess.RW),		// Event Mask on line 17
				MR18 = RegBitField!uint(18,1,RegisterAccess.RW),		// Event Mask on line 18
				MR19 = RegBitField!uint(19,1,RegisterAccess.RW),		// Event Mask on line 19
				MR20 = RegBitField!uint(20,1,RegisterAccess.RW),		// Event Mask on line 20
				MR21 = RegBitField!uint(21,1,RegisterAccess.RW),		// Event Mask on line 21
				MR22 = RegBitField!uint(22,1,RegisterAccess.RW),		// Event Mask on line 22
		}
		enum mask_RTSR {
				TR0 = RegBitField!uint(0,1,RegisterAccess.RW),		// Rising trigger event configuration of line 0
				TR1 = RegBitField!uint(1,1,RegisterAccess.RW),		// Rising trigger event configuration of line 1
				TR2 = RegBitField!uint(2,1,RegisterAccess.RW),		// Rising trigger event configuration of line 2
				TR3 = RegBitField!uint(3,1,RegisterAccess.RW),		// Rising trigger event configuration of line 3
				TR4 = RegBitField!uint(4,1,RegisterAccess.RW),		// Rising trigger event configuration of line 4
				TR5 = RegBitField!uint(5,1,RegisterAccess.RW),		// Rising trigger event configuration of line 5
				TR6 = RegBitField!uint(6,1,RegisterAccess.RW),		// Rising trigger event configuration of line 6
				TR7 = RegBitField!uint(7,1,RegisterAccess.RW),		// Rising trigger event configuration of line 7
				TR8 = RegBitField!uint(8,1,RegisterAccess.RW),		// Rising trigger event configuration of line 8
				TR9 = RegBitField!uint(9,1,RegisterAccess.RW),		// Rising trigger event configuration of line 9
				TR10 = RegBitField!uint(10,1,RegisterAccess.RW),		// Rising trigger event configuration of line 10
				TR11 = RegBitField!uint(11,1,RegisterAccess.RW),		// Rising trigger event configuration of line 11
				TR12 = RegBitField!uint(12,1,RegisterAccess.RW),		// Rising trigger event configuration of line 12
				TR13 = RegBitField!uint(13,1,RegisterAccess.RW),		// Rising trigger event configuration of line 13
				TR14 = RegBitField!uint(14,1,RegisterAccess.RW),		// Rising trigger event configuration of line 14
				TR15 = RegBitField!uint(15,1,RegisterAccess.RW),		// Rising trigger event configuration of line 15
				TR16 = RegBitField!uint(16,1,RegisterAccess.RW),		// Rising trigger event configuration of line 16
				TR17 = RegBitField!uint(17,1,RegisterAccess.RW),		// Rising trigger event configuration of line 17
				TR18 = RegBitField!uint(18,1,RegisterAccess.RW),		// Rising trigger event configuration of line 18
				TR19 = RegBitField!uint(19,1,RegisterAccess.RW),		// Rising trigger event configuration of line 19
				TR20 = RegBitField!uint(20,1,RegisterAccess.RW),		// Rising trigger event configuration of line 20
				TR21 = RegBitField!uint(21,1,RegisterAccess.RW),		// Rising trigger event configuration of line 21
				TR22 = RegBitField!uint(22,1,RegisterAccess.RW),		// Rising trigger event configuration of line 22
		}
		enum mask_FTSR {
				TR0 = RegBitField!uint(0,1,RegisterAccess.RW),		// Falling trigger event configuration of line 0
				TR1 = RegBitField!uint(1,1,RegisterAccess.RW),		// Falling trigger event configuration of line 1
				TR2 = RegBitField!uint(2,1,RegisterAccess.RW),		// Falling trigger event configuration of line 2
				TR3 = RegBitField!uint(3,1,RegisterAccess.RW),		// Falling trigger event configuration of line 3
				TR4 = RegBitField!uint(4,1,RegisterAccess.RW),		// Falling trigger event configuration of line 4
				TR5 = RegBitField!uint(5,1,RegisterAccess.RW),		// Falling trigger event configuration of line 5
				TR6 = RegBitField!uint(6,1,RegisterAccess.RW),		// Falling trigger event configuration of line 6
				TR7 = RegBitField!uint(7,1,RegisterAccess.RW),		// Falling trigger event configuration of line 7
				TR8 = RegBitField!uint(8,1,RegisterAccess.RW),		// Falling trigger event configuration of line 8
				TR9 = RegBitField!uint(9,1,RegisterAccess.RW),		// Falling trigger event configuration of line 9
				TR10 = RegBitField!uint(10,1,RegisterAccess.RW),		// Falling trigger event configuration of line 10
				TR11 = RegBitField!uint(11,1,RegisterAccess.RW),		// Falling trigger event configuration of line 11
				TR12 = RegBitField!uint(12,1,RegisterAccess.RW),		// Falling trigger event configuration of line 12
				TR13 = RegBitField!uint(13,1,RegisterAccess.RW),		// Falling trigger event configuration of line 13
				TR14 = RegBitField!uint(14,1,RegisterAccess.RW),		// Falling trigger event configuration of line 14
				TR15 = RegBitField!uint(15,1,RegisterAccess.RW),		// Falling trigger event configuration of line 15
				TR16 = RegBitField!uint(16,1,RegisterAccess.RW),		// Falling trigger event configuration of line 16
				TR17 = RegBitField!uint(17,1,RegisterAccess.RW),		// Falling trigger event configuration of line 17
				TR18 = RegBitField!uint(18,1,RegisterAccess.RW),		// Falling trigger event configuration of line 18
				TR19 = RegBitField!uint(19,1,RegisterAccess.RW),		// Falling trigger event configuration of line 19
				TR20 = RegBitField!uint(20,1,RegisterAccess.RW),		// Falling trigger event configuration of line 20
				TR21 = RegBitField!uint(21,1,RegisterAccess.RW),		// Falling trigger event configuration of line 21
				TR22 = RegBitField!uint(22,1,RegisterAccess.RW),		// Falling trigger event configuration of line 22
		}
		enum mask_SWIER {
				SWIER0 = RegBitField!uint(0,1,RegisterAccess.RW),		// Software Interrupt on line 0
				SWIER1 = RegBitField!uint(1,1,RegisterAccess.RW),		// Software Interrupt on line 1
				SWIER2 = RegBitField!uint(2,1,RegisterAccess.RW),		// Software Interrupt on line 2
				SWIER3 = RegBitField!uint(3,1,RegisterAccess.RW),		// Software Interrupt on line 3
				SWIER4 = RegBitField!uint(4,1,RegisterAccess.RW),		// Software Interrupt on line 4
				SWIER5 = RegBitField!uint(5,1,RegisterAccess.RW),		// Software Interrupt on line 5
				SWIER6 = RegBitField!uint(6,1,RegisterAccess.RW),		// Software Interrupt on line 6
				SWIER7 = RegBitField!uint(7,1,RegisterAccess.RW),		// Software Interrupt on line 7
				SWIER8 = RegBitField!uint(8,1,RegisterAccess.RW),		// Software Interrupt on line 8
				SWIER9 = RegBitField!uint(9,1,RegisterAccess.RW),		// Software Interrupt on line 9
				SWIER10 = RegBitField!uint(10,1,RegisterAccess.RW),		// Software Interrupt on line 10
				SWIER11 = RegBitField!uint(11,1,RegisterAccess.RW),		// Software Interrupt on line 11
				SWIER12 = RegBitField!uint(12,1,RegisterAccess.RW),		// Software Interrupt on line 12
				SWIER13 = RegBitField!uint(13,1,RegisterAccess.RW),		// Software Interrupt on line 13
				SWIER14 = RegBitField!uint(14,1,RegisterAccess.RW),		// Software Interrupt on line 14
				SWIER15 = RegBitField!uint(15,1,RegisterAccess.RW),		// Software Interrupt on line 15
				SWIER16 = RegBitField!uint(16,1,RegisterAccess.RW),		// Software Interrupt on line 16
				SWIER17 = RegBitField!uint(17,1,RegisterAccess.RW),		// Software Interrupt on line 17
				SWIER18 = RegBitField!uint(18,1,RegisterAccess.RW),		// Software Interrupt on line 18
				SWIER19 = RegBitField!uint(19,1,RegisterAccess.RW),		// Software Interrupt on line 19
				SWIER20 = RegBitField!uint(20,1,RegisterAccess.RW),		// Software Interrupt on line 20
				SWIER21 = RegBitField!uint(21,1,RegisterAccess.RW),		// Software Interrupt on line 21
				SWIER22 = RegBitField!uint(22,1,RegisterAccess.RW),		// Software Interrupt on line 22
		}
		enum mask_PR {
				PR0 = RegBitField!uint(0,1,RegisterAccess.RW),		// Pending bit 0
				PR1 = RegBitField!uint(1,1,RegisterAccess.RW),		// Pending bit 1
				PR2 = RegBitField!uint(2,1,RegisterAccess.RW),		// Pending bit 2
				PR3 = RegBitField!uint(3,1,RegisterAccess.RW),		// Pending bit 3
				PR4 = RegBitField!uint(4,1,RegisterAccess.RW),		// Pending bit 4
				PR5 = RegBitField!uint(5,1,RegisterAccess.RW),		// Pending bit 5
				PR6 = RegBitField!uint(6,1,RegisterAccess.RW),		// Pending bit 6
				PR7 = RegBitField!uint(7,1,RegisterAccess.RW),		// Pending bit 7
				PR8 = RegBitField!uint(8,1,RegisterAccess.RW),		// Pending bit 8
				PR9 = RegBitField!uint(9,1,RegisterAccess.RW),		// Pending bit 9
				PR10 = RegBitField!uint(10,1,RegisterAccess.RW),		// Pending bit 10
				PR11 = RegBitField!uint(11,1,RegisterAccess.RW),		// Pending bit 11
				PR12 = RegBitField!uint(12,1,RegisterAccess.RW),		// Pending bit 12
				PR13 = RegBitField!uint(13,1,RegisterAccess.RW),		// Pending bit 13
				PR14 = RegBitField!uint(14,1,RegisterAccess.RW),		// Pending bit 14
				PR15 = RegBitField!uint(15,1,RegisterAccess.RW),		// Pending bit 15
				PR16 = RegBitField!uint(16,1,RegisterAccess.RW),		// Pending bit 16
				PR17 = RegBitField!uint(17,1,RegisterAccess.RW),		// Pending bit 17
				PR18 = RegBitField!uint(18,1,RegisterAccess.RW),		// Pending bit 18
				PR19 = RegBitField!uint(19,1,RegisterAccess.RW),		// Pending bit 19
				PR20 = RegBitField!uint(20,1,RegisterAccess.RW),		// Pending bit 20
				PR21 = RegBitField!uint(21,1,RegisterAccess.RW),		// Pending bit 21
				PR22 = RegBitField!uint(22,1,RegisterAccess.RW),		// Pending bit 22
		}
	}
	// SDIO Group 
	// Secure digital input/output interface
	struct SDIO_Type {
		@RegInfo("POWER",0x0) RegVolatile!(mask_POWER,uint,0x40012c00,0x0,RegisterAccess.RW) POWER;		 // offset:0x0 POWER power control register
		@RegInfo("CLKCR",0x4) RegVolatile!(mask_CLKCR,uint,0x40012c04,0x0,RegisterAccess.RW) CLKCR;		 // offset:0x4 CLKCR SDI clock control register
		@RegInfo("ARG",0x8) RegVolatile!(mask_ARG,uint,0x40012c08,0x0,RegisterAccess.RW) ARG;		 // offset:0x8 ARG argument register
		@RegInfo("CMD",0xc) RegVolatile!(mask_CMD,uint,0x40012c0c,0x0,RegisterAccess.RW) CMD;		 // offset:0x12 CMD command register
		@RegInfo("RESPCMD",0x10) RegVolatile!(mask_RESPCMD,uint,0x40012c10,0x0,RegisterAccess.RO) RESPCMD;		 // offset:0x16 RESPCMD command response register
		@RegInfo("RESP1",0x14) RegVolatile!(mask_RESP1,uint,0x40012c14,0x0,RegisterAccess.RO) RESP1;		 // offset:0x20 RESP1 response 1..4 register
		@RegInfo("RESP2",0x18) RegVolatile!(mask_RESP2,uint,0x40012c18,0x0,RegisterAccess.RO) RESP2;		 // offset:0x24 RESP2 response 1..4 register
		@RegInfo("RESP3",0x1c) RegVolatile!(mask_RESP3,uint,0x40012c1c,0x0,RegisterAccess.RO) RESP3;		 // offset:0x28 RESP3 response 1..4 register
		@RegInfo("RESP4",0x20) RegVolatile!(mask_RESP4,uint,0x40012c20,0x0,RegisterAccess.RO) RESP4;		 // offset:0x32 RESP4 response 1..4 register
		@RegInfo("DTIMER",0x24) RegVolatile!(mask_DTIMER,uint,0x40012c24,0x0,RegisterAccess.RW) DTIMER;		 // offset:0x36 DTIMER data timer register
		@RegInfo("DLEN",0x28) RegVolatile!(mask_DLEN,uint,0x40012c28,0x0,RegisterAccess.RW) DLEN;		 // offset:0x40 DLEN data length register
		@RegInfo("DCTRL",0x2c) RegVolatile!(mask_DCTRL,uint,0x40012c2c,0x0,RegisterAccess.RW) DCTRL;		 // offset:0x44 DCTRL data control register
		@RegInfo("DCOUNT",0x30) RegVolatile!(mask_DCOUNT,uint,0x40012c30,0x0,RegisterAccess.RO) DCOUNT;		 // offset:0x48 DCOUNT data counter register
		@RegInfo("STA",0x34) RegVolatile!(mask_STA,uint,0x40012c34,0x0,RegisterAccess.RO) STA;		 // offset:0x52 STA status register
		@RegInfo("ICR",0x38) RegVolatile!(mask_ICR,uint,0x40012c38,0x0,RegisterAccess.RW) ICR;		 // offset:0x56 ICR interrupt clear register
		@RegInfo("MASK",0x3c) RegVolatile!(mask_MASK,uint,0x40012c3c,0x0,RegisterAccess.RW) MASK;		 // offset:0x60 MASK mask register
		private uint[0x2] reserved16;	// skip 8 bytes
		@RegInfo("FIFOCNT",0x48) RegVolatile!(mask_FIFOCNT,uint,0x40012c48,0x0,RegisterAccess.RO) FIFOCNT;		 // offset:0x72 FIFOCNT FIFO counter register
		private uint[0xd] reserved17;	// skip 52 bytes
		@RegInfo("FIFO",0x80) RegVolatile!(mask_FIFO,uint,0x40012c80,0x0,RegisterAccess.RW) FIFO;		 // offset:0x128 FIFO data FIFO register
		enum mask_POWER {
				PWRCTRL = RegBitField!uint(0,2,RegisterAccess.RW),		// PWRCTRL
		}
		enum mask_CLKCR {
				HWFC_EN = RegBitField!uint(14,1,RegisterAccess.RW),		// HW Flow Control enable
				NEGEDGE = RegBitField!uint(13,1,RegisterAccess.RW),		// SDIO_CK dephasing selection bit
				WIDBUS = RegBitField!uint(11,2,RegisterAccess.RW),		// Wide bus mode enable bit
				BYPASS = RegBitField!uint(10,1,RegisterAccess.RW),		// Clock divider bypass enable bit
				PWRSAV = RegBitField!uint(9,1,RegisterAccess.RW),		// Power saving configuration bit
				CLKEN = RegBitField!uint(8,1,RegisterAccess.RW),		// Clock enable bit
				CLKDIV = RegBitField!uint(0,8,RegisterAccess.RW),		// Clock divide factor
		}
		enum mask_ARG {
				CMDARG = RegBitField!uint(0,32,RegisterAccess.RW),		// Command argument
		}
		enum mask_CMD {
				CE_ATACMD = RegBitField!uint(14,1,RegisterAccess.RW),		// CE-ATA command
				nIEN = RegBitField!uint(13,1,RegisterAccess.RW),		// not Interrupt Enable
				ENCMDcompl = RegBitField!uint(12,1,RegisterAccess.RW),		// Enable CMD completion
				SDIOSuspend = RegBitField!uint(11,1,RegisterAccess.RW),		// SD I/O suspend command
				CPSMEN = RegBitField!uint(10,1,RegisterAccess.RW),		// Command path state machine (CPSM) Enable bit
				WAITPEND = RegBitField!uint(9,1,RegisterAccess.RW),		// CPSM Waits for ends of data transfer (CmdPend internal signal).
				WAITINT = RegBitField!uint(8,1,RegisterAccess.RW),		// CPSM waits for interrupt request
				WAITRESP = RegBitField!uint(6,2,RegisterAccess.RW),		// Wait for response bits
				CMDINDEX = RegBitField!uint(0,6,RegisterAccess.RW),		// Command index
		}
		enum mask_RESPCMD {
				RESPCMD = RegBitField!uint(0,6,RegisterAccess.RO),		// Response command index
		}
		enum mask_RESP1 {
				CARDSTATUS1 = RegBitField!uint(0,32,RegisterAccess.RO),		// Card Status
		}
		enum mask_RESP2 {
				CARDSTATUS2 = RegBitField!uint(0,32,RegisterAccess.RO),		// Card Status
		}
		enum mask_RESP3 {
				CARDSTATUS3 = RegBitField!uint(0,32,RegisterAccess.RO),		// Card Status
		}
		enum mask_RESP4 {
				CARDSTATUS4 = RegBitField!uint(0,32,RegisterAccess.RO),		// Card Status
		}
		enum mask_DTIMER {
				DATATIME = RegBitField!uint(0,32,RegisterAccess.RW),		// Data timeout period
		}
		enum mask_DLEN {
				DATALENGTH = RegBitField!uint(0,25,RegisterAccess.RW),		// Data length value
		}
		enum mask_DCTRL {
				SDIOEN = RegBitField!uint(11,1,RegisterAccess.RW),		// SD I/O enable functions
				RWMOD = RegBitField!uint(10,1,RegisterAccess.RW),		// Read wait mode
				RWSTOP = RegBitField!uint(9,1,RegisterAccess.RW),		// Read wait stop
				RWSTART = RegBitField!uint(8,1,RegisterAccess.RW),		// Read wait start
				DBLOCKSIZE = RegBitField!uint(4,4,RegisterAccess.RW),		// Data block size
				DMAEN = RegBitField!uint(3,1,RegisterAccess.RW),		// DMA enable bit
				DTMODE = RegBitField!uint(2,1,RegisterAccess.RW),		// Data transfer mode selection 1: Stream or SDIO multibyte data transfer.
				DTDIR = RegBitField!uint(1,1,RegisterAccess.RW),		// Data transfer direction selection
				DTEN = RegBitField!uint(0,1,RegisterAccess.RW),		// DTEN
		}
		enum mask_DCOUNT {
				DATACOUNT = RegBitField!uint(0,25,RegisterAccess.RO),		// Data count value
		}
		enum mask_STA {
				CEATAEND = RegBitField!uint(23,1,RegisterAccess.RO),		// CE-ATA command completion signal received for CMD61
				SDIOIT = RegBitField!uint(22,1,RegisterAccess.RO),		// SDIO interrupt received
				RXDAVL = RegBitField!uint(21,1,RegisterAccess.RO),		// Data available in receive FIFO
				TXDAVL = RegBitField!uint(20,1,RegisterAccess.RO),		// Data available in transmit FIFO
				RXFIFOE = RegBitField!uint(19,1,RegisterAccess.RO),		// Receive FIFO empty
				TXFIFOE = RegBitField!uint(18,1,RegisterAccess.RO),		// Transmit FIFO empty
				RXFIFOF = RegBitField!uint(17,1,RegisterAccess.RO),		// Receive FIFO full
				TXFIFOF = RegBitField!uint(16,1,RegisterAccess.RO),		// Transmit FIFO full
				RXFIFOHF = RegBitField!uint(15,1,RegisterAccess.RO),		// Receive FIFO half full: there are at least 8 words in the FIFO
				TXFIFOHE = RegBitField!uint(14,1,RegisterAccess.RO),		// Transmit FIFO half empty: at least 8 words can be written into the FIFO
				RXACT = RegBitField!uint(13,1,RegisterAccess.RO),		// Data receive in progress
				TXACT = RegBitField!uint(12,1,RegisterAccess.RO),		// Data transmit in progress
				CMDACT = RegBitField!uint(11,1,RegisterAccess.RO),		// Command transfer in progress
				DBCKEND = RegBitField!uint(10,1,RegisterAccess.RO),		// Data block sent/received (CRC check passed)
				STBITERR = RegBitField!uint(9,1,RegisterAccess.RO),		// Start bit not detected on all data signals in wide bus mode
				DATAEND = RegBitField!uint(8,1,RegisterAccess.RO),		// Data end (data counter, SDIDCOUNT, is zero)
				CMDSENT = RegBitField!uint(7,1,RegisterAccess.RO),		// Command sent (no response required)
				CMDREND = RegBitField!uint(6,1,RegisterAccess.RO),		// Command response received (CRC check passed)
				RXOVERR = RegBitField!uint(5,1,RegisterAccess.RO),		// Received FIFO overrun error
				TXUNDERR = RegBitField!uint(4,1,RegisterAccess.RO),		// Transmit FIFO underrun error
				DTIMEOUT = RegBitField!uint(3,1,RegisterAccess.RO),		// Data timeout
				CTIMEOUT = RegBitField!uint(2,1,RegisterAccess.RO),		// Command response timeout
				DCRCFAIL = RegBitField!uint(1,1,RegisterAccess.RO),		// Data block sent/received (CRC check failed)
				CCRCFAIL = RegBitField!uint(0,1,RegisterAccess.RO),		// Command response received (CRC check failed)
		}
		enum mask_ICR {
				CEATAENDC = RegBitField!uint(23,1,RegisterAccess.RW),		// CEATAEND flag clear bit
				SDIOITC = RegBitField!uint(22,1,RegisterAccess.RW),		// SDIOIT flag clear bit
				DBCKENDC = RegBitField!uint(10,1,RegisterAccess.RW),		// DBCKEND flag clear bit
				STBITERRC = RegBitField!uint(9,1,RegisterAccess.RW),		// STBITERR flag clear bit
				DATAENDC = RegBitField!uint(8,1,RegisterAccess.RW),		// DATAEND flag clear bit
				CMDSENTC = RegBitField!uint(7,1,RegisterAccess.RW),		// CMDSENT flag clear bit
				CMDRENDC = RegBitField!uint(6,1,RegisterAccess.RW),		// CMDREND flag clear bit
				RXOVERRC = RegBitField!uint(5,1,RegisterAccess.RW),		// RXOVERR flag clear bit
				TXUNDERRC = RegBitField!uint(4,1,RegisterAccess.RW),		// TXUNDERR flag clear bit
				DTIMEOUTC = RegBitField!uint(3,1,RegisterAccess.RW),		// DTIMEOUT flag clear bit
				CTIMEOUTC = RegBitField!uint(2,1,RegisterAccess.RW),		// CTIMEOUT flag clear bit
				DCRCFAILC = RegBitField!uint(1,1,RegisterAccess.RW),		// DCRCFAIL flag clear bit
				CCRCFAILC = RegBitField!uint(0,1,RegisterAccess.RW),		// CCRCFAIL flag clear bit
		}
		enum mask_MASK {
				CEATAENDIE = RegBitField!uint(23,1,RegisterAccess.RW),		// CE-ATA command completion signal received interrupt enable
				SDIOITIE = RegBitField!uint(22,1,RegisterAccess.RW),		// SDIO mode interrupt received interrupt enable
				RXDAVLIE = RegBitField!uint(21,1,RegisterAccess.RW),		// Data available in Rx FIFO interrupt enable
				TXDAVLIE = RegBitField!uint(20,1,RegisterAccess.RW),		// Data available in Tx FIFO interrupt enable
				RXFIFOEIE = RegBitField!uint(19,1,RegisterAccess.RW),		// Rx FIFO empty interrupt enable
				TXFIFOEIE = RegBitField!uint(18,1,RegisterAccess.RW),		// Tx FIFO empty interrupt enable
				RXFIFOFIE = RegBitField!uint(17,1,RegisterAccess.RW),		// Rx FIFO full interrupt enable
				TXFIFOFIE = RegBitField!uint(16,1,RegisterAccess.RW),		// Tx FIFO full interrupt enable
				RXFIFOHFIE = RegBitField!uint(15,1,RegisterAccess.RW),		// Rx FIFO half full interrupt enable
				TXFIFOHEIE = RegBitField!uint(14,1,RegisterAccess.RW),		// Tx FIFO half empty interrupt enable
				RXACTIE = RegBitField!uint(13,1,RegisterAccess.RW),		// Data receive acting interrupt enable
				TXACTIE = RegBitField!uint(12,1,RegisterAccess.RW),		// Data transmit acting interrupt enable
				CMDACTIE = RegBitField!uint(11,1,RegisterAccess.RW),		// Command acting interrupt enable
				DBCKENDIE = RegBitField!uint(10,1,RegisterAccess.RW),		// Data block end interrupt enable
				STBITERRIE = RegBitField!uint(9,1,RegisterAccess.RW),		// Start bit error interrupt enable
				DATAENDIE = RegBitField!uint(8,1,RegisterAccess.RW),		// Data end interrupt enable
				CMDSENTIE = RegBitField!uint(7,1,RegisterAccess.RW),		// Command sent interrupt enable
				CMDRENDIE = RegBitField!uint(6,1,RegisterAccess.RW),		// Command response received interrupt enable
				RXOVERRIE = RegBitField!uint(5,1,RegisterAccess.RW),		// Rx FIFO overrun error interrupt enable
				TXUNDERRIE = RegBitField!uint(4,1,RegisterAccess.RW),		// Tx FIFO underrun error interrupt enable
				DTIMEOUTIE = RegBitField!uint(3,1,RegisterAccess.RW),		// Data timeout interrupt enable
				CTIMEOUTIE = RegBitField!uint(2,1,RegisterAccess.RW),		// Command timeout interrupt enable
				DCRCFAILIE = RegBitField!uint(1,1,RegisterAccess.RW),		// Data CRC fail interrupt enable
				CCRCFAILIE = RegBitField!uint(0,1,RegisterAccess.RW),		// Command CRC fail interrupt enable
		}
		enum mask_FIFOCNT {
				FIFOCOUNT = RegBitField!uint(0,24,RegisterAccess.RO),		// Remaining number of words to be written to or read from the FIFO.
		}
		enum mask_FIFO {
				FIFOData = RegBitField!uint(0,32,RegisterAccess.RW),		// Receive and transmit FIFO data
		}
	}
	// USART Group 
	// Universal synchronous asynchronous receiver transmitter
	struct USART6_Type {
		@RegInfo("SR",0x0) RegVolatile!(mask_SR,uint,0x40011400,0xc00000,RegisterAccess.NO) SR;		 // offset:0x0 SR Status register
		@RegInfo("DR",0x4) RegVolatile!(mask_DR,uint,0x40011404,0x0,RegisterAccess.RW) DR;		 // offset:0x4 DR Data register
		@RegInfo("BRR",0x8) RegVolatile!(mask_BRR,uint,0x40011408,0x0,RegisterAccess.RW) BRR;		 // offset:0x8 BRR Baud rate register
		@RegInfo("CR1",0xc) RegVolatile!(mask_CR1,uint,0x4001140c,0x0,RegisterAccess.RW) CR1;		 // offset:0x12 CR1 Control register 1
		@RegInfo("CR2",0x10) RegVolatile!(mask_CR2,uint,0x40011410,0x0,RegisterAccess.RW) CR2;		 // offset:0x16 CR2 Control register 2
		@RegInfo("CR3",0x14) RegVolatile!(mask_CR3,uint,0x40011414,0x0,RegisterAccess.RW) CR3;		 // offset:0x20 CR3 Control register 3
		@RegInfo("GTPR",0x18) RegVolatile!(mask_GTPR,uint,0x40011418,0x0,RegisterAccess.RW) GTPR;		 // offset:0x24 GTPR Guard time and prescaler register
		enum mask_SR {
				CTS = RegBitField!uint(9,1,RegisterAccess.RW),		// CTS flag
				LBD = RegBitField!uint(8,1,RegisterAccess.RW),		// LIN break detection flag
				TXE = RegBitField!uint(7,1,RegisterAccess.RO),		// Transmit data register empty
				TC = RegBitField!uint(6,1,RegisterAccess.RW),		// Transmission complete
				RXNE = RegBitField!uint(5,1,RegisterAccess.RW),		// Read data register not empty
				IDLE = RegBitField!uint(4,1,RegisterAccess.RO),		// IDLE line detected
				ORE = RegBitField!uint(3,1,RegisterAccess.RO),		// Overrun error
				NF = RegBitField!uint(2,1,RegisterAccess.RO),		// Noise detected flag
				FE = RegBitField!uint(1,1,RegisterAccess.RO),		// Framing error
				PE = RegBitField!uint(0,1,RegisterAccess.RO),		// Parity error
		}
		enum mask_DR {
				DR = RegBitField!uint(0,9,RegisterAccess.RW),		// Data value
		}
		enum mask_BRR {
				DIV_Mantissa = RegBitField!uint(4,12,RegisterAccess.RW),		// mantissa of USARTDIV
				DIV_Fraction = RegBitField!uint(0,4,RegisterAccess.RW),		// fraction of USARTDIV
		}
		enum mask_CR1 {
				OVER8 = RegBitField!uint(15,1,RegisterAccess.RW),		// Oversampling mode
				UE = RegBitField!uint(13,1,RegisterAccess.RW),		// USART enable
				M = RegBitField!uint(12,1,RegisterAccess.RW),		// Word length
				WAKE = RegBitField!uint(11,1,RegisterAccess.RW),		// Wakeup method
				PCE = RegBitField!uint(10,1,RegisterAccess.RW),		// Parity control enable
				PS = RegBitField!uint(9,1,RegisterAccess.RW),		// Parity selection
				PEIE = RegBitField!uint(8,1,RegisterAccess.RW),		// PE interrupt enable
				TXEIE = RegBitField!uint(7,1,RegisterAccess.RW),		// TXE interrupt enable
				TCIE = RegBitField!uint(6,1,RegisterAccess.RW),		// Transmission complete interrupt enable
				RXNEIE = RegBitField!uint(5,1,RegisterAccess.RW),		// RXNE interrupt enable
				IDLEIE = RegBitField!uint(4,1,RegisterAccess.RW),		// IDLE interrupt enable
				TE = RegBitField!uint(3,1,RegisterAccess.RW),		// Transmitter enable
				RE = RegBitField!uint(2,1,RegisterAccess.RW),		// Receiver enable
				RWU = RegBitField!uint(1,1,RegisterAccess.RW),		// Receiver wakeup
				SBK = RegBitField!uint(0,1,RegisterAccess.RW),		// Send break
		}
		enum mask_CR2 {
				LINEN = RegBitField!uint(14,1,RegisterAccess.RW),		// LIN mode enable
				STOP = RegBitField!uint(12,2,RegisterAccess.RW),		// STOP bits
				CLKEN = RegBitField!uint(11,1,RegisterAccess.RW),		// Clock enable
				CPOL = RegBitField!uint(10,1,RegisterAccess.RW),		// Clock polarity
				CPHA = RegBitField!uint(9,1,RegisterAccess.RW),		// Clock phase
				LBCL = RegBitField!uint(8,1,RegisterAccess.RW),		// Last bit clock pulse
				LBDIE = RegBitField!uint(6,1,RegisterAccess.RW),		// LIN break detection interrupt enable
				LBDL = RegBitField!uint(5,1,RegisterAccess.RW),		// lin break detection length
				ADD = RegBitField!uint(0,4,RegisterAccess.RW),		// Address of the USART node
		}
		enum mask_CR3 {
				ONEBIT = RegBitField!uint(11,1,RegisterAccess.RW),		// One sample bit method enable
				CTSIE = RegBitField!uint(10,1,RegisterAccess.RW),		// CTS interrupt enable
				CTSE = RegBitField!uint(9,1,RegisterAccess.RW),		// CTS enable
				RTSE = RegBitField!uint(8,1,RegisterAccess.RW),		// RTS enable
				DMAT = RegBitField!uint(7,1,RegisterAccess.RW),		// DMA enable transmitter
				DMAR = RegBitField!uint(6,1,RegisterAccess.RW),		// DMA enable receiver
				SCEN = RegBitField!uint(5,1,RegisterAccess.RW),		// Smartcard mode enable
				NACK = RegBitField!uint(4,1,RegisterAccess.RW),		// Smartcard NACK enable
				HDSEL = RegBitField!uint(3,1,RegisterAccess.RW),		// Half-duplex selection
				IRLP = RegBitField!uint(2,1,RegisterAccess.RW),		// IrDA low-power
				IREN = RegBitField!uint(1,1,RegisterAccess.RW),		// IrDA mode enable
				EIE = RegBitField!uint(0,1,RegisterAccess.RW),		// Error interrupt enable
		}
		enum mask_GTPR {
				GT = RegBitField!uint(8,8,RegisterAccess.RW),		// Guard time value
				PSC = RegBitField!uint(0,8,RegisterAccess.RW),		// Prescaler value
		}
	}
	// FLASH Group 
	// FLASH
	struct FLASH_Type {
		@RegInfo("ACR",0x0) RegVolatile!(mask_ACR,uint,0x40023c00,0x0,RegisterAccess.NO) ACR;		 // offset:0x0 ACR Flash access control register
		@RegInfo("KEYR",0x4) RegVolatile!(mask_KEYR,uint,0x40023c04,0x0,RegisterAccess.WO) KEYR;		 // offset:0x4 KEYR Flash key register
		@RegInfo("OPTKEYR",0x8) RegVolatile!(mask_OPTKEYR,uint,0x40023c08,0x0,RegisterAccess.WO) OPTKEYR;		 // offset:0x8 OPTKEYR Flash option key register
		@RegInfo("SR",0xc) RegVolatile!(mask_SR,uint,0x40023c0c,0x0,RegisterAccess.NO) SR;		 // offset:0x12 SR Status register
		@RegInfo("CR",0x10) RegVolatile!(mask_CR,uint,0x40023c10,0x80000000,RegisterAccess.RW) CR;		 // offset:0x16 CR Control register
		@RegInfo("OPTCR",0x14) RegVolatile!(mask_OPTCR,uint,0x40023c14,0x14,RegisterAccess.RW) OPTCR;		 // offset:0x20 OPTCR Flash option control register
		enum mask_ACR {
				LATENCY = RegBitField!uint(0,3,RegisterAccess.RW),		// Latency
				PRFTEN = RegBitField!uint(8,1,RegisterAccess.RW),		// Prefetch enable
				ICEN = RegBitField!uint(9,1,RegisterAccess.RW),		// Instruction cache enable
				DCEN = RegBitField!uint(10,1,RegisterAccess.RW),		// Data cache enable
				ICRST = RegBitField!uint(11,1,RegisterAccess.WO),		// Instruction cache reset
				DCRST = RegBitField!uint(12,1,RegisterAccess.RW),		// Data cache reset
		}
		enum mask_KEYR {
				KEY = RegBitField!uint(0,32,RegisterAccess.WO),		// FPEC key
		}
		enum mask_OPTKEYR {
				OPTKEY = RegBitField!uint(0,32,RegisterAccess.WO),		// Option byte key
		}
		enum mask_SR {
				EOP = RegBitField!uint(0,1,RegisterAccess.RW),		// End of operation
				OPERR = RegBitField!uint(1,1,RegisterAccess.RW),		// Operation error
				WRPERR = RegBitField!uint(4,1,RegisterAccess.RW),		// Write protection error
				PGAERR = RegBitField!uint(5,1,RegisterAccess.RW),		// Programming alignment error
				PGPERR = RegBitField!uint(6,1,RegisterAccess.RW),		// Programming parallelism error
				PGSERR = RegBitField!uint(7,1,RegisterAccess.RW),		// Programming sequence error
				BSY = RegBitField!uint(16,1,RegisterAccess.RO),		// Busy
		}
		enum mask_CR {
				PG = RegBitField!uint(0,1,RegisterAccess.RW),		// Programming
				SER = RegBitField!uint(1,1,RegisterAccess.RW),		// Sector Erase
				MER = RegBitField!uint(2,1,RegisterAccess.RW),		// Mass Erase
				SNB = RegBitField!uint(3,4,RegisterAccess.RW),		// Sector number
				PSIZE = RegBitField!uint(8,2,RegisterAccess.RW),		// Program size
				STRT = RegBitField!uint(16,1,RegisterAccess.RW),		// Start
				EOPIE = RegBitField!uint(24,1,RegisterAccess.RW),		// End of operation interrupt enable
				ERRIE = RegBitField!uint(25,1,RegisterAccess.RW),		// Error interrupt enable
				LOCK = RegBitField!uint(31,1,RegisterAccess.RW),		// Lock
		}
		enum mask_OPTCR {
				OPTLOCK = RegBitField!uint(0,1,RegisterAccess.RW),		// Option lock
				OPTSTRT = RegBitField!uint(1,1,RegisterAccess.RW),		// Option start
				BOR_LEV = RegBitField!uint(2,2,RegisterAccess.RW),		// BOR reset Level
				WDG_SW = RegBitField!uint(5,1,RegisterAccess.RW),		// WDG_SW User option bytes
				nRST_STOP = RegBitField!uint(6,1,RegisterAccess.RW),		// nRST_STOP User option bytes
				nRST_STDBY = RegBitField!uint(7,1,RegisterAccess.RW),		// nRST_STDBY User option bytes
				RDP = RegBitField!uint(8,8,RegisterAccess.RW),		// Read protect
				nWRP = RegBitField!uint(16,12,RegisterAccess.RW),		// Not write protect
		}
	}
	// STK Group 
	// SysTick timer
	struct STK_Type {
		@RegInfo("CTRL",0x0) RegVolatile!(mask_CTRL,uint,0xe000e010,0x0,RegisterAccess.RW) CTRL;		 // offset:0x0 CTRL SysTick control and status register
		@RegInfo("LOAD",0x4) RegVolatile!(mask_LOAD,uint,0xe000e014,0x0,RegisterAccess.RW) LOAD;		 // offset:0x4 LOAD SysTick reload value register
		@RegInfo("VAL",0x8) RegVolatile!(mask_VAL,uint,0xe000e018,0x0,RegisterAccess.RW) VAL;		 // offset:0x8 VAL SysTick current value register
		@RegInfo("CALIB",0xc) RegVolatile!(mask_CALIB,uint,0xe000e01c,0x0,RegisterAccess.RW) CALIB;		 // offset:0x12 CALIB SysTick calibration value register
		enum mask_CTRL {
				ENABLE = RegBitField!uint(0,1,RegisterAccess.RW),		// Counter enable
				TICKINT = RegBitField!uint(1,1,RegisterAccess.RW),		// SysTick exception request enable
				CLKSOURCE = RegBitField!uint(2,1,RegisterAccess.RW),		// Clock source selection
				COUNTFLAG = RegBitField!uint(16,1,RegisterAccess.RW),		// COUNTFLAG
		}
		enum mask_LOAD {
				RELOAD = RegBitField!uint(0,24,RegisterAccess.RW),		// RELOAD value
		}
		enum mask_VAL {
				CURRENT = RegBitField!uint(0,24,RegisterAccess.RW),		// Current counter value
		}
		enum mask_CALIB {
				TENMS = RegBitField!uint(0,24,RegisterAccess.RW),		// Calibration value
				SKEW = RegBitField!uint(30,1,RegisterAccess.RW),		// SKEW flag: Indicates whether the TENMS value is exact
				NOREF = RegBitField!uint(31,1,RegisterAccess.RW),		// NOREF flag. Reads as zero
		}
	}
	// DMA Group 
	// DMA controller
	struct DMA2_Type {
		@RegInfo("LISR",0x0) RegVolatile!(mask_LISR,uint,0x40026400,0x0,RegisterAccess.RO) LISR;		 // offset:0x0 LISR low interrupt status register
		@RegInfo("HISR",0x4) RegVolatile!(mask_HISR,uint,0x40026404,0x0,RegisterAccess.RO) HISR;		 // offset:0x4 HISR high interrupt status register
		@RegInfo("LIFCR",0x8) RegVolatile!(mask_LIFCR,uint,0x40026408,0x0,RegisterAccess.WO) LIFCR;		 // offset:0x8 LIFCR low interrupt flag clear register
		@RegInfo("HIFCR",0xc) RegVolatile!(mask_HIFCR,uint,0x4002640c,0x0,RegisterAccess.WO) HIFCR;		 // offset:0x12 HIFCR high interrupt flag clear register
		@RegInfo("S0CR",0x10) RegVolatile!(mask_S0CR,uint,0x40026410,0x0,RegisterAccess.RW) S0CR;		 // offset:0x16 S0CR stream x configuration register
		@RegInfo("S0NDTR",0x14) RegVolatile!(mask_S0NDTR,uint,0x40026414,0x0,RegisterAccess.RW) S0NDTR;		 // offset:0x20 S0NDTR stream x number of data register
		@RegInfo("S0PAR",0x18) RegVolatile!(mask_S0PAR,uint,0x40026418,0x0,RegisterAccess.RW) S0PAR;		 // offset:0x24 S0PAR stream x peripheral address register
		@RegInfo("S0M0AR",0x1c) RegVolatile!(mask_S0M0AR,uint,0x4002641c,0x0,RegisterAccess.RW) S0M0AR;		 // offset:0x28 S0M0AR stream x memory 0 address register
		@RegInfo("S0M1AR",0x20) RegVolatile!(mask_S0M1AR,uint,0x40026420,0x0,RegisterAccess.RW) S0M1AR;		 // offset:0x32 S0M1AR stream x memory 1 address register
		@RegInfo("S0FCR",0x24) RegVolatile!(mask_S0FCR,uint,0x40026424,0x21,RegisterAccess.NO) S0FCR;		 // offset:0x36 S0FCR stream x FIFO control register
		@RegInfo("S1CR",0x28) RegVolatile!(mask_S1CR,uint,0x40026428,0x0,RegisterAccess.RW) S1CR;		 // offset:0x40 S1CR stream x configuration register
		@RegInfo("S1NDTR",0x2c) RegVolatile!(mask_S1NDTR,uint,0x4002642c,0x0,RegisterAccess.RW) S1NDTR;		 // offset:0x44 S1NDTR stream x number of data register
		@RegInfo("S1PAR",0x30) RegVolatile!(mask_S1PAR,uint,0x40026430,0x0,RegisterAccess.RW) S1PAR;		 // offset:0x48 S1PAR stream x peripheral address register
		@RegInfo("S1M0AR",0x34) RegVolatile!(mask_S1M0AR,uint,0x40026434,0x0,RegisterAccess.RW) S1M0AR;		 // offset:0x52 S1M0AR stream x memory 0 address register
		@RegInfo("S1M1AR",0x38) RegVolatile!(mask_S1M1AR,uint,0x40026438,0x0,RegisterAccess.RW) S1M1AR;		 // offset:0x56 S1M1AR stream x memory 1 address register
		@RegInfo("S1FCR",0x3c) RegVolatile!(mask_S1FCR,uint,0x4002643c,0x21,RegisterAccess.NO) S1FCR;		 // offset:0x60 S1FCR stream x FIFO control register
		@RegInfo("S2CR",0x40) RegVolatile!(mask_S2CR,uint,0x40026440,0x0,RegisterAccess.RW) S2CR;		 // offset:0x64 S2CR stream x configuration register
		@RegInfo("S2NDTR",0x44) RegVolatile!(mask_S2NDTR,uint,0x40026444,0x0,RegisterAccess.RW) S2NDTR;		 // offset:0x68 S2NDTR stream x number of data register
		@RegInfo("S2PAR",0x48) RegVolatile!(mask_S2PAR,uint,0x40026448,0x0,RegisterAccess.RW) S2PAR;		 // offset:0x72 S2PAR stream x peripheral address register
		@RegInfo("S2M0AR",0x4c) RegVolatile!(mask_S2M0AR,uint,0x4002644c,0x0,RegisterAccess.RW) S2M0AR;		 // offset:0x76 S2M0AR stream x memory 0 address register
		@RegInfo("S2M1AR",0x50) RegVolatile!(mask_S2M1AR,uint,0x40026450,0x0,RegisterAccess.RW) S2M1AR;		 // offset:0x80 S2M1AR stream x memory 1 address register
		@RegInfo("S2FCR",0x54) RegVolatile!(mask_S2FCR,uint,0x40026454,0x21,RegisterAccess.NO) S2FCR;		 // offset:0x84 S2FCR stream x FIFO control register
		@RegInfo("S3CR",0x58) RegVolatile!(mask_S3CR,uint,0x40026458,0x0,RegisterAccess.RW) S3CR;		 // offset:0x88 S3CR stream x configuration register
		@RegInfo("S3NDTR",0x5c) RegVolatile!(mask_S3NDTR,uint,0x4002645c,0x0,RegisterAccess.RW) S3NDTR;		 // offset:0x92 S3NDTR stream x number of data register
		@RegInfo("S3PAR",0x60) RegVolatile!(mask_S3PAR,uint,0x40026460,0x0,RegisterAccess.RW) S3PAR;		 // offset:0x96 S3PAR stream x peripheral address register
		@RegInfo("S3M0AR",0x64) RegVolatile!(mask_S3M0AR,uint,0x40026464,0x0,RegisterAccess.RW) S3M0AR;		 // offset:0x100 S3M0AR stream x memory 0 address register
		@RegInfo("S3M1AR",0x68) RegVolatile!(mask_S3M1AR,uint,0x40026468,0x0,RegisterAccess.RW) S3M1AR;		 // offset:0x104 S3M1AR stream x memory 1 address register
		@RegInfo("S3FCR",0x6c) RegVolatile!(mask_S3FCR,uint,0x4002646c,0x21,RegisterAccess.NO) S3FCR;		 // offset:0x108 S3FCR stream x FIFO control register
		@RegInfo("S4CR",0x70) RegVolatile!(mask_S4CR,uint,0x40026470,0x0,RegisterAccess.RW) S4CR;		 // offset:0x112 S4CR stream x configuration register
		@RegInfo("S4NDTR",0x74) RegVolatile!(mask_S4NDTR,uint,0x40026474,0x0,RegisterAccess.RW) S4NDTR;		 // offset:0x116 S4NDTR stream x number of data register
		@RegInfo("S4PAR",0x78) RegVolatile!(mask_S4PAR,uint,0x40026478,0x0,RegisterAccess.RW) S4PAR;		 // offset:0x120 S4PAR stream x peripheral address register
		@RegInfo("S4M0AR",0x7c) RegVolatile!(mask_S4M0AR,uint,0x4002647c,0x0,RegisterAccess.RW) S4M0AR;		 // offset:0x124 S4M0AR stream x memory 0 address register
		@RegInfo("S4M1AR",0x80) RegVolatile!(mask_S4M1AR,uint,0x40026480,0x0,RegisterAccess.RW) S4M1AR;		 // offset:0x128 S4M1AR stream x memory 1 address register
		@RegInfo("S4FCR",0x84) RegVolatile!(mask_S4FCR,uint,0x40026484,0x21,RegisterAccess.NO) S4FCR;		 // offset:0x132 S4FCR stream x FIFO control register
		@RegInfo("S5CR",0x88) RegVolatile!(mask_S5CR,uint,0x40026488,0x0,RegisterAccess.RW) S5CR;		 // offset:0x136 S5CR stream x configuration register
		@RegInfo("S5NDTR",0x8c) RegVolatile!(mask_S5NDTR,uint,0x4002648c,0x0,RegisterAccess.RW) S5NDTR;		 // offset:0x140 S5NDTR stream x number of data register
		@RegInfo("S5PAR",0x90) RegVolatile!(mask_S5PAR,uint,0x40026490,0x0,RegisterAccess.RW) S5PAR;		 // offset:0x144 S5PAR stream x peripheral address register
		@RegInfo("S5M0AR",0x94) RegVolatile!(mask_S5M0AR,uint,0x40026494,0x0,RegisterAccess.RW) S5M0AR;		 // offset:0x148 S5M0AR stream x memory 0 address register
		@RegInfo("S5M1AR",0x98) RegVolatile!(mask_S5M1AR,uint,0x40026498,0x0,RegisterAccess.RW) S5M1AR;		 // offset:0x152 S5M1AR stream x memory 1 address register
		@RegInfo("S5FCR",0x9c) RegVolatile!(mask_S5FCR,uint,0x4002649c,0x21,RegisterAccess.NO) S5FCR;		 // offset:0x156 S5FCR stream x FIFO control register
		@RegInfo("S6CR",0xa0) RegVolatile!(mask_S6CR,uint,0x400264a0,0x0,RegisterAccess.RW) S6CR;		 // offset:0x160 S6CR stream x configuration register
		@RegInfo("S6NDTR",0xa4) RegVolatile!(mask_S6NDTR,uint,0x400264a4,0x0,RegisterAccess.RW) S6NDTR;		 // offset:0x164 S6NDTR stream x number of data register
		@RegInfo("S6PAR",0xa8) RegVolatile!(mask_S6PAR,uint,0x400264a8,0x0,RegisterAccess.RW) S6PAR;		 // offset:0x168 S6PAR stream x peripheral address register
		@RegInfo("S6M0AR",0xac) RegVolatile!(mask_S6M0AR,uint,0x400264ac,0x0,RegisterAccess.RW) S6M0AR;		 // offset:0x172 S6M0AR stream x memory 0 address register
		@RegInfo("S6M1AR",0xb0) RegVolatile!(mask_S6M1AR,uint,0x400264b0,0x0,RegisterAccess.RW) S6M1AR;		 // offset:0x176 S6M1AR stream x memory 1 address register
		@RegInfo("S6FCR",0xb4) RegVolatile!(mask_S6FCR,uint,0x400264b4,0x21,RegisterAccess.NO) S6FCR;		 // offset:0x180 S6FCR stream x FIFO control register
		@RegInfo("S7CR",0xb8) RegVolatile!(mask_S7CR,uint,0x400264b8,0x0,RegisterAccess.RW) S7CR;		 // offset:0x184 S7CR stream x configuration register
		@RegInfo("S7NDTR",0xbc) RegVolatile!(mask_S7NDTR,uint,0x400264bc,0x0,RegisterAccess.RW) S7NDTR;		 // offset:0x188 S7NDTR stream x number of data register
		@RegInfo("S7PAR",0xc0) RegVolatile!(mask_S7PAR,uint,0x400264c0,0x0,RegisterAccess.RW) S7PAR;		 // offset:0x192 S7PAR stream x peripheral address register
		@RegInfo("S7M0AR",0xc4) RegVolatile!(mask_S7M0AR,uint,0x400264c4,0x0,RegisterAccess.RW) S7M0AR;		 // offset:0x196 S7M0AR stream x memory 0 address register
		@RegInfo("S7M1AR",0xc8) RegVolatile!(mask_S7M1AR,uint,0x400264c8,0x0,RegisterAccess.RW) S7M1AR;		 // offset:0x200 S7M1AR stream x memory 1 address register
		@RegInfo("S7FCR",0xcc) RegVolatile!(mask_S7FCR,uint,0x400264cc,0x21,RegisterAccess.NO) S7FCR;		 // offset:0x204 S7FCR stream x FIFO control register
		enum mask_LISR {
				TCIF3 = RegBitField!uint(27,1,RegisterAccess.RO),		// Stream x transfer complete interrupt flag (x = 3..0)
				HTIF3 = RegBitField!uint(26,1,RegisterAccess.RO),		// Stream x half transfer interrupt flag (x=3..0)
				TEIF3 = RegBitField!uint(25,1,RegisterAccess.RO),		// Stream x transfer error interrupt flag (x=3..0)
				DMEIF3 = RegBitField!uint(24,1,RegisterAccess.RO),		// Stream x direct mode error interrupt flag (x=3..0)
				FEIF3 = RegBitField!uint(22,1,RegisterAccess.RO),		// Stream x FIFO error interrupt flag (x=3..0)
				TCIF2 = RegBitField!uint(21,1,RegisterAccess.RO),		// Stream x transfer complete interrupt flag (x = 3..0)
				HTIF2 = RegBitField!uint(20,1,RegisterAccess.RO),		// Stream x half transfer interrupt flag (x=3..0)
				TEIF2 = RegBitField!uint(19,1,RegisterAccess.RO),		// Stream x transfer error interrupt flag (x=3..0)
				DMEIF2 = RegBitField!uint(18,1,RegisterAccess.RO),		// Stream x direct mode error interrupt flag (x=3..0)
				FEIF2 = RegBitField!uint(16,1,RegisterAccess.RO),		// Stream x FIFO error interrupt flag (x=3..0)
				TCIF1 = RegBitField!uint(11,1,RegisterAccess.RO),		// Stream x transfer complete interrupt flag (x = 3..0)
				HTIF1 = RegBitField!uint(10,1,RegisterAccess.RO),		// Stream x half transfer interrupt flag (x=3..0)
				TEIF1 = RegBitField!uint(9,1,RegisterAccess.RO),		// Stream x transfer error interrupt flag (x=3..0)
				DMEIF1 = RegBitField!uint(8,1,RegisterAccess.RO),		// Stream x direct mode error interrupt flag (x=3..0)
				FEIF1 = RegBitField!uint(6,1,RegisterAccess.RO),		// Stream x FIFO error interrupt flag (x=3..0)
				TCIF0 = RegBitField!uint(5,1,RegisterAccess.RO),		// Stream x transfer complete interrupt flag (x = 3..0)
				HTIF0 = RegBitField!uint(4,1,RegisterAccess.RO),		// Stream x half transfer interrupt flag (x=3..0)
				TEIF0 = RegBitField!uint(3,1,RegisterAccess.RO),		// Stream x transfer error interrupt flag (x=3..0)
				DMEIF0 = RegBitField!uint(2,1,RegisterAccess.RO),		// Stream x direct mode error interrupt flag (x=3..0)
				FEIF0 = RegBitField!uint(0,1,RegisterAccess.RO),		// Stream x FIFO error interrupt flag (x=3..0)
		}
		enum mask_HISR {
				TCIF7 = RegBitField!uint(27,1,RegisterAccess.RO),		// Stream x transfer complete interrupt flag (x=7..4)
				HTIF7 = RegBitField!uint(26,1,RegisterAccess.RO),		// Stream x half transfer interrupt flag (x=7..4)
				TEIF7 = RegBitField!uint(25,1,RegisterAccess.RO),		// Stream x transfer error interrupt flag (x=7..4)
				DMEIF7 = RegBitField!uint(24,1,RegisterAccess.RO),		// Stream x direct mode error interrupt flag (x=7..4)
				FEIF7 = RegBitField!uint(22,1,RegisterAccess.RO),		// Stream x FIFO error interrupt flag (x=7..4)
				TCIF6 = RegBitField!uint(21,1,RegisterAccess.RO),		// Stream x transfer complete interrupt flag (x=7..4)
				HTIF6 = RegBitField!uint(20,1,RegisterAccess.RO),		// Stream x half transfer interrupt flag (x=7..4)
				TEIF6 = RegBitField!uint(19,1,RegisterAccess.RO),		// Stream x transfer error interrupt flag (x=7..4)
				DMEIF6 = RegBitField!uint(18,1,RegisterAccess.RO),		// Stream x direct mode error interrupt flag (x=7..4)
				FEIF6 = RegBitField!uint(16,1,RegisterAccess.RO),		// Stream x FIFO error interrupt flag (x=7..4)
				TCIF5 = RegBitField!uint(11,1,RegisterAccess.RO),		// Stream x transfer complete interrupt flag (x=7..4)
				HTIF5 = RegBitField!uint(10,1,RegisterAccess.RO),		// Stream x half transfer interrupt flag (x=7..4)
				TEIF5 = RegBitField!uint(9,1,RegisterAccess.RO),		// Stream x transfer error interrupt flag (x=7..4)
				DMEIF5 = RegBitField!uint(8,1,RegisterAccess.RO),		// Stream x direct mode error interrupt flag (x=7..4)
				FEIF5 = RegBitField!uint(6,1,RegisterAccess.RO),		// Stream x FIFO error interrupt flag (x=7..4)
				TCIF4 = RegBitField!uint(5,1,RegisterAccess.RO),		// Stream x transfer complete interrupt flag (x=7..4)
				HTIF4 = RegBitField!uint(4,1,RegisterAccess.RO),		// Stream x half transfer interrupt flag (x=7..4)
				TEIF4 = RegBitField!uint(3,1,RegisterAccess.RO),		// Stream x transfer error interrupt flag (x=7..4)
				DMEIF4 = RegBitField!uint(2,1,RegisterAccess.RO),		// Stream x direct mode error interrupt flag (x=7..4)
				FEIF4 = RegBitField!uint(0,1,RegisterAccess.RO),		// Stream x FIFO error interrupt flag (x=7..4)
		}
		enum mask_LIFCR {
				CTCIF3 = RegBitField!uint(27,1,RegisterAccess.WO),		// Stream x clear transfer complete interrupt flag (x = 3..0)
				CHTIF3 = RegBitField!uint(26,1,RegisterAccess.WO),		// Stream x clear half transfer interrupt flag (x = 3..0)
				CTEIF3 = RegBitField!uint(25,1,RegisterAccess.WO),		// Stream x clear transfer error interrupt flag (x = 3..0)
				CDMEIF3 = RegBitField!uint(24,1,RegisterAccess.WO),		// Stream x clear direct mode error interrupt flag (x = 3..0)
				CFEIF3 = RegBitField!uint(22,1,RegisterAccess.WO),		// Stream x clear FIFO error interrupt flag (x = 3..0)
				CTCIF2 = RegBitField!uint(21,1,RegisterAccess.WO),		// Stream x clear transfer complete interrupt flag (x = 3..0)
				CHTIF2 = RegBitField!uint(20,1,RegisterAccess.WO),		// Stream x clear half transfer interrupt flag (x = 3..0)
				CTEIF2 = RegBitField!uint(19,1,RegisterAccess.WO),		// Stream x clear transfer error interrupt flag (x = 3..0)
				CDMEIF2 = RegBitField!uint(18,1,RegisterAccess.WO),		// Stream x clear direct mode error interrupt flag (x = 3..0)
				CFEIF2 = RegBitField!uint(16,1,RegisterAccess.WO),		// Stream x clear FIFO error interrupt flag (x = 3..0)
				CTCIF1 = RegBitField!uint(11,1,RegisterAccess.WO),		// Stream x clear transfer complete interrupt flag (x = 3..0)
				CHTIF1 = RegBitField!uint(10,1,RegisterAccess.WO),		// Stream x clear half transfer interrupt flag (x = 3..0)
				CTEIF1 = RegBitField!uint(9,1,RegisterAccess.WO),		// Stream x clear transfer error interrupt flag (x = 3..0)
				CDMEIF1 = RegBitField!uint(8,1,RegisterAccess.WO),		// Stream x clear direct mode error interrupt flag (x = 3..0)
				CFEIF1 = RegBitField!uint(6,1,RegisterAccess.WO),		// Stream x clear FIFO error interrupt flag (x = 3..0)
				CTCIF0 = RegBitField!uint(5,1,RegisterAccess.WO),		// Stream x clear transfer complete interrupt flag (x = 3..0)
				CHTIF0 = RegBitField!uint(4,1,RegisterAccess.WO),		// Stream x clear half transfer interrupt flag (x = 3..0)
				CTEIF0 = RegBitField!uint(3,1,RegisterAccess.WO),		// Stream x clear transfer error interrupt flag (x = 3..0)
				CDMEIF0 = RegBitField!uint(2,1,RegisterAccess.WO),		// Stream x clear direct mode error interrupt flag (x = 3..0)
				CFEIF0 = RegBitField!uint(0,1,RegisterAccess.WO),		// Stream x clear FIFO error interrupt flag (x = 3..0)
		}
		enum mask_HIFCR {
				CTCIF7 = RegBitField!uint(27,1,RegisterAccess.WO),		// Stream x clear transfer complete interrupt flag (x = 7..4)
				CHTIF7 = RegBitField!uint(26,1,RegisterAccess.WO),		// Stream x clear half transfer interrupt flag (x = 7..4)
				CTEIF7 = RegBitField!uint(25,1,RegisterAccess.WO),		// Stream x clear transfer error interrupt flag (x = 7..4)
				CDMEIF7 = RegBitField!uint(24,1,RegisterAccess.WO),		// Stream x clear direct mode error interrupt flag (x = 7..4)
				CFEIF7 = RegBitField!uint(22,1,RegisterAccess.WO),		// Stream x clear FIFO error interrupt flag (x = 7..4)
				CTCIF6 = RegBitField!uint(21,1,RegisterAccess.WO),		// Stream x clear transfer complete interrupt flag (x = 7..4)
				CHTIF6 = RegBitField!uint(20,1,RegisterAccess.WO),		// Stream x clear half transfer interrupt flag (x = 7..4)
				CTEIF6 = RegBitField!uint(19,1,RegisterAccess.WO),		// Stream x clear transfer error interrupt flag (x = 7..4)
				CDMEIF6 = RegBitField!uint(18,1,RegisterAccess.WO),		// Stream x clear direct mode error interrupt flag (x = 7..4)
				CFEIF6 = RegBitField!uint(16,1,RegisterAccess.WO),		// Stream x clear FIFO error interrupt flag (x = 7..4)
				CTCIF5 = RegBitField!uint(11,1,RegisterAccess.WO),		// Stream x clear transfer complete interrupt flag (x = 7..4)
				CHTIF5 = RegBitField!uint(10,1,RegisterAccess.WO),		// Stream x clear half transfer interrupt flag (x = 7..4)
				CTEIF5 = RegBitField!uint(9,1,RegisterAccess.WO),		// Stream x clear transfer error interrupt flag (x = 7..4)
				CDMEIF5 = RegBitField!uint(8,1,RegisterAccess.WO),		// Stream x clear direct mode error interrupt flag (x = 7..4)
				CFEIF5 = RegBitField!uint(6,1,RegisterAccess.WO),		// Stream x clear FIFO error interrupt flag (x = 7..4)
				CTCIF4 = RegBitField!uint(5,1,RegisterAccess.WO),		// Stream x clear transfer complete interrupt flag (x = 7..4)
				CHTIF4 = RegBitField!uint(4,1,RegisterAccess.WO),		// Stream x clear half transfer interrupt flag (x = 7..4)
				CTEIF4 = RegBitField!uint(3,1,RegisterAccess.WO),		// Stream x clear transfer error interrupt flag (x = 7..4)
				CDMEIF4 = RegBitField!uint(2,1,RegisterAccess.WO),		// Stream x clear direct mode error interrupt flag (x = 7..4)
				CFEIF4 = RegBitField!uint(0,1,RegisterAccess.WO),		// Stream x clear FIFO error interrupt flag (x = 7..4)
		}
		enum mask_S0CR {
				CHSEL = RegBitField!uint(25,3,RegisterAccess.RW),		// Channel selection
				MBURST = RegBitField!uint(23,2,RegisterAccess.RW),		// Memory burst transfer configuration
				PBURST = RegBitField!uint(21,2,RegisterAccess.RW),		// Peripheral burst transfer configuration
				CT = RegBitField!uint(19,1,RegisterAccess.RW),		// Current target (only in double buffer mode)
				DBM = RegBitField!uint(18,1,RegisterAccess.RW),		// Double buffer mode
				PL = RegBitField!uint(16,2,RegisterAccess.RW),		// Priority level
				PINCOS = RegBitField!uint(15,1,RegisterAccess.RW),		// Peripheral increment offset size
				MSIZE = RegBitField!uint(13,2,RegisterAccess.RW),		// Memory data size
				PSIZE = RegBitField!uint(11,2,RegisterAccess.RW),		// Peripheral data size
				MINC = RegBitField!uint(10,1,RegisterAccess.RW),		// Memory increment mode
				PINC = RegBitField!uint(9,1,RegisterAccess.RW),		// Peripheral increment mode
				CIRC = RegBitField!uint(8,1,RegisterAccess.RW),		// Circular mode
				DIR = RegBitField!uint(6,2,RegisterAccess.RW),		// Data transfer direction
				PFCTRL = RegBitField!uint(5,1,RegisterAccess.RW),		// Peripheral flow controller
				TCIE = RegBitField!uint(4,1,RegisterAccess.RW),		// Transfer complete interrupt enable
				HTIE = RegBitField!uint(3,1,RegisterAccess.RW),		// Half transfer interrupt enable
				TEIE = RegBitField!uint(2,1,RegisterAccess.RW),		// Transfer error interrupt enable
				DMEIE = RegBitField!uint(1,1,RegisterAccess.RW),		// Direct mode error interrupt enable
				EN = RegBitField!uint(0,1,RegisterAccess.RW),		// Stream enable / flag stream ready when read low
		}
		enum mask_S0NDTR {
				NDT = RegBitField!uint(0,16,RegisterAccess.RW),		// Number of data items to transfer
		}
		enum mask_S0PAR {
				PA = RegBitField!uint(0,32,RegisterAccess.RW),		// Peripheral address
		}
		enum mask_S0M0AR {
				M0A = RegBitField!uint(0,32,RegisterAccess.RW),		// Memory 0 address
		}
		enum mask_S0M1AR {
				M1A = RegBitField!uint(0,32,RegisterAccess.RW),		// Memory 1 address (used in case of Double buffer mode)
		}
		enum mask_S0FCR {
				FEIE = RegBitField!uint(7,1,RegisterAccess.RW),		// FIFO error interrupt enable
				FS = RegBitField!uint(3,3,RegisterAccess.RO),		// FIFO status
				DMDIS = RegBitField!uint(2,1,RegisterAccess.RW),		// Direct mode disable
				FTH = RegBitField!uint(0,2,RegisterAccess.RW),		// FIFO threshold selection
		}
		enum mask_S1CR {
				CHSEL = RegBitField!uint(25,3,RegisterAccess.RW),		// Channel selection
				MBURST = RegBitField!uint(23,2,RegisterAccess.RW),		// Memory burst transfer configuration
				PBURST = RegBitField!uint(21,2,RegisterAccess.RW),		// Peripheral burst transfer configuration
				ACK = RegBitField!uint(20,1,RegisterAccess.RW),		// ACK
				CT = RegBitField!uint(19,1,RegisterAccess.RW),		// Current target (only in double buffer mode)
				DBM = RegBitField!uint(18,1,RegisterAccess.RW),		// Double buffer mode
				PL = RegBitField!uint(16,2,RegisterAccess.RW),		// Priority level
				PINCOS = RegBitField!uint(15,1,RegisterAccess.RW),		// Peripheral increment offset size
				MSIZE = RegBitField!uint(13,2,RegisterAccess.RW),		// Memory data size
				PSIZE = RegBitField!uint(11,2,RegisterAccess.RW),		// Peripheral data size
				MINC = RegBitField!uint(10,1,RegisterAccess.RW),		// Memory increment mode
				PINC = RegBitField!uint(9,1,RegisterAccess.RW),		// Peripheral increment mode
				CIRC = RegBitField!uint(8,1,RegisterAccess.RW),		// Circular mode
				DIR = RegBitField!uint(6,2,RegisterAccess.RW),		// Data transfer direction
				PFCTRL = RegBitField!uint(5,1,RegisterAccess.RW),		// Peripheral flow controller
				TCIE = RegBitField!uint(4,1,RegisterAccess.RW),		// Transfer complete interrupt enable
				HTIE = RegBitField!uint(3,1,RegisterAccess.RW),		// Half transfer interrupt enable
				TEIE = RegBitField!uint(2,1,RegisterAccess.RW),		// Transfer error interrupt enable
				DMEIE = RegBitField!uint(1,1,RegisterAccess.RW),		// Direct mode error interrupt enable
				EN = RegBitField!uint(0,1,RegisterAccess.RW),		// Stream enable / flag stream ready when read low
		}
		enum mask_S1NDTR {
				NDT = RegBitField!uint(0,16,RegisterAccess.RW),		// Number of data items to transfer
		}
		enum mask_S1PAR {
				PA = RegBitField!uint(0,32,RegisterAccess.RW),		// Peripheral address
		}
		enum mask_S1M0AR {
				M0A = RegBitField!uint(0,32,RegisterAccess.RW),		// Memory 0 address
		}
		enum mask_S1M1AR {
				M1A = RegBitField!uint(0,32,RegisterAccess.RW),		// Memory 1 address (used in case of Double buffer mode)
		}
		enum mask_S1FCR {
				FEIE = RegBitField!uint(7,1,RegisterAccess.RW),		// FIFO error interrupt enable
				FS = RegBitField!uint(3,3,RegisterAccess.RO),		// FIFO status
				DMDIS = RegBitField!uint(2,1,RegisterAccess.RW),		// Direct mode disable
				FTH = RegBitField!uint(0,2,RegisterAccess.RW),		// FIFO threshold selection
		}
		enum mask_S2CR {
				CHSEL = RegBitField!uint(25,3,RegisterAccess.RW),		// Channel selection
				MBURST = RegBitField!uint(23,2,RegisterAccess.RW),		// Memory burst transfer configuration
				PBURST = RegBitField!uint(21,2,RegisterAccess.RW),		// Peripheral burst transfer configuration
				ACK = RegBitField!uint(20,1,RegisterAccess.RW),		// ACK
				CT = RegBitField!uint(19,1,RegisterAccess.RW),		// Current target (only in double buffer mode)
				DBM = RegBitField!uint(18,1,RegisterAccess.RW),		// Double buffer mode
				PL = RegBitField!uint(16,2,RegisterAccess.RW),		// Priority level
				PINCOS = RegBitField!uint(15,1,RegisterAccess.RW),		// Peripheral increment offset size
				MSIZE = RegBitField!uint(13,2,RegisterAccess.RW),		// Memory data size
				PSIZE = RegBitField!uint(11,2,RegisterAccess.RW),		// Peripheral data size
				MINC = RegBitField!uint(10,1,RegisterAccess.RW),		// Memory increment mode
				PINC = RegBitField!uint(9,1,RegisterAccess.RW),		// Peripheral increment mode
				CIRC = RegBitField!uint(8,1,RegisterAccess.RW),		// Circular mode
				DIR = RegBitField!uint(6,2,RegisterAccess.RW),		// Data transfer direction
				PFCTRL = RegBitField!uint(5,1,RegisterAccess.RW),		// Peripheral flow controller
				TCIE = RegBitField!uint(4,1,RegisterAccess.RW),		// Transfer complete interrupt enable
				HTIE = RegBitField!uint(3,1,RegisterAccess.RW),		// Half transfer interrupt enable
				TEIE = RegBitField!uint(2,1,RegisterAccess.RW),		// Transfer error interrupt enable
				DMEIE = RegBitField!uint(1,1,RegisterAccess.RW),		// Direct mode error interrupt enable
				EN = RegBitField!uint(0,1,RegisterAccess.RW),		// Stream enable / flag stream ready when read low
		}
		enum mask_S2NDTR {
				NDT = RegBitField!uint(0,16,RegisterAccess.RW),		// Number of data items to transfer
		}
		enum mask_S2PAR {
				PA = RegBitField!uint(0,32,RegisterAccess.RW),		// Peripheral address
		}
		enum mask_S2M0AR {
				M0A = RegBitField!uint(0,32,RegisterAccess.RW),		// Memory 0 address
		}
		enum mask_S2M1AR {
				M1A = RegBitField!uint(0,32,RegisterAccess.RW),		// Memory 1 address (used in case of Double buffer mode)
		}
		enum mask_S2FCR {
				FEIE = RegBitField!uint(7,1,RegisterAccess.RW),		// FIFO error interrupt enable
				FS = RegBitField!uint(3,3,RegisterAccess.RO),		// FIFO status
				DMDIS = RegBitField!uint(2,1,RegisterAccess.RW),		// Direct mode disable
				FTH = RegBitField!uint(0,2,RegisterAccess.RW),		// FIFO threshold selection
		}
		enum mask_S3CR {
				CHSEL = RegBitField!uint(25,3,RegisterAccess.RW),		// Channel selection
				MBURST = RegBitField!uint(23,2,RegisterAccess.RW),		// Memory burst transfer configuration
				PBURST = RegBitField!uint(21,2,RegisterAccess.RW),		// Peripheral burst transfer configuration
				ACK = RegBitField!uint(20,1,RegisterAccess.RW),		// ACK
				CT = RegBitField!uint(19,1,RegisterAccess.RW),		// Current target (only in double buffer mode)
				DBM = RegBitField!uint(18,1,RegisterAccess.RW),		// Double buffer mode
				PL = RegBitField!uint(16,2,RegisterAccess.RW),		// Priority level
				PINCOS = RegBitField!uint(15,1,RegisterAccess.RW),		// Peripheral increment offset size
				MSIZE = RegBitField!uint(13,2,RegisterAccess.RW),		// Memory data size
				PSIZE = RegBitField!uint(11,2,RegisterAccess.RW),		// Peripheral data size
				MINC = RegBitField!uint(10,1,RegisterAccess.RW),		// Memory increment mode
				PINC = RegBitField!uint(9,1,RegisterAccess.RW),		// Peripheral increment mode
				CIRC = RegBitField!uint(8,1,RegisterAccess.RW),		// Circular mode
				DIR = RegBitField!uint(6,2,RegisterAccess.RW),		// Data transfer direction
				PFCTRL = RegBitField!uint(5,1,RegisterAccess.RW),		// Peripheral flow controller
				TCIE = RegBitField!uint(4,1,RegisterAccess.RW),		// Transfer complete interrupt enable
				HTIE = RegBitField!uint(3,1,RegisterAccess.RW),		// Half transfer interrupt enable
				TEIE = RegBitField!uint(2,1,RegisterAccess.RW),		// Transfer error interrupt enable
				DMEIE = RegBitField!uint(1,1,RegisterAccess.RW),		// Direct mode error interrupt enable
				EN = RegBitField!uint(0,1,RegisterAccess.RW),		// Stream enable / flag stream ready when read low
		}
		enum mask_S3NDTR {
				NDT = RegBitField!uint(0,16,RegisterAccess.RW),		// Number of data items to transfer
		}
		enum mask_S3PAR {
				PA = RegBitField!uint(0,32,RegisterAccess.RW),		// Peripheral address
		}
		enum mask_S3M0AR {
				M0A = RegBitField!uint(0,32,RegisterAccess.RW),		// Memory 0 address
		}
		enum mask_S3M1AR {
				M1A = RegBitField!uint(0,32,RegisterAccess.RW),		// Memory 1 address (used in case of Double buffer mode)
		}
		enum mask_S3FCR {
				FEIE = RegBitField!uint(7,1,RegisterAccess.RW),		// FIFO error interrupt enable
				FS = RegBitField!uint(3,3,RegisterAccess.RO),		// FIFO status
				DMDIS = RegBitField!uint(2,1,RegisterAccess.RW),		// Direct mode disable
				FTH = RegBitField!uint(0,2,RegisterAccess.RW),		// FIFO threshold selection
		}
		enum mask_S4CR {
				CHSEL = RegBitField!uint(25,3,RegisterAccess.RW),		// Channel selection
				MBURST = RegBitField!uint(23,2,RegisterAccess.RW),		// Memory burst transfer configuration
				PBURST = RegBitField!uint(21,2,RegisterAccess.RW),		// Peripheral burst transfer configuration
				ACK = RegBitField!uint(20,1,RegisterAccess.RW),		// ACK
				CT = RegBitField!uint(19,1,RegisterAccess.RW),		// Current target (only in double buffer mode)
				DBM = RegBitField!uint(18,1,RegisterAccess.RW),		// Double buffer mode
				PL = RegBitField!uint(16,2,RegisterAccess.RW),		// Priority level
				PINCOS = RegBitField!uint(15,1,RegisterAccess.RW),		// Peripheral increment offset size
				MSIZE = RegBitField!uint(13,2,RegisterAccess.RW),		// Memory data size
				PSIZE = RegBitField!uint(11,2,RegisterAccess.RW),		// Peripheral data size
				MINC = RegBitField!uint(10,1,RegisterAccess.RW),		// Memory increment mode
				PINC = RegBitField!uint(9,1,RegisterAccess.RW),		// Peripheral increment mode
				CIRC = RegBitField!uint(8,1,RegisterAccess.RW),		// Circular mode
				DIR = RegBitField!uint(6,2,RegisterAccess.RW),		// Data transfer direction
				PFCTRL = RegBitField!uint(5,1,RegisterAccess.RW),		// Peripheral flow controller
				TCIE = RegBitField!uint(4,1,RegisterAccess.RW),		// Transfer complete interrupt enable
				HTIE = RegBitField!uint(3,1,RegisterAccess.RW),		// Half transfer interrupt enable
				TEIE = RegBitField!uint(2,1,RegisterAccess.RW),		// Transfer error interrupt enable
				DMEIE = RegBitField!uint(1,1,RegisterAccess.RW),		// Direct mode error interrupt enable
				EN = RegBitField!uint(0,1,RegisterAccess.RW),		// Stream enable / flag stream ready when read low
		}
		enum mask_S4NDTR {
				NDT = RegBitField!uint(0,16,RegisterAccess.RW),		// Number of data items to transfer
		}
		enum mask_S4PAR {
				PA = RegBitField!uint(0,32,RegisterAccess.RW),		// Peripheral address
		}
		enum mask_S4M0AR {
				M0A = RegBitField!uint(0,32,RegisterAccess.RW),		// Memory 0 address
		}
		enum mask_S4M1AR {
				M1A = RegBitField!uint(0,32,RegisterAccess.RW),		// Memory 1 address (used in case of Double buffer mode)
		}
		enum mask_S4FCR {
				FEIE = RegBitField!uint(7,1,RegisterAccess.RW),		// FIFO error interrupt enable
				FS = RegBitField!uint(3,3,RegisterAccess.RO),		// FIFO status
				DMDIS = RegBitField!uint(2,1,RegisterAccess.RW),		// Direct mode disable
				FTH = RegBitField!uint(0,2,RegisterAccess.RW),		// FIFO threshold selection
		}
		enum mask_S5CR {
				CHSEL = RegBitField!uint(25,3,RegisterAccess.RW),		// Channel selection
				MBURST = RegBitField!uint(23,2,RegisterAccess.RW),		// Memory burst transfer configuration
				PBURST = RegBitField!uint(21,2,RegisterAccess.RW),		// Peripheral burst transfer configuration
				ACK = RegBitField!uint(20,1,RegisterAccess.RW),		// ACK
				CT = RegBitField!uint(19,1,RegisterAccess.RW),		// Current target (only in double buffer mode)
				DBM = RegBitField!uint(18,1,RegisterAccess.RW),		// Double buffer mode
				PL = RegBitField!uint(16,2,RegisterAccess.RW),		// Priority level
				PINCOS = RegBitField!uint(15,1,RegisterAccess.RW),		// Peripheral increment offset size
				MSIZE = RegBitField!uint(13,2,RegisterAccess.RW),		// Memory data size
				PSIZE = RegBitField!uint(11,2,RegisterAccess.RW),		// Peripheral data size
				MINC = RegBitField!uint(10,1,RegisterAccess.RW),		// Memory increment mode
				PINC = RegBitField!uint(9,1,RegisterAccess.RW),		// Peripheral increment mode
				CIRC = RegBitField!uint(8,1,RegisterAccess.RW),		// Circular mode
				DIR = RegBitField!uint(6,2,RegisterAccess.RW),		// Data transfer direction
				PFCTRL = RegBitField!uint(5,1,RegisterAccess.RW),		// Peripheral flow controller
				TCIE = RegBitField!uint(4,1,RegisterAccess.RW),		// Transfer complete interrupt enable
				HTIE = RegBitField!uint(3,1,RegisterAccess.RW),		// Half transfer interrupt enable
				TEIE = RegBitField!uint(2,1,RegisterAccess.RW),		// Transfer error interrupt enable
				DMEIE = RegBitField!uint(1,1,RegisterAccess.RW),		// Direct mode error interrupt enable
				EN = RegBitField!uint(0,1,RegisterAccess.RW),		// Stream enable / flag stream ready when read low
		}
		enum mask_S5NDTR {
				NDT = RegBitField!uint(0,16,RegisterAccess.RW),		// Number of data items to transfer
		}
		enum mask_S5PAR {
				PA = RegBitField!uint(0,32,RegisterAccess.RW),		// Peripheral address
		}
		enum mask_S5M0AR {
				M0A = RegBitField!uint(0,32,RegisterAccess.RW),		// Memory 0 address
		}
		enum mask_S5M1AR {
				M1A = RegBitField!uint(0,32,RegisterAccess.RW),		// Memory 1 address (used in case of Double buffer mode)
		}
		enum mask_S5FCR {
				FEIE = RegBitField!uint(7,1,RegisterAccess.RW),		// FIFO error interrupt enable
				FS = RegBitField!uint(3,3,RegisterAccess.RO),		// FIFO status
				DMDIS = RegBitField!uint(2,1,RegisterAccess.RW),		// Direct mode disable
				FTH = RegBitField!uint(0,2,RegisterAccess.RW),		// FIFO threshold selection
		}
		enum mask_S6CR {
				CHSEL = RegBitField!uint(25,3,RegisterAccess.RW),		// Channel selection
				MBURST = RegBitField!uint(23,2,RegisterAccess.RW),		// Memory burst transfer configuration
				PBURST = RegBitField!uint(21,2,RegisterAccess.RW),		// Peripheral burst transfer configuration
				ACK = RegBitField!uint(20,1,RegisterAccess.RW),		// ACK
				CT = RegBitField!uint(19,1,RegisterAccess.RW),		// Current target (only in double buffer mode)
				DBM = RegBitField!uint(18,1,RegisterAccess.RW),		// Double buffer mode
				PL = RegBitField!uint(16,2,RegisterAccess.RW),		// Priority level
				PINCOS = RegBitField!uint(15,1,RegisterAccess.RW),		// Peripheral increment offset size
				MSIZE = RegBitField!uint(13,2,RegisterAccess.RW),		// Memory data size
				PSIZE = RegBitField!uint(11,2,RegisterAccess.RW),		// Peripheral data size
				MINC = RegBitField!uint(10,1,RegisterAccess.RW),		// Memory increment mode
				PINC = RegBitField!uint(9,1,RegisterAccess.RW),		// Peripheral increment mode
				CIRC = RegBitField!uint(8,1,RegisterAccess.RW),		// Circular mode
				DIR = RegBitField!uint(6,2,RegisterAccess.RW),		// Data transfer direction
				PFCTRL = RegBitField!uint(5,1,RegisterAccess.RW),		// Peripheral flow controller
				TCIE = RegBitField!uint(4,1,RegisterAccess.RW),		// Transfer complete interrupt enable
				HTIE = RegBitField!uint(3,1,RegisterAccess.RW),		// Half transfer interrupt enable
				TEIE = RegBitField!uint(2,1,RegisterAccess.RW),		// Transfer error interrupt enable
				DMEIE = RegBitField!uint(1,1,RegisterAccess.RW),		// Direct mode error interrupt enable
				EN = RegBitField!uint(0,1,RegisterAccess.RW),		// Stream enable / flag stream ready when read low
		}
		enum mask_S6NDTR {
				NDT = RegBitField!uint(0,16,RegisterAccess.RW),		// Number of data items to transfer
		}
		enum mask_S6PAR {
				PA = RegBitField!uint(0,32,RegisterAccess.RW),		// Peripheral address
		}
		enum mask_S6M0AR {
				M0A = RegBitField!uint(0,32,RegisterAccess.RW),		// Memory 0 address
		}
		enum mask_S6M1AR {
				M1A = RegBitField!uint(0,32,RegisterAccess.RW),		// Memory 1 address (used in case of Double buffer mode)
		}
		enum mask_S6FCR {
				FEIE = RegBitField!uint(7,1,RegisterAccess.RW),		// FIFO error interrupt enable
				FS = RegBitField!uint(3,3,RegisterAccess.RO),		// FIFO status
				DMDIS = RegBitField!uint(2,1,RegisterAccess.RW),		// Direct mode disable
				FTH = RegBitField!uint(0,2,RegisterAccess.RW),		// FIFO threshold selection
		}
		enum mask_S7CR {
				CHSEL = RegBitField!uint(25,3,RegisterAccess.RW),		// Channel selection
				MBURST = RegBitField!uint(23,2,RegisterAccess.RW),		// Memory burst transfer configuration
				PBURST = RegBitField!uint(21,2,RegisterAccess.RW),		// Peripheral burst transfer configuration
				ACK = RegBitField!uint(20,1,RegisterAccess.RW),		// ACK
				CT = RegBitField!uint(19,1,RegisterAccess.RW),		// Current target (only in double buffer mode)
				DBM = RegBitField!uint(18,1,RegisterAccess.RW),		// Double buffer mode
				PL = RegBitField!uint(16,2,RegisterAccess.RW),		// Priority level
				PINCOS = RegBitField!uint(15,1,RegisterAccess.RW),		// Peripheral increment offset size
				MSIZE = RegBitField!uint(13,2,RegisterAccess.RW),		// Memory data size
				PSIZE = RegBitField!uint(11,2,RegisterAccess.RW),		// Peripheral data size
				MINC = RegBitField!uint(10,1,RegisterAccess.RW),		// Memory increment mode
				PINC = RegBitField!uint(9,1,RegisterAccess.RW),		// Peripheral increment mode
				CIRC = RegBitField!uint(8,1,RegisterAccess.RW),		// Circular mode
				DIR = RegBitField!uint(6,2,RegisterAccess.RW),		// Data transfer direction
				PFCTRL = RegBitField!uint(5,1,RegisterAccess.RW),		// Peripheral flow controller
				TCIE = RegBitField!uint(4,1,RegisterAccess.RW),		// Transfer complete interrupt enable
				HTIE = RegBitField!uint(3,1,RegisterAccess.RW),		// Half transfer interrupt enable
				TEIE = RegBitField!uint(2,1,RegisterAccess.RW),		// Transfer error interrupt enable
				DMEIE = RegBitField!uint(1,1,RegisterAccess.RW),		// Direct mode error interrupt enable
				EN = RegBitField!uint(0,1,RegisterAccess.RW),		// Stream enable / flag stream ready when read low
		}
		enum mask_S7NDTR {
				NDT = RegBitField!uint(0,16,RegisterAccess.RW),		// Number of data items to transfer
		}
		enum mask_S7PAR {
				PA = RegBitField!uint(0,32,RegisterAccess.RW),		// Peripheral address
		}
		enum mask_S7M0AR {
				M0A = RegBitField!uint(0,32,RegisterAccess.RW),		// Memory 0 address
		}
		enum mask_S7M1AR {
				M1A = RegBitField!uint(0,32,RegisterAccess.RW),		// Memory 1 address (used in case of Double buffer mode)
		}
		enum mask_S7FCR {
				FEIE = RegBitField!uint(7,1,RegisterAccess.RW),		// FIFO error interrupt enable
				FS = RegBitField!uint(3,3,RegisterAccess.RO),		// FIFO status
				DMDIS = RegBitField!uint(2,1,RegisterAccess.RW),		// Direct mode disable
				FTH = RegBitField!uint(0,2,RegisterAccess.RW),		// FIFO threshold selection
		}
	}
	// SCB Group 
	// System control block
	struct SCB_Type {
		@RegInfo("CPUID",0x0) RegVolatile!(mask_CPUID,uint,0xe000ed00,0x410fc241,RegisterAccess.RO) CPUID;		 // offset:0x0 CPUID CPUID base register
		@RegInfo("ICSR",0x4) RegVolatile!(mask_ICSR,uint,0xe000ed04,0x0,RegisterAccess.RW) ICSR;		 // offset:0x4 ICSR Interrupt control and state register
		@RegInfo("VTOR",0x8) RegVolatile!(mask_VTOR,uint,0xe000ed08,0x0,RegisterAccess.RW) VTOR;		 // offset:0x8 VTOR Vector table offset register
		@RegInfo("AIRCR",0xc) RegVolatile!(mask_AIRCR,uint,0xe000ed0c,0x0,RegisterAccess.RW) AIRCR;		 // offset:0x12 AIRCR Application interrupt and reset control register
		@RegInfo("SCR",0x10) RegVolatile!(mask_SCR,uint,0xe000ed10,0x0,RegisterAccess.RW) SCR;		 // offset:0x16 SCR System control register
		@RegInfo("CCR",0x14) RegVolatile!(mask_CCR,uint,0xe000ed14,0x0,RegisterAccess.RW) CCR;		 // offset:0x20 CCR Configuration and control register
		@RegInfo("SHPR1",0x18) RegVolatile!(mask_SHPR1,uint,0xe000ed18,0x0,RegisterAccess.RW) SHPR1;		 // offset:0x24 SHPR1 System handler priority registers
		@RegInfo("SHPR2",0x1c) RegVolatile!(mask_SHPR2,uint,0xe000ed1c,0x0,RegisterAccess.RW) SHPR2;		 // offset:0x28 SHPR2 System handler priority registers
		@RegInfo("SHPR3",0x20) RegVolatile!(mask_SHPR3,uint,0xe000ed20,0x0,RegisterAccess.RW) SHPR3;		 // offset:0x32 SHPR3 System handler priority registers
		@RegInfo("SHCRS",0x24) RegVolatile!(mask_SHCRS,uint,0xe000ed24,0x0,RegisterAccess.RW) SHCRS;		 // offset:0x36 SHCRS System handler control and state register
		@RegInfo("CFSR_UFSR_BFSR_MMFSR",0x28) RegVolatile!(mask_CFSR_UFSR_BFSR_MMFSR,uint,0xe000ed28,0x0,RegisterAccess.RW) CFSR_UFSR_BFSR_MMFSR;		 // offset:0x40 CFSR_UFSR_BFSR_MMFSR Configurable fault status register
		@RegInfo("HFSR",0x2c) RegVolatile!(mask_HFSR,uint,0xe000ed2c,0x0,RegisterAccess.RW) HFSR;		 // offset:0x44 HFSR Hard fault status register
		private uint[0x1] reserved12;	// skip 4 bytes
		@RegInfo("MMFAR",0x34) RegVolatile!(mask_MMFAR,uint,0xe000ed34,0x0,RegisterAccess.RW) MMFAR;		 // offset:0x52 MMFAR Memory management fault address register
		@RegInfo("BFAR",0x38) RegVolatile!(mask_BFAR,uint,0xe000ed38,0x0,RegisterAccess.RW) BFAR;		 // offset:0x56 BFAR Bus fault address register
		@RegInfo("AFSR",0x3c) RegVolatile!(mask_AFSR,uint,0xe000ed3c,0x0,RegisterAccess.RW) AFSR;		 // offset:0x60 AFSR Auxiliary fault status register
		enum mask_CPUID {
				Revision = RegBitField!uint(0,4,RegisterAccess.RO),		// Revision number
				PartNo = RegBitField!uint(4,12,RegisterAccess.RO),		// Part number of the processor
				Constant = RegBitField!uint(16,4,RegisterAccess.RO),		// Reads as 0xF
				Variant = RegBitField!uint(20,4,RegisterAccess.RO),		// Variant number
				Implementer = RegBitField!uint(24,8,RegisterAccess.RO),		// Implementer code
		}
		enum mask_ICSR {
				VECTACTIVE = RegBitField!uint(0,9,RegisterAccess.RW),		// Active vector
				RETTOBASE = RegBitField!uint(11,1,RegisterAccess.RW),		// Return to base level
				VECTPENDING = RegBitField!uint(12,7,RegisterAccess.RW),		// Pending vector
				ISRPENDING = RegBitField!uint(22,1,RegisterAccess.RW),		// Interrupt pending flag
				PENDSTCLR = RegBitField!uint(25,1,RegisterAccess.RW),		// SysTick exception clear-pending bit
				PENDSTSET = RegBitField!uint(26,1,RegisterAccess.RW),		// SysTick exception set-pending bit
				PENDSVCLR = RegBitField!uint(27,1,RegisterAccess.RW),		// PendSV clear-pending bit
				PENDSVSET = RegBitField!uint(28,1,RegisterAccess.RW),		// PendSV set-pending bit
				NMIPENDSET = RegBitField!uint(31,1,RegisterAccess.RW),		// NMI set-pending bit.
		}
		enum mask_VTOR {
				TBLOFF = RegBitField!uint(9,21,RegisterAccess.RW),		// Vector table base offset field
		}
		enum mask_AIRCR {
				VECTRESET = RegBitField!uint(0,1,RegisterAccess.RW),		// VECTRESET
				VECTCLRACTIVE = RegBitField!uint(1,1,RegisterAccess.RW),		// VECTCLRACTIVE
				SYSRESETREQ = RegBitField!uint(2,1,RegisterAccess.RW),		// SYSRESETREQ
				PRIGROUP = RegBitField!uint(8,3,RegisterAccess.RW),		// PRIGROUP
				ENDIANESS = RegBitField!uint(15,1,RegisterAccess.RW),		// ENDIANESS
				VECTKEY = RegBitField!uint(16,16,RegisterAccess.RW),		// Register key
		}
		enum mask_SCR {
				SLEEPONEXIT = RegBitField!uint(1,1,RegisterAccess.RW),		// SLEEPONEXIT
				SLEEPDEEP = RegBitField!uint(2,1,RegisterAccess.RW),		// SLEEPDEEP
				SEVEONPEND = RegBitField!uint(4,1,RegisterAccess.RW),		// Send Event on Pending bit
		}
		enum mask_CCR {
				NONBASETHRDENA = RegBitField!uint(0,1,RegisterAccess.RW),		// Configures how the processor enters Thread mode
				USERSETMPEND = RegBitField!uint(1,1,RegisterAccess.RW),		// USERSETMPEND
				UNALIGN__TRP = RegBitField!uint(3,1,RegisterAccess.RW),		// UNALIGN_ TRP
				DIV_0_TRP = RegBitField!uint(4,1,RegisterAccess.RW),		// DIV_0_TRP
				BFHFNMIGN = RegBitField!uint(8,1,RegisterAccess.RW),		// BFHFNMIGN
				STKALIGN = RegBitField!uint(9,1,RegisterAccess.RW),		// STKALIGN
		}
		enum mask_SHPR1 {
				PRI_4 = RegBitField!uint(0,8,RegisterAccess.RW),		// Priority of system handler 4
				PRI_5 = RegBitField!uint(8,8,RegisterAccess.RW),		// Priority of system handler 5
				PRI_6 = RegBitField!uint(16,8,RegisterAccess.RW),		// Priority of system handler 6
		}
		enum mask_SHPR2 {
				PRI_11 = RegBitField!uint(24,8,RegisterAccess.RW),		// Priority of system handler 11
		}
		enum mask_SHPR3 {
				PRI_14 = RegBitField!uint(16,8,RegisterAccess.RW),		// Priority of system handler 14
				PRI_15 = RegBitField!uint(24,8,RegisterAccess.RW),		// Priority of system handler 15
		}
		enum mask_SHCRS {
				MEMFAULTACT = RegBitField!uint(0,1,RegisterAccess.RW),		// Memory management fault exception active bit
				BUSFAULTACT = RegBitField!uint(1,1,RegisterAccess.RW),		// Bus fault exception active bit
				USGFAULTACT = RegBitField!uint(3,1,RegisterAccess.RW),		// Usage fault exception active bit
				SVCALLACT = RegBitField!uint(7,1,RegisterAccess.RW),		// SVC call active bit
				MONITORACT = RegBitField!uint(8,1,RegisterAccess.RW),		// Debug monitor active bit
				PENDSVACT = RegBitField!uint(10,1,RegisterAccess.RW),		// PendSV exception active bit
				SYSTICKACT = RegBitField!uint(11,1,RegisterAccess.RW),		// SysTick exception active bit
				USGFAULTPENDED = RegBitField!uint(12,1,RegisterAccess.RW),		// Usage fault exception pending bit
				MEMFAULTPENDED = RegBitField!uint(13,1,RegisterAccess.RW),		// Memory management fault exception pending bit
				BUSFAULTPENDED = RegBitField!uint(14,1,RegisterAccess.RW),		// Bus fault exception pending bit
				SVCALLPENDED = RegBitField!uint(15,1,RegisterAccess.RW),		// SVC call pending bit
				MEMFAULTENA = RegBitField!uint(16,1,RegisterAccess.RW),		// Memory management fault enable bit
				BUSFAULTENA = RegBitField!uint(17,1,RegisterAccess.RW),		// Bus fault enable bit
				USGFAULTENA = RegBitField!uint(18,1,RegisterAccess.RW),		// Usage fault enable bit
		}
		enum mask_CFSR_UFSR_BFSR_MMFSR {
				IACCVIOL = RegBitField!uint(1,1,RegisterAccess.RW),		// Instruction access violation flag
				MUNSTKERR = RegBitField!uint(3,1,RegisterAccess.RW),		// Memory manager fault on unstacking for a return from exception
				MSTKERR = RegBitField!uint(4,1,RegisterAccess.RW),		// Memory manager fault on stacking for exception entry.
				MLSPERR = RegBitField!uint(5,1,RegisterAccess.RW),		// MLSPERR
				MMARVALID = RegBitField!uint(7,1,RegisterAccess.RW),		// Memory Management Fault Address Register (MMAR) valid flag
				IBUSERR = RegBitField!uint(8,1,RegisterAccess.RW),		// Instruction bus error
				PRECISERR = RegBitField!uint(9,1,RegisterAccess.RW),		// Precise data bus error
				IMPRECISERR = RegBitField!uint(10,1,RegisterAccess.RW),		// Imprecise data bus error
				UNSTKERR = RegBitField!uint(11,1,RegisterAccess.RW),		// Bus fault on unstacking for a return from exception
				STKERR = RegBitField!uint(12,1,RegisterAccess.RW),		// Bus fault on stacking for exception entry
				LSPERR = RegBitField!uint(13,1,RegisterAccess.RW),		// Bus fault on floating-point lazy state preservation
				BFARVALID = RegBitField!uint(15,1,RegisterAccess.RW),		// Bus Fault Address Register (BFAR) valid flag
				UNDEFINSTR = RegBitField!uint(16,1,RegisterAccess.RW),		// Undefined instruction usage fault
				INVSTATE = RegBitField!uint(17,1,RegisterAccess.RW),		// Invalid state usage fault
				INVPC = RegBitField!uint(18,1,RegisterAccess.RW),		// Invalid PC load usage fault
				NOCP = RegBitField!uint(19,1,RegisterAccess.RW),		// No coprocessor usage fault.
				UNALIGNED = RegBitField!uint(24,1,RegisterAccess.RW),		// Unaligned access usage fault
				DIVBYZERO = RegBitField!uint(25,1,RegisterAccess.RW),		// Divide by zero usage fault
		}
		enum mask_HFSR {
				VECTTBL = RegBitField!uint(1,1,RegisterAccess.RW),		// Vector table hard fault
				FORCED = RegBitField!uint(30,1,RegisterAccess.RW),		// Forced hard fault
				DEBUG_VT = RegBitField!uint(31,1,RegisterAccess.RW),		// Reserved for Debug use
		}
		enum mask_MMFAR {
				MMFAR = RegBitField!uint(0,32,RegisterAccess.RW),		// Memory management fault address
		}
		enum mask_BFAR {
				BFAR = RegBitField!uint(0,32,RegisterAccess.RW),		// Bus fault address
		}
		enum mask_AFSR {
				IMPDEF = RegBitField!uint(0,32,RegisterAccess.RW),		// Implementation defined
		}
	}
	// I2C Group 
	// Inter-integrated circuit
	struct I2C3_Type {
		@RegInfo("CR1",0x0) RegVolatile!(mask_CR1,uint,0x40005c00,0x0,RegisterAccess.RW) CR1;		 // offset:0x0 CR1 Control register 1
		@RegInfo("CR2",0x4) RegVolatile!(mask_CR2,uint,0x40005c04,0x0,RegisterAccess.RW) CR2;		 // offset:0x4 CR2 Control register 2
		@RegInfo("OAR1",0x8) RegVolatile!(mask_OAR1,uint,0x40005c08,0x0,RegisterAccess.RW) OAR1;		 // offset:0x8 OAR1 Own address register 1
		@RegInfo("OAR2",0xc) RegVolatile!(mask_OAR2,uint,0x40005c0c,0x0,RegisterAccess.RW) OAR2;		 // offset:0x12 OAR2 Own address register 2
		@RegInfo("DR",0x10) RegVolatile!(mask_DR,uint,0x40005c10,0x0,RegisterAccess.RW) DR;		 // offset:0x16 DR Data register
		@RegInfo("SR1",0x14) RegVolatile!(mask_SR1,uint,0x40005c14,0x0,RegisterAccess.NO) SR1;		 // offset:0x20 SR1 Status register 1
		@RegInfo("SR2",0x18) RegVolatile!(mask_SR2,uint,0x40005c18,0x0,RegisterAccess.RO) SR2;		 // offset:0x24 SR2 Status register 2
		@RegInfo("CCR",0x1c) RegVolatile!(mask_CCR,uint,0x40005c1c,0x0,RegisterAccess.RW) CCR;		 // offset:0x28 CCR Clock control register
		@RegInfo("TRISE",0x20) RegVolatile!(mask_TRISE,uint,0x40005c20,0x2,RegisterAccess.RW) TRISE;		 // offset:0x32 TRISE TRISE register
		enum mask_CR1 {
				SWRST = RegBitField!uint(15,1,RegisterAccess.RW),		// Software reset
				ALERT = RegBitField!uint(13,1,RegisterAccess.RW),		// SMBus alert
				PEC = RegBitField!uint(12,1,RegisterAccess.RW),		// Packet error checking
				POS = RegBitField!uint(11,1,RegisterAccess.RW),		// Acknowledge/PEC Position (for data reception)
				ACK = RegBitField!uint(10,1,RegisterAccess.RW),		// Acknowledge enable
				STOP = RegBitField!uint(9,1,RegisterAccess.RW),		// Stop generation
				START = RegBitField!uint(8,1,RegisterAccess.RW),		// Start generation
				NOSTRETCH = RegBitField!uint(7,1,RegisterAccess.RW),		// Clock stretching disable (Slave mode)
				ENGC = RegBitField!uint(6,1,RegisterAccess.RW),		// General call enable
				ENPEC = RegBitField!uint(5,1,RegisterAccess.RW),		// PEC enable
				ENARP = RegBitField!uint(4,1,RegisterAccess.RW),		// ARP enable
				SMBTYPE = RegBitField!uint(3,1,RegisterAccess.RW),		// SMBus type
				SMBUS = RegBitField!uint(1,1,RegisterAccess.RW),		// SMBus mode
				PE = RegBitField!uint(0,1,RegisterAccess.RW),		// Peripheral enable
		}
		enum mask_CR2 {
				LAST = RegBitField!uint(12,1,RegisterAccess.RW),		// DMA last transfer
				DMAEN = RegBitField!uint(11,1,RegisterAccess.RW),		// DMA requests enable
				ITBUFEN = RegBitField!uint(10,1,RegisterAccess.RW),		// Buffer interrupt enable
				ITEVTEN = RegBitField!uint(9,1,RegisterAccess.RW),		// Event interrupt enable
				ITERREN = RegBitField!uint(8,1,RegisterAccess.RW),		// Error interrupt enable
				FREQ = RegBitField!uint(0,6,RegisterAccess.RW),		// Peripheral clock frequency
		}
		enum mask_OAR1 {
				ADDMODE = RegBitField!uint(15,1,RegisterAccess.RW),		// Addressing mode (slave mode)
				ADD10 = RegBitField!uint(8,2,RegisterAccess.RW),		// Interface address
				ADD7 = RegBitField!uint(1,7,RegisterAccess.RW),		// Interface address
				ADD0 = RegBitField!uint(0,1,RegisterAccess.RW),		// Interface address
		}
		enum mask_OAR2 {
				ADD2 = RegBitField!uint(1,7,RegisterAccess.RW),		// Interface address
				ENDUAL = RegBitField!uint(0,1,RegisterAccess.RW),		// Dual addressing mode enable
		}
		enum mask_DR {
				DR = RegBitField!uint(0,8,RegisterAccess.RW),		// 8-bit data register
		}
		enum mask_SR1 {
				SMBALERT = RegBitField!uint(15,1,RegisterAccess.RW),		// SMBus alert
				TIMEOUT = RegBitField!uint(14,1,RegisterAccess.RW),		// Timeout or Tlow error
				PECERR = RegBitField!uint(12,1,RegisterAccess.RW),		// PEC Error in reception
				OVR = RegBitField!uint(11,1,RegisterAccess.RW),		// Overrun/Underrun
				AF = RegBitField!uint(10,1,RegisterAccess.RW),		// Acknowledge failure
				ARLO = RegBitField!uint(9,1,RegisterAccess.RW),		// Arbitration lost (master mode)
				BERR = RegBitField!uint(8,1,RegisterAccess.RW),		// Bus error
				TxE = RegBitField!uint(7,1,RegisterAccess.RO),		// Data register empty (transmitters)
				RxNE = RegBitField!uint(6,1,RegisterAccess.RO),		// Data register not empty (receivers)
				STOPF = RegBitField!uint(4,1,RegisterAccess.RO),		// Stop detection (slave mode)
				ADD10 = RegBitField!uint(3,1,RegisterAccess.RO),		// 10-bit header sent (Master mode)
				BTF = RegBitField!uint(2,1,RegisterAccess.RO),		// Byte transfer finished
				ADDR = RegBitField!uint(1,1,RegisterAccess.RO),		// Address sent (master mode)/matched (slave mode)
				SB = RegBitField!uint(0,1,RegisterAccess.RO),		// Start bit (Master mode)
		}
		enum mask_SR2 {
				PEC = RegBitField!uint(8,8,RegisterAccess.RO),		// acket error checking register
				DUALF = RegBitField!uint(7,1,RegisterAccess.RO),		// Dual flag (Slave mode)
				SMBHOST = RegBitField!uint(6,1,RegisterAccess.RO),		// SMBus host header (Slave mode)
				SMBDEFAULT = RegBitField!uint(5,1,RegisterAccess.RO),		// SMBus device default address (Slave mode)
				GENCALL = RegBitField!uint(4,1,RegisterAccess.RO),		// General call address (Slave mode)
				TRA = RegBitField!uint(2,1,RegisterAccess.RO),		// Transmitter/receiver
				BUSY = RegBitField!uint(1,1,RegisterAccess.RO),		// Bus busy
				MSL = RegBitField!uint(0,1,RegisterAccess.RO),		// Master/slave
		}
		enum mask_CCR {
				F_S = RegBitField!uint(15,1,RegisterAccess.RW),		// I2C master mode selection
				DUTY = RegBitField!uint(14,1,RegisterAccess.RW),		// Fast mode duty cycle
				CCR = RegBitField!uint(0,12,RegisterAccess.RW),		// Clock control register in Fast/Standard mode (Master mode)
		}
		enum mask_TRISE {
				TRISE = RegBitField!uint(0,6,RegisterAccess.RW),		// Maximum rise time in Fast/Standard mode (Master mode)
		}
	}
	// RTC Group 
	// Real-time clock
	struct RTC_Type {
		@RegInfo("TR",0x0) RegVolatile!(mask_TR,uint,0x40002800,0x0,RegisterAccess.RW) TR;		 // offset:0x0 TR time register
		@RegInfo("DR",0x4) RegVolatile!(mask_DR,uint,0x40002804,0x2101,RegisterAccess.RW) DR;		 // offset:0x4 DR date register
		@RegInfo("CR",0x8) RegVolatile!(mask_CR,uint,0x40002808,0x0,RegisterAccess.RW) CR;		 // offset:0x8 CR control register
		@RegInfo("ISR",0xc) RegVolatile!(mask_ISR,uint,0x4000280c,0x7,RegisterAccess.NO) ISR;		 // offset:0x12 ISR initialization and status register
		@RegInfo("PRER",0x10) RegVolatile!(mask_PRER,uint,0x40002810,0x7f00ff,RegisterAccess.RW) PRER;		 // offset:0x16 PRER prescaler register
		@RegInfo("WUTR",0x14) RegVolatile!(mask_WUTR,uint,0x40002814,0xffff,RegisterAccess.RW) WUTR;		 // offset:0x20 WUTR wakeup timer register
		@RegInfo("CALIBR",0x18) RegVolatile!(mask_CALIBR,uint,0x40002818,0x0,RegisterAccess.RW) CALIBR;		 // offset:0x24 CALIBR calibration register
		@RegInfo("ALRMAR",0x1c) RegVolatile!(mask_ALRMAR,uint,0x4000281c,0x0,RegisterAccess.RW) ALRMAR;		 // offset:0x28 ALRMAR alarm A register
		@RegInfo("ALRMBR",0x20) RegVolatile!(mask_ALRMBR,uint,0x40002820,0x0,RegisterAccess.RW) ALRMBR;		 // offset:0x32 ALRMBR alarm B register
		@RegInfo("WPR",0x24) RegVolatile!(mask_WPR,uint,0x40002824,0x0,RegisterAccess.WO) WPR;		 // offset:0x36 WPR write protection register
		@RegInfo("SSR",0x28) RegVolatile!(mask_SSR,uint,0x40002828,0x0,RegisterAccess.RO) SSR;		 // offset:0x40 SSR sub second register
		@RegInfo("SHIFTR",0x2c) RegVolatile!(mask_SHIFTR,uint,0x4000282c,0x0,RegisterAccess.WO) SHIFTR;		 // offset:0x44 SHIFTR shift control register
		@RegInfo("TSTR",0x30) RegVolatile!(mask_TSTR,uint,0x40002830,0x0,RegisterAccess.RO) TSTR;		 // offset:0x48 TSTR time stamp time register
		@RegInfo("TSDR",0x34) RegVolatile!(mask_TSDR,uint,0x40002834,0x0,RegisterAccess.RO) TSDR;		 // offset:0x52 TSDR time stamp date register
		@RegInfo("TSSSR",0x38) RegVolatile!(mask_TSSSR,uint,0x40002838,0x0,RegisterAccess.RO) TSSSR;		 // offset:0x56 TSSSR timestamp sub second register
		@RegInfo("CALR",0x3c) RegVolatile!(mask_CALR,uint,0x4000283c,0x0,RegisterAccess.RW) CALR;		 // offset:0x60 CALR calibration register
		@RegInfo("TAFCR",0x40) RegVolatile!(mask_TAFCR,uint,0x40002840,0x0,RegisterAccess.RW) TAFCR;		 // offset:0x64 TAFCR tamper and alternate function configuration register
		@RegInfo("ALRMASSR",0x44) RegVolatile!(mask_ALRMASSR,uint,0x40002844,0x0,RegisterAccess.RW) ALRMASSR;		 // offset:0x68 ALRMASSR alarm A sub second register
		@RegInfo("ALRMBSSR",0x48) RegVolatile!(mask_ALRMBSSR,uint,0x40002848,0x0,RegisterAccess.RW) ALRMBSSR;		 // offset:0x72 ALRMBSSR alarm B sub second register
		private uint[0x1] reserved19;	// skip 4 bytes
		@RegInfo("BKP0R",0x50) RegVolatile!(mask_BKP0R,uint,0x40002850,0x0,RegisterAccess.RW) BKP0R;		 // offset:0x80 BKP0R backup register
		@RegInfo("BKP1R",0x54) RegVolatile!(mask_BKP1R,uint,0x40002854,0x0,RegisterAccess.RW) BKP1R;		 // offset:0x84 BKP1R backup register
		@RegInfo("BKP2R",0x58) RegVolatile!(mask_BKP2R,uint,0x40002858,0x0,RegisterAccess.RW) BKP2R;		 // offset:0x88 BKP2R backup register
		@RegInfo("BKP3R",0x5c) RegVolatile!(mask_BKP3R,uint,0x4000285c,0x0,RegisterAccess.RW) BKP3R;		 // offset:0x92 BKP3R backup register
		@RegInfo("BKP4R",0x60) RegVolatile!(mask_BKP4R,uint,0x40002860,0x0,RegisterAccess.RW) BKP4R;		 // offset:0x96 BKP4R backup register
		@RegInfo("BKP5R",0x64) RegVolatile!(mask_BKP5R,uint,0x40002864,0x0,RegisterAccess.RW) BKP5R;		 // offset:0x100 BKP5R backup register
		@RegInfo("BKP6R",0x68) RegVolatile!(mask_BKP6R,uint,0x40002868,0x0,RegisterAccess.RW) BKP6R;		 // offset:0x104 BKP6R backup register
		@RegInfo("BKP7R",0x6c) RegVolatile!(mask_BKP7R,uint,0x4000286c,0x0,RegisterAccess.RW) BKP7R;		 // offset:0x108 BKP7R backup register
		@RegInfo("BKP8R",0x70) RegVolatile!(mask_BKP8R,uint,0x40002870,0x0,RegisterAccess.RW) BKP8R;		 // offset:0x112 BKP8R backup register
		@RegInfo("BKP9R",0x74) RegVolatile!(mask_BKP9R,uint,0x40002874,0x0,RegisterAccess.RW) BKP9R;		 // offset:0x116 BKP9R backup register
		@RegInfo("BKP10R",0x78) RegVolatile!(mask_BKP10R,uint,0x40002878,0x0,RegisterAccess.RW) BKP10R;		 // offset:0x120 BKP10R backup register
		@RegInfo("BKP11R",0x7c) RegVolatile!(mask_BKP11R,uint,0x4000287c,0x0,RegisterAccess.RW) BKP11R;		 // offset:0x124 BKP11R backup register
		@RegInfo("BKP12R",0x80) RegVolatile!(mask_BKP12R,uint,0x40002880,0x0,RegisterAccess.RW) BKP12R;		 // offset:0x128 BKP12R backup register
		@RegInfo("BKP13R",0x84) RegVolatile!(mask_BKP13R,uint,0x40002884,0x0,RegisterAccess.RW) BKP13R;		 // offset:0x132 BKP13R backup register
		@RegInfo("BKP14R",0x88) RegVolatile!(mask_BKP14R,uint,0x40002888,0x0,RegisterAccess.RW) BKP14R;		 // offset:0x136 BKP14R backup register
		@RegInfo("BKP15R",0x8c) RegVolatile!(mask_BKP15R,uint,0x4000288c,0x0,RegisterAccess.RW) BKP15R;		 // offset:0x140 BKP15R backup register
		@RegInfo("BKP16R",0x90) RegVolatile!(mask_BKP16R,uint,0x40002890,0x0,RegisterAccess.RW) BKP16R;		 // offset:0x144 BKP16R backup register
		@RegInfo("BKP17R",0x94) RegVolatile!(mask_BKP17R,uint,0x40002894,0x0,RegisterAccess.RW) BKP17R;		 // offset:0x148 BKP17R backup register
		@RegInfo("BKP18R",0x98) RegVolatile!(mask_BKP18R,uint,0x40002898,0x0,RegisterAccess.RW) BKP18R;		 // offset:0x152 BKP18R backup register
		@RegInfo("BKP19R",0x9c) RegVolatile!(mask_BKP19R,uint,0x4000289c,0x0,RegisterAccess.RW) BKP19R;		 // offset:0x156 BKP19R backup register
		enum mask_TR {
				PM = RegBitField!uint(22,1,RegisterAccess.RW),		// AM/PM notation
				HT = RegBitField!uint(20,2,RegisterAccess.RW),		// Hour tens in BCD format
				HU = RegBitField!uint(16,4,RegisterAccess.RW),		// Hour units in BCD format
				MNT = RegBitField!uint(12,3,RegisterAccess.RW),		// Minute tens in BCD format
				MNU = RegBitField!uint(8,4,RegisterAccess.RW),		// Minute units in BCD format
				ST = RegBitField!uint(4,3,RegisterAccess.RW),		// Second tens in BCD format
				SU = RegBitField!uint(0,4,RegisterAccess.RW),		// Second units in BCD format
		}
		enum mask_DR {
				YT = RegBitField!uint(20,4,RegisterAccess.RW),		// Year tens in BCD format
				YU = RegBitField!uint(16,4,RegisterAccess.RW),		// Year units in BCD format
				WDU = RegBitField!uint(13,3,RegisterAccess.RW),		// Week day units
				MT = RegBitField!uint(12,1,RegisterAccess.RW),		// Month tens in BCD format
				MU = RegBitField!uint(8,4,RegisterAccess.RW),		// Month units in BCD format
				DT = RegBitField!uint(4,2,RegisterAccess.RW),		// Date tens in BCD format
				DU = RegBitField!uint(0,4,RegisterAccess.RW),		// Date units in BCD format
		}
		enum mask_CR {
				COE = RegBitField!uint(23,1,RegisterAccess.RW),		// Calibration output enable
				OSEL = RegBitField!uint(21,2,RegisterAccess.RW),		// Output selection
				POL = RegBitField!uint(20,1,RegisterAccess.RW),		// Output polarity
				COSEL = RegBitField!uint(19,1,RegisterAccess.RW),		// Calibration Output selection
				BKP = RegBitField!uint(18,1,RegisterAccess.RW),		// Backup
				SUB1H = RegBitField!uint(17,1,RegisterAccess.RW),		// Subtract 1 hour (winter time change)
				ADD1H = RegBitField!uint(16,1,RegisterAccess.RW),		// Add 1 hour (summer time change)
				TSIE = RegBitField!uint(15,1,RegisterAccess.RW),		// Time-stamp interrupt enable
				WUTIE = RegBitField!uint(14,1,RegisterAccess.RW),		// Wakeup timer interrupt enable
				ALRBIE = RegBitField!uint(13,1,RegisterAccess.RW),		// Alarm B interrupt enable
				ALRAIE = RegBitField!uint(12,1,RegisterAccess.RW),		// Alarm A interrupt enable
				TSE = RegBitField!uint(11,1,RegisterAccess.RW),		// Time stamp enable
				WUTE = RegBitField!uint(10,1,RegisterAccess.RW),		// Wakeup timer enable
				ALRBE = RegBitField!uint(9,1,RegisterAccess.RW),		// Alarm B enable
				ALRAE = RegBitField!uint(8,1,RegisterAccess.RW),		// Alarm A enable
				DCE = RegBitField!uint(7,1,RegisterAccess.RW),		// Coarse digital calibration enable
				FMT = RegBitField!uint(6,1,RegisterAccess.RW),		// Hour format
				BYPSHAD = RegBitField!uint(5,1,RegisterAccess.RW),		// Bypass the shadow registers
				REFCKON = RegBitField!uint(4,1,RegisterAccess.RW),		// Reference clock detection enable (50 or 60 Hz)
				TSEDGE = RegBitField!uint(3,1,RegisterAccess.RW),		// Time-stamp event active edge
				WCKSEL = RegBitField!uint(0,3,RegisterAccess.RW),		// Wakeup clock selection
		}
		enum mask_ISR {
				ALRAWF = RegBitField!uint(0,1,RegisterAccess.RO),		// Alarm A write flag
				ALRBWF = RegBitField!uint(1,1,RegisterAccess.RO),		// Alarm B write flag
				WUTWF = RegBitField!uint(2,1,RegisterAccess.RO),		// Wakeup timer write flag
				SHPF = RegBitField!uint(3,1,RegisterAccess.RW),		// Shift operation pending
				INITS = RegBitField!uint(4,1,RegisterAccess.RO),		// Initialization status flag
				RSF = RegBitField!uint(5,1,RegisterAccess.RW),		// Registers synchronization flag
				INITF = RegBitField!uint(6,1,RegisterAccess.RO),		// Initialization flag
				INIT = RegBitField!uint(7,1,RegisterAccess.RW),		// Initialization mode
				ALRAF = RegBitField!uint(8,1,RegisterAccess.RW),		// Alarm A flag
				ALRBF = RegBitField!uint(9,1,RegisterAccess.RW),		// Alarm B flag
				WUTF = RegBitField!uint(10,1,RegisterAccess.RW),		// Wakeup timer flag
				TSF = RegBitField!uint(11,1,RegisterAccess.RW),		// Time-stamp flag
				TSOVF = RegBitField!uint(12,1,RegisterAccess.RW),		// Time-stamp overflow flag
				TAMP1F = RegBitField!uint(13,1,RegisterAccess.RW),		// Tamper detection flag
				TAMP2F = RegBitField!uint(14,1,RegisterAccess.RW),		// TAMPER2 detection flag
				RECALPF = RegBitField!uint(16,1,RegisterAccess.RO),		// Recalibration pending Flag
		}
		enum mask_PRER {
				PREDIV_A = RegBitField!uint(16,7,RegisterAccess.RW),		// Asynchronous prescaler factor
				PREDIV_S = RegBitField!uint(0,15,RegisterAccess.RW),		// Synchronous prescaler factor
		}
		enum mask_WUTR {
				WUT = RegBitField!uint(0,16,RegisterAccess.RW),		// Wakeup auto-reload value bits
		}
		enum mask_CALIBR {
				DCS = RegBitField!uint(7,1,RegisterAccess.RW),		// Digital calibration sign
				DC = RegBitField!uint(0,5,RegisterAccess.RW),		// Digital calibration
		}
		enum mask_ALRMAR {
				MSK4 = RegBitField!uint(31,1,RegisterAccess.RW),		// Alarm A date mask
				WDSEL = RegBitField!uint(30,1,RegisterAccess.RW),		// Week day selection
				DT = RegBitField!uint(28,2,RegisterAccess.RW),		// Date tens in BCD format
				DU = RegBitField!uint(24,4,RegisterAccess.RW),		// Date units or day in BCD format
				MSK3 = RegBitField!uint(23,1,RegisterAccess.RW),		// Alarm A hours mask
				PM = RegBitField!uint(22,1,RegisterAccess.RW),		// AM/PM notation
				HT = RegBitField!uint(20,2,RegisterAccess.RW),		// Hour tens in BCD format
				HU = RegBitField!uint(16,4,RegisterAccess.RW),		// Hour units in BCD format
				MSK2 = RegBitField!uint(15,1,RegisterAccess.RW),		// Alarm A minutes mask
				MNT = RegBitField!uint(12,3,RegisterAccess.RW),		// Minute tens in BCD format
				MNU = RegBitField!uint(8,4,RegisterAccess.RW),		// Minute units in BCD format
				MSK1 = RegBitField!uint(7,1,RegisterAccess.RW),		// Alarm A seconds mask
				ST = RegBitField!uint(4,3,RegisterAccess.RW),		// Second tens in BCD format
				SU = RegBitField!uint(0,4,RegisterAccess.RW),		// Second units in BCD format
		}
		enum mask_ALRMBR {
				MSK4 = RegBitField!uint(31,1,RegisterAccess.RW),		// Alarm B date mask
				WDSEL = RegBitField!uint(30,1,RegisterAccess.RW),		// Week day selection
				DT = RegBitField!uint(28,2,RegisterAccess.RW),		// Date tens in BCD format
				DU = RegBitField!uint(24,4,RegisterAccess.RW),		// Date units or day in BCD format
				MSK3 = RegBitField!uint(23,1,RegisterAccess.RW),		// Alarm B hours mask
				PM = RegBitField!uint(22,1,RegisterAccess.RW),		// AM/PM notation
				HT = RegBitField!uint(20,2,RegisterAccess.RW),		// Hour tens in BCD format
				HU = RegBitField!uint(16,4,RegisterAccess.RW),		// Hour units in BCD format
				MSK2 = RegBitField!uint(15,1,RegisterAccess.RW),		// Alarm B minutes mask
				MNT = RegBitField!uint(12,3,RegisterAccess.RW),		// Minute tens in BCD format
				MNU = RegBitField!uint(8,4,RegisterAccess.RW),		// Minute units in BCD format
				MSK1 = RegBitField!uint(7,1,RegisterAccess.RW),		// Alarm B seconds mask
				ST = RegBitField!uint(4,3,RegisterAccess.RW),		// Second tens in BCD format
				SU = RegBitField!uint(0,4,RegisterAccess.RW),		// Second units in BCD format
		}
		enum mask_WPR {
				KEY = RegBitField!uint(0,8,RegisterAccess.WO),		// Write protection key
		}
		enum mask_SSR {
				SS = RegBitField!uint(0,16,RegisterAccess.RO),		// Sub second value
		}
		enum mask_SHIFTR {
				ADD1S = RegBitField!uint(31,1,RegisterAccess.WO),		// Add one second
				SUBFS = RegBitField!uint(0,15,RegisterAccess.WO),		// Subtract a fraction of a second
		}
		enum mask_TSTR {
				PM = RegBitField!uint(22,1,RegisterAccess.RO),		// AM/PM notation
				HT = RegBitField!uint(20,2,RegisterAccess.RO),		// Hour tens in BCD format
				HU = RegBitField!uint(16,4,RegisterAccess.RO),		// Hour units in BCD format
				MNT = RegBitField!uint(12,3,RegisterAccess.RO),		// Minute tens in BCD format
				MNU = RegBitField!uint(8,4,RegisterAccess.RO),		// Minute units in BCD format
				ST = RegBitField!uint(4,3,RegisterAccess.RO),		// Second tens in BCD format
				SU = RegBitField!uint(0,4,RegisterAccess.RO),		// Second units in BCD format
		}
		enum mask_TSDR {
				WDU = RegBitField!uint(13,3,RegisterAccess.RO),		// Week day units
				MT = RegBitField!uint(12,1,RegisterAccess.RO),		// Month tens in BCD format
				MU = RegBitField!uint(8,4,RegisterAccess.RO),		// Month units in BCD format
				DT = RegBitField!uint(4,2,RegisterAccess.RO),		// Date tens in BCD format
				DU = RegBitField!uint(0,4,RegisterAccess.RO),		// Date units in BCD format
		}
		enum mask_TSSSR {
				SS = RegBitField!uint(0,16,RegisterAccess.RO),		// Sub second value
		}
		enum mask_CALR {
				CALP = RegBitField!uint(15,1,RegisterAccess.RW),		// Increase frequency of RTC by 488.5 ppm
				CALW8 = RegBitField!uint(14,1,RegisterAccess.RW),		// Use an 8-second calibration cycle period
				CALW16 = RegBitField!uint(13,1,RegisterAccess.RW),		// Use a 16-second calibration cycle period
				CALM = RegBitField!uint(0,9,RegisterAccess.RW),		// Calibration minus
		}
		enum mask_TAFCR {
				ALARMOUTTYPE = RegBitField!uint(18,1,RegisterAccess.RW),		// AFO_ALARM output type
				TSINSEL = RegBitField!uint(17,1,RegisterAccess.RW),		// TIMESTAMP mapping
				TAMP1INSEL = RegBitField!uint(16,1,RegisterAccess.RW),		// TAMPER1 mapping
				TAMPPUDIS = RegBitField!uint(15,1,RegisterAccess.RW),		// TAMPER pull-up disable
				TAMPPRCH = RegBitField!uint(13,2,RegisterAccess.RW),		// Tamper precharge duration
				TAMPFLT = RegBitField!uint(11,2,RegisterAccess.RW),		// Tamper filter count
				TAMPFREQ = RegBitField!uint(8,3,RegisterAccess.RW),		// Tamper sampling frequency
				TAMPTS = RegBitField!uint(7,1,RegisterAccess.RW),		// Activate timestamp on tamper detection event
				TAMP2TRG = RegBitField!uint(4,1,RegisterAccess.RW),		// Active level for tamper 2
				TAMP2E = RegBitField!uint(3,1,RegisterAccess.RW),		// Tamper 2 detection enable
				TAMPIE = RegBitField!uint(2,1,RegisterAccess.RW),		// Tamper interrupt enable
				TAMP1TRG = RegBitField!uint(1,1,RegisterAccess.RW),		// Active level for tamper 1
				TAMP1E = RegBitField!uint(0,1,RegisterAccess.RW),		// Tamper 1 detection enable
		}
		enum mask_ALRMASSR {
				MASKSS = RegBitField!uint(24,4,RegisterAccess.RW),		// Mask the most-significant bits starting at this bit
				SS = RegBitField!uint(0,15,RegisterAccess.RW),		// Sub seconds value
		}
		enum mask_ALRMBSSR {
				MASKSS = RegBitField!uint(24,4,RegisterAccess.RW),		// Mask the most-significant bits starting at this bit
				SS = RegBitField!uint(0,15,RegisterAccess.RW),		// Sub seconds value
		}
		enum mask_BKP0R {
				BKP = RegBitField!uint(0,32,RegisterAccess.RW),		// BKP
		}
		enum mask_BKP1R {
				BKP = RegBitField!uint(0,32,RegisterAccess.RW),		// BKP
		}
		enum mask_BKP2R {
				BKP = RegBitField!uint(0,32,RegisterAccess.RW),		// BKP
		}
		enum mask_BKP3R {
				BKP = RegBitField!uint(0,32,RegisterAccess.RW),		// BKP
		}
		enum mask_BKP4R {
				BKP = RegBitField!uint(0,32,RegisterAccess.RW),		// BKP
		}
		enum mask_BKP5R {
				BKP = RegBitField!uint(0,32,RegisterAccess.RW),		// BKP
		}
		enum mask_BKP6R {
				BKP = RegBitField!uint(0,32,RegisterAccess.RW),		// BKP
		}
		enum mask_BKP7R {
				BKP = RegBitField!uint(0,32,RegisterAccess.RW),		// BKP
		}
		enum mask_BKP8R {
				BKP = RegBitField!uint(0,32,RegisterAccess.RW),		// BKP
		}
		enum mask_BKP9R {
				BKP = RegBitField!uint(0,32,RegisterAccess.RW),		// BKP
		}
		enum mask_BKP10R {
				BKP = RegBitField!uint(0,32,RegisterAccess.RW),		// BKP
		}
		enum mask_BKP11R {
				BKP = RegBitField!uint(0,32,RegisterAccess.RW),		// BKP
		}
		enum mask_BKP12R {
				BKP = RegBitField!uint(0,32,RegisterAccess.RW),		// BKP
		}
		enum mask_BKP13R {
				BKP = RegBitField!uint(0,32,RegisterAccess.RW),		// BKP
		}
		enum mask_BKP14R {
				BKP = RegBitField!uint(0,32,RegisterAccess.RW),		// BKP
		}
		enum mask_BKP15R {
				BKP = RegBitField!uint(0,32,RegisterAccess.RW),		// BKP
		}
		enum mask_BKP16R {
				BKP = RegBitField!uint(0,32,RegisterAccess.RW),		// BKP
		}
		enum mask_BKP17R {
				BKP = RegBitField!uint(0,32,RegisterAccess.RW),		// BKP
		}
		enum mask_BKP18R {
				BKP = RegBitField!uint(0,32,RegisterAccess.RW),		// BKP
		}
		enum mask_BKP19R {
				BKP = RegBitField!uint(0,32,RegisterAccess.RW),		// BKP
		}
	}
	// NVIC Group 
	// Nested Vectored Interrupt Controller
	struct NVIC_Type {
		private uint[0x1] reserved0;	// skip 4 bytes
		@RegInfo("ICTR",0x4) RegVolatile!(mask_ICTR,uint,0xe000e004,0x0,RegisterAccess.RO) ICTR;		 // offset:0x4 ICTR Interrupt Controller Type Register
		private uint[0x3e] reserved1;	// skip 248 bytes
		@RegInfo("ISER0",0x100) RegVolatile!(mask_ISER0,uint,0xe000e100,0x0,RegisterAccess.RW) ISER0;		 // offset:0x256 ISER0 Interrupt Set-Enable Register
		@RegInfo("ISER1",0x104) RegVolatile!(mask_ISER1,uint,0xe000e104,0x0,RegisterAccess.RW) ISER1;		 // offset:0x260 ISER1 Interrupt Set-Enable Register
		@RegInfo("ISER2",0x108) RegVolatile!(mask_ISER2,uint,0xe000e108,0x0,RegisterAccess.RW) ISER2;		 // offset:0x264 ISER2 Interrupt Set-Enable Register
		private uint[0x1d] reserved4;	// skip 116 bytes
		@RegInfo("ICER0",0x180) RegVolatile!(mask_ICER0,uint,0xe000e180,0x0,RegisterAccess.RW) ICER0;		 // offset:0x384 ICER0 Interrupt Clear-Enable Register
		@RegInfo("ICER1",0x184) RegVolatile!(mask_ICER1,uint,0xe000e184,0x0,RegisterAccess.RW) ICER1;		 // offset:0x388 ICER1 Interrupt Clear-Enable Register
		@RegInfo("ICER2",0x188) RegVolatile!(mask_ICER2,uint,0xe000e188,0x0,RegisterAccess.RW) ICER2;		 // offset:0x392 ICER2 Interrupt Clear-Enable Register
		private uint[0x1d] reserved7;	// skip 116 bytes
		@RegInfo("ISPR0",0x200) RegVolatile!(mask_ISPR0,uint,0xe000e200,0x0,RegisterAccess.RW) ISPR0;		 // offset:0x512 ISPR0 Interrupt Set-Pending Register
		@RegInfo("ISPR1",0x204) RegVolatile!(mask_ISPR1,uint,0xe000e204,0x0,RegisterAccess.RW) ISPR1;		 // offset:0x516 ISPR1 Interrupt Set-Pending Register
		@RegInfo("ISPR2",0x208) RegVolatile!(mask_ISPR2,uint,0xe000e208,0x0,RegisterAccess.RW) ISPR2;		 // offset:0x520 ISPR2 Interrupt Set-Pending Register
		private uint[0x1d] reserved10;	// skip 116 bytes
		@RegInfo("ICPR0",0x280) RegVolatile!(mask_ICPR0,uint,0xe000e280,0x0,RegisterAccess.RW) ICPR0;		 // offset:0x640 ICPR0 Interrupt Clear-Pending Register
		@RegInfo("ICPR1",0x284) RegVolatile!(mask_ICPR1,uint,0xe000e284,0x0,RegisterAccess.RW) ICPR1;		 // offset:0x644 ICPR1 Interrupt Clear-Pending Register
		@RegInfo("ICPR2",0x288) RegVolatile!(mask_ICPR2,uint,0xe000e288,0x0,RegisterAccess.RW) ICPR2;		 // offset:0x648 ICPR2 Interrupt Clear-Pending Register
		private uint[0x1d] reserved13;	// skip 116 bytes
		@RegInfo("IABR0",0x300) RegVolatile!(mask_IABR0,uint,0xe000e300,0x0,RegisterAccess.RO) IABR0;		 // offset:0x768 IABR0 Interrupt Active Bit Register
		@RegInfo("IABR1",0x304) RegVolatile!(mask_IABR1,uint,0xe000e304,0x0,RegisterAccess.RO) IABR1;		 // offset:0x772 IABR1 Interrupt Active Bit Register
		@RegInfo("IABR2",0x308) RegVolatile!(mask_IABR2,uint,0xe000e308,0x0,RegisterAccess.RO) IABR2;		 // offset:0x776 IABR2 Interrupt Active Bit Register
		private uint[0x3d] reserved16;	// skip 244 bytes
		@RegInfo("IPR0",0x400) RegVolatile!(mask_IPR0,uint,0xe000e400,0x0,RegisterAccess.RW) IPR0;		 // offset:0x1024 IPR0 Interrupt Priority Register
		@RegInfo("IPR1",0x404) RegVolatile!(mask_IPR1,uint,0xe000e404,0x0,RegisterAccess.RW) IPR1;		 // offset:0x1028 IPR1 Interrupt Priority Register
		@RegInfo("IPR2",0x408) RegVolatile!(mask_IPR2,uint,0xe000e408,0x0,RegisterAccess.RW) IPR2;		 // offset:0x1032 IPR2 Interrupt Priority Register
		@RegInfo("IPR3",0x40c) RegVolatile!(mask_IPR3,uint,0xe000e40c,0x0,RegisterAccess.RW) IPR3;		 // offset:0x1036 IPR3 Interrupt Priority Register
		@RegInfo("IPR4",0x410) RegVolatile!(mask_IPR4,uint,0xe000e410,0x0,RegisterAccess.RW) IPR4;		 // offset:0x1040 IPR4 Interrupt Priority Register
		@RegInfo("IPR5",0x414) RegVolatile!(mask_IPR5,uint,0xe000e414,0x0,RegisterAccess.RW) IPR5;		 // offset:0x1044 IPR5 Interrupt Priority Register
		@RegInfo("IPR6",0x418) RegVolatile!(mask_IPR6,uint,0xe000e418,0x0,RegisterAccess.RW) IPR6;		 // offset:0x1048 IPR6 Interrupt Priority Register
		@RegInfo("IPR7",0x41c) RegVolatile!(mask_IPR7,uint,0xe000e41c,0x0,RegisterAccess.RW) IPR7;		 // offset:0x1052 IPR7 Interrupt Priority Register
		@RegInfo("IPR8",0x420) RegVolatile!(mask_IPR8,uint,0xe000e420,0x0,RegisterAccess.RW) IPR8;		 // offset:0x1056 IPR8 Interrupt Priority Register
		@RegInfo("IPR9",0x424) RegVolatile!(mask_IPR9,uint,0xe000e424,0x0,RegisterAccess.RW) IPR9;		 // offset:0x1060 IPR9 Interrupt Priority Register
		@RegInfo("IPR10",0x428) RegVolatile!(mask_IPR10,uint,0xe000e428,0x0,RegisterAccess.RW) IPR10;		 // offset:0x1064 IPR10 Interrupt Priority Register
		@RegInfo("IPR11",0x42c) RegVolatile!(mask_IPR11,uint,0xe000e42c,0x0,RegisterAccess.RW) IPR11;		 // offset:0x1068 IPR11 Interrupt Priority Register
		@RegInfo("IPR12",0x430) RegVolatile!(mask_IPR12,uint,0xe000e430,0x0,RegisterAccess.RW) IPR12;		 // offset:0x1072 IPR12 Interrupt Priority Register
		@RegInfo("IPR13",0x434) RegVolatile!(mask_IPR13,uint,0xe000e434,0x0,RegisterAccess.RW) IPR13;		 // offset:0x1076 IPR13 Interrupt Priority Register
		@RegInfo("IPR14",0x438) RegVolatile!(mask_IPR14,uint,0xe000e438,0x0,RegisterAccess.RW) IPR14;		 // offset:0x1080 IPR14 Interrupt Priority Register
		@RegInfo("IPR15",0x43c) RegVolatile!(mask_IPR15,uint,0xe000e43c,0x0,RegisterAccess.RW) IPR15;		 // offset:0x1084 IPR15 Interrupt Priority Register
		@RegInfo("IPR16",0x440) RegVolatile!(mask_IPR16,uint,0xe000e440,0x0,RegisterAccess.RW) IPR16;		 // offset:0x1088 IPR16 Interrupt Priority Register
		@RegInfo("IPR17",0x444) RegVolatile!(mask_IPR17,uint,0xe000e444,0x0,RegisterAccess.RW) IPR17;		 // offset:0x1092 IPR17 Interrupt Priority Register
		@RegInfo("IPR18",0x448) RegVolatile!(mask_IPR18,uint,0xe000e448,0x0,RegisterAccess.RW) IPR18;		 // offset:0x1096 IPR18 Interrupt Priority Register
		@RegInfo("IPR19",0x44c) RegVolatile!(mask_IPR19,uint,0xe000e44c,0x0,RegisterAccess.RW) IPR19;		 // offset:0x1100 IPR19 Interrupt Priority Register
		private uint[0x2ac] reserved36;	// skip 2736 bytes
		@RegInfo("STIR",0xf00) RegVolatile!(mask_STIR,uint,0xe000ef00,0x0,RegisterAccess.WO) STIR;		 // offset:0x3840 STIR Software Triggered Interrupt Register
		enum mask_ICTR {
				INTLINESNUM = RegBitField!uint(0,4,RegisterAccess.RO),		// Total number of interrupt lines in groups
		}
		enum mask_ISER0 {
				SETENA = RegBitField!uint(0,32,RegisterAccess.RW),		// SETENA
		}
		enum mask_ISER1 {
				SETENA = RegBitField!uint(0,32,RegisterAccess.RW),		// SETENA
		}
		enum mask_ISER2 {
				SETENA = RegBitField!uint(0,32,RegisterAccess.RW),		// SETENA
		}
		enum mask_ICER0 {
				CLRENA = RegBitField!uint(0,32,RegisterAccess.RW),		// CLRENA
		}
		enum mask_ICER1 {
				CLRENA = RegBitField!uint(0,32,RegisterAccess.RW),		// CLRENA
		}
		enum mask_ICER2 {
				CLRENA = RegBitField!uint(0,32,RegisterAccess.RW),		// CLRENA
		}
		enum mask_ISPR0 {
				SETPEND = RegBitField!uint(0,32,RegisterAccess.RW),		// SETPEND
		}
		enum mask_ISPR1 {
				SETPEND = RegBitField!uint(0,32,RegisterAccess.RW),		// SETPEND
		}
		enum mask_ISPR2 {
				SETPEND = RegBitField!uint(0,32,RegisterAccess.RW),		// SETPEND
		}
		enum mask_ICPR0 {
				CLRPEND = RegBitField!uint(0,32,RegisterAccess.RW),		// CLRPEND
		}
		enum mask_ICPR1 {
				CLRPEND = RegBitField!uint(0,32,RegisterAccess.RW),		// CLRPEND
		}
		enum mask_ICPR2 {
				CLRPEND = RegBitField!uint(0,32,RegisterAccess.RW),		// CLRPEND
		}
		enum mask_IABR0 {
				ACTIVE = RegBitField!uint(0,32,RegisterAccess.RO),		// ACTIVE
		}
		enum mask_IABR1 {
				ACTIVE = RegBitField!uint(0,32,RegisterAccess.RO),		// ACTIVE
		}
		enum mask_IABR2 {
				ACTIVE = RegBitField!uint(0,32,RegisterAccess.RO),		// ACTIVE
		}
		enum mask_IPR0 {
				IPR_N0 = RegBitField!uint(0,8,RegisterAccess.RW),		// IPR_N0
				IPR_N1 = RegBitField!uint(8,8,RegisterAccess.RW),		// IPR_N1
				IPR_N2 = RegBitField!uint(16,8,RegisterAccess.RW),		// IPR_N2
				IPR_N3 = RegBitField!uint(24,8,RegisterAccess.RW),		// IPR_N3
		}
		enum mask_IPR1 {
				IPR_N0 = RegBitField!uint(0,8,RegisterAccess.RW),		// IPR_N0
				IPR_N1 = RegBitField!uint(8,8,RegisterAccess.RW),		// IPR_N1
				IPR_N2 = RegBitField!uint(16,8,RegisterAccess.RW),		// IPR_N2
				IPR_N3 = RegBitField!uint(24,8,RegisterAccess.RW),		// IPR_N3
		}
		enum mask_IPR2 {
				IPR_N0 = RegBitField!uint(0,8,RegisterAccess.RW),		// IPR_N0
				IPR_N1 = RegBitField!uint(8,8,RegisterAccess.RW),		// IPR_N1
				IPR_N2 = RegBitField!uint(16,8,RegisterAccess.RW),		// IPR_N2
				IPR_N3 = RegBitField!uint(24,8,RegisterAccess.RW),		// IPR_N3
		}
		enum mask_IPR3 {
				IPR_N0 = RegBitField!uint(0,8,RegisterAccess.RW),		// IPR_N0
				IPR_N1 = RegBitField!uint(8,8,RegisterAccess.RW),		// IPR_N1
				IPR_N2 = RegBitField!uint(16,8,RegisterAccess.RW),		// IPR_N2
				IPR_N3 = RegBitField!uint(24,8,RegisterAccess.RW),		// IPR_N3
		}
		enum mask_IPR4 {
				IPR_N0 = RegBitField!uint(0,8,RegisterAccess.RW),		// IPR_N0
				IPR_N1 = RegBitField!uint(8,8,RegisterAccess.RW),		// IPR_N1
				IPR_N2 = RegBitField!uint(16,8,RegisterAccess.RW),		// IPR_N2
				IPR_N3 = RegBitField!uint(24,8,RegisterAccess.RW),		// IPR_N3
		}
		enum mask_IPR5 {
				IPR_N0 = RegBitField!uint(0,8,RegisterAccess.RW),		// IPR_N0
				IPR_N1 = RegBitField!uint(8,8,RegisterAccess.RW),		// IPR_N1
				IPR_N2 = RegBitField!uint(16,8,RegisterAccess.RW),		// IPR_N2
				IPR_N3 = RegBitField!uint(24,8,RegisterAccess.RW),		// IPR_N3
		}
		enum mask_IPR6 {
				IPR_N0 = RegBitField!uint(0,8,RegisterAccess.RW),		// IPR_N0
				IPR_N1 = RegBitField!uint(8,8,RegisterAccess.RW),		// IPR_N1
				IPR_N2 = RegBitField!uint(16,8,RegisterAccess.RW),		// IPR_N2
				IPR_N3 = RegBitField!uint(24,8,RegisterAccess.RW),		// IPR_N3
		}
		enum mask_IPR7 {
				IPR_N0 = RegBitField!uint(0,8,RegisterAccess.RW),		// IPR_N0
				IPR_N1 = RegBitField!uint(8,8,RegisterAccess.RW),		// IPR_N1
				IPR_N2 = RegBitField!uint(16,8,RegisterAccess.RW),		// IPR_N2
				IPR_N3 = RegBitField!uint(24,8,RegisterAccess.RW),		// IPR_N3
		}
		enum mask_IPR8 {
				IPR_N0 = RegBitField!uint(0,8,RegisterAccess.RW),		// IPR_N0
				IPR_N1 = RegBitField!uint(8,8,RegisterAccess.RW),		// IPR_N1
				IPR_N2 = RegBitField!uint(16,8,RegisterAccess.RW),		// IPR_N2
				IPR_N3 = RegBitField!uint(24,8,RegisterAccess.RW),		// IPR_N3
		}
		enum mask_IPR9 {
				IPR_N0 = RegBitField!uint(0,8,RegisterAccess.RW),		// IPR_N0
				IPR_N1 = RegBitField!uint(8,8,RegisterAccess.RW),		// IPR_N1
				IPR_N2 = RegBitField!uint(16,8,RegisterAccess.RW),		// IPR_N2
				IPR_N3 = RegBitField!uint(24,8,RegisterAccess.RW),		// IPR_N3
		}
		enum mask_IPR10 {
				IPR_N0 = RegBitField!uint(0,8,RegisterAccess.RW),		// IPR_N0
				IPR_N1 = RegBitField!uint(8,8,RegisterAccess.RW),		// IPR_N1
				IPR_N2 = RegBitField!uint(16,8,RegisterAccess.RW),		// IPR_N2
				IPR_N3 = RegBitField!uint(24,8,RegisterAccess.RW),		// IPR_N3
		}
		enum mask_IPR11 {
				IPR_N0 = RegBitField!uint(0,8,RegisterAccess.RW),		// IPR_N0
				IPR_N1 = RegBitField!uint(8,8,RegisterAccess.RW),		// IPR_N1
				IPR_N2 = RegBitField!uint(16,8,RegisterAccess.RW),		// IPR_N2
				IPR_N3 = RegBitField!uint(24,8,RegisterAccess.RW),		// IPR_N3
		}
		enum mask_IPR12 {
				IPR_N0 = RegBitField!uint(0,8,RegisterAccess.RW),		// IPR_N0
				IPR_N1 = RegBitField!uint(8,8,RegisterAccess.RW),		// IPR_N1
				IPR_N2 = RegBitField!uint(16,8,RegisterAccess.RW),		// IPR_N2
				IPR_N3 = RegBitField!uint(24,8,RegisterAccess.RW),		// IPR_N3
		}
		enum mask_IPR13 {
				IPR_N0 = RegBitField!uint(0,8,RegisterAccess.RW),		// IPR_N0
				IPR_N1 = RegBitField!uint(8,8,RegisterAccess.RW),		// IPR_N1
				IPR_N2 = RegBitField!uint(16,8,RegisterAccess.RW),		// IPR_N2
				IPR_N3 = RegBitField!uint(24,8,RegisterAccess.RW),		// IPR_N3
		}
		enum mask_IPR14 {
				IPR_N0 = RegBitField!uint(0,8,RegisterAccess.RW),		// IPR_N0
				IPR_N1 = RegBitField!uint(8,8,RegisterAccess.RW),		// IPR_N1
				IPR_N2 = RegBitField!uint(16,8,RegisterAccess.RW),		// IPR_N2
				IPR_N3 = RegBitField!uint(24,8,RegisterAccess.RW),		// IPR_N3
		}
		enum mask_IPR15 {
				IPR_N0 = RegBitField!uint(0,8,RegisterAccess.RW),		// IPR_N0
				IPR_N1 = RegBitField!uint(8,8,RegisterAccess.RW),		// IPR_N1
				IPR_N2 = RegBitField!uint(16,8,RegisterAccess.RW),		// IPR_N2
				IPR_N3 = RegBitField!uint(24,8,RegisterAccess.RW),		// IPR_N3
		}
		enum mask_IPR16 {
				IPR_N0 = RegBitField!uint(0,8,RegisterAccess.RW),		// IPR_N0
				IPR_N1 = RegBitField!uint(8,8,RegisterAccess.RW),		// IPR_N1
				IPR_N2 = RegBitField!uint(16,8,RegisterAccess.RW),		// IPR_N2
				IPR_N3 = RegBitField!uint(24,8,RegisterAccess.RW),		// IPR_N3
		}
		enum mask_IPR17 {
				IPR_N0 = RegBitField!uint(0,8,RegisterAccess.RW),		// IPR_N0
				IPR_N1 = RegBitField!uint(8,8,RegisterAccess.RW),		// IPR_N1
				IPR_N2 = RegBitField!uint(16,8,RegisterAccess.RW),		// IPR_N2
				IPR_N3 = RegBitField!uint(24,8,RegisterAccess.RW),		// IPR_N3
		}
		enum mask_IPR18 {
				IPR_N0 = RegBitField!uint(0,8,RegisterAccess.RW),		// IPR_N0
				IPR_N1 = RegBitField!uint(8,8,RegisterAccess.RW),		// IPR_N1
				IPR_N2 = RegBitField!uint(16,8,RegisterAccess.RW),		// IPR_N2
				IPR_N3 = RegBitField!uint(24,8,RegisterAccess.RW),		// IPR_N3
		}
		enum mask_IPR19 {
				IPR_N0 = RegBitField!uint(0,8,RegisterAccess.RW),		// IPR_N0
				IPR_N1 = RegBitField!uint(8,8,RegisterAccess.RW),		// IPR_N1
				IPR_N2 = RegBitField!uint(16,8,RegisterAccess.RW),		// IPR_N2
				IPR_N3 = RegBitField!uint(24,8,RegisterAccess.RW),		// IPR_N3
		}
		enum mask_STIR {
				INTID = RegBitField!uint(0,9,RegisterAccess.WO),		// interrupt to be triggered
		}
	}
	// SPI Group 
	// Serial peripheral interface
	struct SPI1_Type {
		@RegInfo("CR1",0x0) RegVolatile!(mask_CR1,uint,0x40013000,0x0,RegisterAccess.RW) CR1;		 // offset:0x0 CR1 control register 1
		@RegInfo("CR2",0x4) RegVolatile!(mask_CR2,uint,0x40013004,0x0,RegisterAccess.RW) CR2;		 // offset:0x4 CR2 control register 2
		@RegInfo("SR",0x8) RegVolatile!(mask_SR,uint,0x40013008,0x2,RegisterAccess.NO) SR;		 // offset:0x8 SR status register
		@RegInfo("DR",0xc) RegVolatile!(mask_DR,uint,0x4001300c,0x0,RegisterAccess.RW) DR;		 // offset:0x12 DR data register
		@RegInfo("CRCPR",0x10) RegVolatile!(mask_CRCPR,uint,0x40013010,0x7,RegisterAccess.RW) CRCPR;		 // offset:0x16 CRCPR CRC polynomial register
		@RegInfo("RXCRCR",0x14) RegVolatile!(mask_RXCRCR,uint,0x40013014,0x0,RegisterAccess.RO) RXCRCR;		 // offset:0x20 RXCRCR RX CRC register
		@RegInfo("TXCRCR",0x18) RegVolatile!(mask_TXCRCR,uint,0x40013018,0x0,RegisterAccess.RO) TXCRCR;		 // offset:0x24 TXCRCR TX CRC register
		@RegInfo("I2SCFGR",0x1c) RegVolatile!(mask_I2SCFGR,uint,0x4001301c,0x0,RegisterAccess.RW) I2SCFGR;		 // offset:0x28 I2SCFGR I2S configuration register
		@RegInfo("I2SPR",0x20) RegVolatile!(mask_I2SPR,uint,0x40013020,0xa,RegisterAccess.RW) I2SPR;		 // offset:0x32 I2SPR I2S prescaler register
		enum mask_CR1 {
				BIDIMODE = RegBitField!uint(15,1,RegisterAccess.RW),		// Bidirectional data mode enable
				BIDIOE = RegBitField!uint(14,1,RegisterAccess.RW),		// Output enable in bidirectional mode
				CRCEN = RegBitField!uint(13,1,RegisterAccess.RW),		// Hardware CRC calculation enable
				CRCNEXT = RegBitField!uint(12,1,RegisterAccess.RW),		// CRC transfer next
				DFF = RegBitField!uint(11,1,RegisterAccess.RW),		// Data frame format
				RXONLY = RegBitField!uint(10,1,RegisterAccess.RW),		// Receive only
				SSM = RegBitField!uint(9,1,RegisterAccess.RW),		// Software slave management
				SSI = RegBitField!uint(8,1,RegisterAccess.RW),		// Internal slave select
				LSBFIRST = RegBitField!uint(7,1,RegisterAccess.RW),		// Frame format
				SPE = RegBitField!uint(6,1,RegisterAccess.RW),		// SPI enable
				BR = RegBitField!uint(3,3,RegisterAccess.RW),		// Baud rate control
				MSTR = RegBitField!uint(2,1,RegisterAccess.RW),		// Master selection
				CPOL = RegBitField!uint(1,1,RegisterAccess.RW),		// Clock polarity
				CPHA = RegBitField!uint(0,1,RegisterAccess.RW),		// Clock phase
		}
		enum mask_CR2 {
				TXEIE = RegBitField!uint(7,1,RegisterAccess.RW),		// Tx buffer empty interrupt enable
				RXNEIE = RegBitField!uint(6,1,RegisterAccess.RW),		// RX buffer not empty interrupt enable
				ERRIE = RegBitField!uint(5,1,RegisterAccess.RW),		// Error interrupt enable
				FRF = RegBitField!uint(4,1,RegisterAccess.RW),		// Frame format
				SSOE = RegBitField!uint(2,1,RegisterAccess.RW),		// SS output enable
				TXDMAEN = RegBitField!uint(1,1,RegisterAccess.RW),		// Tx buffer DMA enable
				RXDMAEN = RegBitField!uint(0,1,RegisterAccess.RW),		// Rx buffer DMA enable
		}
		enum mask_SR {
				TIFRFE = RegBitField!uint(8,1,RegisterAccess.RO),		// TI frame format error
				BSY = RegBitField!uint(7,1,RegisterAccess.RO),		// Busy flag
				OVR = RegBitField!uint(6,1,RegisterAccess.RO),		// Overrun flag
				MODF = RegBitField!uint(5,1,RegisterAccess.RO),		// Mode fault
				CRCERR = RegBitField!uint(4,1,RegisterAccess.RW),		// CRC error flag
				UDR = RegBitField!uint(3,1,RegisterAccess.RO),		// Underrun flag
				CHSIDE = RegBitField!uint(2,1,RegisterAccess.RO),		// Channel side
				TXE = RegBitField!uint(1,1,RegisterAccess.RO),		// Transmit buffer empty
				RXNE = RegBitField!uint(0,1,RegisterAccess.RO),		// Receive buffer not empty
		}
		enum mask_DR {
				DR = RegBitField!uint(0,16,RegisterAccess.RW),		// Data register
		}
		enum mask_CRCPR {
				CRCPOLY = RegBitField!uint(0,16,RegisterAccess.RW),		// CRC polynomial register
		}
		enum mask_RXCRCR {
				RxCRC = RegBitField!uint(0,16,RegisterAccess.RO),		// Rx CRC register
		}
		enum mask_TXCRCR {
				TxCRC = RegBitField!uint(0,16,RegisterAccess.RO),		// Tx CRC register
		}
		enum mask_I2SCFGR {
				I2SMOD = RegBitField!uint(11,1,RegisterAccess.RW),		// I2S mode selection
				I2SE = RegBitField!uint(10,1,RegisterAccess.RW),		// I2S Enable
				I2SCFG = RegBitField!uint(8,2,RegisterAccess.RW),		// I2S configuration mode
				PCMSYNC = RegBitField!uint(7,1,RegisterAccess.RW),		// PCM frame synchronization
				I2SSTD = RegBitField!uint(4,2,RegisterAccess.RW),		// I2S standard selection
				CKPOL = RegBitField!uint(3,1,RegisterAccess.RW),		// Steady state clock polarity
				DATLEN = RegBitField!uint(1,2,RegisterAccess.RW),		// Data length to be transferred
				CHLEN = RegBitField!uint(0,1,RegisterAccess.RW),		// Channel length (number of bits per audio channel)
		}
		enum mask_I2SPR {
				MCKOE = RegBitField!uint(9,1,RegisterAccess.RW),		// Master clock output enable
				ODD = RegBitField!uint(8,1,RegisterAccess.RW),		// Odd factor for the prescaler
				I2SDIV = RegBitField!uint(0,8,RegisterAccess.RW),		// I2S Linear prescaler
		}
	}
	// FPU Group 
	// Floting point unit
	struct FPU_Type {
		@RegInfo("FPCCR",0x0) RegVolatile!(mask_FPCCR,uint,0xe000ef34,0x0,RegisterAccess.RW) FPCCR;		 // offset:0x0 FPCCR Floating-point context control register
		@RegInfo("FPCAR",0x4) RegVolatile!(mask_FPCAR,uint,0xe000ef38,0x0,RegisterAccess.RW) FPCAR;		 // offset:0x4 FPCAR Floating-point context address register
		@RegInfo("FPSCR",0x8) RegVolatile!(mask_FPSCR,uint,0xe000ef3c,0x0,RegisterAccess.RW) FPSCR;		 // offset:0x8 FPSCR Floating-point status control register
		enum mask_FPCCR {
				LSPACT = RegBitField!uint(0,1,RegisterAccess.RW),		// LSPACT
				USER = RegBitField!uint(1,1,RegisterAccess.RW),		// USER
				THREAD = RegBitField!uint(3,1,RegisterAccess.RW),		// THREAD
				HFRDY = RegBitField!uint(4,1,RegisterAccess.RW),		// HFRDY
				MMRDY = RegBitField!uint(5,1,RegisterAccess.RW),		// MMRDY
				BFRDY = RegBitField!uint(6,1,RegisterAccess.RW),		// BFRDY
				MONRDY = RegBitField!uint(8,1,RegisterAccess.RW),		// MONRDY
				LSPEN = RegBitField!uint(30,1,RegisterAccess.RW),		// LSPEN
				ASPEN = RegBitField!uint(31,1,RegisterAccess.RW),		// ASPEN
		}
		enum mask_FPCAR {
				ADDRESS = RegBitField!uint(3,29,RegisterAccess.RW),		// Location of unpopulated floating-point
		}
		enum mask_FPSCR {
				IOC = RegBitField!uint(0,1,RegisterAccess.RW),		// Invalid operation cumulative exception bit
				DZC = RegBitField!uint(1,1,RegisterAccess.RW),		// Division by zero cumulative exception bit.
				OFC = RegBitField!uint(2,1,RegisterAccess.RW),		// Overflow cumulative exception bit
				UFC = RegBitField!uint(3,1,RegisterAccess.RW),		// Underflow cumulative exception bit
				IXC = RegBitField!uint(4,1,RegisterAccess.RW),		// Inexact cumulative exception bit
				IDC = RegBitField!uint(7,1,RegisterAccess.RW),		// Input denormal cumulative exception bit.
				RMode = RegBitField!uint(22,2,RegisterAccess.RW),		// Rounding Mode control field
				FZ = RegBitField!uint(24,1,RegisterAccess.RW),		// Flush-to-zero mode control bit:
				DN = RegBitField!uint(25,1,RegisterAccess.RW),		// Default NaN mode control bit
				AHP = RegBitField!uint(26,1,RegisterAccess.RW),		// Alternative half-precision control bit
				V = RegBitField!uint(28,1,RegisterAccess.RW),		// Overflow condition code flag
				C = RegBitField!uint(29,1,RegisterAccess.RW),		// Carry condition code flag
				Z = RegBitField!uint(30,1,RegisterAccess.RW),		// Zero condition code flag
				N = RegBitField!uint(31,1,RegisterAccess.RW),		// Negative condition code flag
		}
	}
	// CRC Group 
	// Cryptographic processor
	struct CRC_Type {
		@RegInfo("DR",0x0) RegVolatile!(mask_DR,uint,0x40023000,0xffffffff,RegisterAccess.RW) DR;		 // offset:0x0 DR Data register
		@RegInfo("IDR",0x4) RegVolatile!(mask_IDR,uint,0x40023004,0x0,RegisterAccess.RW) IDR;		 // offset:0x4 IDR Independent Data register
		@RegInfo("CR",0x8) RegVolatile!(mask_CR,uint,0x40023008,0x0,RegisterAccess.WO) CR;		 // offset:0x8 CR Control register
		enum mask_DR {
				DR = RegBitField!uint(0,32,RegisterAccess.RW),		// Data Register
		}
		enum mask_IDR {
				IDR = RegBitField!uint(0,8,RegisterAccess.RW),		// Independent Data register
		}
		enum mask_CR {
				CR = RegBitField!uint(0,1,RegisterAccess.WO),		// Control regidter
		}
	}
	// IWDG Group 
	// Independent watchdog
	struct IWDG_Type {
		@RegInfo("KR",0x0) RegVolatile!(mask_KR,uint,0x40003000,0x0,RegisterAccess.WO) KR;		 // offset:0x0 KR Key register
		@RegInfo("PR",0x4) RegVolatile!(mask_PR,uint,0x40003004,0x0,RegisterAccess.RW) PR;		 // offset:0x4 PR Prescaler register
		@RegInfo("RLR",0x8) RegVolatile!(mask_RLR,uint,0x40003008,0xfff,RegisterAccess.RW) RLR;		 // offset:0x8 RLR Reload register
		@RegInfo("SR",0xc) RegVolatile!(mask_SR,uint,0x4000300c,0x0,RegisterAccess.RO) SR;		 // offset:0x12 SR Status register
		enum mask_KR {
				KEY = RegBitField!uint(0,16,RegisterAccess.WO),		// Key value
		}
		enum mask_PR {
				PR = RegBitField!uint(0,3,RegisterAccess.RW),		// Prescaler divider
		}
		enum mask_RLR {
				RL = RegBitField!uint(0,12,RegisterAccess.RW),		// Watchdog counter reload value
		}
		enum mask_SR {
				RVU = RegBitField!uint(1,1,RegisterAccess.RO),		// Watchdog counter reload value update
				PVU = RegBitField!uint(0,1,RegisterAccess.RO),		// Watchdog prescaler value update
		}
	}
	// RCC Group 
	// Reset and clock control
	struct RCC_Type {
		@RegInfo("CR",0x0) RegVolatile!(mask_CR,uint,0x40023800,0x83,RegisterAccess.NO) CR;		 // offset:0x0 CR clock control register
		@RegInfo("PLLCFGR",0x4) RegVolatile!(mask_PLLCFGR,uint,0x40023804,0x24003010,RegisterAccess.RW) PLLCFGR;		 // offset:0x4 PLLCFGR PLL configuration register
		@RegInfo("CFGR",0x8) RegVolatile!(mask_CFGR,uint,0x40023808,0x0,RegisterAccess.NO) CFGR;		 // offset:0x8 CFGR clock configuration register
		@RegInfo("CIR",0xc) RegVolatile!(mask_CIR,uint,0x4002380c,0x0,RegisterAccess.NO) CIR;		 // offset:0x12 CIR clock interrupt register
		@RegInfo("AHB1RSTR",0x10) RegVolatile!(mask_AHB1RSTR,uint,0x40023810,0x0,RegisterAccess.RW) AHB1RSTR;		 // offset:0x16 AHB1RSTR AHB1 peripheral reset register
		@RegInfo("AHB2RSTR",0x14) RegVolatile!(mask_AHB2RSTR,uint,0x40023814,0x0,RegisterAccess.RW) AHB2RSTR;		 // offset:0x20 AHB2RSTR AHB2 peripheral reset register
		private uint[0x2] reserved6;	// skip 8 bytes
		@RegInfo("APB1RSTR",0x20) RegVolatile!(mask_APB1RSTR,uint,0x40023820,0x0,RegisterAccess.RW) APB1RSTR;		 // offset:0x32 APB1RSTR APB1 peripheral reset register
		@RegInfo("APB2RSTR",0x24) RegVolatile!(mask_APB2RSTR,uint,0x40023824,0x0,RegisterAccess.RW) APB2RSTR;		 // offset:0x36 APB2RSTR APB2 peripheral reset register
		private uint[0x2] reserved8;	// skip 8 bytes
		@RegInfo("AHB1ENR",0x30) RegVolatile!(mask_AHB1ENR,uint,0x40023830,0x100000,RegisterAccess.RW) AHB1ENR;		 // offset:0x48 AHB1ENR AHB1 peripheral clock register
		@RegInfo("AHB2ENR",0x34) RegVolatile!(mask_AHB2ENR,uint,0x40023834,0x0,RegisterAccess.RW) AHB2ENR;		 // offset:0x52 AHB2ENR AHB2 peripheral clock enable register
		private uint[0x2] reserved10;	// skip 8 bytes
		@RegInfo("APB1ENR",0x40) RegVolatile!(mask_APB1ENR,uint,0x40023840,0x0,RegisterAccess.RW) APB1ENR;		 // offset:0x64 APB1ENR APB1 peripheral clock enable register
		@RegInfo("APB2ENR",0x44) RegVolatile!(mask_APB2ENR,uint,0x40023844,0x0,RegisterAccess.RW) APB2ENR;		 // offset:0x68 APB2ENR APB2 peripheral clock enable register
		private uint[0x2] reserved12;	// skip 8 bytes
		@RegInfo("AHB1LPENR",0x50) RegVolatile!(mask_AHB1LPENR,uint,0x40023850,0x7e6791ff,RegisterAccess.RW) AHB1LPENR;		 // offset:0x80 AHB1LPENR AHB1 peripheral clock enable in low power mode register
		@RegInfo("AHB2LPENR",0x54) RegVolatile!(mask_AHB2LPENR,uint,0x40023854,0xf1,RegisterAccess.RW) AHB2LPENR;		 // offset:0x84 AHB2LPENR AHB2 peripheral clock enable in low power mode register
		private uint[0x2] reserved14;	// skip 8 bytes
		@RegInfo("APB1LPENR",0x60) RegVolatile!(mask_APB1LPENR,uint,0x40023860,0x36fec9ff,RegisterAccess.RW) APB1LPENR;		 // offset:0x96 APB1LPENR APB1 peripheral clock enable in low power mode register
		@RegInfo("APB2LPENR",0x64) RegVolatile!(mask_APB2LPENR,uint,0x40023864,0x75f33,RegisterAccess.RW) APB2LPENR;		 // offset:0x100 APB2LPENR APB2 peripheral clock enabled in low power mode register
		private uint[0x2] reserved16;	// skip 8 bytes
		@RegInfo("BDCR",0x70) RegVolatile!(mask_BDCR,uint,0x40023870,0x0,RegisterAccess.NO) BDCR;		 // offset:0x112 BDCR Backup domain control register
		@RegInfo("CSR",0x74) RegVolatile!(mask_CSR,uint,0x40023874,0xe000000,RegisterAccess.NO) CSR;		 // offset:0x116 CSR clock control &amp; status register
		private uint[0x2] reserved18;	// skip 8 bytes
		@RegInfo("SSCGR",0x80) RegVolatile!(mask_SSCGR,uint,0x40023880,0x0,RegisterAccess.RW) SSCGR;		 // offset:0x128 SSCGR spread spectrum clock generation register
		@RegInfo("PLLI2SCFGR",0x84) RegVolatile!(mask_PLLI2SCFGR,uint,0x40023884,0x20003000,RegisterAccess.RW) PLLI2SCFGR;		 // offset:0x132 PLLI2SCFGR PLLI2S configuration register
		enum mask_CR {
				PLLI2SRDY = RegBitField!uint(27,1,RegisterAccess.RO),		// PLLI2S clock ready flag
				PLLI2SON = RegBitField!uint(26,1,RegisterAccess.RW),		// PLLI2S enable
				PLLRDY = RegBitField!uint(25,1,RegisterAccess.RO),		// Main PLL (PLL) clock ready flag
				PLLON = RegBitField!uint(24,1,RegisterAccess.RW),		// Main PLL (PLL) enable
				CSSON = RegBitField!uint(19,1,RegisterAccess.RW),		// Clock security system enable
				HSEBYP = RegBitField!uint(18,1,RegisterAccess.RW),		// HSE clock bypass
				HSERDY = RegBitField!uint(17,1,RegisterAccess.RO),		// HSE clock ready flag
				HSEON = RegBitField!uint(16,1,RegisterAccess.RW),		// HSE clock enable
				HSICAL = RegBitField!uint(8,8,RegisterAccess.RO),		// Internal high-speed clock calibration
				HSITRIM = RegBitField!uint(3,5,RegisterAccess.RW),		// Internal high-speed clock trimming
				HSIRDY = RegBitField!uint(1,1,RegisterAccess.RO),		// Internal high-speed clock ready flag
				HSION = RegBitField!uint(0,1,RegisterAccess.RW),		// Internal high-speed clock enable
		}
		enum mask_PLLCFGR {
				PLLQ3 = RegBitField!uint(27,1,RegisterAccess.RW),		// Main PLL (PLL) division factor for USB OTG FS, SDIO and random number generator clocks
				PLLQ2 = RegBitField!uint(26,1,RegisterAccess.RW),		// Main PLL (PLL) division factor for USB OTG FS, SDIO and random number generator clocks
				PLLQ1 = RegBitField!uint(25,1,RegisterAccess.RW),		// Main PLL (PLL) division factor for USB OTG FS, SDIO and random number generator clocks
				PLLQ0 = RegBitField!uint(24,1,RegisterAccess.RW),		// Main PLL (PLL) division factor for USB OTG FS, SDIO and random number generator clocks
				PLLSRC = RegBitField!uint(22,1,RegisterAccess.RW),		// Main PLL(PLL) and audio PLL (PLLI2S) entry clock source
				PLLP1 = RegBitField!uint(17,1,RegisterAccess.RW),		// Main PLL (PLL) division factor for main system clock
				PLLP0 = RegBitField!uint(16,1,RegisterAccess.RW),		// Main PLL (PLL) division factor for main system clock
				PLLN8 = RegBitField!uint(14,1,RegisterAccess.RW),		// Main PLL (PLL) multiplication factor for VCO
				PLLN7 = RegBitField!uint(13,1,RegisterAccess.RW),		// Main PLL (PLL) multiplication factor for VCO
				PLLN6 = RegBitField!uint(12,1,RegisterAccess.RW),		// Main PLL (PLL) multiplication factor for VCO
				PLLN5 = RegBitField!uint(11,1,RegisterAccess.RW),		// Main PLL (PLL) multiplication factor for VCO
				PLLN4 = RegBitField!uint(10,1,RegisterAccess.RW),		// Main PLL (PLL) multiplication factor for VCO
				PLLN3 = RegBitField!uint(9,1,RegisterAccess.RW),		// Main PLL (PLL) multiplication factor for VCO
				PLLN2 = RegBitField!uint(8,1,RegisterAccess.RW),		// Main PLL (PLL) multiplication factor for VCO
				PLLN1 = RegBitField!uint(7,1,RegisterAccess.RW),		// Main PLL (PLL) multiplication factor for VCO
				PLLN0 = RegBitField!uint(6,1,RegisterAccess.RW),		// Main PLL (PLL) multiplication factor for VCO
				PLLM5 = RegBitField!uint(5,1,RegisterAccess.RW),		// Division factor for the main PLL (PLL) and audio PLL (PLLI2S) input clock
				PLLM4 = RegBitField!uint(4,1,RegisterAccess.RW),		// Division factor for the main PLL (PLL) and audio PLL (PLLI2S) input clock
				PLLM3 = RegBitField!uint(3,1,RegisterAccess.RW),		// Division factor for the main PLL (PLL) and audio PLL (PLLI2S) input clock
				PLLM2 = RegBitField!uint(2,1,RegisterAccess.RW),		// Division factor for the main PLL (PLL) and audio PLL (PLLI2S) input clock
				PLLM1 = RegBitField!uint(1,1,RegisterAccess.RW),		// Division factor for the main PLL (PLL) and audio PLL (PLLI2S) input clock
				PLLM0 = RegBitField!uint(0,1,RegisterAccess.RW),		// Division factor for the main PLL (PLL) and audio PLL (PLLI2S) input clock
		}
		enum mask_CFGR {
				MCO2 = RegBitField!uint(30,2,RegisterAccess.RW),		// Microcontroller clock output 2
				MCO2PRE = RegBitField!uint(27,3,RegisterAccess.RW),		// MCO2 prescaler
				MCO1PRE = RegBitField!uint(24,3,RegisterAccess.RW),		// MCO1 prescaler
				I2SSRC = RegBitField!uint(23,1,RegisterAccess.RW),		// I2S clock selection
				MCO1 = RegBitField!uint(21,2,RegisterAccess.RW),		// Microcontroller clock output 1
				RTCPRE = RegBitField!uint(16,5,RegisterAccess.RW),		// HSE division factor for RTC clock
				PPRE2 = RegBitField!uint(13,3,RegisterAccess.RW),		// APB high-speed prescaler (APB2)
				PPRE1 = RegBitField!uint(10,3,RegisterAccess.RW),		// APB Low speed prescaler (APB1)
				HPRE = RegBitField!uint(4,4,RegisterAccess.RW),		// AHB prescaler
				SWS1 = RegBitField!uint(3,1,RegisterAccess.RO),		// System clock switch status
				SWS0 = RegBitField!uint(2,1,RegisterAccess.RO),		// System clock switch status
				SW1 = RegBitField!uint(1,1,RegisterAccess.RW),		// System clock switch
				SW0 = RegBitField!uint(0,1,RegisterAccess.RW),		// System clock switch
		}
		enum mask_CIR {
				CSSC = RegBitField!uint(23,1,RegisterAccess.WO),		// Clock security system interrupt clear
				PLLI2SRDYC = RegBitField!uint(21,1,RegisterAccess.WO),		// PLLI2S ready interrupt clear
				PLLRDYC = RegBitField!uint(20,1,RegisterAccess.WO),		// Main PLL(PLL) ready interrupt clear
				HSERDYC = RegBitField!uint(19,1,RegisterAccess.WO),		// HSE ready interrupt clear
				HSIRDYC = RegBitField!uint(18,1,RegisterAccess.WO),		// HSI ready interrupt clear
				LSERDYC = RegBitField!uint(17,1,RegisterAccess.WO),		// LSE ready interrupt clear
				LSIRDYC = RegBitField!uint(16,1,RegisterAccess.WO),		// LSI ready interrupt clear
				PLLI2SRDYIE = RegBitField!uint(13,1,RegisterAccess.RW),		// PLLI2S ready interrupt enable
				PLLRDYIE = RegBitField!uint(12,1,RegisterAccess.RW),		// Main PLL (PLL) ready interrupt enable
				HSERDYIE = RegBitField!uint(11,1,RegisterAccess.RW),		// HSE ready interrupt enable
				HSIRDYIE = RegBitField!uint(10,1,RegisterAccess.RW),		// HSI ready interrupt enable
				LSERDYIE = RegBitField!uint(9,1,RegisterAccess.RW),		// LSE ready interrupt enable
				LSIRDYIE = RegBitField!uint(8,1,RegisterAccess.RW),		// LSI ready interrupt enable
				CSSF = RegBitField!uint(7,1,RegisterAccess.RO),		// Clock security system interrupt flag
				PLLI2SRDYF = RegBitField!uint(5,1,RegisterAccess.RO),		// PLLI2S ready interrupt flag
				PLLRDYF = RegBitField!uint(4,1,RegisterAccess.RO),		// Main PLL (PLL) ready interrupt flag
				HSERDYF = RegBitField!uint(3,1,RegisterAccess.RO),		// HSE ready interrupt flag
				HSIRDYF = RegBitField!uint(2,1,RegisterAccess.RO),		// HSI ready interrupt flag
				LSERDYF = RegBitField!uint(1,1,RegisterAccess.RO),		// LSE ready interrupt flag
				LSIRDYF = RegBitField!uint(0,1,RegisterAccess.RO),		// LSI ready interrupt flag
		}
		enum mask_AHB1RSTR {
				DMA2RST = RegBitField!uint(22,1,RegisterAccess.RW),		// DMA2 reset
				DMA1RST = RegBitField!uint(21,1,RegisterAccess.RW),		// DMA2 reset
				CRCRST = RegBitField!uint(12,1,RegisterAccess.RW),		// CRC reset
				GPIOHRST = RegBitField!uint(7,1,RegisterAccess.RW),		// IO port H reset
				GPIOERST = RegBitField!uint(4,1,RegisterAccess.RW),		// IO port E reset
				GPIODRST = RegBitField!uint(3,1,RegisterAccess.RW),		// IO port D reset
				GPIOCRST = RegBitField!uint(2,1,RegisterAccess.RW),		// IO port C reset
				GPIOBRST = RegBitField!uint(1,1,RegisterAccess.RW),		// IO port B reset
				GPIOARST = RegBitField!uint(0,1,RegisterAccess.RW),		// IO port A reset
		}
		enum mask_AHB2RSTR {
				OTGFSRST = RegBitField!uint(7,1,RegisterAccess.RW),		// USB OTG FS module reset
		}
		enum mask_APB1RSTR {
				PWRRST = RegBitField!uint(28,1,RegisterAccess.RW),		// Power interface reset
				I2C3RST = RegBitField!uint(23,1,RegisterAccess.RW),		// I2C3 reset
				I2C2RST = RegBitField!uint(22,1,RegisterAccess.RW),		// I2C 2 reset
				I2C1RST = RegBitField!uint(21,1,RegisterAccess.RW),		// I2C 1 reset
				UART2RST = RegBitField!uint(17,1,RegisterAccess.RW),		// USART 2 reset
				SPI3RST = RegBitField!uint(15,1,RegisterAccess.RW),		// SPI 3 reset
				SPI2RST = RegBitField!uint(14,1,RegisterAccess.RW),		// SPI 2 reset
				WWDGRST = RegBitField!uint(11,1,RegisterAccess.RW),		// Window watchdog reset
				TIM5RST = RegBitField!uint(3,1,RegisterAccess.RW),		// TIM5 reset
				TIM4RST = RegBitField!uint(2,1,RegisterAccess.RW),		// TIM4 reset
				TIM3RST = RegBitField!uint(1,1,RegisterAccess.RW),		// TIM3 reset
				TIM2RST = RegBitField!uint(0,1,RegisterAccess.RW),		// TIM2 reset
		}
		enum mask_APB2RSTR {
				TIM11RST = RegBitField!uint(18,1,RegisterAccess.RW),		// TIM11 reset
				TIM10RST = RegBitField!uint(17,1,RegisterAccess.RW),		// TIM10 reset
				TIM9RST = RegBitField!uint(16,1,RegisterAccess.RW),		// TIM9 reset
				SYSCFGRST = RegBitField!uint(14,1,RegisterAccess.RW),		// System configuration controller reset
				SPI1RST = RegBitField!uint(12,1,RegisterAccess.RW),		// SPI 1 reset
				SDIORST = RegBitField!uint(11,1,RegisterAccess.RW),		// SDIO reset
				ADCRST = RegBitField!uint(8,1,RegisterAccess.RW),		// ADC interface reset (common to all ADCs)
				USART6RST = RegBitField!uint(5,1,RegisterAccess.RW),		// USART6 reset
				USART1RST = RegBitField!uint(4,1,RegisterAccess.RW),		// USART1 reset
				TIM1RST = RegBitField!uint(0,1,RegisterAccess.RW),		// TIM1 reset
		}
		enum mask_AHB1ENR {
				DMA2EN = RegBitField!uint(22,1,RegisterAccess.RW),		// DMA2 clock enable
				DMA1EN = RegBitField!uint(21,1,RegisterAccess.RW),		// DMA1 clock enable
				CRCEN = RegBitField!uint(12,1,RegisterAccess.RW),		// CRC clock enable
				GPIOHEN = RegBitField!uint(7,1,RegisterAccess.RW),		// IO port H clock enable
				GPIOEEN = RegBitField!uint(4,1,RegisterAccess.RW),		// IO port E clock enable
				GPIODEN = RegBitField!uint(3,1,RegisterAccess.RW),		// IO port D clock enable
				GPIOCEN = RegBitField!uint(2,1,RegisterAccess.RW),		// IO port C clock enable
				GPIOBEN = RegBitField!uint(1,1,RegisterAccess.RW),		// IO port B clock enable
				GPIOAEN = RegBitField!uint(0,1,RegisterAccess.RW),		// IO port A clock enable
		}
		enum mask_AHB2ENR {
				OTGFSEN = RegBitField!uint(7,1,RegisterAccess.RW),		// USB OTG FS clock enable
		}
		enum mask_APB1ENR {
				PWREN = RegBitField!uint(28,1,RegisterAccess.RW),		// Power interface clock enable
				I2C3EN = RegBitField!uint(23,1,RegisterAccess.RW),		// I2C3 clock enable
				I2C2EN = RegBitField!uint(22,1,RegisterAccess.RW),		// I2C2 clock enable
				I2C1EN = RegBitField!uint(21,1,RegisterAccess.RW),		// I2C1 clock enable
				USART2EN = RegBitField!uint(17,1,RegisterAccess.RW),		// USART 2 clock enable
				SPI3EN = RegBitField!uint(15,1,RegisterAccess.RW),		// SPI3 clock enable
				SPI2EN = RegBitField!uint(14,1,RegisterAccess.RW),		// SPI2 clock enable
				WWDGEN = RegBitField!uint(11,1,RegisterAccess.RW),		// Window watchdog clock enable
				TIM5EN = RegBitField!uint(3,1,RegisterAccess.RW),		// TIM5 clock enable
				TIM4EN = RegBitField!uint(2,1,RegisterAccess.RW),		// TIM4 clock enable
				TIM3EN = RegBitField!uint(1,1,RegisterAccess.RW),		// TIM3 clock enable
				TIM2EN = RegBitField!uint(0,1,RegisterAccess.RW),		// TIM2 clock enable
		}
		enum mask_APB2ENR {
				TIM11EN = RegBitField!uint(18,1,RegisterAccess.RW),		// TIM11 clock enable
				TIM10EN = RegBitField!uint(17,1,RegisterAccess.RW),		// TIM10 clock enable
				TIM9EN = RegBitField!uint(16,1,RegisterAccess.RW),		// TIM9 clock enable
				SYSCFGEN = RegBitField!uint(14,1,RegisterAccess.RW),		// System configuration controller clock enable
				SPI1EN = RegBitField!uint(12,1,RegisterAccess.RW),		// SPI1 clock enable
				SDIOEN = RegBitField!uint(11,1,RegisterAccess.RW),		// SDIO clock enable
				ADC1EN = RegBitField!uint(8,1,RegisterAccess.RW),		// ADC1 clock enable
				USART6EN = RegBitField!uint(5,1,RegisterAccess.RW),		// USART6 clock enable
				USART1EN = RegBitField!uint(4,1,RegisterAccess.RW),		// USART1 clock enable
				TIM1EN = RegBitField!uint(0,1,RegisterAccess.RW),		// TIM1 clock enable
		}
		enum mask_AHB1LPENR {
				DMA2LPEN = RegBitField!uint(22,1,RegisterAccess.RW),		// DMA2 clock enable during Sleep mode
				DMA1LPEN = RegBitField!uint(21,1,RegisterAccess.RW),		// DMA1 clock enable during Sleep mode
				SRAM1LPEN = RegBitField!uint(16,1,RegisterAccess.RW),		// SRAM 1interface clock enable during Sleep mode
				FLITFLPEN = RegBitField!uint(15,1,RegisterAccess.RW),		// Flash interface clock enable during Sleep mode
				CRCLPEN = RegBitField!uint(12,1,RegisterAccess.RW),		// CRC clock enable during Sleep mode
				GPIOHLPEN = RegBitField!uint(7,1,RegisterAccess.RW),		// IO port H clock enable during Sleep mode
				GPIOELPEN = RegBitField!uint(4,1,RegisterAccess.RW),		// IO port E clock enable during Sleep mode
				GPIODLPEN = RegBitField!uint(3,1,RegisterAccess.RW),		// IO port D clock enable during Sleep mode
				GPIOCLPEN = RegBitField!uint(2,1,RegisterAccess.RW),		// IO port C clock enable during Sleep mode
				GPIOBLPEN = RegBitField!uint(1,1,RegisterAccess.RW),		// IO port B clock enable during Sleep mode
				GPIOALPEN = RegBitField!uint(0,1,RegisterAccess.RW),		// IO port A clock enable during sleep mode
		}
		enum mask_AHB2LPENR {
				OTGFSLPEN = RegBitField!uint(7,1,RegisterAccess.RW),		// USB OTG FS clock enable during Sleep mode
		}
		enum mask_APB1LPENR {
				PWRLPEN = RegBitField!uint(28,1,RegisterAccess.RW),		// Power interface clock enable during Sleep mode
				I2C3LPEN = RegBitField!uint(23,1,RegisterAccess.RW),		// I2C3 clock enable during Sleep mode
				I2C2LPEN = RegBitField!uint(22,1,RegisterAccess.RW),		// I2C2 clock enable during Sleep mode
				I2C1LPEN = RegBitField!uint(21,1,RegisterAccess.RW),		// I2C1 clock enable during Sleep mode
				USART2LPEN = RegBitField!uint(17,1,RegisterAccess.RW),		// USART2 clock enable during Sleep mode
				SPI3LPEN = RegBitField!uint(15,1,RegisterAccess.RW),		// SPI3 clock enable during Sleep mode
				SPI2LPEN = RegBitField!uint(14,1,RegisterAccess.RW),		// SPI2 clock enable during Sleep mode
				WWDGLPEN = RegBitField!uint(11,1,RegisterAccess.RW),		// Window watchdog clock enable during Sleep mode
				TIM5LPEN = RegBitField!uint(3,1,RegisterAccess.RW),		// TIM5 clock enable during Sleep mode
				TIM4LPEN = RegBitField!uint(2,1,RegisterAccess.RW),		// TIM4 clock enable during Sleep mode
				TIM3LPEN = RegBitField!uint(1,1,RegisterAccess.RW),		// TIM3 clock enable during Sleep mode
				TIM2LPEN = RegBitField!uint(0,1,RegisterAccess.RW),		// TIM2 clock enable during Sleep mode
		}
		enum mask_APB2LPENR {
				TIM11LPEN = RegBitField!uint(18,1,RegisterAccess.RW),		// TIM11 clock enable during Sleep mode
				TIM10LPEN = RegBitField!uint(17,1,RegisterAccess.RW),		// TIM10 clock enable during Sleep mode
				TIM9LPEN = RegBitField!uint(16,1,RegisterAccess.RW),		// TIM9 clock enable during sleep mode
				SYSCFGLPEN = RegBitField!uint(14,1,RegisterAccess.RW),		// System configuration controller clock enable during Sleep mode
				SPI1LPEN = RegBitField!uint(12,1,RegisterAccess.RW),		// SPI 1 clock enable during Sleep mode
				SDIOLPEN = RegBitField!uint(11,1,RegisterAccess.RW),		// SDIO clock enable during Sleep mode
				ADC1LPEN = RegBitField!uint(8,1,RegisterAccess.RW),		// ADC1 clock enable during Sleep mode
				USART6LPEN = RegBitField!uint(5,1,RegisterAccess.RW),		// USART6 clock enable during Sleep mode
				USART1LPEN = RegBitField!uint(4,1,RegisterAccess.RW),		// USART1 clock enable during Sleep mode
				TIM1LPEN = RegBitField!uint(0,1,RegisterAccess.RW),		// TIM1 clock enable during Sleep mode
		}
		enum mask_BDCR {
				BDRST = RegBitField!uint(16,1,RegisterAccess.RW),		// Backup domain software reset
				RTCEN = RegBitField!uint(15,1,RegisterAccess.RW),		// RTC clock enable
				RTCSEL1 = RegBitField!uint(9,1,RegisterAccess.RW),		// RTC clock source selection
				RTCSEL0 = RegBitField!uint(8,1,RegisterAccess.RW),		// RTC clock source selection
				LSEBYP = RegBitField!uint(2,1,RegisterAccess.RW),		// External low-speed oscillator bypass
				LSERDY = RegBitField!uint(1,1,RegisterAccess.RO),		// External low-speed oscillator ready
				LSEON = RegBitField!uint(0,1,RegisterAccess.RW),		// External low-speed oscillator enable
		}
		enum mask_CSR {
				LPWRRSTF = RegBitField!uint(31,1,RegisterAccess.RW),		// Low-power reset flag
				WWDGRSTF = RegBitField!uint(30,1,RegisterAccess.RW),		// Window watchdog reset flag
				WDGRSTF = RegBitField!uint(29,1,RegisterAccess.RW),		// Independent watchdog reset flag
				SFTRSTF = RegBitField!uint(28,1,RegisterAccess.RW),		// Software reset flag
				PORRSTF = RegBitField!uint(27,1,RegisterAccess.RW),		// POR/PDR reset flag
				PADRSTF = RegBitField!uint(26,1,RegisterAccess.RW),		// PIN reset flag
				BORRSTF = RegBitField!uint(25,1,RegisterAccess.RW),		// BOR reset flag
				RMVF = RegBitField!uint(24,1,RegisterAccess.RW),		// Remove reset flag
				LSIRDY = RegBitField!uint(1,1,RegisterAccess.RO),		// Internal low-speed oscillator ready
				LSION = RegBitField!uint(0,1,RegisterAccess.RW),		// Internal low-speed oscillator enable
		}
		enum mask_SSCGR {
				SSCGEN = RegBitField!uint(31,1,RegisterAccess.RW),		// Spread spectrum modulation enable
				SPREADSEL = RegBitField!uint(30,1,RegisterAccess.RW),		// Spread Select
				INCSTEP = RegBitField!uint(13,15,RegisterAccess.RW),		// Incrementation step
				MODPER = RegBitField!uint(0,13,RegisterAccess.RW),		// Modulation period
		}
		enum mask_PLLI2SCFGR {
				PLLI2SRx = RegBitField!uint(28,3,RegisterAccess.RW),		// PLLI2S division factor for I2S clocks
				PLLI2SNx = RegBitField!uint(6,9,RegisterAccess.RW),		// PLLI2S multiplication factor for VCO
		}
	}
	// TIM Group 
	// Advanced-timers
	struct TIM1_Type {
		@RegInfo("CR1",0x0) RegVolatile!(mask_CR1,uint,0x40010000,0x0,RegisterAccess.RW) CR1;		 // offset:0x0 CR1 control register 1
		@RegInfo("CR2",0x4) RegVolatile!(mask_CR2,uint,0x40010004,0x0,RegisterAccess.RW) CR2;		 // offset:0x4 CR2 control register 2
		@RegInfo("SMCR",0x8) RegVolatile!(mask_SMCR,uint,0x40010008,0x0,RegisterAccess.RW) SMCR;		 // offset:0x8 SMCR slave mode control register
		@RegInfo("DIER",0xc) RegVolatile!(mask_DIER,uint,0x4001000c,0x0,RegisterAccess.RW) DIER;		 // offset:0x12 DIER DMA/Interrupt enable register
		@RegInfo("SR",0x10) RegVolatile!(mask_SR,uint,0x40010010,0x0,RegisterAccess.RW) SR;		 // offset:0x16 SR status register
		@RegInfo("EGR",0x14) RegVolatile!(mask_EGR,uint,0x40010014,0x0,RegisterAccess.WO) EGR;		 // offset:0x20 EGR event generation register
		@RegInfo("CCMR1_Output",0x18) RegVolatile!(mask_CCMR1_Output,uint,0x40010018,0x0,RegisterAccess.RW) CCMR1_Output;		 // offset:0x24 CCMR1_Output capture/compare mode register 1 (output mode)
		@RegInfo("CCMR1_Input",0x18) RegVolatile!(mask_CCMR1_Input,uint,0x40010018,0x0,RegisterAccess.RW) CCMR1_Input;		 // offset:0x24 CCMR1_Input capture/compare mode register 1 (input mode)
		@RegInfo("CCMR2_Output",0x1c) RegVolatile!(mask_CCMR2_Output,uint,0x4001001c,0x0,RegisterAccess.RW) CCMR2_Output;		 // offset:0x28 CCMR2_Output capture/compare mode register 2 (output mode)
		@RegInfo("CCMR2_Input",0x1c) RegVolatile!(mask_CCMR2_Input,uint,0x4001001c,0x0,RegisterAccess.RW) CCMR2_Input;		 // offset:0x28 CCMR2_Input capture/compare mode register 2 (input mode)
		@RegInfo("CCER",0x20) RegVolatile!(mask_CCER,uint,0x40010020,0x0,RegisterAccess.RW) CCER;		 // offset:0x32 CCER capture/compare enable register
		@RegInfo("CNT",0x24) RegVolatile!(mask_CNT,uint,0x40010024,0x0,RegisterAccess.RW) CNT;		 // offset:0x36 CNT counter
		@RegInfo("PSC",0x28) RegVolatile!(mask_PSC,uint,0x40010028,0x0,RegisterAccess.RW) PSC;		 // offset:0x40 PSC prescaler
		@RegInfo("ARR",0x2c) RegVolatile!(mask_ARR,uint,0x4001002c,0x0,RegisterAccess.RW) ARR;		 // offset:0x44 ARR auto-reload register
		@RegInfo("RCR",0x30) RegVolatile!(mask_RCR,uint,0x40010030,0x0,RegisterAccess.RW) RCR;		 // offset:0x48 RCR repetition counter register
		@RegInfo("CCR1",0x34) RegVolatile!(mask_CCR1,uint,0x40010034,0x0,RegisterAccess.RW) CCR1;		 // offset:0x52 CCR1 capture/compare register 1
		@RegInfo("CCR2",0x38) RegVolatile!(mask_CCR2,uint,0x40010038,0x0,RegisterAccess.RW) CCR2;		 // offset:0x56 CCR2 capture/compare register 2
		@RegInfo("CCR3",0x3c) RegVolatile!(mask_CCR3,uint,0x4001003c,0x0,RegisterAccess.RW) CCR3;		 // offset:0x60 CCR3 capture/compare register 3
		@RegInfo("CCR4",0x40) RegVolatile!(mask_CCR4,uint,0x40010040,0x0,RegisterAccess.RW) CCR4;		 // offset:0x64 CCR4 capture/compare register 4
		@RegInfo("BDTR",0x44) RegVolatile!(mask_BDTR,uint,0x40010044,0x0,RegisterAccess.RW) BDTR;		 // offset:0x68 BDTR break and dead-time register
		@RegInfo("DCR",0x48) RegVolatile!(mask_DCR,uint,0x40010048,0x0,RegisterAccess.RW) DCR;		 // offset:0x72 DCR DMA control register
		@RegInfo("DMAR",0x4c) RegVolatile!(mask_DMAR,uint,0x4001004c,0x0,RegisterAccess.RW) DMAR;		 // offset:0x76 DMAR DMA address for full transfer
		enum mask_CR1 {
				CKD = RegBitField!uint(8,2,RegisterAccess.RW),		// Clock division
				ARPE = RegBitField!uint(7,1,RegisterAccess.RW),		// Auto-reload preload enable
				CMS = RegBitField!uint(5,2,RegisterAccess.RW),		// Center-aligned mode selection
				DIR = RegBitField!uint(4,1,RegisterAccess.RW),		// Direction
				OPM = RegBitField!uint(3,1,RegisterAccess.RW),		// One-pulse mode
				URS = RegBitField!uint(2,1,RegisterAccess.RW),		// Update request source
				UDIS = RegBitField!uint(1,1,RegisterAccess.RW),		// Update disable
				CEN = RegBitField!uint(0,1,RegisterAccess.RW),		// Counter enable
		}
		enum mask_CR2 {
				OIS4 = RegBitField!uint(14,1,RegisterAccess.RW),		// Output Idle state 4
				OIS3N = RegBitField!uint(13,1,RegisterAccess.RW),		// Output Idle state 3
				OIS3 = RegBitField!uint(12,1,RegisterAccess.RW),		// Output Idle state 3
				OIS2N = RegBitField!uint(11,1,RegisterAccess.RW),		// Output Idle state 2
				OIS2 = RegBitField!uint(10,1,RegisterAccess.RW),		// Output Idle state 2
				OIS1N = RegBitField!uint(9,1,RegisterAccess.RW),		// Output Idle state 1
				OIS1 = RegBitField!uint(8,1,RegisterAccess.RW),		// Output Idle state 1
				TI1S = RegBitField!uint(7,1,RegisterAccess.RW),		// TI1 selection
				MMS = RegBitField!uint(4,3,RegisterAccess.RW),		// Master mode selection
				CCDS = RegBitField!uint(3,1,RegisterAccess.RW),		// Capture/compare DMA selection
				CCUS = RegBitField!uint(2,1,RegisterAccess.RW),		// Capture/compare control update selection
				CCPC = RegBitField!uint(0,1,RegisterAccess.RW),		// Capture/compare preloaded control
		}
		enum mask_SMCR {
				ETP = RegBitField!uint(15,1,RegisterAccess.RW),		// External trigger polarity
				ECE = RegBitField!uint(14,1,RegisterAccess.RW),		// External clock enable
				ETPS = RegBitField!uint(12,2,RegisterAccess.RW),		// External trigger prescaler
				ETF = RegBitField!uint(8,4,RegisterAccess.RW),		// External trigger filter
				MSM = RegBitField!uint(7,1,RegisterAccess.RW),		// Master/Slave mode
				TS = RegBitField!uint(4,3,RegisterAccess.RW),		// Trigger selection
				SMS = RegBitField!uint(0,3,RegisterAccess.RW),		// Slave mode selection
		}
		enum mask_DIER {
				TDE = RegBitField!uint(14,1,RegisterAccess.RW),		// Trigger DMA request enable
				COMDE = RegBitField!uint(13,1,RegisterAccess.RW),		// COM DMA request enable
				CC4DE = RegBitField!uint(12,1,RegisterAccess.RW),		// Capture/Compare 4 DMA request enable
				CC3DE = RegBitField!uint(11,1,RegisterAccess.RW),		// Capture/Compare 3 DMA request enable
				CC2DE = RegBitField!uint(10,1,RegisterAccess.RW),		// Capture/Compare 2 DMA request enable
				CC1DE = RegBitField!uint(9,1,RegisterAccess.RW),		// Capture/Compare 1 DMA request enable
				UDE = RegBitField!uint(8,1,RegisterAccess.RW),		// Update DMA request enable
				BIE = RegBitField!uint(7,1,RegisterAccess.RW),		// Break interrupt enable
				TIE = RegBitField!uint(6,1,RegisterAccess.RW),		// Trigger interrupt enable
				COMIE = RegBitField!uint(5,1,RegisterAccess.RW),		// COM interrupt enable
				CC4IE = RegBitField!uint(4,1,RegisterAccess.RW),		// Capture/Compare 4 interrupt enable
				CC3IE = RegBitField!uint(3,1,RegisterAccess.RW),		// Capture/Compare 3 interrupt enable
				CC2IE = RegBitField!uint(2,1,RegisterAccess.RW),		// Capture/Compare 2 interrupt enable
				CC1IE = RegBitField!uint(1,1,RegisterAccess.RW),		// Capture/Compare 1 interrupt enable
				UIE = RegBitField!uint(0,1,RegisterAccess.RW),		// Update interrupt enable
		}
		enum mask_SR {
				CC4OF = RegBitField!uint(12,1,RegisterAccess.RW),		// Capture/Compare 4 overcapture flag
				CC3OF = RegBitField!uint(11,1,RegisterAccess.RW),		// Capture/Compare 3 overcapture flag
				CC2OF = RegBitField!uint(10,1,RegisterAccess.RW),		// Capture/compare 2 overcapture flag
				CC1OF = RegBitField!uint(9,1,RegisterAccess.RW),		// Capture/Compare 1 overcapture flag
				BIF = RegBitField!uint(7,1,RegisterAccess.RW),		// Break interrupt flag
				TIF = RegBitField!uint(6,1,RegisterAccess.RW),		// Trigger interrupt flag
				COMIF = RegBitField!uint(5,1,RegisterAccess.RW),		// COM interrupt flag
				CC4IF = RegBitField!uint(4,1,RegisterAccess.RW),		// Capture/Compare 4 interrupt flag
				CC3IF = RegBitField!uint(3,1,RegisterAccess.RW),		// Capture/Compare 3 interrupt flag
				CC2IF = RegBitField!uint(2,1,RegisterAccess.RW),		// Capture/Compare 2 interrupt flag
				CC1IF = RegBitField!uint(1,1,RegisterAccess.RW),		// Capture/compare 1 interrupt flag
				UIF = RegBitField!uint(0,1,RegisterAccess.RW),		// Update interrupt flag
		}
		enum mask_EGR {
				BG = RegBitField!uint(7,1,RegisterAccess.WO),		// Break generation
				TG = RegBitField!uint(6,1,RegisterAccess.WO),		// Trigger generation
				COMG = RegBitField!uint(5,1,RegisterAccess.WO),		// Capture/Compare control update generation
				CC4G = RegBitField!uint(4,1,RegisterAccess.WO),		// Capture/compare 4 generation
				CC3G = RegBitField!uint(3,1,RegisterAccess.WO),		// Capture/compare 3 generation
				CC2G = RegBitField!uint(2,1,RegisterAccess.WO),		// Capture/compare 2 generation
				CC1G = RegBitField!uint(1,1,RegisterAccess.WO),		// Capture/compare 1 generation
				UG = RegBitField!uint(0,1,RegisterAccess.WO),		// Update generation
		}
		enum mask_CCMR1_Output {
				OC2CE = RegBitField!uint(15,1,RegisterAccess.RW),		// Output Compare 2 clear enable
				OC2M = RegBitField!uint(12,3,RegisterAccess.RW),		// Output Compare 2 mode
				OC2PE = RegBitField!uint(11,1,RegisterAccess.RW),		// Output Compare 2 preload enable
				OC2FE = RegBitField!uint(10,1,RegisterAccess.RW),		// Output Compare 2 fast enable
				CC2S = RegBitField!uint(8,2,RegisterAccess.RW),		// Capture/Compare 2 selection
				OC1CE = RegBitField!uint(7,1,RegisterAccess.RW),		// Output Compare 1 clear enable
				OC1M = RegBitField!uint(4,3,RegisterAccess.RW),		// Output Compare 1 mode
				OC1PE = RegBitField!uint(3,1,RegisterAccess.RW),		// Output Compare 1 preload enable
				OC1FE = RegBitField!uint(2,1,RegisterAccess.RW),		// Output Compare 1 fast enable
				CC1S = RegBitField!uint(0,2,RegisterAccess.RW),		// Capture/Compare 1 selection
		}
		enum mask_CCMR1_Input {
				IC2F = RegBitField!uint(12,4,RegisterAccess.RW),		// Input capture 2 filter
				IC2PCS = RegBitField!uint(10,2,RegisterAccess.RW),		// Input capture 2 prescaler
				CC2S = RegBitField!uint(8,2,RegisterAccess.RW),		// Capture/Compare 2 selection
				IC1F = RegBitField!uint(4,4,RegisterAccess.RW),		// Input capture 1 filter
				ICPCS = RegBitField!uint(2,2,RegisterAccess.RW),		// Input capture 1 prescaler
				CC1S = RegBitField!uint(0,2,RegisterAccess.RW),		// Capture/Compare 1 selection
		}
		enum mask_CCMR2_Output {
				OC4CE = RegBitField!uint(15,1,RegisterAccess.RW),		// Output compare 4 clear enable
				OC4M = RegBitField!uint(12,3,RegisterAccess.RW),		// Output compare 4 mode
				OC4PE = RegBitField!uint(11,1,RegisterAccess.RW),		// Output compare 4 preload enable
				OC4FE = RegBitField!uint(10,1,RegisterAccess.RW),		// Output compare 4 fast enable
				CC4S = RegBitField!uint(8,2,RegisterAccess.RW),		// Capture/Compare 4 selection
				OC3CE = RegBitField!uint(7,1,RegisterAccess.RW),		// Output compare 3 clear enable
				OC3M = RegBitField!uint(4,3,RegisterAccess.RW),		// Output compare 3 mode
				OC3PE = RegBitField!uint(3,1,RegisterAccess.RW),		// Output compare 3 preload enable
				OC3FE = RegBitField!uint(2,1,RegisterAccess.RW),		// Output compare 3 fast enable
				CC3S = RegBitField!uint(0,2,RegisterAccess.RW),		// Capture/Compare 3 selection
		}
		enum mask_CCMR2_Input {
				IC4F = RegBitField!uint(12,4,RegisterAccess.RW),		// Input capture 4 filter
				IC4PSC = RegBitField!uint(10,2,RegisterAccess.RW),		// Input capture 4 prescaler
				CC4S = RegBitField!uint(8,2,RegisterAccess.RW),		// Capture/Compare 4 selection
				IC3F = RegBitField!uint(4,4,RegisterAccess.RW),		// Input capture 3 filter
				IC3PSC = RegBitField!uint(2,2,RegisterAccess.RW),		// Input capture 3 prescaler
				CC3S = RegBitField!uint(0,2,RegisterAccess.RW),		// Capture/compare 3 selection
		}
		enum mask_CCER {
				CC4P = RegBitField!uint(13,1,RegisterAccess.RW),		// Capture/Compare 3 output Polarity
				CC4E = RegBitField!uint(12,1,RegisterAccess.RW),		// Capture/Compare 4 output enable
				CC3NP = RegBitField!uint(11,1,RegisterAccess.RW),		// Capture/Compare 3 output Polarity
				CC3NE = RegBitField!uint(10,1,RegisterAccess.RW),		// Capture/Compare 3 complementary output enable
				CC3P = RegBitField!uint(9,1,RegisterAccess.RW),		// Capture/Compare 3 output Polarity
				CC3E = RegBitField!uint(8,1,RegisterAccess.RW),		// Capture/Compare 3 output enable
				CC2NP = RegBitField!uint(7,1,RegisterAccess.RW),		// Capture/Compare 2 output Polarity
				CC2NE = RegBitField!uint(6,1,RegisterAccess.RW),		// Capture/Compare 2 complementary output enable
				CC2P = RegBitField!uint(5,1,RegisterAccess.RW),		// Capture/Compare 2 output Polarity
				CC2E = RegBitField!uint(4,1,RegisterAccess.RW),		// Capture/Compare 2 output enable
				CC1NP = RegBitField!uint(3,1,RegisterAccess.RW),		// Capture/Compare 1 output Polarity
				CC1NE = RegBitField!uint(2,1,RegisterAccess.RW),		// Capture/Compare 1 complementary output enable
				CC1P = RegBitField!uint(1,1,RegisterAccess.RW),		// Capture/Compare 1 output Polarity
				CC1E = RegBitField!uint(0,1,RegisterAccess.RW),		// Capture/Compare 1 output enable
		}
		enum mask_CNT {
				CNT = RegBitField!uint(0,16,RegisterAccess.RW),		// counter value
		}
		enum mask_PSC {
				PSC = RegBitField!uint(0,16,RegisterAccess.RW),		// Prescaler value
		}
		enum mask_ARR {
				ARR = RegBitField!uint(0,16,RegisterAccess.RW),		// Auto-reload value
		}
		enum mask_RCR {
				REP = RegBitField!uint(0,8,RegisterAccess.RW),		// Repetition counter value
		}
		enum mask_CCR1 {
				CCR1 = RegBitField!uint(0,16,RegisterAccess.RW),		// Capture/Compare 1 value
		}
		enum mask_CCR2 {
				CCR2 = RegBitField!uint(0,16,RegisterAccess.RW),		// Capture/Compare 2 value
		}
		enum mask_CCR3 {
				CCR3 = RegBitField!uint(0,16,RegisterAccess.RW),		// Capture/Compare value
		}
		enum mask_CCR4 {
				CCR4 = RegBitField!uint(0,16,RegisterAccess.RW),		// Capture/Compare value
		}
		enum mask_BDTR {
				MOE = RegBitField!uint(15,1,RegisterAccess.RW),		// Main output enable
				AOE = RegBitField!uint(14,1,RegisterAccess.RW),		// Automatic output enable
				BKP = RegBitField!uint(13,1,RegisterAccess.RW),		// Break polarity
				BKE = RegBitField!uint(12,1,RegisterAccess.RW),		// Break enable
				OSSR = RegBitField!uint(11,1,RegisterAccess.RW),		// Off-state selection for Run mode
				OSSI = RegBitField!uint(10,1,RegisterAccess.RW),		// Off-state selection for Idle mode
				LOCK = RegBitField!uint(8,2,RegisterAccess.RW),		// Lock configuration
				DTG = RegBitField!uint(0,8,RegisterAccess.RW),		// Dead-time generator setup
		}
		enum mask_DCR {
				DBL = RegBitField!uint(8,5,RegisterAccess.RW),		// DMA burst length
				DBA = RegBitField!uint(0,5,RegisterAccess.RW),		// DMA base address
		}
		enum mask_DMAR {
				DMAB = RegBitField!uint(0,16,RegisterAccess.RW),		// DMA register for burst accesses
		}
	}
	// General purpose timers
	struct TIM2_Type {
		@RegInfo("CR1",0x0) RegVolatile!(mask_CR1,uint,0x40000000,0x0,RegisterAccess.RW) CR1;		 // offset:0x0 CR1 control register 1
		@RegInfo("CR2",0x4) RegVolatile!(mask_CR2,uint,0x40000004,0x0,RegisterAccess.RW) CR2;		 // offset:0x4 CR2 control register 2
		@RegInfo("SMCR",0x8) RegVolatile!(mask_SMCR,uint,0x40000008,0x0,RegisterAccess.RW) SMCR;		 // offset:0x8 SMCR slave mode control register
		@RegInfo("DIER",0xc) RegVolatile!(mask_DIER,uint,0x4000000c,0x0,RegisterAccess.RW) DIER;		 // offset:0x12 DIER DMA/Interrupt enable register
		@RegInfo("SR",0x10) RegVolatile!(mask_SR,uint,0x40000010,0x0,RegisterAccess.RW) SR;		 // offset:0x16 SR status register
		@RegInfo("EGR",0x14) RegVolatile!(mask_EGR,uint,0x40000014,0x0,RegisterAccess.WO) EGR;		 // offset:0x20 EGR event generation register
		@RegInfo("CCMR1_Output",0x18) RegVolatile!(mask_CCMR1_Output,uint,0x40000018,0x0,RegisterAccess.RW) CCMR1_Output;		 // offset:0x24 CCMR1_Output capture/compare mode register 1 (output mode)
		@RegInfo("CCMR1_Input",0x18) RegVolatile!(mask_CCMR1_Input,uint,0x40000018,0x0,RegisterAccess.RW) CCMR1_Input;		 // offset:0x24 CCMR1_Input capture/compare mode register 1 (input mode)
		@RegInfo("CCMR2_Output",0x1c) RegVolatile!(mask_CCMR2_Output,uint,0x4000001c,0x0,RegisterAccess.RW) CCMR2_Output;		 // offset:0x28 CCMR2_Output capture/compare mode register 2 (output mode)
		@RegInfo("CCMR2_Input",0x1c) RegVolatile!(mask_CCMR2_Input,uint,0x4000001c,0x0,RegisterAccess.RW) CCMR2_Input;		 // offset:0x28 CCMR2_Input capture/compare mode register 2 (input mode)
		@RegInfo("CCER",0x20) RegVolatile!(mask_CCER,uint,0x40000020,0x0,RegisterAccess.RW) CCER;		 // offset:0x32 CCER capture/compare enable register
		@RegInfo("CNT",0x24) RegVolatile!(mask_CNT,uint,0x40000024,0x0,RegisterAccess.RW) CNT;		 // offset:0x36 CNT counter
		@RegInfo("PSC",0x28) RegVolatile!(mask_PSC,uint,0x40000028,0x0,RegisterAccess.RW) PSC;		 // offset:0x40 PSC prescaler
		@RegInfo("ARR",0x2c) RegVolatile!(mask_ARR,uint,0x4000002c,0x0,RegisterAccess.RW) ARR;		 // offset:0x44 ARR auto-reload register
		private uint[0x1] reserved14;	// skip 4 bytes
		@RegInfo("CCR1",0x34) RegVolatile!(mask_CCR1,uint,0x40000034,0x0,RegisterAccess.RW) CCR1;		 // offset:0x52 CCR1 capture/compare register 1
		@RegInfo("CCR2",0x38) RegVolatile!(mask_CCR2,uint,0x40000038,0x0,RegisterAccess.RW) CCR2;		 // offset:0x56 CCR2 capture/compare register 2
		@RegInfo("CCR3",0x3c) RegVolatile!(mask_CCR3,uint,0x4000003c,0x0,RegisterAccess.RW) CCR3;		 // offset:0x60 CCR3 capture/compare register 3
		@RegInfo("CCR4",0x40) RegVolatile!(mask_CCR4,uint,0x40000040,0x0,RegisterAccess.RW) CCR4;		 // offset:0x64 CCR4 capture/compare register 4
		private uint[0x1] reserved18;	// skip 4 bytes
		@RegInfo("DCR",0x48) RegVolatile!(mask_DCR,uint,0x40000048,0x0,RegisterAccess.RW) DCR;		 // offset:0x72 DCR DMA control register
		@RegInfo("DMAR",0x4c) RegVolatile!(mask_DMAR,uint,0x4000004c,0x0,RegisterAccess.RW) DMAR;		 // offset:0x76 DMAR DMA address for full transfer
		@RegInfo("OR",0x50) RegVolatile!(mask_OR,uint,0x40000050,0x0,RegisterAccess.RW) OR;		 // offset:0x80 OR TIM5 option register
		enum mask_CR1 {
				CKD = RegBitField!uint(8,2,RegisterAccess.RW),		// Clock division
				ARPE = RegBitField!uint(7,1,RegisterAccess.RW),		// Auto-reload preload enable
				CMS = RegBitField!uint(5,2,RegisterAccess.RW),		// Center-aligned mode selection
				DIR = RegBitField!uint(4,1,RegisterAccess.RW),		// Direction
				OPM = RegBitField!uint(3,1,RegisterAccess.RW),		// One-pulse mode
				URS = RegBitField!uint(2,1,RegisterAccess.RW),		// Update request source
				UDIS = RegBitField!uint(1,1,RegisterAccess.RW),		// Update disable
				CEN = RegBitField!uint(0,1,RegisterAccess.RW),		// Counter enable
		}
		enum mask_CR2 {
				TI1S = RegBitField!uint(7,1,RegisterAccess.RW),		// TI1 selection
				MMS = RegBitField!uint(4,3,RegisterAccess.RW),		// Master mode selection
				CCDS = RegBitField!uint(3,1,RegisterAccess.RW),		// Capture/compare DMA selection
		}
		enum mask_SMCR {
				ETP = RegBitField!uint(15,1,RegisterAccess.RW),		// External trigger polarity
				ECE = RegBitField!uint(14,1,RegisterAccess.RW),		// External clock enable
				ETPS = RegBitField!uint(12,2,RegisterAccess.RW),		// External trigger prescaler
				ETF = RegBitField!uint(8,4,RegisterAccess.RW),		// External trigger filter
				MSM = RegBitField!uint(7,1,RegisterAccess.RW),		// Master/Slave mode
				TS = RegBitField!uint(4,3,RegisterAccess.RW),		// Trigger selection
				SMS = RegBitField!uint(0,3,RegisterAccess.RW),		// Slave mode selection
		}
		enum mask_DIER {
				TDE = RegBitField!uint(14,1,RegisterAccess.RW),		// Trigger DMA request enable
				CC4DE = RegBitField!uint(12,1,RegisterAccess.RW),		// Capture/Compare 4 DMA request enable
				CC3DE = RegBitField!uint(11,1,RegisterAccess.RW),		// Capture/Compare 3 DMA request enable
				CC2DE = RegBitField!uint(10,1,RegisterAccess.RW),		// Capture/Compare 2 DMA request enable
				CC1DE = RegBitField!uint(9,1,RegisterAccess.RW),		// Capture/Compare 1 DMA request enable
				UDE = RegBitField!uint(8,1,RegisterAccess.RW),		// Update DMA request enable
				TIE = RegBitField!uint(6,1,RegisterAccess.RW),		// Trigger interrupt enable
				CC4IE = RegBitField!uint(4,1,RegisterAccess.RW),		// Capture/Compare 4 interrupt enable
				CC3IE = RegBitField!uint(3,1,RegisterAccess.RW),		// Capture/Compare 3 interrupt enable
				CC2IE = RegBitField!uint(2,1,RegisterAccess.RW),		// Capture/Compare 2 interrupt enable
				CC1IE = RegBitField!uint(1,1,RegisterAccess.RW),		// Capture/Compare 1 interrupt enable
				UIE = RegBitField!uint(0,1,RegisterAccess.RW),		// Update interrupt enable
		}
		enum mask_SR {
				CC4OF = RegBitField!uint(12,1,RegisterAccess.RW),		// Capture/Compare 4 overcapture flag
				CC3OF = RegBitField!uint(11,1,RegisterAccess.RW),		// Capture/Compare 3 overcapture flag
				CC2OF = RegBitField!uint(10,1,RegisterAccess.RW),		// Capture/compare 2 overcapture flag
				CC1OF = RegBitField!uint(9,1,RegisterAccess.RW),		// Capture/Compare 1 overcapture flag
				TIF = RegBitField!uint(6,1,RegisterAccess.RW),		// Trigger interrupt flag
				CC4IF = RegBitField!uint(4,1,RegisterAccess.RW),		// Capture/Compare 4 interrupt flag
				CC3IF = RegBitField!uint(3,1,RegisterAccess.RW),		// Capture/Compare 3 interrupt flag
				CC2IF = RegBitField!uint(2,1,RegisterAccess.RW),		// Capture/Compare 2 interrupt flag
				CC1IF = RegBitField!uint(1,1,RegisterAccess.RW),		// Capture/compare 1 interrupt flag
				UIF = RegBitField!uint(0,1,RegisterAccess.RW),		// Update interrupt flag
		}
		enum mask_EGR {
				TG = RegBitField!uint(6,1,RegisterAccess.WO),		// Trigger generation
				CC4G = RegBitField!uint(4,1,RegisterAccess.WO),		// Capture/compare 4 generation
				CC3G = RegBitField!uint(3,1,RegisterAccess.WO),		// Capture/compare 3 generation
				CC2G = RegBitField!uint(2,1,RegisterAccess.WO),		// Capture/compare 2 generation
				CC1G = RegBitField!uint(1,1,RegisterAccess.WO),		// Capture/compare 1 generation
				UG = RegBitField!uint(0,1,RegisterAccess.WO),		// Update generation
		}
		enum mask_CCMR1_Output {
				OC2CE = RegBitField!uint(15,1,RegisterAccess.RW),		// OC2CE
				OC2M = RegBitField!uint(12,3,RegisterAccess.RW),		// OC2M
				OC2PE = RegBitField!uint(11,1,RegisterAccess.RW),		// OC2PE
				OC2FE = RegBitField!uint(10,1,RegisterAccess.RW),		// OC2FE
				CC2S = RegBitField!uint(8,2,RegisterAccess.RW),		// CC2S
				OC1CE = RegBitField!uint(7,1,RegisterAccess.RW),		// OC1CE
				OC1M = RegBitField!uint(4,3,RegisterAccess.RW),		// OC1M
				OC1PE = RegBitField!uint(3,1,RegisterAccess.RW),		// OC1PE
				OC1FE = RegBitField!uint(2,1,RegisterAccess.RW),		// OC1FE
				CC1S = RegBitField!uint(0,2,RegisterAccess.RW),		// CC1S
		}
		enum mask_CCMR1_Input {
				IC2F = RegBitField!uint(12,4,RegisterAccess.RW),		// Input capture 2 filter
				IC2PCS = RegBitField!uint(10,2,RegisterAccess.RW),		// Input capture 2 prescaler
				CC2S = RegBitField!uint(8,2,RegisterAccess.RW),		// Capture/Compare 2 selection
				IC1F = RegBitField!uint(4,4,RegisterAccess.RW),		// Input capture 1 filter
				ICPCS = RegBitField!uint(2,2,RegisterAccess.RW),		// Input capture 1 prescaler
				CC1S = RegBitField!uint(0,2,RegisterAccess.RW),		// Capture/Compare 1 selection
		}
		enum mask_CCMR2_Output {
				O24CE = RegBitField!uint(15,1,RegisterAccess.RW),		// O24CE
				OC4M = RegBitField!uint(12,3,RegisterAccess.RW),		// OC4M
				OC4PE = RegBitField!uint(11,1,RegisterAccess.RW),		// OC4PE
				OC4FE = RegBitField!uint(10,1,RegisterAccess.RW),		// OC4FE
				CC4S = RegBitField!uint(8,2,RegisterAccess.RW),		// CC4S
				OC3CE = RegBitField!uint(7,1,RegisterAccess.RW),		// OC3CE
				OC3M = RegBitField!uint(4,3,RegisterAccess.RW),		// OC3M
				OC3PE = RegBitField!uint(3,1,RegisterAccess.RW),		// OC3PE
				OC3FE = RegBitField!uint(2,1,RegisterAccess.RW),		// OC3FE
				CC3S = RegBitField!uint(0,2,RegisterAccess.RW),		// CC3S
		}
		enum mask_CCMR2_Input {
				IC4F = RegBitField!uint(12,4,RegisterAccess.RW),		// Input capture 4 filter
				IC4PSC = RegBitField!uint(10,2,RegisterAccess.RW),		// Input capture 4 prescaler
				CC4S = RegBitField!uint(8,2,RegisterAccess.RW),		// Capture/Compare 4 selection
				IC3F = RegBitField!uint(4,4,RegisterAccess.RW),		// Input capture 3 filter
				IC3PSC = RegBitField!uint(2,2,RegisterAccess.RW),		// Input capture 3 prescaler
				CC3S = RegBitField!uint(0,2,RegisterAccess.RW),		// Capture/compare 3 selection
		}
		enum mask_CCER {
				CC4NP = RegBitField!uint(15,1,RegisterAccess.RW),		// Capture/Compare 4 output Polarity
				CC4P = RegBitField!uint(13,1,RegisterAccess.RW),		// Capture/Compare 3 output Polarity
				CC4E = RegBitField!uint(12,1,RegisterAccess.RW),		// Capture/Compare 4 output enable
				CC3NP = RegBitField!uint(11,1,RegisterAccess.RW),		// Capture/Compare 3 output Polarity
				CC3P = RegBitField!uint(9,1,RegisterAccess.RW),		// Capture/Compare 3 output Polarity
				CC3E = RegBitField!uint(8,1,RegisterAccess.RW),		// Capture/Compare 3 output enable
				CC2NP = RegBitField!uint(7,1,RegisterAccess.RW),		// Capture/Compare 2 output Polarity
				CC2P = RegBitField!uint(5,1,RegisterAccess.RW),		// Capture/Compare 2 output Polarity
				CC2E = RegBitField!uint(4,1,RegisterAccess.RW),		// Capture/Compare 2 output enable
				CC1NP = RegBitField!uint(3,1,RegisterAccess.RW),		// Capture/Compare 1 output Polarity
				CC1P = RegBitField!uint(1,1,RegisterAccess.RW),		// Capture/Compare 1 output Polarity
				CC1E = RegBitField!uint(0,1,RegisterAccess.RW),		// Capture/Compare 1 output enable
		}
		enum mask_CNT {
				CNT_H = RegBitField!uint(16,16,RegisterAccess.RW),		// High counter value
				CNT_L = RegBitField!uint(0,16,RegisterAccess.RW),		// Low counter value
		}
		enum mask_PSC {
				PSC = RegBitField!uint(0,16,RegisterAccess.RW),		// Prescaler value
		}
		enum mask_ARR {
				ARR_H = RegBitField!uint(16,16,RegisterAccess.RW),		// High Auto-reload value
				ARR_L = RegBitField!uint(0,16,RegisterAccess.RW),		// Low Auto-reload value
		}
		enum mask_CCR1 {
				CCR1_H = RegBitField!uint(16,16,RegisterAccess.RW),		// High Capture/Compare 1 value
				CCR1_L = RegBitField!uint(0,16,RegisterAccess.RW),		// Low Capture/Compare 1 value
		}
		enum mask_CCR2 {
				CCR2_H = RegBitField!uint(16,16,RegisterAccess.RW),		// High Capture/Compare 2 value
				CCR2_L = RegBitField!uint(0,16,RegisterAccess.RW),		// Low Capture/Compare 2 value
		}
		enum mask_CCR3 {
				CCR3_H = RegBitField!uint(16,16,RegisterAccess.RW),		// High Capture/Compare value
				CCR3_L = RegBitField!uint(0,16,RegisterAccess.RW),		// Low Capture/Compare value
		}
		enum mask_CCR4 {
				CCR4_H = RegBitField!uint(16,16,RegisterAccess.RW),		// High Capture/Compare value
				CCR4_L = RegBitField!uint(0,16,RegisterAccess.RW),		// Low Capture/Compare value
		}
		enum mask_DCR {
				DBL = RegBitField!uint(8,5,RegisterAccess.RW),		// DMA burst length
				DBA = RegBitField!uint(0,5,RegisterAccess.RW),		// DMA base address
		}
		enum mask_DMAR {
				DMAB = RegBitField!uint(0,16,RegisterAccess.RW),		// DMA register for burst accesses
		}
		enum mask_OR {
				ITR1_RMP = RegBitField!uint(10,2,RegisterAccess.RW),		// Timer Input 4 remap
		}
	}
	// General purpose timers
	struct TIM3_Type {
		@RegInfo("CR1",0x0) RegVolatile!(mask_CR1,uint,0x40000400,0x0,RegisterAccess.RW) CR1;		 // offset:0x0 CR1 control register 1
		@RegInfo("CR2",0x4) RegVolatile!(mask_CR2,uint,0x40000404,0x0,RegisterAccess.RW) CR2;		 // offset:0x4 CR2 control register 2
		@RegInfo("SMCR",0x8) RegVolatile!(mask_SMCR,uint,0x40000408,0x0,RegisterAccess.RW) SMCR;		 // offset:0x8 SMCR slave mode control register
		@RegInfo("DIER",0xc) RegVolatile!(mask_DIER,uint,0x4000040c,0x0,RegisterAccess.RW) DIER;		 // offset:0x12 DIER DMA/Interrupt enable register
		@RegInfo("SR",0x10) RegVolatile!(mask_SR,uint,0x40000410,0x0,RegisterAccess.RW) SR;		 // offset:0x16 SR status register
		@RegInfo("EGR",0x14) RegVolatile!(mask_EGR,uint,0x40000414,0x0,RegisterAccess.WO) EGR;		 // offset:0x20 EGR event generation register
		@RegInfo("CCMR1_Output",0x18) RegVolatile!(mask_CCMR1_Output,uint,0x40000418,0x0,RegisterAccess.RW) CCMR1_Output;		 // offset:0x24 CCMR1_Output capture/compare mode register 1 (output mode)
		@RegInfo("CCMR1_Input",0x18) RegVolatile!(mask_CCMR1_Input,uint,0x40000418,0x0,RegisterAccess.RW) CCMR1_Input;		 // offset:0x24 CCMR1_Input capture/compare mode register 1 (input mode)
		@RegInfo("CCMR2_Output",0x1c) RegVolatile!(mask_CCMR2_Output,uint,0x4000041c,0x0,RegisterAccess.RW) CCMR2_Output;		 // offset:0x28 CCMR2_Output capture/compare mode register 2 (output mode)
		@RegInfo("CCMR2_Input",0x1c) RegVolatile!(mask_CCMR2_Input,uint,0x4000041c,0x0,RegisterAccess.RW) CCMR2_Input;		 // offset:0x28 CCMR2_Input capture/compare mode register 2 (input mode)
		@RegInfo("CCER",0x20) RegVolatile!(mask_CCER,uint,0x40000420,0x0,RegisterAccess.RW) CCER;		 // offset:0x32 CCER capture/compare enable register
		@RegInfo("CNT",0x24) RegVolatile!(mask_CNT,uint,0x40000424,0x0,RegisterAccess.RW) CNT;		 // offset:0x36 CNT counter
		@RegInfo("PSC",0x28) RegVolatile!(mask_PSC,uint,0x40000428,0x0,RegisterAccess.RW) PSC;		 // offset:0x40 PSC prescaler
		@RegInfo("ARR",0x2c) RegVolatile!(mask_ARR,uint,0x4000042c,0x0,RegisterAccess.RW) ARR;		 // offset:0x44 ARR auto-reload register
		private uint[0x1] reserved14;	// skip 4 bytes
		@RegInfo("CCR1",0x34) RegVolatile!(mask_CCR1,uint,0x40000434,0x0,RegisterAccess.RW) CCR1;		 // offset:0x52 CCR1 capture/compare register 1
		@RegInfo("CCR2",0x38) RegVolatile!(mask_CCR2,uint,0x40000438,0x0,RegisterAccess.RW) CCR2;		 // offset:0x56 CCR2 capture/compare register 2
		@RegInfo("CCR3",0x3c) RegVolatile!(mask_CCR3,uint,0x4000043c,0x0,RegisterAccess.RW) CCR3;		 // offset:0x60 CCR3 capture/compare register 3
		@RegInfo("CCR4",0x40) RegVolatile!(mask_CCR4,uint,0x40000440,0x0,RegisterAccess.RW) CCR4;		 // offset:0x64 CCR4 capture/compare register 4
		private uint[0x1] reserved18;	// skip 4 bytes
		@RegInfo("DCR",0x48) RegVolatile!(mask_DCR,uint,0x40000448,0x0,RegisterAccess.RW) DCR;		 // offset:0x72 DCR DMA control register
		@RegInfo("DMAR",0x4c) RegVolatile!(mask_DMAR,uint,0x4000044c,0x0,RegisterAccess.RW) DMAR;		 // offset:0x76 DMAR DMA address for full transfer
		enum mask_CR1 {
				CKD = RegBitField!uint(8,2,RegisterAccess.RW),		// Clock division
				ARPE = RegBitField!uint(7,1,RegisterAccess.RW),		// Auto-reload preload enable
				CMS = RegBitField!uint(5,2,RegisterAccess.RW),		// Center-aligned mode selection
				DIR = RegBitField!uint(4,1,RegisterAccess.RW),		// Direction
				OPM = RegBitField!uint(3,1,RegisterAccess.RW),		// One-pulse mode
				URS = RegBitField!uint(2,1,RegisterAccess.RW),		// Update request source
				UDIS = RegBitField!uint(1,1,RegisterAccess.RW),		// Update disable
				CEN = RegBitField!uint(0,1,RegisterAccess.RW),		// Counter enable
		}
		enum mask_CR2 {
				TI1S = RegBitField!uint(7,1,RegisterAccess.RW),		// TI1 selection
				MMS = RegBitField!uint(4,3,RegisterAccess.RW),		// Master mode selection
				CCDS = RegBitField!uint(3,1,RegisterAccess.RW),		// Capture/compare DMA selection
		}
		enum mask_SMCR {
				ETP = RegBitField!uint(15,1,RegisterAccess.RW),		// External trigger polarity
				ECE = RegBitField!uint(14,1,RegisterAccess.RW),		// External clock enable
				ETPS = RegBitField!uint(12,2,RegisterAccess.RW),		// External trigger prescaler
				ETF = RegBitField!uint(8,4,RegisterAccess.RW),		// External trigger filter
				MSM = RegBitField!uint(7,1,RegisterAccess.RW),		// Master/Slave mode
				TS = RegBitField!uint(4,3,RegisterAccess.RW),		// Trigger selection
				SMS = RegBitField!uint(0,3,RegisterAccess.RW),		// Slave mode selection
		}
		enum mask_DIER {
				TDE = RegBitField!uint(14,1,RegisterAccess.RW),		// Trigger DMA request enable
				CC4DE = RegBitField!uint(12,1,RegisterAccess.RW),		// Capture/Compare 4 DMA request enable
				CC3DE = RegBitField!uint(11,1,RegisterAccess.RW),		// Capture/Compare 3 DMA request enable
				CC2DE = RegBitField!uint(10,1,RegisterAccess.RW),		// Capture/Compare 2 DMA request enable
				CC1DE = RegBitField!uint(9,1,RegisterAccess.RW),		// Capture/Compare 1 DMA request enable
				UDE = RegBitField!uint(8,1,RegisterAccess.RW),		// Update DMA request enable
				TIE = RegBitField!uint(6,1,RegisterAccess.RW),		// Trigger interrupt enable
				CC4IE = RegBitField!uint(4,1,RegisterAccess.RW),		// Capture/Compare 4 interrupt enable
				CC3IE = RegBitField!uint(3,1,RegisterAccess.RW),		// Capture/Compare 3 interrupt enable
				CC2IE = RegBitField!uint(2,1,RegisterAccess.RW),		// Capture/Compare 2 interrupt enable
				CC1IE = RegBitField!uint(1,1,RegisterAccess.RW),		// Capture/Compare 1 interrupt enable
				UIE = RegBitField!uint(0,1,RegisterAccess.RW),		// Update interrupt enable
		}
		enum mask_SR {
				CC4OF = RegBitField!uint(12,1,RegisterAccess.RW),		// Capture/Compare 4 overcapture flag
				CC3OF = RegBitField!uint(11,1,RegisterAccess.RW),		// Capture/Compare 3 overcapture flag
				CC2OF = RegBitField!uint(10,1,RegisterAccess.RW),		// Capture/compare 2 overcapture flag
				CC1OF = RegBitField!uint(9,1,RegisterAccess.RW),		// Capture/Compare 1 overcapture flag
				TIF = RegBitField!uint(6,1,RegisterAccess.RW),		// Trigger interrupt flag
				CC4IF = RegBitField!uint(4,1,RegisterAccess.RW),		// Capture/Compare 4 interrupt flag
				CC3IF = RegBitField!uint(3,1,RegisterAccess.RW),		// Capture/Compare 3 interrupt flag
				CC2IF = RegBitField!uint(2,1,RegisterAccess.RW),		// Capture/Compare 2 interrupt flag
				CC1IF = RegBitField!uint(1,1,RegisterAccess.RW),		// Capture/compare 1 interrupt flag
				UIF = RegBitField!uint(0,1,RegisterAccess.RW),		// Update interrupt flag
		}
		enum mask_EGR {
				TG = RegBitField!uint(6,1,RegisterAccess.WO),		// Trigger generation
				CC4G = RegBitField!uint(4,1,RegisterAccess.WO),		// Capture/compare 4 generation
				CC3G = RegBitField!uint(3,1,RegisterAccess.WO),		// Capture/compare 3 generation
				CC2G = RegBitField!uint(2,1,RegisterAccess.WO),		// Capture/compare 2 generation
				CC1G = RegBitField!uint(1,1,RegisterAccess.WO),		// Capture/compare 1 generation
				UG = RegBitField!uint(0,1,RegisterAccess.WO),		// Update generation
		}
		enum mask_CCMR1_Output {
				OC2CE = RegBitField!uint(15,1,RegisterAccess.RW),		// OC2CE
				OC2M = RegBitField!uint(12,3,RegisterAccess.RW),		// OC2M
				OC2PE = RegBitField!uint(11,1,RegisterAccess.RW),		// OC2PE
				OC2FE = RegBitField!uint(10,1,RegisterAccess.RW),		// OC2FE
				CC2S = RegBitField!uint(8,2,RegisterAccess.RW),		// CC2S
				OC1CE = RegBitField!uint(7,1,RegisterAccess.RW),		// OC1CE
				OC1M = RegBitField!uint(4,3,RegisterAccess.RW),		// OC1M
				OC1PE = RegBitField!uint(3,1,RegisterAccess.RW),		// OC1PE
				OC1FE = RegBitField!uint(2,1,RegisterAccess.RW),		// OC1FE
				CC1S = RegBitField!uint(0,2,RegisterAccess.RW),		// CC1S
		}
		enum mask_CCMR1_Input {
				IC2F = RegBitField!uint(12,4,RegisterAccess.RW),		// Input capture 2 filter
				IC2PCS = RegBitField!uint(10,2,RegisterAccess.RW),		// Input capture 2 prescaler
				CC2S = RegBitField!uint(8,2,RegisterAccess.RW),		// Capture/Compare 2 selection
				IC1F = RegBitField!uint(4,4,RegisterAccess.RW),		// Input capture 1 filter
				ICPCS = RegBitField!uint(2,2,RegisterAccess.RW),		// Input capture 1 prescaler
				CC1S = RegBitField!uint(0,2,RegisterAccess.RW),		// Capture/Compare 1 selection
		}
		enum mask_CCMR2_Output {
				O24CE = RegBitField!uint(15,1,RegisterAccess.RW),		// O24CE
				OC4M = RegBitField!uint(12,3,RegisterAccess.RW),		// OC4M
				OC4PE = RegBitField!uint(11,1,RegisterAccess.RW),		// OC4PE
				OC4FE = RegBitField!uint(10,1,RegisterAccess.RW),		// OC4FE
				CC4S = RegBitField!uint(8,2,RegisterAccess.RW),		// CC4S
				OC3CE = RegBitField!uint(7,1,RegisterAccess.RW),		// OC3CE
				OC3M = RegBitField!uint(4,3,RegisterAccess.RW),		// OC3M
				OC3PE = RegBitField!uint(3,1,RegisterAccess.RW),		// OC3PE
				OC3FE = RegBitField!uint(2,1,RegisterAccess.RW),		// OC3FE
				CC3S = RegBitField!uint(0,2,RegisterAccess.RW),		// CC3S
		}
		enum mask_CCMR2_Input {
				IC4F = RegBitField!uint(12,4,RegisterAccess.RW),		// Input capture 4 filter
				IC4PSC = RegBitField!uint(10,2,RegisterAccess.RW),		// Input capture 4 prescaler
				CC4S = RegBitField!uint(8,2,RegisterAccess.RW),		// Capture/Compare 4 selection
				IC3F = RegBitField!uint(4,4,RegisterAccess.RW),		// Input capture 3 filter
				IC3PSC = RegBitField!uint(2,2,RegisterAccess.RW),		// Input capture 3 prescaler
				CC3S = RegBitField!uint(0,2,RegisterAccess.RW),		// Capture/compare 3 selection
		}
		enum mask_CCER {
				CC4NP = RegBitField!uint(15,1,RegisterAccess.RW),		// Capture/Compare 4 output Polarity
				CC4P = RegBitField!uint(13,1,RegisterAccess.RW),		// Capture/Compare 3 output Polarity
				CC4E = RegBitField!uint(12,1,RegisterAccess.RW),		// Capture/Compare 4 output enable
				CC3NP = RegBitField!uint(11,1,RegisterAccess.RW),		// Capture/Compare 3 output Polarity
				CC3P = RegBitField!uint(9,1,RegisterAccess.RW),		// Capture/Compare 3 output Polarity
				CC3E = RegBitField!uint(8,1,RegisterAccess.RW),		// Capture/Compare 3 output enable
				CC2NP = RegBitField!uint(7,1,RegisterAccess.RW),		// Capture/Compare 2 output Polarity
				CC2P = RegBitField!uint(5,1,RegisterAccess.RW),		// Capture/Compare 2 output Polarity
				CC2E = RegBitField!uint(4,1,RegisterAccess.RW),		// Capture/Compare 2 output enable
				CC1NP = RegBitField!uint(3,1,RegisterAccess.RW),		// Capture/Compare 1 output Polarity
				CC1P = RegBitField!uint(1,1,RegisterAccess.RW),		// Capture/Compare 1 output Polarity
				CC1E = RegBitField!uint(0,1,RegisterAccess.RW),		// Capture/Compare 1 output enable
		}
		enum mask_CNT {
				CNT_H = RegBitField!uint(16,16,RegisterAccess.RW),		// High counter value
				CNT_L = RegBitField!uint(0,16,RegisterAccess.RW),		// Low counter value
		}
		enum mask_PSC {
				PSC = RegBitField!uint(0,16,RegisterAccess.RW),		// Prescaler value
		}
		enum mask_ARR {
				ARR_H = RegBitField!uint(16,16,RegisterAccess.RW),		// High Auto-reload value
				ARR_L = RegBitField!uint(0,16,RegisterAccess.RW),		// Low Auto-reload value
		}
		enum mask_CCR1 {
				CCR1_H = RegBitField!uint(16,16,RegisterAccess.RW),		// High Capture/Compare 1 value
				CCR1_L = RegBitField!uint(0,16,RegisterAccess.RW),		// Low Capture/Compare 1 value
		}
		enum mask_CCR2 {
				CCR2_H = RegBitField!uint(16,16,RegisterAccess.RW),		// High Capture/Compare 2 value
				CCR2_L = RegBitField!uint(0,16,RegisterAccess.RW),		// Low Capture/Compare 2 value
		}
		enum mask_CCR3 {
				CCR3_H = RegBitField!uint(16,16,RegisterAccess.RW),		// High Capture/Compare value
				CCR3_L = RegBitField!uint(0,16,RegisterAccess.RW),		// Low Capture/Compare value
		}
		enum mask_CCR4 {
				CCR4_H = RegBitField!uint(16,16,RegisterAccess.RW),		// High Capture/Compare value
				CCR4_L = RegBitField!uint(0,16,RegisterAccess.RW),		// Low Capture/Compare value
		}
		enum mask_DCR {
				DBL = RegBitField!uint(8,5,RegisterAccess.RW),		// DMA burst length
				DBA = RegBitField!uint(0,5,RegisterAccess.RW),		// DMA base address
		}
		enum mask_DMAR {
				DMAB = RegBitField!uint(0,16,RegisterAccess.RW),		// DMA register for burst accesses
		}
	}
	// General-purpose-timers
	struct TIM5_Type {
		@RegInfo("CR1",0x0) RegVolatile!(mask_CR1,uint,0x40000c00,0x0,RegisterAccess.RW) CR1;		 // offset:0x0 CR1 control register 1
		@RegInfo("CR2",0x4) RegVolatile!(mask_CR2,uint,0x40000c04,0x0,RegisterAccess.RW) CR2;		 // offset:0x4 CR2 control register 2
		@RegInfo("SMCR",0x8) RegVolatile!(mask_SMCR,uint,0x40000c08,0x0,RegisterAccess.RW) SMCR;		 // offset:0x8 SMCR slave mode control register
		@RegInfo("DIER",0xc) RegVolatile!(mask_DIER,uint,0x40000c0c,0x0,RegisterAccess.RW) DIER;		 // offset:0x12 DIER DMA/Interrupt enable register
		@RegInfo("SR",0x10) RegVolatile!(mask_SR,uint,0x40000c10,0x0,RegisterAccess.RW) SR;		 // offset:0x16 SR status register
		@RegInfo("EGR",0x14) RegVolatile!(mask_EGR,uint,0x40000c14,0x0,RegisterAccess.WO) EGR;		 // offset:0x20 EGR event generation register
		@RegInfo("CCMR1_Output",0x18) RegVolatile!(mask_CCMR1_Output,uint,0x40000c18,0x0,RegisterAccess.RW) CCMR1_Output;		 // offset:0x24 CCMR1_Output capture/compare mode register 1 (output mode)
		@RegInfo("CCMR1_Input",0x18) RegVolatile!(mask_CCMR1_Input,uint,0x40000c18,0x0,RegisterAccess.RW) CCMR1_Input;		 // offset:0x24 CCMR1_Input capture/compare mode register 1 (input mode)
		@RegInfo("CCMR2_Output",0x1c) RegVolatile!(mask_CCMR2_Output,uint,0x40000c1c,0x0,RegisterAccess.RW) CCMR2_Output;		 // offset:0x28 CCMR2_Output capture/compare mode register 2 (output mode)
		@RegInfo("CCMR2_Input",0x1c) RegVolatile!(mask_CCMR2_Input,uint,0x40000c1c,0x0,RegisterAccess.RW) CCMR2_Input;		 // offset:0x28 CCMR2_Input capture/compare mode register 2 (input mode)
		@RegInfo("CCER",0x20) RegVolatile!(mask_CCER,uint,0x40000c20,0x0,RegisterAccess.RW) CCER;		 // offset:0x32 CCER capture/compare enable register
		@RegInfo("CNT",0x24) RegVolatile!(mask_CNT,uint,0x40000c24,0x0,RegisterAccess.RW) CNT;		 // offset:0x36 CNT counter
		@RegInfo("PSC",0x28) RegVolatile!(mask_PSC,uint,0x40000c28,0x0,RegisterAccess.RW) PSC;		 // offset:0x40 PSC prescaler
		@RegInfo("ARR",0x2c) RegVolatile!(mask_ARR,uint,0x40000c2c,0x0,RegisterAccess.RW) ARR;		 // offset:0x44 ARR auto-reload register
		private uint[0x1] reserved14;	// skip 4 bytes
		@RegInfo("CCR1",0x34) RegVolatile!(mask_CCR1,uint,0x40000c34,0x0,RegisterAccess.RW) CCR1;		 // offset:0x52 CCR1 capture/compare register 1
		@RegInfo("CCR2",0x38) RegVolatile!(mask_CCR2,uint,0x40000c38,0x0,RegisterAccess.RW) CCR2;		 // offset:0x56 CCR2 capture/compare register 2
		@RegInfo("CCR3",0x3c) RegVolatile!(mask_CCR3,uint,0x40000c3c,0x0,RegisterAccess.RW) CCR3;		 // offset:0x60 CCR3 capture/compare register 3
		@RegInfo("CCR4",0x40) RegVolatile!(mask_CCR4,uint,0x40000c40,0x0,RegisterAccess.RW) CCR4;		 // offset:0x64 CCR4 capture/compare register 4
		private uint[0x1] reserved18;	// skip 4 bytes
		@RegInfo("DCR",0x48) RegVolatile!(mask_DCR,uint,0x40000c48,0x0,RegisterAccess.RW) DCR;		 // offset:0x72 DCR DMA control register
		@RegInfo("DMAR",0x4c) RegVolatile!(mask_DMAR,uint,0x40000c4c,0x0,RegisterAccess.RW) DMAR;		 // offset:0x76 DMAR DMA address for full transfer
		@RegInfo("OR",0x50) RegVolatile!(mask_OR,uint,0x40000c50,0x0,RegisterAccess.RW) OR;		 // offset:0x80 OR TIM5 option register
		enum mask_CR1 {
				CKD = RegBitField!uint(8,2,RegisterAccess.RW),		// Clock division
				ARPE = RegBitField!uint(7,1,RegisterAccess.RW),		// Auto-reload preload enable
				CMS = RegBitField!uint(5,2,RegisterAccess.RW),		// Center-aligned mode selection
				DIR = RegBitField!uint(4,1,RegisterAccess.RW),		// Direction
				OPM = RegBitField!uint(3,1,RegisterAccess.RW),		// One-pulse mode
				URS = RegBitField!uint(2,1,RegisterAccess.RW),		// Update request source
				UDIS = RegBitField!uint(1,1,RegisterAccess.RW),		// Update disable
				CEN = RegBitField!uint(0,1,RegisterAccess.RW),		// Counter enable
		}
		enum mask_CR2 {
				TI1S = RegBitField!uint(7,1,RegisterAccess.RW),		// TI1 selection
				MMS = RegBitField!uint(4,3,RegisterAccess.RW),		// Master mode selection
				CCDS = RegBitField!uint(3,1,RegisterAccess.RW),		// Capture/compare DMA selection
		}
		enum mask_SMCR {
				ETP = RegBitField!uint(15,1,RegisterAccess.RW),		// External trigger polarity
				ECE = RegBitField!uint(14,1,RegisterAccess.RW),		// External clock enable
				ETPS = RegBitField!uint(12,2,RegisterAccess.RW),		// External trigger prescaler
				ETF = RegBitField!uint(8,4,RegisterAccess.RW),		// External trigger filter
				MSM = RegBitField!uint(7,1,RegisterAccess.RW),		// Master/Slave mode
				TS = RegBitField!uint(4,3,RegisterAccess.RW),		// Trigger selection
				SMS = RegBitField!uint(0,3,RegisterAccess.RW),		// Slave mode selection
		}
		enum mask_DIER {
				TDE = RegBitField!uint(14,1,RegisterAccess.RW),		// Trigger DMA request enable
				CC4DE = RegBitField!uint(12,1,RegisterAccess.RW),		// Capture/Compare 4 DMA request enable
				CC3DE = RegBitField!uint(11,1,RegisterAccess.RW),		// Capture/Compare 3 DMA request enable
				CC2DE = RegBitField!uint(10,1,RegisterAccess.RW),		// Capture/Compare 2 DMA request enable
				CC1DE = RegBitField!uint(9,1,RegisterAccess.RW),		// Capture/Compare 1 DMA request enable
				UDE = RegBitField!uint(8,1,RegisterAccess.RW),		// Update DMA request enable
				TIE = RegBitField!uint(6,1,RegisterAccess.RW),		// Trigger interrupt enable
				CC4IE = RegBitField!uint(4,1,RegisterAccess.RW),		// Capture/Compare 4 interrupt enable
				CC3IE = RegBitField!uint(3,1,RegisterAccess.RW),		// Capture/Compare 3 interrupt enable
				CC2IE = RegBitField!uint(2,1,RegisterAccess.RW),		// Capture/Compare 2 interrupt enable
				CC1IE = RegBitField!uint(1,1,RegisterAccess.RW),		// Capture/Compare 1 interrupt enable
				UIE = RegBitField!uint(0,1,RegisterAccess.RW),		// Update interrupt enable
		}
		enum mask_SR {
				CC4OF = RegBitField!uint(12,1,RegisterAccess.RW),		// Capture/Compare 4 overcapture flag
				CC3OF = RegBitField!uint(11,1,RegisterAccess.RW),		// Capture/Compare 3 overcapture flag
				CC2OF = RegBitField!uint(10,1,RegisterAccess.RW),		// Capture/compare 2 overcapture flag
				CC1OF = RegBitField!uint(9,1,RegisterAccess.RW),		// Capture/Compare 1 overcapture flag
				TIF = RegBitField!uint(6,1,RegisterAccess.RW),		// Trigger interrupt flag
				CC4IF = RegBitField!uint(4,1,RegisterAccess.RW),		// Capture/Compare 4 interrupt flag
				CC3IF = RegBitField!uint(3,1,RegisterAccess.RW),		// Capture/Compare 3 interrupt flag
				CC2IF = RegBitField!uint(2,1,RegisterAccess.RW),		// Capture/Compare 2 interrupt flag
				CC1IF = RegBitField!uint(1,1,RegisterAccess.RW),		// Capture/compare 1 interrupt flag
				UIF = RegBitField!uint(0,1,RegisterAccess.RW),		// Update interrupt flag
		}
		enum mask_EGR {
				TG = RegBitField!uint(6,1,RegisterAccess.WO),		// Trigger generation
				CC4G = RegBitField!uint(4,1,RegisterAccess.WO),		// Capture/compare 4 generation
				CC3G = RegBitField!uint(3,1,RegisterAccess.WO),		// Capture/compare 3 generation
				CC2G = RegBitField!uint(2,1,RegisterAccess.WO),		// Capture/compare 2 generation
				CC1G = RegBitField!uint(1,1,RegisterAccess.WO),		// Capture/compare 1 generation
				UG = RegBitField!uint(0,1,RegisterAccess.WO),		// Update generation
		}
		enum mask_CCMR1_Output {
				OC2CE = RegBitField!uint(15,1,RegisterAccess.RW),		// OC2CE
				OC2M = RegBitField!uint(12,3,RegisterAccess.RW),		// OC2M
				OC2PE = RegBitField!uint(11,1,RegisterAccess.RW),		// OC2PE
				OC2FE = RegBitField!uint(10,1,RegisterAccess.RW),		// OC2FE
				CC2S = RegBitField!uint(8,2,RegisterAccess.RW),		// CC2S
				OC1CE = RegBitField!uint(7,1,RegisterAccess.RW),		// OC1CE
				OC1M = RegBitField!uint(4,3,RegisterAccess.RW),		// OC1M
				OC1PE = RegBitField!uint(3,1,RegisterAccess.RW),		// OC1PE
				OC1FE = RegBitField!uint(2,1,RegisterAccess.RW),		// OC1FE
				CC1S = RegBitField!uint(0,2,RegisterAccess.RW),		// CC1S
		}
		enum mask_CCMR1_Input {
				IC2F = RegBitField!uint(12,4,RegisterAccess.RW),		// Input capture 2 filter
				IC2PCS = RegBitField!uint(10,2,RegisterAccess.RW),		// Input capture 2 prescaler
				CC2S = RegBitField!uint(8,2,RegisterAccess.RW),		// Capture/Compare 2 selection
				IC1F = RegBitField!uint(4,4,RegisterAccess.RW),		// Input capture 1 filter
				ICPCS = RegBitField!uint(2,2,RegisterAccess.RW),		// Input capture 1 prescaler
				CC1S = RegBitField!uint(0,2,RegisterAccess.RW),		// Capture/Compare 1 selection
		}
		enum mask_CCMR2_Output {
				O24CE = RegBitField!uint(15,1,RegisterAccess.RW),		// O24CE
				OC4M = RegBitField!uint(12,3,RegisterAccess.RW),		// OC4M
				OC4PE = RegBitField!uint(11,1,RegisterAccess.RW),		// OC4PE
				OC4FE = RegBitField!uint(10,1,RegisterAccess.RW),		// OC4FE
				CC4S = RegBitField!uint(8,2,RegisterAccess.RW),		// CC4S
				OC3CE = RegBitField!uint(7,1,RegisterAccess.RW),		// OC3CE
				OC3M = RegBitField!uint(4,3,RegisterAccess.RW),		// OC3M
				OC3PE = RegBitField!uint(3,1,RegisterAccess.RW),		// OC3PE
				OC3FE = RegBitField!uint(2,1,RegisterAccess.RW),		// OC3FE
				CC3S = RegBitField!uint(0,2,RegisterAccess.RW),		// CC3S
		}
		enum mask_CCMR2_Input {
				IC4F = RegBitField!uint(12,4,RegisterAccess.RW),		// Input capture 4 filter
				IC4PSC = RegBitField!uint(10,2,RegisterAccess.RW),		// Input capture 4 prescaler
				CC4S = RegBitField!uint(8,2,RegisterAccess.RW),		// Capture/Compare 4 selection
				IC3F = RegBitField!uint(4,4,RegisterAccess.RW),		// Input capture 3 filter
				IC3PSC = RegBitField!uint(2,2,RegisterAccess.RW),		// Input capture 3 prescaler
				CC3S = RegBitField!uint(0,2,RegisterAccess.RW),		// Capture/compare 3 selection
		}
		enum mask_CCER {
				CC4NP = RegBitField!uint(15,1,RegisterAccess.RW),		// Capture/Compare 4 output Polarity
				CC4P = RegBitField!uint(13,1,RegisterAccess.RW),		// Capture/Compare 3 output Polarity
				CC4E = RegBitField!uint(12,1,RegisterAccess.RW),		// Capture/Compare 4 output enable
				CC3NP = RegBitField!uint(11,1,RegisterAccess.RW),		// Capture/Compare 3 output Polarity
				CC3P = RegBitField!uint(9,1,RegisterAccess.RW),		// Capture/Compare 3 output Polarity
				CC3E = RegBitField!uint(8,1,RegisterAccess.RW),		// Capture/Compare 3 output enable
				CC2NP = RegBitField!uint(7,1,RegisterAccess.RW),		// Capture/Compare 2 output Polarity
				CC2P = RegBitField!uint(5,1,RegisterAccess.RW),		// Capture/Compare 2 output Polarity
				CC2E = RegBitField!uint(4,1,RegisterAccess.RW),		// Capture/Compare 2 output enable
				CC1NP = RegBitField!uint(3,1,RegisterAccess.RW),		// Capture/Compare 1 output Polarity
				CC1P = RegBitField!uint(1,1,RegisterAccess.RW),		// Capture/Compare 1 output Polarity
				CC1E = RegBitField!uint(0,1,RegisterAccess.RW),		// Capture/Compare 1 output enable
		}
		enum mask_CNT {
				CNT_H = RegBitField!uint(16,16,RegisterAccess.RW),		// High counter value
				CNT_L = RegBitField!uint(0,16,RegisterAccess.RW),		// Low counter value
		}
		enum mask_PSC {
				PSC = RegBitField!uint(0,16,RegisterAccess.RW),		// Prescaler value
		}
		enum mask_ARR {
				ARR_H = RegBitField!uint(16,16,RegisterAccess.RW),		// High Auto-reload value
				ARR_L = RegBitField!uint(0,16,RegisterAccess.RW),		// Low Auto-reload value
		}
		enum mask_CCR1 {
				CCR1_H = RegBitField!uint(16,16,RegisterAccess.RW),		// High Capture/Compare 1 value
				CCR1_L = RegBitField!uint(0,16,RegisterAccess.RW),		// Low Capture/Compare 1 value
		}
		enum mask_CCR2 {
				CCR2_H = RegBitField!uint(16,16,RegisterAccess.RW),		// High Capture/Compare 2 value
				CCR2_L = RegBitField!uint(0,16,RegisterAccess.RW),		// Low Capture/Compare 2 value
		}
		enum mask_CCR3 {
				CCR3_H = RegBitField!uint(16,16,RegisterAccess.RW),		// High Capture/Compare value
				CCR3_L = RegBitField!uint(0,16,RegisterAccess.RW),		// Low Capture/Compare value
		}
		enum mask_CCR4 {
				CCR4_H = RegBitField!uint(16,16,RegisterAccess.RW),		// High Capture/Compare value
				CCR4_L = RegBitField!uint(0,16,RegisterAccess.RW),		// Low Capture/Compare value
		}
		enum mask_DCR {
				DBL = RegBitField!uint(8,5,RegisterAccess.RW),		// DMA burst length
				DBA = RegBitField!uint(0,5,RegisterAccess.RW),		// DMA base address
		}
		enum mask_DMAR {
				DMAB = RegBitField!uint(0,16,RegisterAccess.RW),		// DMA register for burst accesses
		}
		enum mask_OR {
				IT4_RMP = RegBitField!uint(6,2,RegisterAccess.RW),		// Timer Input 4 remap
		}
	}
	// General purpose timers
	struct TIM9_Type {
		@RegInfo("CR1",0x0) RegVolatile!(mask_CR1,uint,0x40014000,0x0,RegisterAccess.RW) CR1;		 // offset:0x0 CR1 control register 1
		@RegInfo("CR2",0x4) RegVolatile!(mask_CR2,uint,0x40014004,0x0,RegisterAccess.RW) CR2;		 // offset:0x4 CR2 control register 2
		@RegInfo("SMCR",0x8) RegVolatile!(mask_SMCR,uint,0x40014008,0x0,RegisterAccess.RW) SMCR;		 // offset:0x8 SMCR slave mode control register
		@RegInfo("DIER",0xc) RegVolatile!(mask_DIER,uint,0x4001400c,0x0,RegisterAccess.RW) DIER;		 // offset:0x12 DIER DMA/Interrupt enable register
		@RegInfo("SR",0x10) RegVolatile!(mask_SR,uint,0x40014010,0x0,RegisterAccess.RW) SR;		 // offset:0x16 SR status register
		@RegInfo("EGR",0x14) RegVolatile!(mask_EGR,uint,0x40014014,0x0,RegisterAccess.WO) EGR;		 // offset:0x20 EGR event generation register
		@RegInfo("CCMR1_Output",0x18) RegVolatile!(mask_CCMR1_Output,uint,0x40014018,0x0,RegisterAccess.RW) CCMR1_Output;		 // offset:0x24 CCMR1_Output capture/compare mode register 1 (output mode)
		@RegInfo("CCMR1_Input",0x18) RegVolatile!(mask_CCMR1_Input,uint,0x40014018,0x0,RegisterAccess.RW) CCMR1_Input;		 // offset:0x24 CCMR1_Input capture/compare mode register 1 (input mode)
		private uint[0x1] reserved8;	// skip 4 bytes
		@RegInfo("CCER",0x20) RegVolatile!(mask_CCER,uint,0x40014020,0x0,RegisterAccess.RW) CCER;		 // offset:0x32 CCER capture/compare enable register
		@RegInfo("CNT",0x24) RegVolatile!(mask_CNT,uint,0x40014024,0x0,RegisterAccess.RW) CNT;		 // offset:0x36 CNT counter
		@RegInfo("PSC",0x28) RegVolatile!(mask_PSC,uint,0x40014028,0x0,RegisterAccess.RW) PSC;		 // offset:0x40 PSC prescaler
		@RegInfo("ARR",0x2c) RegVolatile!(mask_ARR,uint,0x4001402c,0x0,RegisterAccess.RW) ARR;		 // offset:0x44 ARR auto-reload register
		private uint[0x1] reserved12;	// skip 4 bytes
		@RegInfo("CCR1",0x34) RegVolatile!(mask_CCR1,uint,0x40014034,0x0,RegisterAccess.RW) CCR1;		 // offset:0x52 CCR1 capture/compare register 1
		@RegInfo("CCR2",0x38) RegVolatile!(mask_CCR2,uint,0x40014038,0x0,RegisterAccess.RW) CCR2;		 // offset:0x56 CCR2 capture/compare register 2
		enum mask_CR1 {
				CKD = RegBitField!uint(8,2,RegisterAccess.RW),		// Clock division
				ARPE = RegBitField!uint(7,1,RegisterAccess.RW),		// Auto-reload preload enable
				OPM = RegBitField!uint(3,1,RegisterAccess.RW),		// One-pulse mode
				URS = RegBitField!uint(2,1,RegisterAccess.RW),		// Update request source
				UDIS = RegBitField!uint(1,1,RegisterAccess.RW),		// Update disable
				CEN = RegBitField!uint(0,1,RegisterAccess.RW),		// Counter enable
		}
		enum mask_CR2 {
				MMS = RegBitField!uint(4,3,RegisterAccess.RW),		// Master mode selection
		}
		enum mask_SMCR {
				MSM = RegBitField!uint(7,1,RegisterAccess.RW),		// Master/Slave mode
				TS = RegBitField!uint(4,3,RegisterAccess.RW),		// Trigger selection
				SMS = RegBitField!uint(0,3,RegisterAccess.RW),		// Slave mode selection
		}
		enum mask_DIER {
				TIE = RegBitField!uint(6,1,RegisterAccess.RW),		// Trigger interrupt enable
				CC2IE = RegBitField!uint(2,1,RegisterAccess.RW),		// Capture/Compare 2 interrupt enable
				CC1IE = RegBitField!uint(1,1,RegisterAccess.RW),		// Capture/Compare 1 interrupt enable
				UIE = RegBitField!uint(0,1,RegisterAccess.RW),		// Update interrupt enable
		}
		enum mask_SR {
				CC2OF = RegBitField!uint(10,1,RegisterAccess.RW),		// Capture/compare 2 overcapture flag
				CC1OF = RegBitField!uint(9,1,RegisterAccess.RW),		// Capture/Compare 1 overcapture flag
				TIF = RegBitField!uint(6,1,RegisterAccess.RW),		// Trigger interrupt flag
				CC2IF = RegBitField!uint(2,1,RegisterAccess.RW),		// Capture/Compare 2 interrupt flag
				CC1IF = RegBitField!uint(1,1,RegisterAccess.RW),		// Capture/compare 1 interrupt flag
				UIF = RegBitField!uint(0,1,RegisterAccess.RW),		// Update interrupt flag
		}
		enum mask_EGR {
				TG = RegBitField!uint(6,1,RegisterAccess.WO),		// Trigger generation
				CC2G = RegBitField!uint(2,1,RegisterAccess.WO),		// Capture/compare 2 generation
				CC1G = RegBitField!uint(1,1,RegisterAccess.WO),		// Capture/compare 1 generation
				UG = RegBitField!uint(0,1,RegisterAccess.WO),		// Update generation
		}
		enum mask_CCMR1_Output {
				OC2M = RegBitField!uint(12,3,RegisterAccess.RW),		// Output Compare 2 mode
				OC2PE = RegBitField!uint(11,1,RegisterAccess.RW),		// Output Compare 2 preload enable
				OC2FE = RegBitField!uint(10,1,RegisterAccess.RW),		// Output Compare 2 fast enable
				CC2S = RegBitField!uint(8,2,RegisterAccess.RW),		// Capture/Compare 2 selection
				OC1M = RegBitField!uint(4,3,RegisterAccess.RW),		// Output Compare 1 mode
				OC1PE = RegBitField!uint(3,1,RegisterAccess.RW),		// Output Compare 1 preload enable
				OC1FE = RegBitField!uint(2,1,RegisterAccess.RW),		// Output Compare 1 fast enable
				CC1S = RegBitField!uint(0,2,RegisterAccess.RW),		// Capture/Compare 1 selection
		}
		enum mask_CCMR1_Input {
				IC2F = RegBitField!uint(12,3,RegisterAccess.RW),		// Input capture 2 filter
				IC2PCS = RegBitField!uint(10,2,RegisterAccess.RW),		// Input capture 2 prescaler
				CC2S = RegBitField!uint(8,2,RegisterAccess.RW),		// Capture/Compare 2 selection
				IC1F = RegBitField!uint(4,3,RegisterAccess.RW),		// Input capture 1 filter
				ICPCS = RegBitField!uint(2,2,RegisterAccess.RW),		// Input capture 1 prescaler
				CC1S = RegBitField!uint(0,2,RegisterAccess.RW),		// Capture/Compare 1 selection
		}
		enum mask_CCER {
				CC2NP = RegBitField!uint(7,1,RegisterAccess.RW),		// Capture/Compare 2 output Polarity
				CC2P = RegBitField!uint(5,1,RegisterAccess.RW),		// Capture/Compare 2 output Polarity
				CC2E = RegBitField!uint(4,1,RegisterAccess.RW),		// Capture/Compare 2 output enable
				CC1NP = RegBitField!uint(3,1,RegisterAccess.RW),		// Capture/Compare 1 output Polarity
				CC1P = RegBitField!uint(1,1,RegisterAccess.RW),		// Capture/Compare 1 output Polarity
				CC1E = RegBitField!uint(0,1,RegisterAccess.RW),		// Capture/Compare 1 output enable
		}
		enum mask_CNT {
				CNT = RegBitField!uint(0,16,RegisterAccess.RW),		// counter value
		}
		enum mask_PSC {
				PSC = RegBitField!uint(0,16,RegisterAccess.RW),		// Prescaler value
		}
		enum mask_ARR {
				ARR = RegBitField!uint(0,16,RegisterAccess.RW),		// Auto-reload value
		}
		enum mask_CCR1 {
				CCR1 = RegBitField!uint(0,16,RegisterAccess.RW),		// Capture/Compare 1 value
		}
		enum mask_CCR2 {
				CCR2 = RegBitField!uint(0,16,RegisterAccess.RW),		// Capture/Compare 2 value
		}
	}
	// General-purpose-timers
	struct TIM10_Type {
		@RegInfo("CR1",0x0) RegVolatile!(mask_CR1,uint,0x40014400,0x0,RegisterAccess.RW) CR1;		 // offset:0x0 CR1 control register 1
		private uint[0x2] reserved1;	// skip 8 bytes
		@RegInfo("DIER",0xc) RegVolatile!(mask_DIER,uint,0x4001440c,0x0,RegisterAccess.RW) DIER;		 // offset:0x12 DIER DMA/Interrupt enable register
		@RegInfo("SR",0x10) RegVolatile!(mask_SR,uint,0x40014410,0x0,RegisterAccess.RW) SR;		 // offset:0x16 SR status register
		@RegInfo("EGR",0x14) RegVolatile!(mask_EGR,uint,0x40014414,0x0,RegisterAccess.WO) EGR;		 // offset:0x20 EGR event generation register
		@RegInfo("CCMR1_Output",0x18) RegVolatile!(mask_CCMR1_Output,uint,0x40014418,0x0,RegisterAccess.RW) CCMR1_Output;		 // offset:0x24 CCMR1_Output capture/compare mode register 1 (output mode)
		@RegInfo("CCMR1_Input",0x18) RegVolatile!(mask_CCMR1_Input,uint,0x40014418,0x0,RegisterAccess.RW) CCMR1_Input;		 // offset:0x24 CCMR1_Input capture/compare mode register 1 (input mode)
		private uint[0x1] reserved6;	// skip 4 bytes
		@RegInfo("CCER",0x20) RegVolatile!(mask_CCER,uint,0x40014420,0x0,RegisterAccess.RW) CCER;		 // offset:0x32 CCER capture/compare enable register
		@RegInfo("CNT",0x24) RegVolatile!(mask_CNT,uint,0x40014424,0x0,RegisterAccess.RW) CNT;		 // offset:0x36 CNT counter
		@RegInfo("PSC",0x28) RegVolatile!(mask_PSC,uint,0x40014428,0x0,RegisterAccess.RW) PSC;		 // offset:0x40 PSC prescaler
		@RegInfo("ARR",0x2c) RegVolatile!(mask_ARR,uint,0x4001442c,0x0,RegisterAccess.RW) ARR;		 // offset:0x44 ARR auto-reload register
		private uint[0x1] reserved10;	// skip 4 bytes
		@RegInfo("CCR1",0x34) RegVolatile!(mask_CCR1,uint,0x40014434,0x0,RegisterAccess.RW) CCR1;		 // offset:0x52 CCR1 capture/compare register 1
		enum mask_CR1 {
				CKD = RegBitField!uint(8,2,RegisterAccess.RW),		// Clock division
				ARPE = RegBitField!uint(7,1,RegisterAccess.RW),		// Auto-reload preload enable
				URS = RegBitField!uint(2,1,RegisterAccess.RW),		// Update request source
				UDIS = RegBitField!uint(1,1,RegisterAccess.RW),		// Update disable
				CEN = RegBitField!uint(0,1,RegisterAccess.RW),		// Counter enable
		}
		enum mask_DIER {
				CC1IE = RegBitField!uint(1,1,RegisterAccess.RW),		// Capture/Compare 1 interrupt enable
				UIE = RegBitField!uint(0,1,RegisterAccess.RW),		// Update interrupt enable
		}
		enum mask_SR {
				CC1OF = RegBitField!uint(9,1,RegisterAccess.RW),		// Capture/Compare 1 overcapture flag
				CC1IF = RegBitField!uint(1,1,RegisterAccess.RW),		// Capture/compare 1 interrupt flag
				UIF = RegBitField!uint(0,1,RegisterAccess.RW),		// Update interrupt flag
		}
		enum mask_EGR {
				CC1G = RegBitField!uint(1,1,RegisterAccess.WO),		// Capture/compare 1 generation
				UG = RegBitField!uint(0,1,RegisterAccess.WO),		// Update generation
		}
		enum mask_CCMR1_Output {
				OC1M = RegBitField!uint(4,3,RegisterAccess.RW),		// Output Compare 1 mode
				OC1PE = RegBitField!uint(3,1,RegisterAccess.RW),		// Output Compare 1 preload enable
				OC1FE = RegBitField!uint(2,1,RegisterAccess.RW),		// Output Compare 1 fast enable
				CC1S = RegBitField!uint(0,2,RegisterAccess.RW),		// Capture/Compare 1 selection
		}
		enum mask_CCMR1_Input {
				IC1F = RegBitField!uint(4,4,RegisterAccess.RW),		// Input capture 1 filter
				ICPCS = RegBitField!uint(2,2,RegisterAccess.RW),		// Input capture 1 prescaler
				CC1S = RegBitField!uint(0,2,RegisterAccess.RW),		// Capture/Compare 1 selection
		}
		enum mask_CCER {
				CC1NP = RegBitField!uint(3,1,RegisterAccess.RW),		// Capture/Compare 1 output Polarity
				CC1P = RegBitField!uint(1,1,RegisterAccess.RW),		// Capture/Compare 1 output Polarity
				CC1E = RegBitField!uint(0,1,RegisterAccess.RW),		// Capture/Compare 1 output enable
		}
		enum mask_CNT {
				CNT = RegBitField!uint(0,16,RegisterAccess.RW),		// counter value
		}
		enum mask_PSC {
				PSC = RegBitField!uint(0,16,RegisterAccess.RW),		// Prescaler value
		}
		enum mask_ARR {
				ARR = RegBitField!uint(0,16,RegisterAccess.RW),		// Auto-reload value
		}
		enum mask_CCR1 {
				CCR1 = RegBitField!uint(0,16,RegisterAccess.RW),		// Capture/Compare 1 value
		}
	}
	// General-purpose-timers
	struct TIM11_Type {
		@RegInfo("CR1",0x0) RegVolatile!(mask_CR1,uint,0x40014800,0x0,RegisterAccess.RW) CR1;		 // offset:0x0 CR1 control register 1
		private uint[0x2] reserved1;	// skip 8 bytes
		@RegInfo("DIER",0xc) RegVolatile!(mask_DIER,uint,0x4001480c,0x0,RegisterAccess.RW) DIER;		 // offset:0x12 DIER DMA/Interrupt enable register
		@RegInfo("SR",0x10) RegVolatile!(mask_SR,uint,0x40014810,0x0,RegisterAccess.RW) SR;		 // offset:0x16 SR status register
		@RegInfo("EGR",0x14) RegVolatile!(mask_EGR,uint,0x40014814,0x0,RegisterAccess.WO) EGR;		 // offset:0x20 EGR event generation register
		@RegInfo("CCMR1_Output",0x18) RegVolatile!(mask_CCMR1_Output,uint,0x40014818,0x0,RegisterAccess.RW) CCMR1_Output;		 // offset:0x24 CCMR1_Output capture/compare mode register 1 (output mode)
		@RegInfo("CCMR1_Input",0x18) RegVolatile!(mask_CCMR1_Input,uint,0x40014818,0x0,RegisterAccess.RW) CCMR1_Input;		 // offset:0x24 CCMR1_Input capture/compare mode register 1 (input mode)
		private uint[0x1] reserved6;	// skip 4 bytes
		@RegInfo("CCER",0x20) RegVolatile!(mask_CCER,uint,0x40014820,0x0,RegisterAccess.RW) CCER;		 // offset:0x32 CCER capture/compare enable register
		@RegInfo("CNT",0x24) RegVolatile!(mask_CNT,uint,0x40014824,0x0,RegisterAccess.RW) CNT;		 // offset:0x36 CNT counter
		@RegInfo("PSC",0x28) RegVolatile!(mask_PSC,uint,0x40014828,0x0,RegisterAccess.RW) PSC;		 // offset:0x40 PSC prescaler
		@RegInfo("ARR",0x2c) RegVolatile!(mask_ARR,uint,0x4001482c,0x0,RegisterAccess.RW) ARR;		 // offset:0x44 ARR auto-reload register
		private uint[0x1] reserved10;	// skip 4 bytes
		@RegInfo("CCR1",0x34) RegVolatile!(mask_CCR1,uint,0x40014834,0x0,RegisterAccess.RW) CCR1;		 // offset:0x52 CCR1 capture/compare register 1
		private uint[0x6] reserved11;	// skip 24 bytes
		@RegInfo("OR",0x50) RegVolatile!(mask_OR,uint,0x40014850,0x0,RegisterAccess.RW) OR;		 // offset:0x80 OR option register
		enum mask_CR1 {
				CKD = RegBitField!uint(8,2,RegisterAccess.RW),		// Clock division
				ARPE = RegBitField!uint(7,1,RegisterAccess.RW),		// Auto-reload preload enable
				URS = RegBitField!uint(2,1,RegisterAccess.RW),		// Update request source
				UDIS = RegBitField!uint(1,1,RegisterAccess.RW),		// Update disable
				CEN = RegBitField!uint(0,1,RegisterAccess.RW),		// Counter enable
		}
		enum mask_DIER {
				CC1IE = RegBitField!uint(1,1,RegisterAccess.RW),		// Capture/Compare 1 interrupt enable
				UIE = RegBitField!uint(0,1,RegisterAccess.RW),		// Update interrupt enable
		}
		enum mask_SR {
				CC1OF = RegBitField!uint(9,1,RegisterAccess.RW),		// Capture/Compare 1 overcapture flag
				CC1IF = RegBitField!uint(1,1,RegisterAccess.RW),		// Capture/compare 1 interrupt flag
				UIF = RegBitField!uint(0,1,RegisterAccess.RW),		// Update interrupt flag
		}
		enum mask_EGR {
				CC1G = RegBitField!uint(1,1,RegisterAccess.WO),		// Capture/compare 1 generation
				UG = RegBitField!uint(0,1,RegisterAccess.WO),		// Update generation
		}
		enum mask_CCMR1_Output {
				OC1M = RegBitField!uint(4,3,RegisterAccess.RW),		// Output Compare 1 mode
				OC1PE = RegBitField!uint(3,1,RegisterAccess.RW),		// Output Compare 1 preload enable
				OC1FE = RegBitField!uint(2,1,RegisterAccess.RW),		// Output Compare 1 fast enable
				CC1S = RegBitField!uint(0,2,RegisterAccess.RW),		// Capture/Compare 1 selection
		}
		enum mask_CCMR1_Input {
				IC1F = RegBitField!uint(4,4,RegisterAccess.RW),		// Input capture 1 filter
				ICPCS = RegBitField!uint(2,2,RegisterAccess.RW),		// Input capture 1 prescaler
				CC1S = RegBitField!uint(0,2,RegisterAccess.RW),		// Capture/Compare 1 selection
		}
		enum mask_CCER {
				CC1NP = RegBitField!uint(3,1,RegisterAccess.RW),		// Capture/Compare 1 output Polarity
				CC1P = RegBitField!uint(1,1,RegisterAccess.RW),		// Capture/Compare 1 output Polarity
				CC1E = RegBitField!uint(0,1,RegisterAccess.RW),		// Capture/Compare 1 output enable
		}
		enum mask_CNT {
				CNT = RegBitField!uint(0,16,RegisterAccess.RW),		// counter value
		}
		enum mask_PSC {
				PSC = RegBitField!uint(0,16,RegisterAccess.RW),		// Prescaler value
		}
		enum mask_ARR {
				ARR = RegBitField!uint(0,16,RegisterAccess.RW),		// Auto-reload value
		}
		enum mask_CCR1 {
				CCR1 = RegBitField!uint(0,16,RegisterAccess.RW),		// Capture/Compare 1 value
		}
		enum mask_OR {
				RMP = RegBitField!uint(0,2,RegisterAccess.RW),		// Input 1 remapping capability
		}
	}
}
