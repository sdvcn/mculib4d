module old.chip;

//import arm.misc:CmpVal;

import std.traits : isAutodecodableString;
import std.meta : allSatisfy;
//import baremetal;
//import arm.cortex_m4;



//enum TargetChips = GetConfig!("TargetChips");
//enum SVDWidth = GetConfig!("SVDWidth");
//enum pIm1 = CmpVal!(TargetChips,"stm32f401cc");



//imported!("stm32f401cc");
/*
pragma(msg,pIm1);
static if(pIm1 == true){
    public import chip.stm32f401cc;
    
    
}
*/
/// 导入芯片定义
public{
    //version(STM32F401CC) import chip.stm32f401cc;
    import chip.st.stm32f401cc;
    
}



/**
* 判定是否拥有对应的外设
* Params:
*   T = 外设名称,支持字符串及字符串数组
*/
template HasPeriph(T...) if (allSatisfy!(isAutodecodableString, typeof(T)) )
{
    static if(T.length > 1){
        enum HasPeriph = __traits(hasMember, Peripherals, T[0]) || HasPeriph!(T[1..$]);
    }else{
        enum HasPeriph = __traits(hasMember, Peripherals, T[0]);
    }
}

version(None):



/**
* 获取配置信息
* Params:
*   op = `ChipConfig`中的配置名称
*/
template GetConfig(string op)
{
    import chipconf;
    static if(__traits(hasMember,ChipConfig, op)){
        enum GetConfig = __traits(getMember, ChipConfig,op);
    }else{
        enum GetConfig = false;
    }
}


/// 生成向量表
//enum VectorFunc[] Cortex_Mx = Cortex_M ~ Cortex_M4[Isr_Ext.min .. $];

enum VectorFunc[] Cortex_Mx = Cortex_M ~ Cortex_M4;
@isr_vector extern(C) immutable VectorFunc[Cortex_Mx.length] CortexM_Isr = cast(immutable VectorFunc[])Cortex_Mx;







