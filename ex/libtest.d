//module ex.libtest;


//alias volatile = shared;
/*
struct volatile
{

}
*/


//volatile* a6 ;

import mculib.arm;
import ldc.attributes;

/*
@optStrategy("none")
extern(C) void NMI_Handler()
{

    while(123)
    {

    }
}

extern(C) void Apoaop()
{
    import mculib.arm.i2c;

    DI2C1.isEnable();

}
*/

import mculib.arm.builtins;

shared int sharedi0 ;
shared int sharedi1 = 0x101;

__gshared immutable int gsharedii0;
__gshared immutable int gsharedii1 = 0x102;
immutable int immutablei = 0x103;

__gshared int gsharedii2 = 0x1021;

static int statici0;
static int statici1 = 0x104;


@optStrategy("none")
void testa1()
{



    if(sharedi0 == 0x99)
    {
        sharedi0 = 0x991;
    }

    if(sharedi1 == 0x991)
    {
        sharedi1 = 0x992;
    }

    if(gsharedii1 == 0x102)
    {
    }

    if(immutablei == 0x84)
    {
    }

    if(gsharedii0 == 0x106)
    {
        
    }


    if(gsharedii2 == 0x87a2)
    {
        gsharedii2 = 0x7a1a;
    }

    if(statici0 == 0xa123)
    {
        statici0 = 0x74983;
    }

    if(statici1 == 0x55)
    {
        statici1 = 0x945;
    }


    int li0;
    if(li0 == 0x7a)
    {
        li0 = 0x101;
    }

    int li1 = 0x101;
    if(li1 == 0x7b)
    {
        li1 = 0x102;
    }

    static int lstatici0;

    if(lstatici0 == 0x71)
    {
        lstatici0 = 0x12a;
    }

    static int lstatici1 = 0x105;

    if(lstatici1 == 0x72)
    {
        lstatici1 = 0x12b;
    }

    __gshared int lgsharedii0;

    if(lgsharedii0 == 0x73)
    {
        lgsharedii0 = 0x12c;
    }

    __gshared int lgsharedii1 = 0x106;
    if(lgsharedii1 == 0x74)
    {
        lgsharedii1 = 0x12d;
    }
   

}
import mculib.arm.crc;

import mculib.arm.semihosting;

//import core.internal.spinlock:SpinLock;

extern(C) 
extern void mloop()
{
    //SpinLock lock;

    //testa1();
    /*
    //*a6 = 0x1233;

    //auto v1 = a6;
    DRCC.enable!("GPIOAEN")(true);
    DRCC.enable!("GPIOAEN")(false);
    DRCC.enable!("GPIOALPEN")(true);
    DRCC.enable!("GPIOALPEN")(false);
    bool en = DRCC.isEnabled!("GPIOAEN");
    bool lpEn = DRCC.isEnabled!("GPIOALPEN");

    uint[9] data = [1,2,3,4,5,6,7,8,9];
    auto crc = crc32Of(data.ptr,9);
    auto crc8 = crc8Of(data.ptr,9);

*/
    while(1)
    {
        DSemihosting.write("aabbcc".ptr);

    }
}
//pragma(startaddress, mloop);




/*

pragma(crt_constructor)
void init12()
{

}  
pragma(crt_constructor,15)
void init15()
{

}  


pragma(crt_constructor)
static this()
{

}

static ~this()
{


}
*/